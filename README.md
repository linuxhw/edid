EDID Repository
===============

This is a repository of decoded EDIDs from various digital and analog monitors collected
by Linux users at https://linux-hardware.org.

Everyone can contribute to this repository by uploading probes of their computers
by the [hw-probe](https://github.com/linuxhw/hw-probe) tool:

    sudo hw-probe -all -upload

EDIDs of all connected monitors will be uploaded to the database and repository.

Total monitors: 16759.

Contents
--------

1. [ About           ](#about)
2. [ Digital display ](#digital-display)
3. [ Analog display  ](#analog-display)

About
-----

The structure of the repository is the following:

    {TYPE}/{VENDOR}/{MODEL}/{ID}

    ( e.g. Digital/LG Display/LGD0217/925C880E8A08 )

ID of a monitor is MD5 of EDID.

Digital display
---------------

You can find decoded EDIDs for listed monitors in the repository by vendor name,
model and ID.

| MFG          | Model   | Name         | Res       | Size       | Inch | Made | ID    |
|--------------|---------|--------------|-----------|------------|------|------|-------|
| AMT Inter... | AMT0038 | L2-150T+     | 1280x720  | 300x230mm  | 14.9 |      | 5C393 |
| AMW          | AMW0000 | X2210WAS     | 1680x1050 | 480x300mm  | 22.3 | 2007 | BA284 |
| AOC          | AOC0000 | FTV          | 1920x1080 | 640x360mm  | 28.9 | 2016 | 909E2 |
| AOC          | AOC0000 | FHD LCD      | 1920x1080 | 480x270mm  | 21.7 | 2013 | 4068A |
| AOC          | AOC0001 | 2460         | 1920x1080 | 530x300mm  | 24.0 | 2016 | 7FB19 |
| AOC          | AOC1519 | 519W         | 1280x720  | 330x190mm  | 15.0 | 2008 | 5E13C |
| AOC          | AOC1780 | TFT1780      | 1280x1024 | 340x270mm  | 17.1 | 2007 | 8CAB8 |
| AOC          | AOC1900 | F19          | 1366x768  | 410x230mm  | 18.5 | 2009 | E2C05 |
| AOC          | AOC1912 | 912Vwa       | 1440x900  | 400x250mm  | 18.6 | 2007 | B0BEF |
| AOC          | AOC1919 | 919W         | 1440x900  | 400x250mm  | 18.6 | 2011 | 089F1 |
| AOC          | AOC1919 | 919W         | 1440x900  | 400x250mm  | 18.6 | 2010 | A58B8 |
| AOC          | AOC191A | L19W981      | 1440x900  | 400x250mm  | 18.6 | 2009 | 018C1 |
| AOC          | AOC1931 | L19W831      | 1280x1024 | 410x260mm  | 19.1 | 2009 | B9059 |
| AOC          | AOC1942 | T942we       | 1366x768  | 410x230mm  | 18.5 | 2010 | D3568 |
| AOC          | AOC1950 | 1950W        | 1366x768  | 410x230mm  | 18.5 | 2013 | 55A9B |
| AOC          | AOC1950 | 1950W        | 1366x768  | 410x230mm  | 18.5 | 2011 | 7FF0F |
| AOC          | AOC1960 | 1960R        | 1280x1024 | 380x300mm  | 19.1 | 2017 | C3CA8 |
| AOC          | AOC1960 | 1960         | 1440x900  | 410x260mm  | 19.1 | 2013 | CC2EF |
| AOC          | AOC1970 | 1970W        | 1366x768  | 410x230mm  | 18.5 | 2017 | 3AB83 |
| AOC          | AOC1970 | 1970W        | 1366x768  | 410x230mm  | 18.5 | 2014 | 3CB87 |
| AOC          | AOC1982 | LW98         | 1440x900  | 400x250mm  | 18.6 | 2007 | 1E289 |
| AOC          | AOC2023 | 2023W        | 1600x900  | 430x240mm  | 19.4 | 2014 | 0AE01 |
| AOC          | AOC2036 | 2036         | 1600x900  | 440x250mm  | 19.9 | 2010 | A3128 |
| AOC          | AOC2036 | 2036         | 1600x900  | 440x250mm  | 19.9 | 2009 | 38670 |
| AOC          | AOC2043 | 2043         | 1600x900  | 440x250mm  | 19.9 | 2010 | 6C0CE |
| AOC          | AOC2050 | 2050W        | 1600x900  | 430x240mm  | 19.4 | 2014 | F079B |
| AOC          | AOC2050 | 2050         | 1600x900  | 440x250mm  | 19.9 | 2013 | A0D16 |
| AOC          | AOC2060 | 2060W3       | 1920x1080 | 440x240mm  | 19.7 | 2016 | 7A33E |
| AOC          | AOC2060 | 2060W        | 1600x900  | 430x240mm  | 19.4 | 2014 | 3628F |
| AOC          | AOC2070 | 2070W        | 1600x900  | 430x240mm  | 19.4 | 2018 | 002AB |
| AOC          | AOC2200 | 2200W        | 1920x1080 | 480x270mm  | 21.7 | 2017 | 7C79B |
| AOC          | AOC2200 | F22          | 1920x1080 | 470x260mm  | 21.1 | 2010 | DA833 |
| AOC          | AOC2201 | 22B1W        | 1920x1080 | 480x270mm  | 21.7 | 2018 | D0065 |
| AOC          | AOC2202 | 22V2WG5      | 1920x1080 | 480x270mm  | 21.7 | 2018 | 43768 |
| AOC          | AOC2219 | 2219         | 1680x1050 | 470x300mm  | 22.0 | 2011 | 4DA9C |
| AOC          | AOC2219 | 2219         | 1680x1050 | 470x300mm  | 22.0 | 2010 | ED0DD |
| AOC          | AOC2230 | 2230         | 1680x1050 | 530x300mm  | 24.0 | 2009 | 4CAA0 |
| AOC          | AOC2230 | 212Va        | 1680x1050 | 470x300mm  | 22.0 | 2008 | 2FC7D |
| AOC          | AOC2231 | L22W931      | 1360x768  | 580x320mm  | 26.1 | 2009 | FF91A |
| AOC          | AOC2236 | 2236         | 1920x1080 | 480x270mm  | 21.7 | 2010 | F54A8 |
| AOC          | AOC2236 | 2236         | 1920x1080 | 480x270mm  | 21.7 | 2009 | 0CCB5 |
| AOC          | AOC2243 | 2243W        | 1920x1080 | 480x270mm  | 21.7 | 2012 | B8C0F |
| AOC          | AOC2243 | 2243W        | 1920x1080 | 480x270mm  | 21.7 | 2010 | 8C2CC |
| AOC          | AOC2250 | 2250W        | 1920x1080 | 480x270mm  | 21.7 | 2015 | CD8D1 |
| AOC          | AOC2250 | 2250W        | 1920x1080 | 480x270mm  | 21.7 | 2014 | 2FA0F |
| AOC          | AOC2250 | 2250W        | 1920x1080 | 480x270mm  | 21.7 | 2013 | 84939 |
| AOC          | AOC2250 | 2250W        | 1920x1080 | 480x270mm  | 21.7 | 2012 | 02E92 |
| AOC          | AOC2250 | 2250W        | 1920x1080 | 480x270mm  | 21.7 | 2011 | 71E6F |
| AOC          | AOC2251 | 2251w        | 1920x1080 | 480x270mm  | 21.7 | 2012 | ADA2B |
| AOC          | AOC2252 | 2252W        | 1920x1080 | 480x270mm  | 21.7 | 2011 | 5AF40 |
| AOC          | AOC2260 | 2260WG5      | 1920x1080 | 480x270mm  | 21.7 | 2018 | 09D1D |
| AOC          | AOC2260 | 2260WG5      | 1920x1080 | 480x270mm  | 21.7 | 2017 | 8B40F |
| AOC          | AOC2260 | 2260WG5      | 1920x1080 | 480x270mm  | 21.7 | 2016 | 0BEBA |
| AOC          | AOC2260 | 2260         | 1680x1050 | 470x300mm  | 22.0 | 2016 | 7D0C8 |
| AOC          | AOC2260 | 2260W        | 1920x1080 | 480x270mm  | 21.7 | 2013 | 51908 |
| AOC          | AOC2260 | 2260W        | 1920x1080 | 480x270mm  | 21.7 | 2012 | 2BDE9 |
| AOC          | AOC2260 | 2260         | 1680x1050 | 470x300mm  | 22.0 | 2012 | EDB82 |
| AOC          | AOC2262 | 2262w        | 1920x1080 | 480x270mm  | 21.7 | 2012 | F407A |
| AOC          | AOC2267 | 2267W        | 1920x1080 | 480x270mm  | 21.7 | 2014 | 7A222 |
| AOC          | AOC2267 | 2267W        | 1920x1080 | 480x270mm  | 21.7 | 2013 | 4B71F |
| AOC          | AOC2269 | 2269WM       | 1920x1080 | 480x270mm  | 21.7 | 2017 | 409E6 |
| AOC          | AOC2269 | 2269WM       | 1920x1080 | 480x270mm  | 21.7 | 2016 | 2A599 |
| AOC          | AOC2269 | 2269W        | 1920x1080 | 480x270mm  | 21.7 | 2016 | 566D1 |
| AOC          | AOC2269 | 2269WM       | 1920x1080 | 480x270mm  | 21.7 | 2015 | 5FC90 |
| AOC          | AOC2269 | 2269WM       | 1920x1080 | 480x270mm  | 21.7 | 2014 | 94D98 |
| AOC          | AOC2269 | 2269W        | 1920x1080 | 480x270mm  | 21.7 | 2014 | C0C95 |
| AOC          | AOC2269 | 2269W        | 1920x1080 | 480x270mm  | 21.7 | 2013 | 22539 |
| AOC          | AOC2269 | 2269WM       | 1920x1080 | 480x270mm  | 21.7 | 2013 | B8C30 |
| AOC          | AOC2270 | 2270W        | 1920x1080 | 480x270mm  | 21.7 | 2018 | C2986 |
| AOC          | AOC2270 | 2270W        | 1920x1080 | 480x270mm  | 21.7 | 2017 | 0D202 |
| AOC          | AOC2270 | 2270W        | 1920x1080 | 480x270mm  | 21.7 | 2016 | A5226 |
| AOC          | AOC2275 | 2275W        | 1920x1080 | 480x270mm  | 21.7 | 2017 | 4EA97 |
| AOC          | AOC2276 | 2276W        | 1920x1080 | 480x270mm  | 21.7 | 2017 | 3659C |
| AOC          | AOC2276 | 2276WM       | 1920x1080 | 480x270mm  | 21.7 | 2015 | 50A92 |
| AOC          | AOC2276 | 2276WM       | 1920x1080 | 480x270mm  | 21.7 | 2014 | 6A333 |
| AOC          | AOC2276 | 2276W        | 1920x1080 | 480x270mm  | 21.7 | 2014 | F8F37 |
| AOC          | AOC2280 | 2280W        | 1920x1080 | 480x270mm  | 21.7 | 2018 | 4A80C |
| AOC          | AOC2343 | 2343         | 1920x1080 | 510x290mm  | 23.1 | 2013 | A8BF8 |
| AOC          | AOC2343 | 2343         | 1920x1080 | 510x290mm  | 23.1 | 2011 | 5698A |
| AOC          | AOC2350 | 2350         | 1920x1080 | 510x290mm  | 23.1 | 2012 | 38982 |
| AOC          | AOC2352 | i2352Vh      | 1920x1080 | 510x290mm  | 23.1 | 2012 | 20CF9 |
| AOC          | AOC2353 | 2353         | 1920x1080 | 510x290mm  | 23.1 | 2011 | 3AE08 |
| AOC          | AOC2357 | D2357Ph      | 1920x1080 | 510x290mm  | 23.1 | 2012 | 6D588 |
| AOC          | AOC2367 | 2367         | 1920x1080 | 510x290mm  | 23.1 | 2016 | D68B2 |
| AOC          | AOC2367 | 2367M        | 1920x1080 | 510x290mm  | 23.1 | 2014 | 09DFE |
| AOC          | AOC2367 | 2367         | 1920x1080 | 510x290mm  | 23.1 | 2013 | 13123 |
| AOC          | AOC2367 | D2367        | 1920x1080 | 510x290mm  | 23.1 | 2013 | 246AB |
| AOC          | AOC2367 | 2367         | 1920x1080 | 510x290mm  | 23.1 | 2012 | E77B9 |
| AOC          | AOC2369 | 2369M        | 1920x1080 | 510x290mm  | 23.1 | 2017 | 2A6C7 |
| AOC          | AOC2369 | 2369M        | 1920x1080 | 510x290mm  | 23.1 | 2016 | 175F2 |
| AOC          | AOC2369 | 2369         | 1920x1080 | 510x290mm  | 23.1 | 2015 | 08404 |
| AOC          | AOC2369 | 2369M        | 1920x1080 | 510x290mm  | 23.1 | 2015 | ED62A |
| AOC          | AOC2369 | 2369M        | 1920x1080 | 510x290mm  | 23.1 | 2014 | 229B6 |
| AOC          | AOC2369 | 2369         | 1920x1080 | 510x290mm  | 23.1 | 2014 | D541E |
| AOC          | AOC2369 | 2369         | 1920x1080 | 510x290mm  | 23.1 | 2013 | 5EE27 |
| AOC          | AOC2369 | 2369M        | 1920x1080 | 510x290mm  | 23.1 | 2013 | B2B39 |
| AOC          | AOC2375 | 2375         | 1920x1080 | 510x290mm  | 23.1 | 2017 | 7A597 |
| AOC          | AOC2381 | 2381         | 1920x1080 | 510x290mm  | 23.1 | 2016 | CC497 |
| AOC          | AOC2401 | 24B1W        | 1920x1080 | 520x290mm  | 23.4 | 2018 | 33FF6 |
| AOC          | AOC2429 | 2429W        | 1920x1080 | 520x290mm  | 23.4 | 2016 | 2D18F |
| AOC          | AOC2434 | 2434         | 1920x1080 | 520x290mm  | 23.4 | 2010 | B4619 |
| AOC          | AOC2436 | 2436         | 1920x1080 | 520x290mm  | 23.4 | 2011 | ADF37 |
| AOC          | AOC2436 | 2436         | 1920x1080 | 520x290mm  | 23.4 | 2009 | 3B66B |
| AOC          | AOC2440 | 2440         | 1920x1080 | 530x300mm  | 24.0 | 2011 | 841A7 |
| AOC          | AOC2450 | 2450W        | 1920x1080 | 520x290mm  | 23.4 | 2013 | 73660 |
| AOC          | AOC2450 | 2450W        | 1920x1080 | 520x290mm  | 23.4 | 2012 | 9230E |
| AOC          | AOC2450 | 2450W        | 1920x1080 | 520x290mm  | 23.4 | 2011 | 7BB7C |
| AOC          | AOC2460 | 2460G4       | 1920x1080 | 530x300mm  | 24.0 | 2017 | F3EAF |
| AOC          | AOC2460 | 2460         | 1920x1080 | 530x300mm  | 24.0 | 2016 | 48C18 |
| AOC          | AOC2460 | 2460         | 1920x1080 | 530x300mm  | 24.0 | 2015 | FEBCD |
| AOC          | AOC2460 | 2460         | 1920x1080 | 530x300mm  | 24.0 | 2014 | 6F0D4 |
| AOC          | AOC2460 | G2460        | 1920x1080 | 530x300mm  | 24.0 | 2014 | E1B16 |
| AOC          | AOC2460 | 2460         | 1920x1080 | 530x300mm  | 24.0 | 2012 | 61344 |
| AOC          | AOC2460 | G2460        | 1920x1080 | 530x300mm  | 24.0 |      | 669BD |
| AOC          | AOC246A | 2460G5       | 1920x1080 | 530x300mm  | 24.0 | 2018 | 040A1 |
| AOC          | AOC2470 | 2470W        | 1920x1080 | 520x290mm  | 23.4 | 2017 | 1E7D4 |
| AOC          | AOC2470 | 2470W        | 1920x1080 | 520x290mm  | 23.4 | 2016 | 0733B |
| AOC          | AOC2470 | 2470W        | 1920x1080 | 520x290mm  | 23.4 | 2014 | 6FC29 |
| AOC          | AOC2471 | 2471M        | 1920x1080 | 530x300mm  | 24.0 | 2013 | 58D6B |
| AOC          | AOC2472 | 2472WM       | 1920x1080 | 520x290mm  | 23.4 | 2014 | 5FFB5 |
| AOC          | AOC2475 | 2475W1       | 1920x1080 | 530x300mm  | 24.0 | 2018 | 52251 |
| AOC          | AOC2475 | 2475W1       | 1920x1080 | 530x300mm  | 24.0 | 2017 | 02605 |
| AOC          | AOC2475 | 2475W        | 1920x1080 | 520x290mm  | 23.4 | 2017 | CFAA4 |
| AOC          | AOC2475 | 2475W1       | 1920x1080 | 530x300mm  | 24.0 | 2016 | D252B |
| AOC          | AOC2475 | 2475W1       | 1920x1080 | 530x300mm  | 24.0 | 2015 | 1BA2C |
| AOC          | AOC2476 | 2476WM       | 1920x1080 | 520x290mm  | 23.4 | 2017 | 95307 |
| AOC          | AOC2476 | 2476WM       | 1920x1080 | 520x290mm  | 23.4 | 2016 | 1B5DF |
| AOC          | AOC2476 | 2476W        | 1920x1080 | 520x290mm  | 23.4 | 2016 | F892C |
| AOC          | AOC2480 | 2480W1       | 1920x1080 | 530x300mm  | 24.0 | 2017 | 4C018 |
| AOC          | AOC2481 | 2481W        | 1920x1080 | 530x300mm  | 24.0 | 2017 | 7A089 |
| AOC          | AOC2490 | 2490W1       | 1920x1080 | 530x300mm  | 24.0 | 2018 | 5699A |
| AOC          | AOC2491 | 2491W        | 1920x1080 | 520x290mm  | 23.4 | 2019 | AA3CD |
| AOC          | AOC2495 | 2495         | 1920x1080 | 530x300mm  | 24.0 | 2014 | 5D001 |
| AOC          | AOC2495 | 2495         | 1920x1080 | 530x300mm  | 24.0 | 2013 | 5249F |
| AOC          | AOC2510 | AG251FWG2    | 1920x1080 | 540x300mm  | 24.3 | 2018 | CA474 |
| AOC          | AOC2590 | 2590G4       | 1920x1080 | 540x300mm  | 24.3 | 2018 | A68F2 |
| AOC          | AOC2701 | 27E1         | 1920x1080 | 600x340mm  | 27.2 | 2019 | 2B60F |
| AOC          | AOC2727 | 2727         | 1920x1080 | 600x340mm  | 27.2 | 2016 | 74BE8 |
| AOC          | AOC2752 | e2752Vq      | 1920x1080 | 600x340mm  | 27.2 | 2013 | 31F0C |
| AOC          | AOC2752 | 2752H        | 1920x1080 | 600x340mm  | 27.2 | 2013 | 38D9A |
| AOC          | AOC2752 | 2752         | 1920x1080 | 580x340mm  | 26.5 | 2013 | 67C01 |
| AOC          | AOC2757 | 2757         | 1920x1080 | 600x340mm  | 27.2 | 2016 | 24197 |
| AOC          | AOC2757 | 2757M        | 1920x1080 | 600x340mm  | 27.2 | 2015 | 06AF6 |
| AOC          | AOC2757 | D2757        | 1920x1080 | 600x340mm  | 27.2 | 2013 | 849DB |
| AOC          | AOC2757 | 2757         | 1920x1080 | 600x340mm  | 27.2 | 2013 | E3D3B |
| AOC          | AOC2757 | 2757         | 1920x1080 | 600x340mm  | 27.2 | 2012 | 8F7A4 |
| AOC          | AOC2769 | D2769        | 1920x1080 | 600x340mm  | 27.2 | 2014 | 30739 |
| AOC          | AOC2769 | 2769M        | 1920x1080 | 600x340mm  | 27.2 | 2014 | DF365 |
| AOC          | AOC2770 | 2770         | 1920x1080 | 600x340mm  | 27.2 | 2018 | 6B213 |
| AOC          | AOC2770 | 2770G4       | 1920x1080 | 600x340mm  | 27.2 | 2016 | 86F93 |
| AOC          | AOC2770 | 2770         | 1920x1080 | 600x340mm  | 27.2 | 2015 | F3C59 |
| AOC          | AOC2770 | 2770         | 1920x1080 | 600x340mm  | 27.2 | 2014 | B33ED |
| AOC          | AOC2770 | 2770         | 1920x1080 | 600x340mm  | 27.2 | 2013 | 286F1 |
| AOC          | AOC2775 | Q2775        | 2560x1440 | 600x340mm  | 27.2 | 2017 | AC080 |
| AOC          | AOC2775 | Q2775        | 2560x1440 | 600x340mm  | 27.2 | 2016 | E2211 |
| AOC          | AOC2777 | U2777B       | 3840x2160 | 600x340mm  | 27.2 | 2019 | E5C85 |
| AOC          | AOC2781 | 2781         | 1920x1080 | 600x340mm  | 27.2 | 2017 | 072D2 |
| AOC          | AOC2781 | 2781         | 1920x1080 | 600x340mm  | 27.2 | 2016 | 16655 |
| AOC          | AOC2795 | 2795E        | 1920x1080 | 600x340mm  | 27.2 | 2012 | 8688A |
| AOC          | AOC2795 | 2795E        | 1920x1080 | 600x340mm  | 27.2 | 2011 | 6E489 |
| AOC          | AOC2870 | U2870        | 3840x2160 | 620x340mm  | 27.8 | 2015 | 3AE36 |
| AOC          | AOC2870 | 2870         | 1920x1080 | 620x340mm  | 27.8 | 2014 | 5A4C0 |
| AOC          | AOC2879 | U2879G6      | 3840x2160 | 620x340mm  | 27.8 | 2017 | CA943 |
| AOC          | AOC2879 | U2879G6      | 3840x2160 | 620x340mm  | 27.8 | 2016 | A8BB7 |
| AOC          | AOC2963 | 2963         | 2560x1080 | 670x280mm  | 28.6 | 2013 | 6C4C1 |
| AOC          | AOC3201 | Q32G1WG4     | 2560x1440 | 700x390mm  | 31.5 | 2019 | AD7A1 |
| AOC          | AOC321A | L32W751A     | 1920x540  | 690x390mm  | 31.2 | 2008 | 49363 |
| AOC          | AOC3220 | AG322FWG4    | 1920x1080 | 700x390mm  | 31.5 | 2018 | B492E |
| AOC          | AOC3253 | LC32W053     | 1360x768  | 700x390mm  | 31.5 | 2010 | 4FBCE |
| AOC          | AOC3277 | U3277WB      | 3840x2160 | 700x390mm  | 31.5 | 2018 | B94DD |
| AOC          | AOC3277 | U3277        | 3840x2160 | 710x400mm  | 32.1 | 2015 | 96696 |
| AOC          | AOC3279 | Q3279WG5B    | 2560x1440 | 730x430mm  | 33.4 | 2019 | 266D5 |
| AOC          | AOC3279 | Q3279WG5B    | 2560x1440 | 730x430mm  | 33.4 | 2018 | 411DC |
| AOC          | AOC3477 | Q3477        | 2560x1080 | 800x340mm  | 34.2 | 2016 | FBBAA |
| AOC          | AOC3520 | AG352QG2     | 2560x1080 | 820x350mm  | 35.1 | 2017 | 91DBD |
| AOC          | AOC4220 | LE42D5520/20 | 1920x1080 | 940x530mm  | 42.5 | 2011 | 11410 |
| ASUS         | AUS1641 | MB16AC       | 1920x1080 | 340x190mm  | 15.3 | 2019 | B35E6 |
| ASUS         | AUS22CC | VZ229        | 1920x1080 | 480x270mm  | 21.7 | 2018 | 846B9 |
| ASUS         | AUS22CC | VZ229        | 1920x1080 | 480x270mm  | 21.7 | 2017 | 6457D |
| ASUS         | AUS24A1 | VG245        | 1920x1080 | 530x300mm  | 24.0 | 2018 | 4E92A |
| ASUS         | AUS24A1 | VG245        | 1920x1080 | 530x300mm  | 24.0 | 2017 | C5C24 |
| ASUS         | AUS24AA | VP249        | 1920x1080 | 530x300mm  | 24.0 | 2018 | E564C |
| ASUS         | AUS24C1 | VA249        | 1920x1080 | 530x300mm  | 24.0 | 2018 | 79A81 |
| ASUS         | AUS24CA | VP247        | 1920x1080 | 520x290mm  | 23.4 | 2018 | 86FDA |
| ASUS         | AUS25B1 | ROG PG258Q   | 1920x1080 | 540x300mm  | 24.3 | 2019 | DF5E4 |
| ASUS         | AUS27A2 | MX27UC       | 3840x2160 | 600x340mm  | 27.2 | 2018 | 692DC |
| ASUS         | AUS27A2 | MX27UC       | 3840x2160 | 600x340mm  | 27.2 | 2017 | 33864 |
| ASUS         | AUS27A5 | VZ27V        | 1920x1080 | 600x340mm  | 27.2 | 2017 | B70EB |
| ASUS         | AUS27A6 | XG27VQ       | 1920x1080 | 600x340mm  | 27.2 | 2018 | DC884 |
| ASUS         | AUS27AE | VP278        | 1920x1080 | 600x340mm  | 27.2 | 2017 | C8242 |
| ASUS         | AUS27B1 | ROG PG278QR  | 2560x1440 | 600x340mm  | 27.2 | 2016 | D0ECA |
| ASUS         | AUS27C0 | VZ279HE      | 1920x1080 | 600x340mm  | 27.2 | 2017 | B8AD5 |
| ASUS         | AUS32B1 | XG32V        | 2560x1440 | 700x390mm  | 31.5 | 2018 | D8054 |
| ASUS         | AUS32FA | VA326        | 1920x1080 | 700x390mm  | 31.5 | 2016 | 22F04 |
| ASUS         | WWW282C | ZN242IF      | 1920x1080 | 480x270mm  | 21.7 | 2017 | 75C7D |
| ASUS         | WWW282C | ZN242GD      | 1920x1080 | 530x310mm  | 24.2 | 2017 | 9AADE |
| AU Optronics | AUO0025 | M240HW02 V5  | 1920x1080 | 530x300mm  | 24.0 | 2010 | 64FA0 |
| AU Optronics | AUO00ED | 6985X        | 1920x1080 | 340x190mm  | 15.3 | 2011 | 3EEEA |
| AU Optronics | AUO0114 | B140EW01V... | 1280x768  | 330x200mm  | 15.2 |      | 20BCE |
| AU Optronics | AUO01EE | B156RW01 V1  | 1600x900  | 340x190mm  | 15.3 | 2008 | 4AD0A |
| AU Optronics | AUO02EC | B156XW02 V2  | 1366x768  | 340x190mm  | 15.3 | 2009 | 816D0 |
| AU Optronics | AUO039E | B173RW01 V3  | 1600x900  | 380x210mm  | 17.1 | 2009 | 23189 |
| AU Optronics | AUO0908 | B160HW02 V0  | 1920x1080 | 350x200mm  | 15.9 | 2011 | 03F0D |
| AU Optronics | AUO0C01 | B121EW01 ... | 1280x800  | 270x170mm  | 12.6 |      | 094A4 |
| AU Optronics | AUO0F06 | B150XG01V2   | 1024x768  | 300x230mm  | 14.9 |      | 1A8BE |
| AU Optronics | AUO0F07 | D8381        | 1024x768  | 300x230mm  | 14.9 |      | 3F57B |
| AU Optronics | AUO0F0D | B154EW01 V.7 | 1280x800  | 330x210mm  | 15.4 |      | 03416 |
| AU Optronics | AUO1020 | A089SW01 V0  | 1024x600  | 190x110mm  | 8.6  | 2008 | 71D8B |
| AU Optronics | AUO102C | B133XTN01.0  | 1366x768  | 290x160mm  | 13.0 | 2012 | E90D7 |
| AU Optronics | AUO102C | B133XTF01.0  | 1366x768  | 290x160mm  | 13.0 | 2011 | 605D7 |
| AU Optronics | AUO102C | B133XW01 V0  | 1366x768  | 290x160mm  | 13.0 | 2008 | 489ED |
| AU Optronics | AUO102D | 4FHP9        | 1920x1080 | 290x170mm  | 13.2 | 2017 | 699CA |
| AU Optronics | AUO102D | W94FJ        | 1920x1080 | 290x170mm  | 13.2 | 2016 | 09180 |
| AU Optronics | AUO102D | FCTG8        | 1920x1080 | 290x170mm  | 13.2 | 2016 | 23898 |
| AU Optronics | AUO1036 | F0WXV        | 2560x1440 | 310x170mm  | 13.9 | 2014 | CD4F9 |
| AU Optronics | AUO103C | VXKJX        | 1366x768  | 310x170mm  | 13.9 | 2013 | B241B |
| AU Optronics | AUO103C | B140XTT01.0  | 1366x768  | 310x170mm  | 13.9 | 2012 | 6D961 |
| AU Optronics | AUO103C | C591J        | 1366x768  | 310x170mm  | 13.9 | 2009 | 0276F |
| AU Optronics | AUO103C | B140XW01 V0  | 1366x768  | 310x170mm  | 13.9 | 2008 | 22F8D |
| AU Optronics | AUO103D | B140HAK01.0  | 1920x1080 | 310x170mm  | 13.9 | 2016 | 5E95C |
| AU Optronics | AUO103E | W3V10        | 1600x900  | 310x170mm  | 13.9 | 2014 | BB6D4 |
| AU Optronics | AUO103E | XYH93        | 1600x900  | 310x170mm  | 13.9 | 2011 | DE984 |
| AU Optronics | AUO105C | G7TKC        | 1366x768  | 260x140mm  | 11.6 | 2016 | 4037E |
| AU Optronics | AUO105C | B116XTB01.0  | 1366x768  | 260x140mm  | 11.6 | 2014 | 6E2C8 |
| AU Optronics | AUO105C | B116XTN01.0  | 1366x768  | 260x140mm  | 11.6 | 2013 | 26F08 |
| AU Optronics | AUO105C | B116XW01 V0  | 1366x768  | 260x140mm  | 11.6 | 2009 | 18DE7 |
| AU Optronics | AUO105C | V1V85        | 1366x768  | 260x140mm  | 11.6 | 2009 | 1E789 |
| AU Optronics | AUO105C | B116XW01 V0  | 1366x768  | 260x140mm  | 11.6 | 2008 | 72FFC |
| AU Optronics | AUO106C | 9X5G1        | 1366x768  | 280x160mm  | 12.7 | 2016 | 98D96 |
| AU Optronics | AUO106C | B125XTN01.0  | 1366x768  | 280x160mm  | 12.7 | 2012 | 7EEB3 |
| AU Optronics | AUO106C | 8X9KT        | 1366x768  | 280x160mm  | 12.7 | 2011 | FF2F8 |
| AU Optronics | AUO106C | B125XW01 V0  | 1366x768  | 280x160mm  | 12.7 | 2010 | 31E1A |
| AU Optronics | AUO106D | B125HAK01.0  | 1920x1080 | 280x160mm  | 12.7 | 2017 | F1391 |
| AU Optronics | AUO1088 | B170UW01 V0  | 1920x1200 | 370x230mm  | 17.2 | 2007 | B20CF |
| AU Optronics | AUO109B | B173ZAN01.0  | 3840x2160 | 380x210mm  | 17.1 | 2017 | E4482 |
| AU Optronics | AUO109B | 8CJK2        | 3840x2160 | 380x210mm  | 17.1 | 2015 | 78445 |
| AU Optronics | AUO109B | 2DK4K        | 3840x2160 | 380x210mm  | 17.1 | 2015 | 825D8 |
| AU Optronics | AUO109D | Y147T        | 1920x1080 | 380x210mm  | 17.1 | 2015 | 5D5AC |
| AU Optronics | AUO109D |              | 1920x1080 | 380x210mm  | 17.1 | 2015 | FEDCF |
| AU Optronics | AUO109D | B173HAN01.0  | 1920x1080 | 380x210mm  | 17.1 | 2014 | FF533 |
| AU Optronics | AUO109E | 4PG9N        | 1600x900  | 380x210mm  | 17.1 | 2009 | 0FA08 |
| AU Optronics | AUO109E | B173RW01 V0  | 1600x900  | 380x210mm  | 17.1 | 2008 | A6DF2 |
| AU Optronics | AUO10C2 | B089AW01 V0  | 1024x600  | 200x110mm  | 9.0  | 2008 | 0905D |
| AU Optronics | AUO10D1 | K253P        | 1024x576  | 220x130mm  | 10.1 | 2009 | 297EC |
| AU Optronics | AUO10DC | B101XTN01.0  | 1366x768  | 220x130mm  | 10.1 | 2012 | 42DF2 |
| AU Optronics | AUO10EC |              | 1366x768  | 340x190mm  | 15.3 | 2017 | 61EAD |
| AU Optronics | AUO10EC |              | 1366x768  | 340x190mm  | 15.3 | 2016 | 52DB5 |
| AU Optronics | AUO10EC | 2YTDP        | 1366x768  | 340x190mm  | 15.3 | 2016 | ADB7C |
| AU Optronics | AUO10EC | JJ45K        | 1366x768  | 340x190mm  | 15.3 | 2015 | C8EB7 |
| AU Optronics | AUO10EC | B156XTK01.0  | 1366x768  | 340x190mm  | 15.3 | 2014 | 14B03 |
| AU Optronics | AUO10EC |              | 1366x768  | 340x190mm  | 15.3 | 2014 | C610D |
| AU Optronics | AUO10EC | B156XTT01.0  | 1366x768  | 340x190mm  | 15.3 | 2012 | D9D90 |
| AU Optronics | AUO10EC | B156XTN01.0  | 1366x768  | 340x190mm  | 15.3 | 2011 | 7A434 |
| AU Optronics | AUO10EC | B156XW01 V0  | 1366x768  | 340x190mm  | 15.3 | 2008 | 2EAE0 |
| AU Optronics | AUO10ED | 0079Y        | 1920x1080 | 340x190mm  | 15.3 | 2016 | 387CB |
| AU Optronics | AUO10ED | FNDC6        | 1920x1080 | 340x190mm  | 15.3 | 2016 | 48704 |
| AU Optronics | AUO10ED | H1G7K        | 1920x1080 | 340x190mm  | 15.3 | 2014 | 6F4E7 |
| AU Optronics | AUO10ED | 9F8C8        | 1920x1080 | 340x190mm  | 15.3 | 2014 | D4A4B |
| AU Optronics | AUO10ED | FTKKN        | 1920x1080 | 340x190mm  | 15.3 | 2012 | F7D0A |
| AU Optronics | AUO10ED | B156HW01 V0  | 1920x1080 | 340x190mm  | 15.3 | 2008 | 87871 |
| AU Optronics | AUO1101 | B170PW01 V.1 | 1440x900  | 370x230mm  | 17.2 |      | 99F4B |
| AU Optronics | AUO112C | B133XW01 V1  | 1366x768  | 290x160mm  | 13.0 | 2008 | 7724A |
| AU Optronics | AUO112C | XX31G        | 1366x768  | 290x160mm  | 13.0 | 2008 | C4709 |
| AU Optronics | AUO112D | B133HTN01.1  | 1920x1080 | 290x170mm  | 13.2 | 2013 | 20859 |
| AU Optronics | AUO1136 |              | 2560x1440 | 310x170mm  | 13.9 | 2014 | 01E42 |
| AU Optronics | AUO113D | XTRY9        | 1920x1080 | 310x170mm  | 13.9 | 2013 | 69D2F |
| AU Optronics | AUO113D | B140HAN01.1  | 1920x1080 | 310x170mm  | 13.9 | 2012 | 7F509 |
| AU Optronics | AUO1144 | B141EW01 V1  | 1280x800  | 320x210mm  | 15.1 |      | FE0E0 |
| AU Optronics | AUO1147 | B141PW01 V1  | 1440x900  | 300x190mm  | 14.0 | 2006 | FFFC9 |
| AU Optronics | AUO119D | B173HAN01.1  | 1920x1080 | 380x210mm  | 17.1 | 2016 | FC9DA |
| AU Optronics | AUO119D | B173HTN01.1  | 1920x1080 | 380x210mm  | 17.1 | 2015 | C6C21 |
| AU Optronics | AUO119D | MM77H        | 1920x1080 | 380x210mm  | 17.1 | 2014 | ED20F |
| AU Optronics | AUO119D | B173HTN01.1  | 1920x1080 | 380x210mm  | 17.1 | 2013 | 61411 |
| AU Optronics | AUO119E | B173RTN01.1  | 1600x900  | 380x210mm  | 17.1 | 2014 | E0C6D |
| AU Optronics | AUO119E | 3V6TR        | 1600x900  | 380x210mm  | 17.1 | 2013 | 7D66D |
| AU Optronics | AUO119E | B173RTN01.1  | 1600x900  | 380x210mm  | 17.1 | 2011 | 559D7 |
| AU Optronics | AUO119E | B173RW01 V1  | 1600x900  | 380x210mm  | 17.1 | 2008 | 05CB3 |
| AU Optronics | AUO11C2 | B089AW01 V1  | 1024x600  | 200x110mm  | 9.0  | 2008 | 1989C |
| AU Optronics | AUO11D1 | B101AW01 V1  | 1024x576  | 220x130mm  | 10.1 | 2008 | E5174 |
| AU Optronics | AUO11DC | B101XTN01.1  | 1366x768  | 220x130mm  | 10.1 | 2012 | C963E |
| AU Optronics | AUO11EC | B156XTT01.1  | 1366x768  | 340x190mm  | 15.3 | 2013 | 9D3A3 |
| AU Optronics | AUO11EC | 1K0XP        | 1366x768  | 340x190mm  | 15.3 | 2013 | D2936 |
| AU Optronics | AUO11EC | M094G        | 1366x768  | 340x190mm  | 15.3 | 2008 | 4E08D |
| AU Optronics | AUO11ED | CRN6V        | 1920x1080 | 340x190mm  | 15.3 | 2014 | 2E6AF |
| AU Optronics | AUO11ED | R9P60        | 1920x1080 | 340x190mm  | 15.3 | 2014 | C8066 |
| AU Optronics | AUO11ED | B156HAN01.1  | 1920x1080 | 340x190mm  | 15.3 | 2013 | 18A43 |
| AU Optronics | AUO11ED | B156HTN01.1  | 1920x1080 | 340x190mm  | 15.3 | 2012 | 92D99 |
| AU Optronics | AUO11ED | B156HW01 V1  | 1920x1080 | 340x190mm  | 15.3 | 2009 | 01C83 |
| AU Optronics | AUO11EE | B173RTN01.1  | 1920x1080 | 380x210mm  | 17.1 | 2011 | F5D44 |
| AU Optronics | AUO11EE |              | 1600x900  | 340x190mm  | 15.3 | 2008 | 0D14F |
| AU Optronics | AUO122C | B133XW01 V2  | 1366x768  | 290x160mm  | 13.0 | 2008 | CF91F |
| AU Optronics | AUO123C | 4D3YR        | 1366x768  | 310x170mm  | 13.9 | 2013 | 12F93 |
| AU Optronics | AUO123D | B140HTN01.2  | 1920x1080 | 310x170mm  | 13.9 | 2014 | 563D7 |
| AU Optronics | AUO123D |              | 1920x1080 | 310x170mm  | 13.9 | 2013 | 10931 |
| AU Optronics | AUO123D | B140HTN01.2  | 1920x1080 | 310x170mm  | 13.9 | 2012 | 5C7A7 |
| AU Optronics | AUO123D | B140HAN01.2  | 1920x1080 | 310x170mm  | 13.9 | 2012 | 67C22 |
| AU Optronics | AUO123D |              | 1920x1080 | 310x170mm  | 13.9 | 2012 | D60FB |
| AU Optronics | AUO1247 | B141PW01 V2  | 1440x900  | 300x190mm  | 14.0 |      | 5FB31 |
| AU Optronics | AUO129E |              | 1600x900  | 380x210mm  | 17.1 | 2011 | 75F81 |
| AU Optronics | AUO129E | B173RTN01.2  | 1600x900  | 380x210mm  | 17.1 | 2011 | A69D7 |
| AU Optronics | AUO129E | M99C0        | 1600x900  | 380x210mm  | 17.1 | 2010 | B476D |
| AU Optronics | AUO129E | B173RW01 V2  | 1600x900  | 380x210mm  | 17.1 | 2009 | 8D58B |
| AU Optronics | AUO12D1 | B101AW01 V2  | 1024x576  | 220x130mm  | 10.1 | 2008 | F24AB |
| AU Optronics | AUO12EC | B156XW01 V2  | 1366x768  | 340x190mm  | 15.3 | 2008 | 31D40 |
| AU Optronics | AUO12EC | H597H        | 1366x768  | 340x190mm  | 15.3 | 2008 | BEA7C |
| AU Optronics | AUO12ED | B156HAN01.2  | 1920x1080 | 340x190mm  | 15.3 | 2012 | 62F95 |
| AU Optronics | AUO132C | B133XTN01.3  | 1366x768  | 290x160mm  | 13.0 | 2014 | C086B |
| AU Optronics | AUO132C | B133XTF01.3  | 1366x768  | 290x160mm  | 13.0 | 2012 | 49772 |
| AU Optronics | AUO132C | B133XTN01.3  | 1366x768  | 290x160mm  | 13.0 | 2012 | BE31D |
| AU Optronics | AUO133C | 6V83Y        | 1366x768  | 310x170mm  | 13.9 | 2014 | 218FD |
| AU Optronics | AUO133C | B140XTN01.3  | 1366x768  | 310x170mm  | 13.9 | 2009 | DA7BF |
| AU Optronics | AUO133D |              | 1920x1080 | 310x170mm  | 13.9 | 2014 | E8EAE |
| AU Optronics | AUO133D | M1WHV        | 1920x1080 | 310x170mm  | 13.9 | 2013 | 2D87F |
| AU Optronics | AUO133D | B140HAN01.3  | 1920x1080 | 310x170mm  | 13.9 | 2013 | 5B02B |
| AU Optronics | AUO1347 | GR584 (8CLl  | 1440x900  | 300x190mm  | 14.0 | 2006 | 46A94 |
| AU Optronics | AUO1347 | GR584 (8DMm  | 1440x900  | 300x190mm  | 14.0 | 2006 | A5776 |
| AU Optronics | AUO139D | MWY7K        | 1920x1080 | 380x210mm  | 17.1 | 2017 | 06462 |
| AU Optronics | AUO139D | B173HAN01.3  | 1920x1080 | 380x210mm  | 17.1 | 2015 | BBB88 |
| AU Optronics | AUO139E | B173RTN01.3  | 1600x900  | 380x210mm  | 17.1 | 2012 | 0AD2C |
| AU Optronics | AUO139E |              | 1600x900  | 380x210mm  | 17.1 | 2011 | 31D7B |
| AU Optronics | AUO139E | B173RW01 V3  | 1600x900  | 380x210mm  | 17.1 | 2010 | 2F895 |
| AU Optronics | AUO139E | P97DW        | 1600x900  | 380x210mm  | 17.1 | 2010 | A047B |
| AU Optronics | AUO139E | B173RW01 V3  | 1600x900  | 380x210mm  | 17.1 | 2009 | 27C5D |
| AU Optronics | AUO13D1 | B101AW01 V3  | 1024x576  | 220x130mm  | 10.1 | 2008 | 9231C |
| AU Optronics | AUO13ED |              | 1920x1080 | 340x190mm  | 15.3 | 2008 | 519F5 |
| AU Optronics | AUO13ED | B156HW01 V3  | 1920x1080 | 340x190mm  | 15.3 | 2008 | F877E |
| AU Optronics | AUO1424 | MT679        | 1280x800  | 290x180mm  | 13.4 | 2008 | B4AC5 |
| AU Optronics | AUO1424 | DW909 *:GPr  | 1280x800  | 290x180mm  | 13.4 | 2007 | 709FB |
| AU Optronics | AUO1424 | DW909        | 1280x800  | 290x180mm  | 13.4 | 2007 | 86AB8 |
| AU Optronics | AUO1424 | XU290 #2=Fd  | 1280x800  | 290x180mm  | 13.4 | 2007 | 8BA33 |
| AU Optronics | AUO1424 | R738G #2=Fe  | 1280x800  | 290x180mm  | 13.4 | 2006 | 7C988 |
| AU Optronics | AUO142D | B133HTN01.4  | 1920x1080 | 290x170mm  | 13.2 | 2016 | 63E5A |
| AU Optronics | AUO143C |              | 1366x768  | 310x170mm  | 13.9 | 2009 | 4A8F8 |
| AU Optronics | AUO143D | B140HAN01.4  | 1920x1080 | 310x170mm  | 13.9 | 2014 | 32622 |
| AU Optronics | AUO1447 | U804G )9FOo  | 1440x900  | 300x190mm  | 14.0 | 2006 | 35898 |
| AU Optronics | AUO1474 | CD514 0AMWx  | 1280x800  | 330x210mm  | 15.4 |      | 0676A |
| AU Optronics | AUO1474 | WD318        | 1280x800  | 330x210mm  | 15.4 |      | B7816 |
| AU Optronics | AUO149E |              | 1600x900  | 380x210mm  | 17.1 | 2012 | 293B8 |
| AU Optronics | AUO149E | B173RW01 V4  | 1600x900  | 380x210mm  | 17.1 | 2012 | 4F0F2 |
| AU Optronics | AUO149E | B173RW01 V4  | 1600x900  | 380x210mm  | 17.1 | 2011 | E297C |
| AU Optronics | AUO152C | B133XTN01.5  | 1366x768  | 290x160mm  | 13.0 | 2012 | 27F60 |
| AU Optronics | AUO159D | B173HW01 V5  | 1920x1080 | 380x210mm  | 17.1 | 2011 | 33C7E |
| AU Optronics | AUO159D | B173HW01 V5  | 1920x1080 | 380x210mm  | 17.1 | 2010 | 7F5E4 |
| AU Optronics | AUO159E |              | 1600x900  | 380x210mm  | 17.1 | 2012 | 8630F |
| AU Optronics | AUO159E | DYMX0        | 1600x900  | 380x210mm  | 17.1 | 2011 | 779C5 |
| AU Optronics | AUO159E | B173RW01 V5  | 1600x900  | 380x210mm  | 17.1 | 2011 | FC451 |
| AU Optronics | AUO15ED | B156HW01 V5  | 1920x1080 | 340x190mm  | 15.3 | 2008 | 4A5C7 |
| AU Optronics | AUO162C |              | 1366x768  | 290x160mm  | 13.0 | 2015 | C3A72 |
| AU Optronics | AUO162C | B133XTN01.6  | 1366x768  | 290x160mm  | 13.0 | 2014 | 1DF03 |
| AU Optronics | AUO163C | B140XW01 V6  | 1366x768  | 310x170mm  | 13.9 | 2009 | CAC26 |
| AU Optronics | AUO172C | B133XW01 V7  | 1366x768  | 290x160mm  | 13.0 | 2008 | 5296B |
| AU Optronics | AUO1751 | B150XG01V7   | 1024x768  | 300x230mm  | 14.9 |      | FEC10 |
| AU Optronics | AUO17ED | KYYVK        | 1920x1080 | 340x190mm  | 15.3 | 2010 | B462A |
| AU Optronics | AUO17ED | B156HW01 V7  | 1920x1080 | 340x190mm  | 15.3 | 2010 | E358B |
| AU Optronics | AUO183C | B140XW01 V8  | 1366x768  | 310x170mm  | 13.9 | 2009 | 66CE9 |
| AU Optronics | AUO183C | GP84R        | 1366x768  | 310x170mm  | 13.9 | 2009 | CDC74 |
| AU Optronics | AUO1874 | B154EW01 V8  | 1280x800  | 330x210mm  | 15.4 |      | 0BDC0 |
| AU Optronics | AUO18D4 |              | 1280x800  | 220x140mm  | 10.3 | 2013 | 20D08 |
| AU Optronics | AUO193C | B140XW01 V9  | 1366x768  | 310x170mm  | 13.9 | 2009 | 23B3C |
| AU Optronics | AUO193C | GJ475        | 1366x768  | 310x170mm  | 13.9 | 2009 | DF6FE |
| AU Optronics | AUO1974 | B154EW01 V9  | 1280x800  | 330x210mm  | 15.4 |      | FB18E |
| AU Optronics | AUO197B | B154SW01 V9  | 1680x1050 | 330x210mm  | 15.4 | 2007 | 21C92 |
| AU Optronics | AUO1AD8 |              | 1920x1200 | 220x140mm  | 10.3 | 2013 | 7B9EF |
| AU Optronics | AUO1B3C | B140XW01 VB  | 1366x768  | 310x170mm  | 13.9 | 2009 | B0222 |
| AU Optronics | AUO1B7B | B154SW01 VB  | 1680x1050 | 330x210mm  | 15.4 | 2007 | 0A215 |
| AU Optronics | AUO1C3C | B140XW01 VC  | 1366x768  | 310x170mm  | 13.9 | 2016 | 72718 |
| AU Optronics | AUO1D3D | B140HTN01.D  | 1920x1080 | 310x170mm  | 13.9 | 2015 | 9C3A7 |
| AU Optronics | AUO1E3D |              | 1920x1080 | 310x170mm  | 13.9 | 2016 | 7ECDE |
| AU Optronics | AUO1E3D | B140HTN01.E  | 1920x1080 | 310x170mm  | 13.9 | 2015 | AFB11 |
| AU Optronics | AUO202C | B133XW02 V0  | 1366x768  | 290x160mm  | 13.0 | 2008 | 0331F |
| AU Optronics | AUO202D | B133HAN02.0  | 1920x1080 | 290x170mm  | 13.2 | 2012 | BB472 |
| AU Optronics | AUO2036 | B140QAN02.0  | 2560x1440 | 310x170mm  | 13.9 | 2017 | 16F41 |
| AU Optronics | AUO203C |              | 1366x768  | 310x170mm  | 13.9 | 2011 | 0250C |
| AU Optronics | AUO203E | B140RW02 V0  | 1600x900  | 310x170mm  | 13.9 | 2010 | B0C70 |
| AU Optronics | AUO203E | B140RW02 V0  | 1600x900  | 310x170mm  | 13.9 | 2009 | 4FCC0 |
| AU Optronics | AUO2052 | B116AW02 V0  | 1024x600  | 260x140mm  | 11.6 | 2009 | BC809 |
| AU Optronics | AUO205C | B116XAN02.0  | 1366x768  | 260x140mm  | 11.6 | 2012 | DC012 |
| AU Optronics | AUO205C | B116XW02 V0  | 1366x768  | 260x140mm  | 11.6 | 2011 | B36E0 |
| AU Optronics | AUO205C | B116XW02 V0  | 1366x768  | 260x140mm  | 11.6 | 2008 | 86024 |
| AU Optronics | AUO206C | M6F9D        | 1366x768  | 280x160mm  | 12.7 | 2013 | A69BC |
| AU Optronics | AUO206C |              | 1366x768  | 280x160mm  | 12.7 | 2012 | 9BA00 |
| AU Optronics | AUO206C |              | 1366x768  | 280x160mm  | 12.7 | 2010 | 2D63D |
| AU Optronics | AUO2074 | B154EW02 V0  | 1280x800  | 330x210mm  | 15.4 |      | 68E81 |
| AU Optronics | AUO2077 | W659G &5@Hf  | 1440x900  | 330x210mm  | 15.4 | 2008 | 32ACF |
| AU Optronics | AUO2077 | GU429 &5AJg  | 1440x900  | 330x210mm  | 15.4 | 2006 | 44DCF |
| AU Optronics | AUO2077 | RP641 &5AJg  | 1440x900  | 330x210mm  | 15.4 | 2006 | 47B55 |
| AU Optronics | AUO2088 | B170UW02 V0  | 1920x1200 | 370x230mm  | 17.2 | 2007 | DF3BD |
| AU Optronics | AUO209D | B173HW02 V0  | 1920x1080 | 380x210mm  | 17.1 | 2011 | DEFFB |
| AU Optronics | AUO20D2 | B101AW02 V0  | 1024x600  | 220x130mm  | 10.1 | 2008 | 44427 |
| AU Optronics | AUO20D5 | B101EW02 V0  | 1280x720  | 220x130mm  | 10.1 | 2009 | A2668 |
| AU Optronics | AUO20EB | F2TW2        | 3840x2160 | 350x190mm  | 15.7 | 2016 | A7D21 |
| AU Optronics | AUO20EC | B156XTN02.0  | 1366x768  | 340x190mm  | 15.3 | 2013 | 2079F |
| AU Optronics | AUO20EC | P5CK7        | 1366x768  | 340x190mm  | 15.3 | 2011 | 1DD61 |
| AU Optronics | AUO20EC |              | 1366x768  | 340x190mm  | 15.3 | 2011 | 3362D |
| AU Optronics | AUO20EC | B156XTN02.0  | 1366x768  | 340x190mm  | 15.3 | 2011 | BC0B1 |
| AU Optronics | AUO20EC | B156XW02 V0  | 1366x768  | 340x190mm  | 15.3 | 2008 | 1B5DD |
| AU Optronics | AUO20ED | B156HAK02.0  | 1920x1080 | 340x190mm  | 15.3 | 2017 | 52D10 |
| AU Optronics | AUO212C | B133XW02 V1  | 1366x768  | 290x160mm  | 13.0 | 2008 | 11142 |
| AU Optronics | AUO212D | B133HAN02.1  | 1920x1080 | 290x170mm  | 13.2 | 2013 | 029A9 |
| AU Optronics | AUO213C | B140XTN02.1  | 1366x768  | 310x170mm  | 13.9 | 2011 | 4FB3C |
| AU Optronics | AUO213C | B140XW02 V1  | 1366x768  | 310x170mm  | 13.9 | 2008 | 5BEED |
| AU Optronics | AUO213D | 6MN77        | 1920x1080 | 310x170mm  | 13.9 | 2016 | F862D |
| AU Optronics | AUO213D |              | 1920x1080 | 310x170mm  | 13.9 | 2015 | 80AE1 |
| AU Optronics | AUO213D | B140HAN02.1  | 1920x1080 | 310x170mm  | 13.9 | 2015 | BC825 |
| AU Optronics | AUO213E | V3YHF        | 1600x900  | 310x170mm  | 13.9 | 2011 | 50069 |
| AU Optronics | AUO213E | B140RW02 V1  | 1600x900  | 310x170mm  | 13.9 | 2010 | 70CC4 |
| AU Optronics | AUO215C | B116XTN02.1  | 1366x768  | 260x140mm  | 11.6 | 2013 | C0BD5 |
| AU Optronics | AUO2174 | B154EW02 V1  | 1280x800  | 330x210mm  | 15.4 |      | 9029B |
| AU Optronics | AUO219D | P9JNK        | 1920x1080 | 380x210mm  | 17.1 | 2013 | C4DEC |
| AU Optronics | AUO219D | B173HW02 V1  | 1920x1080 | 380x210mm  | 17.1 | 2012 | 6AE87 |
| AU Optronics | AUO219D |              | 1920x1080 | 380x210mm  | 17.1 | 2011 | 552D4 |
| AU Optronics | AUO219E | B173RTN02.1  | 1600x900  | 380x210mm  | 17.1 | 2018 | EA821 |
| AU Optronics | AUO219E | C00WX        | 1600x900  | 380x210mm  | 17.1 | 2016 | D08CF |
| AU Optronics | AUO219E | B173RTN02.1  | 1600x900  | 380x210mm  | 17.1 | 2016 | EA546 |
| AU Optronics | AUO219E | B173RTN02.1  | 1600x900  | 380x210mm  | 17.1 | 2015 | 47069 |
| AU Optronics | AUO219E |              | 1600x900  | 380x210mm  | 17.1 | 2015 | ABF93 |
| AU Optronics | AUO21EC | B156XTN02.1  | 1366x768  | 340x190mm  | 15.3 | 2011 | 0963F |
| AU Optronics | AUO21EC |              | 1366x768  | 340x190mm  | 15.3 | 2011 | B41E2 |
| AU Optronics | AUO21EC | B156XW02 V1  | 1366x768  | 340x190mm  | 15.3 | 2008 | 2B6D2 |
| AU Optronics | AUO21ED | B156HAN02.1  | 1920x1080 | 340x190mm  | 15.3 | 2016 | 3B358 |
| AU Optronics | AUO21ED | VYY56        | 1920x1080 | 340x190mm  | 15.3 | 2012 | A8C6D |
| AU Optronics | AUO21ED | C0T2R        | 1920x1080 | 340x190mm  | 15.3 | 2012 | D6779 |
| AU Optronics | AUO21ED | B156HW02 V1  | 1920x1080 | 340x190mm  | 15.3 | 2011 | 27261 |
| AU Optronics | AUO21ED | 35K06        | 1920x1080 | 340x190mm  | 15.3 | 2011 | D15B1 |
| AU Optronics | AUO21ED | F2J5X        | 1920x1080 | 340x190mm  | 15.3 | 2011 | E1B26 |
| AU Optronics | AUO222C | B133XW02 V2  | 1366x768  | 290x160mm  | 13.0 | 2008 | 1E117 |
| AU Optronics | AUO2236 | B140QAN02.2  | 2560x1440 | 310x170mm  | 13.9 | 2017 | A7FD0 |
| AU Optronics | AUO223C |              | 1366x768  | 310x170mm  | 13.9 | 2010 | 453F6 |
| AU Optronics | AUO223C | B140XW02 V2  | 1366x768  | 310x170mm  | 13.9 | 2010 | ED070 |
| AU Optronics | AUO223E | M4RTT        | 1600x900  | 310x170mm  | 13.9 | 2012 | 54494 |
| AU Optronics | AUO223E |              | 1600x900  | 310x170mm  | 13.9 | 2010 | 4E8BD |
| AU Optronics | AUO225C | B116XTN02.2  | 1366x768  | 260x140mm  | 11.6 | 2014 | 514DE |
| AU Optronics | AUO226D | B125HAN02.2  | 1920x1080 | 280x160mm  | 12.7 | 2015 | C0AF5 |
| AU Optronics | AUO2274 | W651G 1AMUt  | 1280x800  | 330x210mm  | 15.4 | 2008 | 03FF6 |
| AU Optronics | AUO2274 | U515F        | 1280x800  | 330x210mm  | 15.4 | 2008 | 0A465 |
| AU Optronics | AUO2274 | W651G        | 1280x800  | 330x210mm  | 15.4 | 2008 | 6A7A3 |
| AU Optronics | AUO2274 | PY599 &5AIf  | 1280x800  | 330x210mm  | 15.4 | 2006 | 1799A |
| AU Optronics | AUO2274 | PY599 1AMUt  | 1280x800  | 330x210mm  | 15.4 | 2006 | B3BFC |
| AU Optronics | AUO2277 | B154PW02 V2  | 1440x900  | 330x210mm  | 15.4 | 2006 | CCAE1 |
| AU Optronics | AUO229E | B173RTN02.2  | 1600x900  | 380x210mm  | 17.1 | 2015 | 0B976 |
| AU Optronics | AUO229E |              | 1600x900  | 380x210mm  | 17.1 | 2015 | BD0F8 |
| AU Optronics | AUO22EC | B156XTN02.2  | 1366x768  | 340x190mm  | 15.3 | 2011 | 23C9C |
| AU Optronics | AUO22EC |              | 1366x768  | 340x190mm  | 15.3 | 2011 | 7234B |
| AU Optronics | AUO22EC | B156XW02 V2  | 1366x768  | 340x190mm  | 15.3 | 2009 | 717E5 |
| AU Optronics | AUO22EC | 1JC2N        | 1366x768  | 340x190mm  | 15.3 | 2009 | 7CB49 |
| AU Optronics | AUO22ED |              | 1920x1080 | 340x190mm  | 15.3 | 2016 | BEFAE |
| AU Optronics | AUO2336 | B140QAN02.3  | 2560x1440 | 310x170mm  | 13.9 | 2017 | E47BA |
| AU Optronics | AUO233C | 9TMDG        | 1366x768  | 310x170mm  | 13.9 | 2012 | 051CF |
| AU Optronics | AUO233C |              | 1366x768  | 310x170mm  | 13.9 | 2012 | D4298 |
| AU Optronics | AUO233E |              | 1600x900  | 310x170mm  | 13.9 | 2012 | 3B4C8 |
| AU Optronics | AUO233E | B140RTN02.3  | 1600x900  | 310x170mm  | 13.9 | 2012 | 840C6 |
| AU Optronics | AUO2344 | B141EW02 V3  | 1280x800  | 300x190mm  | 14.0 |      | 6620D |
| AU Optronics | AUO2351 | JF383        | 1024x768  | 300x230mm  | 14.9 |      | 3D274 |
| AU Optronics | AUO235C |              | 1366x768  | 260x140mm  | 11.6 | 2014 | 32440 |
| AU Optronics | AUO235C | B116XTN02.3  | 1366x768  | 260x140mm  | 11.6 | 2014 | 8EA3E |
| AU Optronics | AUO235C | CGVHX        | 1366x768  | 260x140mm  | 11.6 | 2014 | A6B70 |
| AU Optronics | AUO235C | 33D1K        | 1366x768  | 260x140mm  | 11.6 | 2014 | BBD8D |
| AU Optronics | AUO235C | B116XTN02.3  | 1366x768  | 260x140mm  | 11.6 | 2013 | B40F2 |
| AU Optronics | AUO236D | M1GMV        | 1920x1080 | 280x160mm  | 12.7 | 2016 | E0F19 |
| AU Optronics | AUO2374 | N876G        | 1280x800  | 330x210mm  | 15.4 | 2008 | 9177B |
| AU Optronics | AUO2374 | GR452 '6BJh  | 1280x800  | 330x210mm  | 15.4 | 2006 | CF1E5 |
| AU Optronics | AUO23D2 | B101AW02 V3  | 1024x600  | 220x130mm  | 10.1 | 2009 | 7E5A5 |
| AU Optronics | AUO23EC | B156XTN02.3  | 1366x768  | 340x190mm  | 15.3 | 2011 | 3FE0A |
| AU Optronics | AUO23EC |              | 1366x768  | 340x190mm  | 15.3 | 2011 | 62760 |
| AU Optronics | AUO23EC | 3XJDG        | 1366x768  | 340x190mm  | 15.3 | 2010 | AEFE5 |
| AU Optronics | AUO23EC | B156XW02 V3  | 1366x768  | 340x190mm  | 15.3 | 2009 | AA279 |
| AU Optronics | AUO23ED |              | 1920x1080 | 340x190mm  | 15.3 | 2018 | BC202 |
| AU Optronics | AUO23ED |              | 1920x1080 | 340x190mm  | 15.3 | 2016 | 0139F |
| AU Optronics | AUO243C | B140XTN02.4  | 1366x768  | 310x170mm  | 13.9 | 2012 | 21263 |
| AU Optronics | AUO243C | H3D38        | 1366x768  | 310x170mm  | 13.9 | 2009 | C5670 |
| AU Optronics | AUO243D | B140HAN02.4  | 1920x1080 | 310x170mm  | 13.9 | 2016 | DD7A8 |
| AU Optronics | AUO2451 | B150XG02 V4  | 1024x768  | 300x230mm  | 14.9 |      | FB23F |
| AU Optronics | AUO24EC | B156XTN02.4  | 1366x768  | 340x190mm  | 15.3 | 2011 | ADEC1 |
| AU Optronics | AUO24ED |              | 1920x1080 | 340x190mm  | 15.3 | 2016 | 853F0 |
| AU Optronics | AUO253C |              | 1366x768  | 310x170mm  | 13.9 | 2012 | 38617 |
| AU Optronics | AUO253C | B140XTN02.5  | 1366x768  | 310x170mm  | 13.9 | 2012 | 3EABA |
| AU Optronics | AUO253D | B140HAN02.5  | 1920x1080 | 310x170mm  | 13.9 | 2016 | 85F14 |
| AU Optronics | AUO2574 | B154EW02 V5  | 1280x800  | 330x210mm  | 15.4 | 2006 | 71C83 |
| AU Optronics | AUO25EC | T5CDR        | 1366x768  | 340x190mm  | 15.3 | 2009 | 140F9 |
| AU Optronics | AUO25ED | B156HW02 V5  | 1920x1080 | 340x190mm  | 15.3 | 2010 | 40445 |
| AU Optronics | AUO263D | KJY05        | 1920x1080 | 310x170mm  | 13.9 | 2018 | ACA0F |
| AU Optronics | AUO263D | V8HK9        | 1920x1080 | 310x170mm  | 13.9 | 2017 | B5F15 |
| AU Optronics | AUO2674 | B154EW02 V6  | 1280x800  | 330x210mm  | 15.4 | 2006 | DD81D |
| AU Optronics | AUO26EC | B156XTN02.6  | 1366x768  | 340x190mm  | 15.3 | 2012 | CA2BB |
| AU Optronics | AUO26EC | B156XW02 V6  | 1366x768  | 340x190mm  | 15.3 | 2009 | BF767 |
| AU Optronics | AUO272D | B133HAN02.7  | 1920x1080 | 290x170mm  | 13.2 | 2015 | A402F |
| AU Optronics | AUO275C | B116XAN02.7  | 1366x768  | 260x140mm  | 11.6 | 2014 | 9790A |
| AU Optronics | AUO2774 | B154EW02 V7  | 1280x800  | 330x210mm  | 15.4 | 2007 | 5C2A8 |
| AU Optronics | AUO2774 | B154EW02 V7  | 1280x800  | 330x210mm  | 15.4 | 2006 | A6A26 |
| AU Optronics | AUO293C | B140XTN02.9  | 1366x768  | 310x170mm  | 13.9 | 2013 | 30DAA |
| AU Optronics | AUO2A3C | B140XTN02.A  | 1366x768  | 310x170mm  | 13.9 | 2013 | 50C4B |
| AU Optronics | AUO2A3C | MHFP8        | 1366x768  | 310x170mm  | 13.9 | 2013 | 64B8B |
| AU Optronics | AUO2D3C |              | 1366x768  | 310x170mm  | 13.9 | 2015 | D37BF |
| AU Optronics | AUO2D3C | B140XTN02.D  | 1366x768  | 310x170mm  | 13.9 | 2013 | 45064 |
| AU Optronics | AUO2D3C | 0DCN5        | 1366x768  | 310x170mm  | 13.9 | 2013 | AD041 |
| AU Optronics | AUO2E3C | KFC4D        | 1366x768  | 310x170mm  | 13.9 | 2017 | A2DC3 |
| AU Optronics | AUO2E3C | B140XTN02.E  | 1366x768  | 310x170mm  | 13.9 | 2015 | EE9EC |
| AU Optronics | AUO2E3C |              | 1366x768  | 310x170mm  | 13.9 | 2013 | 388ED |
| AU Optronics | AUO2E3C | 6WM60        | 1366x768  | 310x170mm  | 13.9 | 2013 | 76F53 |
| AU Optronics | AUO2E3C | B140XTN02.E  | 1366x768  | 310x170mm  | 13.9 | 2013 | BE451 |
| AU Optronics | AUO303C | C1JKP        | 1366x768  | 310x170mm  | 13.9 | 2011 | 6B637 |
| AU Optronics | AUO303C |              | 1366x768  | 310x170mm  | 13.9 | 2011 | 754AB |
| AU Optronics | AUO303C | P1NX1        | 1366x768  | 310x170mm  | 13.9 | 2011 | D0F64 |
| AU Optronics | AUO303C | B140XW03 V0  | 1366x768  | 310x170mm  | 13.9 | 2010 | 2D032 |
| AU Optronics | AUO303C | B140XW03 V0  | 1366x768  | 310x170mm  | 13.9 | 2009 | A3873 |
| AU Optronics | AUO303D | B140HAN03.0  | 1920x1080 | 310x170mm  | 13.9 | 2016 | 662F5 |
| AU Optronics | AUO303D | B139HAN03.0  | 1920x1080 | 310x170mm  | 13.9 | 2016 | 9F81D |
| AU Optronics | AUO303E | B140RTN03.0  | 1600x900  | 310x170mm  | 13.9 | 2012 | 10179 |
| AU Optronics | AUO3047 | R777G #2=Fd  | 1440x900  | 300x190mm  | 14.0 | 2007 | 6F2F3 |
| AU Optronics | AUO305C |              | 1366x768  | 260x140mm  | 11.6 | 2012 | 161EA |
| AU Optronics | AUO305C | B116XW03 V0  | 1366x768  | 260x140mm  | 11.6 | 2009 | 75F85 |
| AU Optronics | AUO305C | 2VD2K        | 1366x768  | 260x140mm  | 11.6 | 2009 | 7F723 |
| AU Optronics | AUO305D | B116HAN03.0  | 1920x1080 | 260x140mm  | 11.6 | 2012 | E8203 |
| AU Optronics | AUO30D2 | C050T        | 1024x600  | 220x130mm  | 10.1 | 2009 | 1F11C |
| AU Optronics | AUO30D2 | B101AW03 V0  | 1024x600  | 220x130mm  | 10.1 | 2009 | 4F9D6 |
| AU Optronics | AUO30D2 | B101AW03 V0  | 1024x600  | 220x130mm  | 10.1 | 2008 | 4D64C |
| AU Optronics | AUO30EC | B156XW03 V0  | 1366x768  | 340x190mm  | 15.3 | 2009 | DF645 |
| AU Optronics | AUO30ED | XWN1R        | 1920x1080 | 340x190mm  | 15.3 | 2016 | 9BBA1 |
| AU Optronics | AUO30ED | B156HAN03.0  | 1920x1080 | 340x190mm  | 15.3 | 2013 | 0DCB9 |
| AU Optronics | AUO30ED | B156HTN03.0  | 1920x1080 | 340x190mm  | 15.3 | 2012 | 4EC3F |
| AU Optronics | AUO30ED | 00R4M        | 1920x1080 | 340x190mm  | 15.3 | 2010 | C2C2B |
| AU Optronics | AUO30ED | NCDF3        | 1920x1080 | 340x190mm  | 15.3 | 2010 | CA63D |
| AU Optronics | AUO312C | 9D0GV        | 1366x768  | 290x160mm  | 13.0 | 2012 | B739A |
| AU Optronics | AUO312C |              | 1366x768  | 290x160mm  | 13.0 | 2011 | 4BC65 |
| AU Optronics | AUO312C | B133XW03 V1  | 1366x768  | 290x160mm  | 13.0 | 2011 | B1618 |
| AU Optronics | AUO312C | B133XW03 V1  | 1366x768  | 290x160mm  | 13.0 | 2010 | 10546 |
| AU Optronics | AUO312C | WJH2R        | 1366x768  | 290x160mm  | 13.0 | 2010 | 46E5F |
| AU Optronics | AUO312C | KK736        | 1366x768  | 290x160mm  | 13.0 | 2010 | B2454 |
| AU Optronics | AUO313C | B140XTN03.1  | 1366x768  | 310x170mm  | 13.9 | 2012 | A6534 |
| AU Optronics | AUO313C | HPK92        | 1366x768  | 310x170mm  | 13.9 | 2011 | 14660 |
| AU Optronics | AUO313C |              | 1366x768  | 310x170mm  | 13.9 | 2011 | D426F |
| AU Optronics | AUO313C | B140XW03 V1  | 1366x768  | 310x170mm  | 13.9 | 2010 | 8C113 |
| AU Optronics | AUO313C | B140XTN03.1  | 1366x768  | 310x170mm  | 13.9 | 2010 | BD9AD |
| AU Optronics | AUO313C |              | 1366x768  | 310x170mm  | 13.9 | 2010 | F9E87 |
| AU Optronics | AUO313D | B140HAN03.1  | 1920x1080 | 310x170mm  | 13.9 | 2016 | A8924 |
| AU Optronics | AUO313E |              | 1600x900  | 310x170mm  | 13.9 | 2012 | 6DEFB |
| AU Optronics | AUO313E | T6N3N        | 1600x900  | 310x170mm  | 13.9 | 2010 | 655F2 |
| AU Optronics | AUO313E | B140RW03 V1  | 1600x900  | 310x170mm  | 13.9 | 2010 | A8809 |
| AU Optronics | AUO313E | WJ139        | 1600x900  | 310x170mm  | 13.9 | 2010 | F64F1 |
| AU Optronics | AUO315C | B116XAT03.1  | 1366x768  | 260x140mm  | 11.6 | 2013 | EA347 |
| AU Optronics | AUO315C |              | 1366x768  | 260x140mm  | 11.6 | 2011 | DD551 |
| AU Optronics | AUO315C | B116XW03 V1  | 1366x768  | 260x140mm  | 11.6 | 2010 | 54063 |
| AU Optronics | AUO319D | B173HAN03.1  | 1920x1080 | 380x210mm  | 17.1 | 2017 | 5FFB9 |
| AU Optronics | AUO31D2 | B101AW03 V1  | 1024x600  | 220x130mm  | 10.1 | 2008 | 8374C |
| AU Optronics | AUO31EC | B156XTN03.1  | 1366x768  | 340x190mm  | 15.3 | 2012 | 1C432 |
| AU Optronics | AUO31EC | B156XTN03.1  | 1366x768  | 340x190mm  | 15.3 | 2011 | FB88E |
| AU Optronics | AUO31EC | B156XW03 V1  | 1366x768  | 340x190mm  | 15.3 | 2009 | 0D843 |
| AU Optronics | AUO31ED | B156HTN03.1  | 1920x1080 | 340x190mm  | 15.3 | 2012 | 5AF05 |
| AU Optronics | AUO3214 | JF298 +:FPp  | 1280x800  | 260x160mm  | 12.0 | 2006 | C0B55 |
| AU Optronics | AUO3214 | B121EW03 V2  | 1280x800  | 260x160mm  | 12.0 | 2006 | D94BA |
| AU Optronics | AUO322C | B133XW03 V2  | 1366x768  | 290x160mm  | 13.0 | 2009 | 347DE |
| AU Optronics | AUO323C | B140XTN03.2  | 1366x768  | 310x170mm  | 13.9 | 2013 | 12846 |
| AU Optronics | AUO323C | 71MRM        | 1366x768  | 310x170mm  | 13.9 | 2013 | 1D9C4 |
| AU Optronics | AUO323C |              | 1366x768  | 310x170mm  | 13.9 | 2013 | 666A8 |
| AU Optronics | AUO323D | B140HAN03.2  | 1920x1080 | 310x170mm  | 13.9 | 2018 | C9D4D |
| AU Optronics | AUO325C | B116XAN03.2  | 1366x768  | 260x140mm  | 11.6 | 2012 | C0554 |
| AU Optronics | AUO325C | B116XW03 V2  | 1366x768  | 260x140mm  | 11.6 | 2011 | 672AE |
| AU Optronics | AUO325D | AUO^ B116... | 1920x1080 | 260x140mm  | 11.6 | 2012 | 03805 |
| AU Optronics | AUO32EB | B156ZAN03.2  | 3840x2160 | 340x190mm  | 15.3 | 2017 | 04E97 |
| AU Optronics | AUO32EC | JGP6V        | 1366x768  | 340x190mm  | 15.3 | 2012 | 089C9 |
| AU Optronics | AUO32EC | B156XTN03.2  | 1366x768  | 340x190mm  | 15.3 | 2012 | 55918 |
| AU Optronics | AUO32EC |              | 1366x768  | 340x190mm  | 15.3 | 2011 | 816A4 |
| AU Optronics | AUO32EC | B156XTN03.2  | 1366x768  | 340x190mm  | 15.3 | 2011 | 9556C |
| AU Optronics | AUO32EC | B156XW03 V2  | 1366x768  | 340x190mm  | 15.3 | 2009 | CAA06 |
| AU Optronics | AUO32ED |              | 1920x1080 | 340x190mm  | 15.3 | 2012 | E3BC0 |
| AU Optronics | AUO3314 | B121EW03 V3  | 1280x800  | 260x160mm  | 12.0 | 2006 | 0DA33 |
| AU Optronics | AUO332C | B133XW03 V3  | 1366x768  | 290x160mm  | 13.0 | 2009 | E295C |
| AU Optronics | AUO333C | B140XTN03.3  | 1366x768  | 310x170mm  | 13.9 | 2013 | 9835B |
| AU Optronics | AUO33ED |              | 1920x1080 | 340x190mm  | 15.3 | 2012 | 7DF76 |
| AU Optronics | AUO342C | B133XW03 V4  | 1366x768  | 290x160mm  | 13.0 | 2010 | 09CEB |
| AU Optronics | AUO343C | B140XTN03.4  | 1366x768  | 310x170mm  | 13.9 | 2012 | B05C3 |
| AU Optronics | AUO3487 | B170PW03 V4  | 1440x900  | 370x230mm  | 17.2 |      | 9A460 |
| AU Optronics | AUO34EB | XWHYC        | 3840x2160 | 340x190mm  | 15.3 | 2018 | B4DAE |
| AU Optronics | AUO34ED |              | 1920x1080 | 340x190mm  | 15.3 | 2012 | 16FA1 |
| AU Optronics | AUO34ED | B156HTN03.4  | 1920x1080 | 340x190mm  | 15.3 | 2012 | 51168 |
| AU Optronics | AUO352C | B133XW03 V5  | 1366x768  | 290x160mm  | 13.0 | 2010 | 00E33 |
| AU Optronics | AUO353D | B140HAN03.5  | 1920x1080 | 310x170mm  | 13.9 | 2015 | 8A199 |
| AU Optronics | AUO35EC | B156XTN03.5  | 1366x768  | 340x190mm  | 15.3 | 2013 | 0C373 |
| AU Optronics | AUO35EC | B156XTN03.5  | 1366x768  | 340x190mm  | 15.3 | 2012 | 59776 |
| AU Optronics | AUO35ED |              | 1920x1080 | 340x190mm  | 15.3 | 2013 | 73684 |
| AU Optronics | AUO363C |              | 1366x768  | 310x170mm  | 13.9 | 2013 | F12E4 |
| AU Optronics | AUO363D | B140HAN03.6  | 1920x1080 | 310x170mm  | 13.9 | 2016 | D5996 |
| AU Optronics | AUO36ED | B156HTN03.6  | 1920x1080 | 340x190mm  | 15.3 | 2013 | 751F4 |
| AU Optronics | AUO3714 | B121EW03 V7  | 1280x800  | 260x160mm  | 12.0 | 2007 | 60F55 |
| AU Optronics | AUO3787 | WR542 ):HQr  | 1440x900  | 370x230mm  | 17.2 | 2007 | 72F23 |
| AU Optronics | AUO3787 | U816G ):HQr  | 1440x900  | 370x230mm  | 17.2 | 2007 | 7B8DF |
| AU Optronics | AUO37ED | B156HTN03.7  | 1920x1080 | 340x190mm  | 15.3 | 2013 | 51009 |
| AU Optronics | AUO38ED |              | 1920x1080 | 340x190mm  | 15.3 | 2017 | B894A |
| AU Optronics | AUO38ED |              | 1920x1080 | 340x190mm  | 15.3 | 2016 | 074E4 |
| AU Optronics | AUO38ED |              | 1920x1080 | 340x190mm  | 15.3 | 2015 | 0BEE2 |
| AU Optronics | AUO38ED | B156HTN03.8  | 1920x1080 | 340x190mm  | 15.3 | 2015 | 1D5C5 |
| AU Optronics | AUO38ED | 28H80        | 1920x1080 | 340x190mm  | 15.3 | 2015 | 5B571 |
| AU Optronics | AUO38ED | B156HTN03.8  | 1920x1080 | 340x190mm  | 15.3 | 2014 | 571ED |
| AU Optronics | AUO3914 | B121EW03 V9  | 1280x800  | 260x160mm  | 12.0 | 2007 | 01BBB |
| AU Optronics | AUO39ED | B156HTN03.9  | 1920x1080 | 340x190mm  | 15.3 | 2014 | C56C2 |
| AU Optronics | AUO3B44 | GM521 '5AHf  | 1280x800  | 300x190mm  | 14.0 | 2006 | 6317A |
| AU Optronics | AUO403D | B140HAN04.0  | 1920x1080 | 310x170mm  | 13.9 | 2017 | F53E5 |
| AU Optronics | AUO403D | B140HAN04.0  | 1920x1080 | 310x170mm  | 13.9 | 2016 | 2859A |
| AU Optronics | AUO403D |              | 1920x1080 | 310x170mm  | 13.9 | 2016 | A7AE2 |
| AU Optronics | AUO4047 | GX968        | 1440x900  | 300x190mm  | 14.0 | 2007 | BF809 |
| AU Optronics | AUO405C |              | 1366x768  | 260x140mm  | 11.6 | 2015 | 249F9 |
| AU Optronics | AUO405C | B116XAN04.0  | 1366x768  | 260x140mm  | 11.6 | 2015 | 85FBF |
| AU Optronics | AUO405C | B116XTN04.0  | 1366x768  | 260x140mm  | 11.6 | 2012 | A1707 |
| AU Optronics | AUO40EC | W64C6        | 1366x768  | 340x190mm  | 15.3 | 2013 | 23540 |
| AU Optronics | AUO40EC | B156XTN04.0  | 1366x768  | 340x190mm  | 15.3 | 2013 | 8990F |
| AU Optronics | AUO40EC | B156XW04 V0  | 1366x768  | 340x190mm  | 15.3 | 2009 | 0024E |
| AU Optronics | AUO4100 | AUO^ B101... | 1920x1200 | 220x140mm  | 10.3 | 2013 | 2EDA3 |
| AU Optronics | AUO412C | B133XW04 V1  | 1366x768  | 290x160mm  | 13.0 | 2010 | 42863 |
| AU Optronics | AUO413D |              | 1920x1080 | 310x170mm  | 13.9 | 2016 | CAA8E |
| AU Optronics | AUO4147 | DV5J1        | 1440x900  | 300x190mm  | 14.0 | 2009 | 53A6B |
| AU Optronics | AUO4147 | CCXMW        | 1440x900  | 300x190mm  | 14.0 | 2009 | 9E757 |
| AU Optronics | AUO41EC | B156XTN04.1  | 1366x768  | 340x190mm  | 15.3 | 2013 | B1B6A |
| AU Optronics | AUO41EC |              | 1366x768  | 340x190mm  | 15.3 | 2013 | B598E |
| AU Optronics | AUO41EC | B156XW04 V1  | 1366x768  | 340x190mm  | 15.3 | 2009 | BCD92 |
| AU Optronics | AUO41ED |              | 1920x1080 | 340x190mm  | 15.3 | 2015 | 4B876 |
| AU Optronics | AUO4214 | KX774        | 1280x800  | 260x160mm  | 12.0 | 2007 | 7EF0D |
| AU Optronics | AUO422D |              | 1920x1080 | 290x170mm  | 13.2 | 2016 | 4228E |
| AU Optronics | AUO423D | B140HAN04.2  | 1920x1080 | 310x170mm  | 13.9 | 2016 | 1B27A |
| AU Optronics | AUO4277 | WP576        | 1440x900  | 330x210mm  | 15.4 | 2007 | 41986 |
| AU Optronics | AUO42EC | B156XTN04.2  | 1366x768  | 340x190mm  | 15.3 | 2013 | D9F6D |
| AU Optronics | AUO42EC |              | 1366x768  | 340x190mm  | 15.3 | 2013 | E5006 |
| AU Optronics | AUO4344 | B141EW04 V3  | 1280x800  | 300x190mm  | 14.0 | 2006 | 49F00 |
| AU Optronics | AUO43EC | B156XTN04.3  | 1366x768  | 340x190mm  | 15.3 | 2013 | 94001 |
| AU Optronics | AUO43EC |              | 1366x768  | 340x190mm  | 15.3 | 2013 | D7021 |
| AU Optronics | AUO442D | B133HAN04.4  | 1920x1080 | 290x170mm  | 13.2 | 2016 | 91761 |
| AU Optronics | AUO4444 | B141EW04 V4  | 1280x800  | 300x190mm  | 14.0 | 2006 | 2D213 |
| AU Optronics | AUO44EC | B156XTN04.4  | 1366x768  | 340x190mm  | 15.3 | 2013 | 00BFA |
| AU Optronics | AUO4544 | WP948        | 1280x800  | 300x190mm  | 14.0 | 2008 | 35BF3 |
| AU Optronics | AUO4544 | B141EW04 V5  | 1280x800  | 300x190mm  | 14.0 | 2007 | 6A593 |
| AU Optronics | AUO4544 | TK033 2GV]   | 1280x800  | 300x190mm  | 14.0 | 2006 | A5A99 |
| AU Optronics | AUO45EC | 3XJ36        | 1366x768  | 340x190mm  | 15.3 | 2015 | 8923E |
| AU Optronics | AUO45EC | B156XTN04.5  | 1366x768  | 340x190mm  | 15.3 | 2015 | A1FFC |
| AU Optronics | AUO45EC |              | 1366x768  | 340x190mm  | 15.3 | 2011 | 17E34 |
| AU Optronics | AUO45EC | B156XW04 V5  | 1366x768  | 340x190mm  | 15.3 | 2011 | 2377B |
| AU Optronics | AUO45EC | M4TK3        | 1366x768  | 340x190mm  | 15.3 | 2011 | F49C5 |
| AU Optronics | AUO45EC | B156XW04 V5  | 1366x768  | 340x190mm  | 15.3 | 2010 | 871DF |
| AU Optronics | AUO45ED | B156HAN04.5  | 1920x1080 | 340x190mm  | 15.3 | 2017 | 4DF73 |
| AU Optronics | AUO462D | F7VDJ        | 1920x1080 | 290x170mm  | 13.2 | 2016 | 00F59 |
| AU Optronics | AUO46EC | B156XTN04.6  | 1366x768  | 340x190mm  | 15.3 | 2015 | D6C67 |
| AU Optronics | AUO46EC |              | 1366x768  | 340x190mm  | 15.3 | 2013 | EFCE1 |
| AU Optronics | AUO46EC | B156XW04 V6  | 1366x768  | 340x190mm  | 15.3 | 2011 | 7DEB8 |
| AU Optronics | AUO46EC |              | 1366x768  | 340x190mm  | 15.3 | 2011 | A43C2 |
| AU Optronics | AUO46EC | 2F9KX        | 1366x768  | 340x190mm  | 15.3 | 2011 | FE5F4 |
| AU Optronics | AUO46EC | B156XW04 V6  | 1366x768  | 340x190mm  | 15.3 | 2010 | AD117 |
| AU Optronics | AUO47EC | B156XW004.7  | 1366x768  | 340x190mm  | 15.3 | 2013 | 2B5AB |
| AU Optronics | AUO47EC |              | 1366x768  | 340x190mm  | 15.3 | 2012 | D57E3 |
| AU Optronics | AUO48EC | B156XW004.8  | 1366x768  | 340x190mm  | 15.3 | 2013 | 0F0F2 |
| AU Optronics | AUO492D | B133HAN04.9  | 1920x1080 | 290x170mm  | 13.2 | 2017 | 1643A |
| AU Optronics | AUO5024 | FM736        | 1280x800  | 290x180mm  | 13.4 | 2008 | E8B51 |
| AU Optronics | AUO5024 | B133EW05 V0  | 1280x800  | 290x180mm  | 13.4 | 2008 | F1210 |
| AU Optronics | AUO502D | RN5TT        | 1920x1080 | 290x160mm  | 13.0 | 2017 | DB55D |
| AU Optronics | AUO512D | B133HAN05.1  | 1920x1080 | 290x170mm  | 13.2 | 2018 | 451AA |
| AU Optronics | AUO52ED | B156HTN05.2  | 1920x1080 | 340x190mm  | 15.3 | 2016 | 42B4D |
| AU Optronics | AUO5344 | C384H        | 1280x800  | 300x190mm  | 14.0 | 2008 | 1F532 |
| AU Optronics | AUO53D4 | B101EW05 V3  | 1280x800  | 220x140mm  | 10.3 | 2010 | CF3C3 |
| AU Optronics | AUO552D |              | 1920x1080 | 290x170mm  | 13.2 | 2017 | 6360D |
| AU Optronics | AUO5544 | 44P64        | 1280x800  | 300x190mm  | 14.0 | 2010 | 48704 |
| AU Optronics | AUO5544 | 2H7P2        | 1280x800  | 300x190mm  | 14.0 | 2010 | 73394 |
| AU Optronics | AUO5A41 | J5596        | 1024x768  | 290x210mm  | 14.1 |      | 53FF6 |
| AU Optronics | AUO5B2D | 06VG6        | 1920x1080 | 290x160mm  | 13.0 | 2018 | DF391 |
| AU Optronics | AUO60D2 | B101AW06 V0  | 1024x600  | 220x130mm  | 10.1 | 2009 | 48256 |
| AU Optronics | AUO60ED | JR8P3        | 1920x1080 | 340x190mm  | 15.3 | 2016 | D398A |
| AU Optronics | AUO61D2 | B101AW06 V1  | 1024x600  | 220x130mm  | 10.1 | 2009 | 84528 |
| AU Optronics | AUO61ED | B156HTN06.1  | 1920x1080 | 340x190mm  | 15.3 | 2018 | 6F380 |
| AU Optronics | AUO61ED | B156HAN06.1  | 1920x1080 | 340x190mm  | 15.3 | 2016 | 6AF32 |
| AU Optronics | AUO6287 | B170PW06 V2  | 1440x900  | 370x230mm  | 17.2 | 2007 | 91F8D |
| AU Optronics | AUO6387 | B170PW06 V3  | 1440x900  | 370x230mm  | 17.2 | 2007 | 8EABD |
| AU Optronics | AUO63ED | CV56F        | 1920x1080 | 340x190mm  | 15.3 | 2017 | 5255C |
| AU Optronics | AUO64D2 | B101AW06 V4  | 1024x600  | 220x130mm  | 10.1 | 2011 | AF726 |
| AU Optronics | AUO70EC |              | 1366x768  | 340x190mm  | 15.3 | 2017 | EDA26 |
| AU Optronics | AUO70EC | FMT2C        | 1366x768  | 340x190mm  | 15.3 | 2016 | EF997 |
| AU Optronics | AUO70EC | B156XTN07.0  | 1366x768  | 340x190mm  | 15.3 | 2015 | 05E4F |
| AU Optronics | AUO70EC |              | 1366x768  | 340x190mm  | 15.3 | 2015 | 6B150 |
| AU Optronics | AUO70ED | B156HAN07.0  | 1920x1080 | 340x190mm  | 15.3 | 2017 | 83F24 |
| AU Optronics | AUO71EC | HRN6M        | 1366x768  | 340x190mm  | 15.3 | 2016 | 401DB |
| AU Optronics | AUO71EC | K30M5        | 1366x768  | 340x190mm  | 15.3 | 2016 | B0F4C |
| AU Optronics | AUO71EC | B156XTN07.1  | 1366x768  | 340x190mm  | 15.3 | 2016 | EA235 |
| AU Optronics | AUO71EC | 91MGD        | 1366x768  | 340x190mm  | 15.3 | 2015 | 95873 |
| AU Optronics | AUO71EC |              | 1366x768  | 340x190mm  | 15.3 | 2015 | 9F982 |
| AU Optronics | AUO71EC | B156XTN07.1  | 1366x768  | 340x190mm  | 15.3 | 2015 | EF315 |
| AU Optronics | AUO71ED | B156HAN07.1  | 1920x1080 | 340x190mm  | 15.3 | 2017 | CAEAC |
| AU Optronics | AUO723C |              | 1366x768  | 310x170mm  | 13.9 | 2017 | 6CE31 |
| AU Optronics | AUO8074 | U448H        | 1280x800  | 330x210mm  | 15.4 | 2008 | 50C56 |
| AU Optronics | AUO8074 | B154EW08 V0  | 1280x800  | 330x210mm  | 15.4 | 2007 | 9DD43 |
| AU Optronics | AUO8074 | B154EW08 V0  | 1280x800  | 330x210mm  | 15.4 | 2006 | 23653 |
| AU Optronics | AUO80ED | K055G        | 1920x1080 | 340x190mm  | 15.3 | 2018 | 894B6 |
| AU Optronics | AUO80ED | B156HAN08.0  | 1920x1080 | 340x190mm  | 15.3 | 2017 | 9286E |
| AU Optronics | AUO8174 | KM307        | 1280x800  | 330x210mm  | 15.4 | 2007 | 040E4 |
| AU Optronics | AUO8174 | B154EW08 V1  | 1280x800  | 330x210mm  | 15.4 | 2007 | A096D |
| AU Optronics | AUO8174 | B154EW08 V1  | 1280x800  | 330x210mm  | 15.4 | 2006 | 4F522 |
| AU Optronics | AUO81EC |              | 1366x768  | 340x190mm  | 15.3 | 2017 | 63E70 |
| AU Optronics | AUO82ED | B156HAN08.2  | 1920x1080 | 340x190mm  | 15.3 | 2018 | E31ED |
| AU Optronics | AUO9074 | B154EW09 V0  | 1280x800  | 330x210mm  | 15.4 | 2008 | 8C572 |
| AU Optronics | AUO9214 | B121EW09 V2  | 1280x800  | 260x160mm  | 12.0 | 2008 | 93444 |
| AU Optronics | AUO9414 |              | 1280x800  | 260x160mm  | 12.0 | 2009 | 6EC69 |
| AU Optronics | AUO9514 | B121EW09 V5  | 1280x800  | 260x160mm  | 12.0 | 2009 | E4834 |
| AU Optronics | AUOA03C | B116XW05 ... | 1366x768  | 260x140mm  | 11.6 | 2012 | 17837 |
| AU Optronics | AUOA114 | F325F        | 1280x800  | 260x160mm  | 12.0 | 2008 | AA921 |
| Acer         | ACR0000 | X243W        | 1920x1200 | 520x320mm  | 24.0 | 2008 | 0839E |
| Acer         | ACR0000 | X243W        | 1920x1200 | 520x320mm  | 24.0 | 2007 | 98CE0 |
| Acer         | ACR0008 | X203W        | 1680x1050 | 430x270mm  | 20.0 | 2008 | 5374B |
| Acer         | ACR0009 | X223W        | 1680x1050 | 470x300mm  | 22.0 | 2009 | 2928F |
| Acer         | ACR0009 | X223W        | 1680x1050 | 470x300mm  | 22.0 | 2008 | EAF2C |
| Acer         | ACR000C | P193WA       | 1440x900  | 410x260mm  | 19.1 | 2008 | 3D7A7 |
| Acer         | ACR000C | P193W        | 1440x900  | 410x260mm  | 19.1 | 2008 | FD26C |
| Acer         | ACR000C | P193WA       | 1440x900  | 410x260mm  | 19.1 | 2007 | B0377 |
| Acer         | ACR000D | X223W        | 1680x1050 | 470x300mm  | 22.0 | 2009 | C3B12 |
| Acer         | ACR000D | X223W        | 1680x1050 | 470x300mm  | 22.0 | 2008 | 41C5D |
| Acer         | ACR0012 | P221W        | 1680x1050 | 470x300mm  | 22.0 | 2008 | 0F62A |
| Acer         | ACR0016 | P223W        | 1680x1050 | 470x300mm  | 22.0 | 2008 | CB3B1 |
| Acer         | ACR0017 | G225HQ       | 1920x1080 | 480x270mm  | 21.7 | 2010 | AB6D6 |
| Acer         | ACR0018 | G185H        | 1366x768  | 400x250mm  | 18.6 | 2012 | 7EA6F |
| Acer         | ACR001B | V223W        | 1680x1050 | 470x300mm  | 22.0 | 2009 | F6BF8 |
| Acer         | ACR001D | B193         | 1280x1024 | 380x300mm  | 19.1 | 2012 | 98417 |
| Acer         | ACR001D | B193         | 1280x1024 | 380x300mm  | 19.1 | 2011 | 1DC47 |
| Acer         | ACR001D | B193         | 1280x1024 | 380x300mm  | 19.1 | 2008 | 52EF6 |
| Acer         | ACR001E | B193W        | 1440x900  | 400x250mm  | 18.6 | 2008 | 73B31 |
| Acer         | ACR0020 | B223W        | 1680x1050 | 470x300mm  | 22.0 | 2008 | 40C76 |
| Acer         | ACR0021 | B243W        | 1920x1200 | 570x370mm  | 26.8 | 2008 | 81A54 |
| Acer         | ACR0024 | V193         | 1280x1024 | 380x300mm  | 19.1 | 2011 | 64C48 |
| Acer         | ACR0024 | V193         | 1280x1024 | 380x300mm  | 19.1 | 2010 | 1473E |
| Acer         | ACR0025 | V193W        | 1440x900  | 400x250mm  | 18.6 | 2012 | B066F |
| Acer         | ACR0027 | V223W        | 1680x1050 | 470x300mm  | 22.0 | 2011 | 9FD74 |
| Acer         | ACR0027 | V223W        | 1680x1050 | 470x300mm  | 22.0 | 2008 | 03733 |
| Acer         | ACR0033 | X213W        | 1680x1050 | 470x300mm  | 22.0 | 2008 | FEB0F |
| Acer         | ACR004C | V193         | 1280x1024 | 380x300mm  | 19.1 | 2010 | 510E7 |
| Acer         | ACR004C | V193         | 1280x1024 | 380x300mm  | 19.1 | 2009 | B9B51 |
| Acer         | ACR0050 | X223W        | 1680x1050 | 470x300mm  | 22.0 | 2009 | 4A218 |
| Acer         | ACR005C | G24          | 1920x1200 | 520x320mm  | 24.0 | 2008 | A3BD3 |
| Acer         | ACR005E | P244W        | 1920x1080 | 530x300mm  | 24.0 | 2008 | 15303 |
| Acer         | ACR0064 | X193HQ       | 1366x768  | 400x250mm  | 18.6 | 2009 | 2B041 |
| Acer         | ACR0070 | V223HQ       | 1920x1080 | 470x270mm  | 21.3 | 2010 | C40C9 |
| Acer         | ACR0070 | V223HQ       | 1920x1080 | 470x270mm  | 21.3 | 2009 | DF319 |
| Acer         | ACR0073 | X203H        | 1600x900  | 440x250mm  | 19.9 | 2009 | B918B |
| Acer         | ACR0074 | H243H        | 1920x1080 | 530x290mm  | 23.8 | 2010 | 2DC1B |
| Acer         | ACR0074 | H243H        | 1920x1080 | 530x290mm  | 23.8 | 2009 | CCECC |
| Acer         | ACR0082 | H213H        | 1920x1080 | 480x270mm  | 21.7 | 2009 | 1454E |
| Acer         | ACR0083 | H223HQ       | 1920x1080 | 480x270mm  | 21.7 | 2009 | 22B36 |
| Acer         | ACR0087 | H213H        | 1920x1080 | 480x270mm  | 21.7 | 2008 | AEDF2 |
| Acer         | ACR0090 | V233H        | 1920x1080 | 510x290mm  | 23.1 | 2010 | 45648 |
| Acer         | ACR0095 | V203H        | 1600x900  | 440x250mm  | 19.9 | 2009 | F315F |
| Acer         | ACR0097 | X203H        | 1600x900  | 440x250mm  | 19.9 | 2009 | AF82D |
| Acer         | ACR0098 | X223HQ       | 1920x1080 | 470x270mm  | 21.3 | 2010 | F48BF |
| Acer         | ACR0098 | X213H        | 1920x1080 | 470x270mm  | 21.3 | 2009 | 3A41E |
| Acer         | ACR0098 | X223HQ       | 1920x1080 | 470x270mm  | 21.3 | 2009 | 46165 |
| Acer         | ACR009A | X233H        | 1920x1080 | 510x290mm  | 23.1 | 2010 | 29F1C |
| Acer         | ACR009A | X233H        | 1920x1080 | 510x290mm  | 23.1 | 2008 | 30252 |
| Acer         | ACR009B | X243H        | 1920x1080 | 530x290mm  | 23.8 | 2009 | D7BAE |
| Acer         | ACR009C | X233H        | 1920x1080 | 440x250mm  | 19.9 | 2009 | EC79F |
| Acer         | ACR009C | X233H        | 1920x1080 | 440x250mm  | 19.9 | 2008 | 805B7 |
| Acer         | ACR00A0 | H233H        | 1920x1080 | 510x290mm  | 23.1 | 2009 | 2D92F |
| Acer         | ACR00A3 | V243H        | 1920x1080 | 530x290mm  | 23.8 | 2010 | 5B84A |
| Acer         | ACR00A8 | X233H        | 1920x1080 | 510x290mm  | 23.1 | 2009 | 15714 |
| Acer         | ACR00AC | X243HQ       | 1920x1080 | 520x290mm  | 23.4 | 2010 | 152E3 |
| Acer         | ACR00AC | X243HQ       | 1920x1080 | 520x290mm  | 23.4 | 2009 | 53EC3 |
| Acer         | ACR00B0 | V243HQ       | 1920x1080 | 520x290mm  | 23.4 | 2012 | 93484 |
| Acer         | ACR00B0 | V243HQ       | 1920x1080 | 520x290mm  | 23.4 | 2011 | 2CA58 |
| Acer         | ACR00B0 | V243HQ       | 1920x1080 | 520x290mm  | 23.4 | 2009 | B618E |
| Acer         | ACR00BE | V243HL       | 1920x1080 | 530x290mm  | 23.8 | 2011 | D4C59 |
| Acer         | ACR00BE | V243HL       | 1920x1080 | 530x290mm  | 23.8 | 2010 | 64FD2 |
| Acer         | ACR00BE | V243HL       | 1920x1080 | 530x290mm  | 23.8 | 2009 | 87926 |
| Acer         | ACR00C5 | P205H        | 1600x900  | 440x250mm  | 19.9 | 2009 | BB2E7 |
| Acer         | ACR00C8 | V233H        | 1920x1080 | 510x290mm  | 23.1 | 2009 | 8462F |
| Acer         | ACR00DB | S273HL       | 1920x1080 | 600x340mm  | 27.2 | 2011 | B97C1 |
| Acer         | ACR00DB | V233H        | 1920x1080 | 510x290mm  | 23.1 | 2011 | C66EE |
| Acer         | ACR00DB | V233H        | 1920x1080 | 510x290mm  | 23.1 | 2010 | 01532 |
| Acer         | ACR00DB | S273HL       | 1920x1080 | 600x340mm  | 27.2 | 2010 | 8E0CD |
| Acer         | ACR00DC | V243H        | 1920x1080 | 530x300mm  | 24.0 | 2011 | 4E5B4 |
| Acer         | ACR00DC | V243H        | 1920x1080 | 530x300mm  | 24.0 | 2010 | C9AF5 |
| Acer         | ACR00DC | V243H        | 1920x1080 | 530x300mm  | 24.0 | 2009 | 5CE9D |
| Acer         | ACR00E6 | P225HQ       | 1920x1080 | 480x270mm  | 21.7 | 2010 | 73672 |
| Acer         | ACR00EF | P225HQ       | 1920x1080 | 480x270mm  | 21.7 | 2010 | 6FCC4 |
| Acer         | ACR00F7 | V193         | 1280x1024 | 380x310mm  | 19.3 | 2011 | EDE49 |
| Acer         | ACR0100 | AIO LCD      | 1920x1080 | 470x260mm  | 21.1 | 2010 | 5C6DF |
| Acer         | ACR0101 | AIO LCD      | 1920x1080 | 520x290mm  | 23.4 | 2010 | 2F667 |
| Acer         | ACR0101 | E211H        | 1920x1080 | 480x270mm  | 21.7 | 2009 | 943A1 |
| Acer         | ACR0102 | V203H        | 1600x900  | 440x250mm  | 19.9 | 2010 | 0E322 |
| Acer         | ACR0104 | V223HQ       | 1920x1080 | 480x290mm  | 22.1 | 2010 | CC1F3 |
| Acer         | ACR0113 | G235H        | 1920x1080 | 510x290mm  | 23.1 | 2010 | 1C2E5 |
| Acer         | ACR0114 | G245H        | 1920x1080 | 530x300mm  | 24.0 | 2011 | 36D1D |
| Acer         | ACR0114 | G245H        | 1920x1080 | 530x300mm  | 24.0 | 2010 | AA5F0 |
| Acer         | ACR0116 | G205H        | 1600x900  | 440x250mm  | 19.9 | 2009 | 0BE0C |
| Acer         | ACR0125 | GD245HQ      | 1920x1080 | 520x290mm  | 23.4 | 2010 | 06DBA |
| Acer         | ACR0126 | GD245HQ      | 1920x1080 | 520x290mm  | 23.4 | 2010 | 78A7E |
| Acer         | ACR012B | G245HQ       | 1920x1080 | 520x290mm  | 23.4 | 2011 | 378AE |
| Acer         | ACR012B | G245HQ       | 1920x1080 | 520x290mm  | 23.4 | 2010 | 21AE3 |
| Acer         | ACR013E | V193HQV      | 1366x768  | 410x230mm  | 18.5 | 2010 | 66BCC |
| Acer         | ACR0172 | H225HQL      | 1920x1080 | 480x270mm  | 21.7 | 2010 | 65040 |
| Acer         | ACR0179 | V233HL       | 1920x1080 | 510x290mm  | 23.1 | 2012 | 67BDF |
| Acer         | ACR0180 | H234H        | 1920x1080 | 530x290mm  | 23.8 | 2011 | 01977 |
| Acer         | ACR0196 | P226HQ       | 1920x1080 | 480x270mm  | 21.7 | 2010 | C3AA1 |
| Acer         | ACR0198 | P226HQL      | 1920x1080 | 480x270mm  | 21.7 | 2011 | 6E866 |
| Acer         | ACR0198 | P226HQL      | 1920x1080 | 480x270mm  | 21.7 | 2010 | C922B |
| Acer         | ACR01A2 | P236H        | 1920x1080 | 510x290mm  | 23.1 | 2010 | 2E532 |
| Acer         | ACR01A3 | A231H        | 1920x1080 | 510x290mm  | 23.1 | 2010 | 199A4 |
| Acer         | ACR01A5 | S201HL       | 1600x900  | 440x250mm  | 19.9 | 2011 | A382B |
| Acer         | ACR01A6 | S231HL       | 1920x1080 | 510x290mm  | 23.1 | 2012 | 127CD |
| Acer         | ACR01A6 | S231HL       | 1920x1080 | 510x290mm  | 23.1 | 2011 | 54793 |
| Acer         | ACR01A6 | S231HL       | 1920x1080 | 510x290mm  | 23.1 | 2010 | B2CDB |
| Acer         | ACR01A9 | S221HQL      | 1920x1080 | 480x270mm  | 21.7 | 2011 | 30326 |
| Acer         | ACR01A9 | S221HQL      | 1920x1080 | 480x270mm  | 21.7 | 2010 | FB0A3 |
| Acer         | ACR01AA | S211HL       | 1920x1080 | 480x270mm  | 21.7 | 2011 | 577FD |
| Acer         | ACR01B3 | A221HQL      | 1920x1080 | 480x270mm  | 21.7 | 2011 | 29F3B |
| Acer         | ACR01D0 | LTM230HT01   | 1920x1080 | 510x290mm  | 23.1 | 2009 | C521A |
| Acer         | ACR01DF | P196HQV      | 1366x768  | 410x230mm  | 18.5 | 2010 | 30982 |
| Acer         | ACR01FB | P236H        | 1920x1080 | 510x290mm  | 23.1 | 2011 | 996EB |
| Acer         | ACR01FB | P236H        | 1920x1080 | 510x290mm  | 23.1 | 2010 | 033F2 |
| Acer         | ACR01FC | A231H        | 1920x1080 | 510x290mm  | 23.1 | 2011 | E8BEF |
| Acer         | ACR0203 | S232HL       | 1920x1080 | 510x290mm  | 23.1 | 2010 | DD530 |
| Acer         | ACR0204 | S222HQL      | 1920x1080 | 480x270mm  | 21.7 | 2010 | 22028 |
| Acer         | ACR020B | S202HL       | 1600x900  | 440x250mm  | 19.9 | 2011 | 18597 |
| Acer         | ACR0216 | S242HL       | 1920x1080 | 530x300mm  | 24.0 | 2015 | 1D28F |
| Acer         | ACR0216 | S242HL       | 1920x1080 | 530x300mm  | 24.0 | 2012 | 47D65 |
| Acer         | ACR0216 | S242HL       | 1920x1080 | 530x300mm  | 24.0 | 2011 | 060C1 |
| Acer         | ACR0216 | S242HL       | 1920x1080 | 530x300mm  | 24.0 | 2010 | 8FF3C |
| Acer         | ACR0228 | G225HQV      | 1920x1080 | 480x270mm  | 21.7 | 2010 | E1C64 |
| Acer         | ACR022A | V223WL       | 1680x1050 | 470x300mm  | 22.0 | 2012 | EF3C2 |
| Acer         | ACR0230 | M230HDL      | 1920x1080 | 510x290mm  | 23.1 | 2011 | A33DD |
| Acer         | ACR0234 | G225HQV      | 1920x1080 | 480x270mm  | 21.7 | 2010 | 296D1 |
| Acer         | ACR0239 | V193L        | 1280x1024 | 380x300mm  | 19.1 | 2013 | DE9E6 |
| Acer         | ACR023B | V193WL       | 1440x900  | 410x260mm  | 19.1 | 2012 | 8E941 |
| Acer         | ACR023F | P246HL       | 1920x1080 | 530x300mm  | 24.0 | 2012 | 9604C |
| Acer         | ACR0246 | V223HQL      | 1920x1080 | 480x270mm  | 21.7 | 2012 | AA553 |
| Acer         | ACR024C | GN245HQ      | 1920x1080 | 520x290mm  | 23.4 | 2011 | 21E07 |
| Acer         | ACR025D | V223HQV      | 1920x1080 | 480x290mm  | 22.1 | 2012 | 168A8 |
| Acer         | ACR025D | V223HQV      | 1920x1080 | 480x270mm  | 21.7 | 2012 | 6564C |
| Acer         | ACR0280 | S230HL       | 1920x1080 | 510x290mm  | 23.1 | 2013 | 6B5EF |
| Acer         | ACR0280 | S230HL       | 1920x1080 | 510x290mm  | 23.1 | 2012 | 20F2E |
| Acer         | ACR0280 | S230HL       | 1920x1080 | 510x290mm  | 23.1 | 2011 | 0986A |
| Acer         | ACR0281 | S220HQL      | 1920x1080 | 480x270mm  | 21.7 | 2013 | F97C9 |
| Acer         | ACR0281 | S220HQL      | 1920x1080 | 480x270mm  | 21.7 | 2012 | 8AE9F |
| Acer         | ACR0281 | S220HQL      | 1920x1080 | 480x270mm  | 21.7 | 2011 | 0A602 |
| Acer         | ACR0282 | S230HL       | 1920x1080 | 510x290mm  | 23.1 | 2014 | DFF6B |
| Acer         | ACR0282 | S230HL       | 1920x1080 | 510x290mm  | 23.1 | 2011 | 3521A |
| Acer         | ACR0289 | S240HL       | 1920x1080 | 530x300mm  | 24.0 | 2016 | 5B948 |
| Acer         | ACR0289 | S240HL       | 1920x1080 | 530x300mm  | 24.0 | 2014 | 6CE46 |
| Acer         | ACR0289 | S240HL       | 1920x1080 | 530x300mm  | 24.0 | 2013 | 81A7D |
| Acer         | ACR0289 | S240HL       | 1920x1080 | 530x300mm  | 24.0 | 2012 | A62D6 |
| Acer         | ACR0289 | S240HL       | 1920x1080 | 530x300mm  | 24.0 | 2011 | 39789 |
| Acer         | ACR02BE | S235HL       | 1920x1080 | 510x290mm  | 23.1 | 2013 | E3C4A |
| Acer         | ACR02C7 | V243PWL      | 1920x1200 | 520x320mm  | 24.0 | 2012 | D8B63 |
| Acer         | ACR02CA | S271HL       | 1920x1080 | 600x340mm  | 27.2 | 2018 | BECC3 |
| Acer         | ACR02CA | S271HL       | 1920x1080 | 600x340mm  | 27.2 | 2014 | F81D0 |
| Acer         | ACR02CA | S271HL       | 1920x1080 | 600x340mm  | 27.2 | 2013 | D14DF |
| Acer         | ACR02CA | S271HL       | 1920x1080 | 600x340mm  | 27.2 | 2012 | C27F6 |
| Acer         | ACR02CD | P238HL       | 1920x1080 | 510x290mm  | 23.1 | 2012 | F0A83 |
| Acer         | ACR02DC | V235HL       | 1920x1080 | 510x290mm  | 23.1 | 2012 | 352E8 |
| Acer         | ACR02EA | G226HQL      | 1920x1080 | 480x270mm  | 21.7 | 2012 | 58C1B |
| Acer         | ACR02EB | G236HL       | 1920x1080 | 510x290mm  | 23.1 | 2016 | 12E20 |
| Acer         | ACR02EB | G236HL       | 1920x1080 | 510x290mm  | 23.1 | 2014 | 0D8B0 |
| Acer         | ACR02F0 | V245HL       | 1920x1080 | 530x300mm  | 24.0 | 2012 | F1FF4 |
| Acer         | ACR02F9 | GN246HL      | 1920x1080 | 530x300mm  | 24.0 | 2017 | BF6BF |
| Acer         | ACR02F9 | GN246HL      | 1920x1080 | 530x300mm  | 24.0 | 2016 | 2E010 |
| Acer         | ACR02F9 | GN246HL      | 1920x1080 | 530x300mm  | 24.0 | 2014 | D1498 |
| Acer         | ACR02FF | G246HL       | 1920x1080 | 530x300mm  | 24.0 | 2016 | 2E7FE |
| Acer         | ACR02FF | G246HL       | 1920x1080 | 530x300mm  | 24.0 | 2015 | 4CAEA |
| Acer         | ACR02FF | G246HL       | 1920x1080 | 530x300mm  | 24.0 | 2014 | C1F4E |
| Acer         | ACR02FF | G246HL       | 1920x1080 | 530x300mm  | 24.0 | 2013 | 6EF60 |
| Acer         | ACR02FF | G246HL       | 1920x1080 | 530x300mm  | 24.0 | 2012 | EB198 |
| Acer         | ACR0300 | G276HL       | 1920x1080 | 600x340mm  | 27.2 | 2017 | 9248B |
| Acer         | ACR0300 | G276HL       | 1920x1080 | 600x340mm  | 27.2 | 2015 | 2D7E2 |
| Acer         | ACR0300 | G276HL       | 1920x1080 | 600x340mm  | 27.2 | 2013 | B6688 |
| Acer         | ACR0303 | V275HL       | 1920x1080 | 600x340mm  | 27.2 | 2012 | 9B216 |
| Acer         | ACR0308 | S231HL       | 1920x1080 | 510x290mm  | 23.1 | 2013 | D1370 |
| Acer         | ACR030C | G236HL       | 1920x1080 | 510x290mm  | 23.1 | 2013 | 7FB93 |
| Acer         | ACR0311 | G226HQL      | 1920x1080 | 480x270mm  | 21.7 | 2013 | 9AFE6 |
| Acer         | ACR0313 | S275HL       | 1920x1080 | 600x340mm  | 27.2 | 2013 | 94292 |
| Acer         | ACR0318 | H236HL       | 1920x1080 | 510x290mm  | 23.1 | 2015 | 27132 |
| Acer         | ACR0318 | H236HL       | 1920x1080 | 510x290mm  | 23.1 | 2014 | EC9D6 |
| Acer         | ACR0318 | H236HL       | 1920x1080 | 510x290mm  | 23.1 | 2013 | 58272 |
| Acer         | ACR0319 | H226HQL      | 1920x1080 | 480x270mm  | 21.7 | 2013 | 3B12C |
| Acer         | ACR032D | V226HQL      | 1920x1080 | 480x270mm  | 21.7 | 2018 | 6204B |
| Acer         | ACR032D | V226HQL      | 1920x1080 | 480x270mm  | 21.7 | 2014 | 0AD1A |
| Acer         | ACR032D | V226HQL      | 1920x1080 | 480x270mm  | 21.7 | 2013 | AE29B |
| Acer         | ACR032E | V246HL       | 1920x1080 | 530x300mm  | 24.0 | 2015 | 441F7 |
| Acer         | ACR032E | V246HL       | 1920x1080 | 530x300mm  | 24.0 | 2013 | 1E725 |
| Acer         | ACR032F | V276HL       | 1920x1080 | 600x340mm  | 27.2 | 2013 | 391D3 |
| Acer         | ACR0331 | B246HL       | 1920x1080 | 530x300mm  | 24.0 | 2013 | 2CB4D |
| Acer         | ACR0333 | V196WL       | 1440x900  | 410x260mm  | 19.1 | 2014 | 2313D |
| Acer         | ACR0334 | V206HQL      | 1600x900  | 430x240mm  | 19.4 | 2016 | 3422C |
| Acer         | ACR0335 | V226HQL      | 1920x1080 | 480x270mm  | 21.7 | 2017 | 3EC97 |
| Acer         | ACR0335 | V226HQL      | 1920x1080 | 480x270mm  | 21.7 | 2016 | C4A7F |
| Acer         | ACR0335 | V226HQL      | 1920x1080 | 480x270mm  | 21.7 | 2013 | C994F |
| Acer         | ACR0336 | V246HL       | 1920x1080 | 530x300mm  | 24.0 | 2017 | 468E8 |
| Acer         | ACR0336 | V246HL       | 1920x1080 | 530x300mm  | 24.0 | 2015 | B46B3 |
| Acer         | ACR0346 | G226HQL      | 1920x1080 | 480x270mm  | 21.7 | 2017 | 32BCA |
| Acer         | ACR0346 | G226HQL      | 1920x1080 | 480x270mm  | 21.7 | 2015 | 7A4EF |
| Acer         | ACR0346 | G226HQL      | 1920x1080 | 480x270mm  | 21.7 | 2014 | F9CA7 |
| Acer         | ACR0347 | S220HQL      | 1920x1080 | 480x270mm  | 21.7 | 2016 | 9C8FB |
| Acer         | ACR0350 | V236HL       | 1920x1080 | 510x290mm  | 23.1 | 2013 | 00AC6 |
| Acer         | ACR035B | G246HYL      | 1920x1080 | 530x300mm  | 24.0 | 2017 | DFD86 |
| Acer         | ACR035B | G246HYL      | 1920x1080 | 530x300mm  | 24.0 | 2015 | 424C9 |
| Acer         | ACR035B | G246HYL      | 1920x1080 | 530x300mm  | 24.0 | 2014 | B5362 |
| Acer         | ACR035B | G246HYL      | 1920x1080 | 530x300mm  | 24.0 | 2013 | 2BAB0 |
| Acer         | ACR0361 | B276HUL      | 2560x1440 | 600x340mm  | 27.2 | 2015 | 3168D |
| Acer         | ACR0361 | B276HUL      | 2560x1440 | 600x340mm  | 27.2 | 2013 | 70E75 |
| Acer         | ACR0363 | V196L        | 1280x1024 | 380x300mm  | 19.1 | 2015 | E0E4E |
| Acer         | ACR0363 | V196L        | 1280x1024 | 380x300mm  | 19.1 | 2014 | 98A5C |
| Acer         | ACR0387 | S236HL       | 1920x1080 | 510x290mm  | 23.1 | 2014 | A60DE |
| Acer         | ACR0388 | S276HL       | 1920x1080 | 600x340mm  | 27.2 | 2013 | D5D0D |
| Acer         | ACR038C | HX1953L      | 1600x900  | 430x240mm  | 19.4 | 2013 | B9781 |
| Acer         | ACR0393 | B296CL       | 2560x1080 | 670x280mm  | 28.6 | 2013 | A2CE7 |
| Acer         | ACR03AA | H276HL       | 1920x1080 | 600x340mm  | 27.2 | 2015 | 4B35D |
| Acer         | ACR03C0 |              | 1920x1080 | 480x270mm  | 21.7 | 2018 | D1AD4 |
| Acer         | ACR03DC | K272HL       | 1920x1080 | 600x340mm  | 27.2 | 2016 | F0587 |
| Acer         | ACR03DC | K272HL       | 1920x1080 | 600x340mm  | 27.2 | 2015 | D9773 |
| Acer         | ACR03DC | K272HL       | 1920x1080 | 600x340mm  | 27.2 | 2014 | 2CD69 |
| Acer         | ACR03DD | K272HUL      | 2560x1440 | 600x340mm  | 27.2 | 2014 | 38DE9 |
| Acer         | ACR03DE | G227HQL      | 1920x1080 | 480x270mm  | 21.7 | 2014 | 3D09C |
| Acer         | ACR03DF | G237HL       | 1920x1080 | 510x290mm  | 23.1 | 2014 | C4B00 |
| Acer         | ACR03E0 | K202HQL      | 1600x900  | 430x240mm  | 19.4 | 2015 | 0900B |
| Acer         | ACR03E0 | K202HQL      | 1600x900  | 430x240mm  | 19.4 | 2014 | E4ACB |
| Acer         | ACR03E1 | K222HQL      | 1920x1080 | 480x270mm  | 21.7 | 2018 | AC57A |
| Acer         | ACR03E1 | K222HQL      | 1920x1080 | 480x270mm  | 21.7 | 2016 | 530A0 |
| Acer         | ACR03E1 | K222HQL      | 1920x1080 | 480x270mm  | 21.7 | 2014 | 25C81 |
| Acer         | ACR03E3 | K242HL       | 1920x1080 | 530x300mm  | 24.0 | 2016 | EDCD7 |
| Acer         | ACR03E3 | K242HL       | 1920x1080 | 530x300mm  | 24.0 | 2015 | 3FD6B |
| Acer         | ACR03E3 | K242HL       | 1920x1080 | 530x300mm  | 24.0 | 2014 | 5F03B |
| Acer         | ACR03F3 | V226HQL      | 1920x1080 | 480x270mm  | 21.7 | 2015 | 101BA |
| Acer         | ACR03F3 | V226HQL      | 1920x1080 | 480x270mm  | 21.7 | 2014 | 9C1F7 |
| Acer         | ACR03FA | G247HL       | 1920x1080 | 530x300mm  | 24.0 | 2014 | E026A |
| Acer         | ACR03FB | G277HL       | 1920x1080 | 600x340mm  | 27.2 | 2014 | B4EC4 |
| Acer         | ACR03FC | V246HYL      | 1920x1080 | 530x300mm  | 24.0 | 2018 | 07DB4 |
| Acer         | ACR03FF | B286HK       | 3840x2160 | 620x340mm  | 27.8 | 2016 | 87935 |
| Acer         | ACR0406 |              | 1920x1080 | 600x340mm  | 27.2 |      | 51B57 |
| Acer         | ACR040A | CB280HK      | 3840x2160 | 620x340mm  | 27.8 | 2015 | 104A8 |
| Acer         | ACR040E | K242HL       | 1920x1080 | 530x300mm  | 24.0 | 2017 | BCBBC |
| Acer         | ACR040E | K242HL       | 1920x1080 | 530x300mm  | 24.0 | 2014 | 2A69E |
| Acer         | ACR0414 | XG270HU      | 2560x1440 | 600x340mm  | 27.2 | 2016 | F6298 |
| Acer         | ACR0416 | G257HU       | 2560x1440 | 550x310mm  | 24.9 | 2015 | 41B9B |
| Acer         | ACR041D | CB240HY      | 1920x1080 | 530x300mm  | 24.0 | 2014 | F35D8 |
| Acer         | ACR0424 | V246HQL      | 1920x1080 | 520x290mm  | 23.4 | 2018 | 07E8F |
| Acer         | ACR0424 | V246HQL      | 1920x1080 | 520x290mm  | 23.4 | 2017 | B558A |
| Acer         | ACR0427 | G247HYL      | 1920x1080 | 530x300mm  | 24.0 | 2016 | EE5E9 |
| Acer         | ACR0440 | S277HK       | 3840x2160 | 600x340mm  | 27.2 | 2014 | 74503 |
| Acer         | ACR0467 | KA220HQ      | 1920x1080 | 480x270mm  | 21.7 | 2018 | 2734B |
| Acer         | ACR0468 | KA240HQ      | 1920x1080 | 520x290mm  | 23.4 | 2015 | 7A2CD |
| Acer         | ACR0473 | XF270HU      | 2560x1440 | 600x340mm  | 27.2 | 2015 | A96C0 |
| Acer         | ACR0475 | K242HYL      | 1920x1080 | 530x300mm  | 24.0 | 2018 | 5F4EB |
| Acer         | ACR0475 | K242HYL      | 1920x1080 | 530x300mm  | 24.0 | 2017 | DAABA |
| Acer         | ACR0476 | CB271HK      | 3840x2160 | 600x340mm  | 27.2 | 2016 | 4CAEA |
| Acer         | ACR0477 | CB281HK      | 3840x2160 | 620x340mm  | 27.8 | 2017 | FEA0A |
| Acer         | ACR0477 | CB281HK      | 3840x2160 | 620x340mm  | 27.8 | 2016 | 8C6E7 |
| Acer         | ACR0478 | Z35          | 2560x1080 | 810x350mm  | 34.7 | 2017 | 31F6A |
| Acer         | ACR0496 | R271         | 1920x1080 | 600x340mm  | 27.2 | 2016 | 9A2E0 |
| Acer         | ACR0503 | R221Q        | 1920x1080 | 480x270mm  | 21.7 | 2018 | 8D831 |
| Acer         | ACR0503 | R221Q        | 1920x1080 | 480x270mm  | 21.7 | 2017 | 3E11B |
| Acer         | ACR0503 | R221Q        | 1920x1080 | 480x270mm  | 21.7 | 2016 | 3A0C8 |
| Acer         | ACR050A | VA220HQ      | 1920x1080 | 480x270mm  | 21.7 | 2016 | 09827 |
| Acer         | ACR0512 | k222HQL      | 1920x1080 | 480x270mm  | 21.7 | 2016 | F8EB2 |
| Acer         | ACR0517 | EB192Q       | 1366x768  | 410x230mm  | 18.5 | 2016 | 7DAD7 |
| Acer         | ACR0520 | H277HK       | 3840x2160 | 600x340mm  | 27.2 | 2017 | 9CD33 |
| Acer         | ACR0523 | K272HL       | 1920x1080 | 600x340mm  | 27.2 | 2019 | 3EFA1 |
| Acer         | ACR0523 | K272HL       | 1920x1080 | 600x340mm  | 27.2 | 2018 | 0AE99 |
| Acer         | ACR0523 | K272HL       | 1920x1080 | 600x340mm  | 27.2 | 2017 | ABC0C |
| Acer         | ACR0523 | K272HL       | 1920x1080 | 600x340mm  | 27.2 | 2016 | 4E6D2 |
| Acer         | ACR0538 | KA240H       | 1920x1080 | 530x300mm  | 24.0 | 2018 | C01B5 |
| Acer         | ACR0538 | KA240H       | 1920x1080 | 530x300mm  | 24.0 | 2017 | F01A4 |
| Acer         | ACR0539 | RT240Y       | 1920x1080 | 530x300mm  | 24.0 | 2018 | 19D45 |
| Acer         | ACR053E | ED322Q       | 1920x1080 | 710x400mm  | 32.1 | 2018 | F50F9 |
| Acer         | ACR0549 | XF270HU      | 2560x1440 | 600x340mm  | 27.2 | 2018 | 160C8 |
| Acer         | ACR055F | GF246        | 1920x1080 | 530x300mm  | 24.0 | 2017 | C5A85 |
| Acer         | ACR056C | ET241Y       | 1920x1080 | 530x300mm  | 24.0 | 2018 | 837BB |
| Acer         | ACR056C | ET241Y       | 1920x1080 | 530x300mm  | 24.0 | 2017 | 72004 |
| Acer         | ACR057E | SA230        | 1920x1080 | 510x290mm  | 23.1 | 2018 | DA89B |
| Acer         | ACR0595 | B246WL       | 1920x1200 | 520x320mm  | 24.0 | 2017 | FF9FD |
| Acer         | ACR0604 | KG241Q       | 1920x1080 | 520x290mm  | 23.4 | 2018 | F274B |
| Acer         | ACR061A | EB490QK      | 3840x2160 | 1080x610mm | 48.8 | 2018 | C6A8E |
| Acer         | ACR0628 | ED273 A      | 1920x1080 | 600x340mm  | 27.2 | 2016 | A1A53 |
| Acer         | ACR0629 | ED242QR      | 1920x1080 | 520x300mm  | 23.6 | 2017 | 4C11D |
| Acer         | ACR062B | EB550K       | 3840x2160 | 1210x680mm | 54.6 | 2017 | 0F2C0 |
| Acer         | ACR0631 | ET322QK      | 3840x2160 | 700x390mm  | 31.5 | 2017 | 9F8AC |
| Acer         | ACR0648 | ED347CKR     | 3440x1440 | 800x340mm  | 34.2 | 2017 | 296F7 |
| Acer         | ACR065B | CB241HY      | 1920x1080 | 530x300mm  | 24.0 | 2018 | 230C1 |
| Acer         | ACR0673 | VG240Y       | 1920x1080 | 530x300mm  | 24.0 | 2018 | 7FB8F |
| Acer         | ACR0687 | ET322QU      | 2560x1440 | 700x390mm  | 31.5 | 2018 | F3FD9 |
| Acer         | ACR06AB | SB220Q       | 1920x1080 | 480x270mm  | 21.7 | 2019 | 15188 |
| Acer         | ACR06B4 | AL1716       | 1280x1024 | 340x270mm  | 17.1 | 2008 | A1745 |
| Acer         | ACR06B4 | AL1716       | 1280x1024 | 340x270mm  | 17.1 | 2006 | B402E |
| Acer         | ACR0783 | AL1923 L6... | 1280x1024 | 370x300mm  | 18.8 | 2007 | DC83C |
| Acer         | ACR07E7 | AL2023 V1.00 | 1600x1200 | 410x310mm  | 20.2 | 2006 | A80E2 |
| Acer         | ACR1922 | AT1922       | 1440x900  | 400x250mm  | 18.6 | 2007 | 472B6 |
| Acer         | ACR2108 | P1185        | 1920x1080 |            |      | 2016 | C30CB |
| Acer         | ACR2358 | AT2358MWL    | 1920x1080 | 930x520mm  | 41.9 | 2010 | 0E52D |
| Acer         | ACR285A | LTM200KT03   | 1600x900  | 440x250mm  | 19.9 | 2009 | 017BB |
| Acer         | ACR40B0 | AIO LCD      | 1920x1080 | 470x260mm  | 21.1 | 2015 | 6C4D9 |
| Acer         | ACR40B0 | AIO LCD      | 1920x1080 | 520x290mm  | 23.4 | 2015 | 74187 |
| Acer         | ACR5770 | AL1715       | 1280x1024 | 340x270mm  | 17.1 |      | AFA9D |
| Acer         | ACRAD04 | AL1722       | 1280x1024 | 340x270mm  | 17.1 |      | 4F7EE |
| Acer         | ACRAD18 | AL1714       | 1280x1024 | 340x270mm  | 17.1 |      | 2116E |
| Acer         | ACRAD40 | AL1751       | 1280x1024 | 340x270mm  | 17.1 |      | 4BC3D |
| Acer         | ACRAD41 | AL1951       | 1280x1024 | 380x300mm  | 19.1 | 2007 | ABE8B |
| Acer         | ACRAD41 | AL1951       | 1280x1024 | 380x300mm  | 19.1 | 2006 | BF3AC |
| Acer         | ACRAD41 | AL1951       | 1280x1024 | 380x300mm  | 19.1 |      | B4ED6 |
| Acer         | ACRAD46 | AL1716       | 1280x1024 | 340x270mm  | 17.1 | 2008 | 0BC6B |
| Acer         | ACRAD49 | AL1916       | 1280x1024 | 380x300mm  | 19.1 | 2008 | 34F62 |
| Acer         | ACRAD49 | AL1916       | 1280x1024 | 380x300mm  | 19.1 | 2007 | F6F77 |
| Acer         | ACRAD51 | AL1716       | 1280x1024 | 340x270mm  | 17.1 | 2008 | F5B39 |
| Acer         | ACRAD52 | AL1916W      | 1440x900  | 410x260mm  | 19.1 | 2007 | 71171 |
| Acer         | ACRAD52 | AL1916W      | 1440x900  | 410x260mm  | 19.1 | 2006 | 341C0 |
| Acer         | ACRAD61 | AL2416W      | 1920x1200 | 520x320mm  | 24.0 | 2008 | 8E729 |
| Acer         | ACRAD61 | AL2416W      | 1920x1200 | 520x320mm  | 24.0 | 2007 | 40CF4 |
| Acer         | ACRAD64 | AL2016W      | 1680x1050 | 430x270mm  | 20.0 | 2008 | 71915 |
| Acer         | ACRAD64 | AL2016W      | 1680x1050 | 430x270mm  | 20.0 | 2007 | 1E0E7 |
| Acer         | ACRAD70 | AL2051W      | 1680x1050 | 430x270mm  | 20.0 | 2006 | 69602 |
| Acer         | ACRAD73 | AL1917       | 1280x1024 | 380x300mm  | 19.1 | 2008 | 9BF76 |
| Acer         | ACRAD73 | AL1917       | 1280x1024 | 380x300mm  | 19.1 | 2007 | 1A63D |
| Acer         | ACRAD73 | L73081974... | 1280x1024 | 380x300mm  | 19.1 | 2006 | 082EC |
| Acer         | ACRAD74 | AL2216W      | 1680x1050 | 470x300mm  | 22.0 | 2008 | 91A47 |
| Acer         | ACRAD74 | AL2216W      | 1680x1050 | 470x300mm  | 22.0 | 2007 | 27A66 |
| Acer         | ACRAD74 | AL2216W      | 1680x1050 | 470x300mm  | 22.0 | 2006 | 396E4 |
| Acer         | ACRAD86 | AL1916W      | 1440x900  | 400x250mm  | 18.6 | 2008 | 75F7C |
| Acer         | ACRAD95 | X192W        | 1440x900  | 410x260mm  | 19.1 | 2007 | 031BE |
| Acer         | ACRAD98 | X221W        | 1680x1050 | 470x300mm  | 22.0 | 2007 | 7E2A9 |
| Acer         | ACRADA1 | AL2216W      | 1680x1050 | 470x300mm  | 22.0 | 2008 | 164BE |
| Acer         | ACRADA1 | AL2216W      | 1680x1050 | 470x300mm  | 22.0 | 2007 | 2338F |
| Acer         | ACRADA4 | AL2202W      | 1680x1050 | 470x290mm  | 21.7 | 2007 | 092F7 |
| Acer         | ACRADAA | P191W        | 1440x900  | 410x260mm  | 19.1 | 2007 | 41DF8 |
| Acer         | ACRADAB | P203W        | 1680x1050 | 430x270mm  | 20.0 | 2007 | 79E92 |
| Acer         | ACRADAE | P223W        | 1680x1050 | 470x300mm  | 22.0 | 2008 | 42450 |
| Acer         | ACRADAE | P223W        | 1680x1050 | 470x300mm  | 22.0 | 2007 | AACF6 |
| Acer         | ACRADAF | P243W        | 1920x1200 | 520x320mm  | 24.0 | 2008 | 138A1 |
| Acer         | ACRF132 | AIO LCD      | 1920x1080 | 510x290mm  | 23.1 | 2012 | 2C544 |
| Achieva S... | ACH00FA | QHD270       | 2560x1440 | 600x340mm  | 27.2 | 2011 | B51C0 |
| Ancor Com... | ACI17B6 | ASUS VB171   | 1280x1024 | 340x270mm  | 17.1 | 2008 | 27D7A |
| Ancor Com... | ACI17B7 | ASUS VB172   | 1280x1024 | 340x270mm  | 17.1 | 2007 | B388C |
| Ancor Com... | ACI17B8 | ASUS VB175   | 1280x1024 | 340x270mm  | 17.1 | 2011 | D8BCD |
| Ancor Com... | ACI1900 | ASUS ET2203  | 1920x1080 | 480x270mm  | 21.7 | 2009 | BB22A |
| Ancor Com... | ACI1911 | ASUS VB198   | 1280x1024 | 380x300mm  | 19.1 | 2012 | C90DA |
| Ancor Com... | ACI19A1 | PW191        | 1440x900  | 410x260mm  | 19.1 | 2006 | A56C8 |
| Ancor Com... | ACI19AB | VW195        | 1440x900  | 410x260mm  | 19.1 | 2010 | A4657 |
| Ancor Com... | ACI19AB | VW195        | 1440x900  | 410x260mm  | 19.1 | 2008 | 51971 |
| Ancor Com... | ACI19AB | VW195        | 1440x900  | 410x260mm  | 19.1 | 2007 | 8A02D |
| Ancor Com... | ACI19AC | VK191        | 1440x900  | 410x260mm  | 19.1 | 2008 | FDB55 |
| Ancor Com... | ACI19B1 | 6CL900359... | 1280x1024 | 380x310mm  | 19.3 | 2006 | DB2B7 |
| Ancor Com... | ACI19B2 | ASUS MB19TU  | 1280x1024 | 380x300mm  | 19.1 | 2007 | 87D9B |
| Ancor Com... | ACI19B4 | ASUS VB191   | 1280x1024 | 380x300mm  | 19.1 | 2008 | FF331 |
| Ancor Com... | ACI19B4 | ASUS VB191   | 1280x1024 | 380x300mm  | 19.1 | 2007 | 6231E |
| Ancor Com... | ACI19F2 | ASUS VS197   | 1366x768  | 410x230mm  | 18.5 | 2016 | 471EB |
| Ancor Com... | ACI19F2 | ASUS VS197   | 1366x768  | 410x230mm  | 18.5 | 2013 | AF0CE |
| Ancor Com... | ACI20A1 | PW201        | 1680x1050 | 430x270mm  | 20.0 | 2007 | 78A50 |
| Ancor Com... | ACI20A2 | VW202        | 1680x1050 | 430x270mm  | 20.0 | 2008 | 209AD |
| Ancor Com... | ACI20A6 | VE205        | 1600x900  | 440x250mm  | 19.9 | 2010 | A38F8 |
| Ancor Com... | ACI20A8 | VE208        | 1600x900  | 440x250mm  | 19.9 | 2012 | 7C77B |
| Ancor Com... | ACI20A8 | VE208        | 1600x900  | 440x250mm  | 19.9 | 2010 | 8DBBE |
| Ancor Com... | ACI20AA | ASUS VS208   | 1600x900  | 440x250mm  | 19.9 | 2011 | A29F6 |
| Ancor Com... | ACI20B2 | ASUS LS201   | 1440x900  | 430x270mm  | 20.0 | 2008 | 58777 |
| Ancor Com... | ACI20D1 | ASUS VS209   | 1600x900  | 440x250mm  | 19.9 | 2011 | 89B88 |
| Ancor Com... | ACI22A0 | VW225        | 1680x1050 | 470x300mm  | 22.0 | 2009 | 9681D |
| Ancor Com... | ACI22A0 | VW225        | 1680x1050 | 470x300mm  | 22.0 | 2008 | 93CB0 |
| Ancor Com... | ACI22A2 | VW222        | 1680x1050 | 470x300mm  | 22.0 | 2008 | 3BA5A |
| Ancor Com... | ACI22A2 | VW222        | 1680x1050 | 470x300mm  | 22.0 | 2007 | 2AD1B |
| Ancor Com... | ACI22A4 | ASUS MK221   | 1680x1050 | 470x290mm  | 21.7 | 2008 | 02BE0 |
| Ancor Com... | ACI22A5 | ASUS VK222   | 1680x1050 | 470x300mm  | 22.0 | 2010 | EADE6 |
| Ancor Com... | ACI22A6 | ASUS VK222H  | 1680x1050 | 530x300mm  | 24.0 | 2008 | 63AF2 |
| Ancor Com... | ACI22A6 | ASUS VK222H  | 1680x1050 | 470x270mm  | 21.3 | 2008 | 6AF49 |
| Ancor Com... | ACI22A9 | ASUS VW220   | 1680x1050 | 430x270mm  | 20.0 | 2008 | 230CB |
| Ancor Com... | ACI22AA | ASUS VW224   | 1680x1050 | 470x300mm  | 22.0 | 2011 | 54903 |
| Ancor Com... | ACI22AA | ASUS VW224   | 1680x1050 | 470x300mm  | 22.0 | 2010 | 632D5 |
| Ancor Com... | ACI22AB | ASUS VH222   | 1920x1080 | 470x260mm  | 21.1 | 2010 | 3F7CF |
| Ancor Com... | ACI22AC | ASUS MS227   | 1680x1050 | 470x300mm  | 22.0 | 2010 | 6587D |
| Ancor Com... | ACI22B1 | MW221        | 1680x1050 | 470x300mm  | 22.0 | 2007 | 08972 |
| Ancor Com... | ACI22B1 | MW221        | 1680x1050 | 470x300mm  | 22.0 | 2006 | 13D08 |
| Ancor Com... | ACI22C2 | ASUS VS229   | 1920x1080 | 480x270mm  | 21.7 | 2017 | 29039 |
| Ancor Com... | ACI22C2 | ASUS VS229   | 1920x1080 | 480x270mm  | 21.7 | 2016 | 9073C |
| Ancor Com... | ACI22C2 | ASUS VS229   | 1920x1080 | 480x270mm  | 21.7 | 2015 | FC461 |
| Ancor Com... | ACI22C2 | ASUS VS229   | 1920x1080 | 480x270mm  | 21.7 | 2014 | 619AE |
| Ancor Com... | ACI22C3 | ASUS VP228   | 1920x1080 | 480x270mm  | 21.7 | 2018 | 0E1B0 |
| Ancor Com... | ACI22C3 | ASUS VP228   | 1920x1080 | 480x270mm  | 21.7 | 2017 | 476E0 |
| Ancor Com... | ACI22C3 | ASUS VP228   | 1920x1080 | 480x270mm  | 21.7 | 2016 | 2549C |
| Ancor Com... | ACI22CA | ASUS VP229   | 1920x1080 | 480x270mm  | 21.7 | 2016 | 0B8CA |
| Ancor Com... | ACI22D1 | VK228        | 1920x1080 | 480x270mm  | 21.7 | 2013 | 0AA81 |
| Ancor Com... | ACI22D1 | VK228        | 1920x1080 | 480x270mm  | 21.7 | 2012 | 1A03B |
| Ancor Com... | ACI22D3 | ASUS VS229   | 1920x1080 | 480x270mm  | 21.7 | 2012 | 5420B |
| Ancor Com... | ACI22E5 | VX229        | 1920x1080 | 480x270mm  | 21.7 | 2016 | F4123 |
| Ancor Com... | ACI22E5 | VX229        | 1920x1080 | 480x270mm  | 21.7 | 2014 | 31FF6 |
| Ancor Com... | ACI22E5 | VX229        | 1920x1080 | 480x270mm  | 21.7 | 2013 | EBE6C |
| Ancor Com... | ACI22F1 |              | 1680x1050 | 470x300mm  | 22.0 | 2008 | 42EAB |
| Ancor Com... | ACI22F2 | VH226        | 1920x1080 | 480x270mm  | 21.7 | 2009 | 1E098 |
| Ancor Com... | ACI22F2 | VH226        | 1920x1080 | 480x270mm  | 21.7 | 2008 | 7C998 |
| Ancor Com... | ACI22F3 | ASUS VH222H  | 1920x1080 | 470x260mm  | 21.1 | 2009 | B92B9 |
| Ancor Com... | ACI22F4 | ASUS 22T1E   | 1920x1080 | 480x270mm  | 21.7 | 2011 | 2D4C4 |
| Ancor Com... | ACI22F4 | ASUS 22T1E   | 1920x1080 | 480x270mm  | 21.7 | 2009 | F6457 |
| Ancor Com... | ACI22F7 | ASUS MS228   | 1920x1080 | 480x270mm  | 21.7 | 2010 | 852AC |
| Ancor Com... | ACI22FA | VE228        | 1920x1080 | 480x270mm  | 21.7 | 2017 | 98D39 |
| Ancor Com... | ACI22FA | VE228        | 1920x1080 | 480x270mm  | 21.7 | 2015 | 22037 |
| Ancor Com... | ACI22FA | VE228        | 1920x1080 | 480x270mm  | 21.7 | 2013 | 022F6 |
| Ancor Com... | ACI22FA | VE228        | 1920x1080 | 480x270mm  | 21.7 | 2012 | 95749 |
| Ancor Com... | ACI22FA | VE228        | 1920x1080 | 480x270mm  | 21.7 | 2011 | 9E2F6 |
| Ancor Com... | ACI22FA | VE228        | 1920x1080 | 480x270mm  | 21.7 | 2010 | 51A63 |
| Ancor Com... | ACI22FB | ASUS ML229   | 1920x1080 | 480x270mm  | 21.7 | 2012 | 03494 |
| Ancor Com... | ACI22FB | ASUS ML229   | 1920x1080 | 480x270mm  | 21.7 | 2011 | 98F72 |
| Ancor Com... | ACI22FC | ASUS VH228   | 1920x1080 | 470x260mm  | 21.1 | 2011 | 59000 |
| Ancor Com... | ACI22FD | ASUS VS228   | 1920x1080 | 480x270mm  | 21.7 | 2017 | 57A98 |
| Ancor Com... | ACI22FD | ASUS VS228   | 1920x1080 | 480x270mm  | 21.7 | 2016 | 30796 |
| Ancor Com... | ACI22FD | ASUS VS228   | 1920x1080 | 480x270mm  | 21.7 | 2015 | EBA76 |
| Ancor Com... | ACI22FD | ASUS VS228   | 1920x1080 | 480x270mm  | 21.7 | 2014 | E6BA8 |
| Ancor Com... | ACI22FD | ASUS VS228   | 1920x1080 | 480x270mm  | 21.7 | 2013 | 1C4B6 |
| Ancor Com... | ACI22FD | ASUS VS228   | 1920x1080 | 480x270mm  | 21.7 | 2012 | 5E018 |
| Ancor Com... | ACI23A2 | ASUS PB238   | 1920x1080 | 510x290mm  | 23.1 | 2013 | 087AB |
| Ancor Com... | ACI23A2 | ASUS PB238   | 1920x1080 | 510x290mm  | 23.1 | 2012 | D7675 |
| Ancor Com... | ACI23B1 | ASUS PA238   | 1920x1080 | 510x290mm  | 23.1 | 2012 | 1BEB6 |
| Ancor Com... | ACI23B1 | ASUS PA238   | 1920x1080 | 510x290mm  | 23.1 | 2011 | AE3D9 |
| Ancor Com... | ACI23C1 | VX238        | 1920x1080 | 510x290mm  | 23.1 | 2016 | 6D8D3 |
| Ancor Com... | ACI23C1 | VX238        | 1920x1080 | 510x290mm  | 23.1 | 2014 | 3986F |
| Ancor Com... | ACI23C2 | ASUS MX239   | 1920x1080 | 530x310mm  | 24.2 | 2015 | 60254 |
| Ancor Com... | ACI23C2 | ASUS MX239   | 1920x1080 | 530x310mm  | 24.2 | 2014 | 0B031 |
| Ancor Com... | ACI23C3 | ASUS VH238   | 1920x1080 | 510x290mm  | 23.1 | 2016 | 0CA67 |
| Ancor Com... | ACI23C3 | ASUS VH238   | 1920x1080 | 510x290mm  | 23.1 | 2015 | 25A22 |
| Ancor Com... | ACI23C4 | ASUS VC239   | 1920x1080 | 510x290mm  | 23.1 | 2017 | 67435 |
| Ancor Com... | ACI23C4 | ASUS VC239   | 1920x1080 | 510x290mm  | 23.1 | 2016 | 5E948 |
| Ancor Com... | ACI23C4 | ASUS VC239   | 1920x1080 | 510x290mm  | 23.1 | 2015 | 4ACF5 |
| Ancor Com... | ACI23C5 | ASUS VP239   | 1920x1080 | 510x290mm  | 23.1 | 2016 | 5FB27 |
| Ancor Com... | ACI23D2 | ASUS VS239   | 1920x1080 | 510x290mm  | 23.1 | 2015 | A1212 |
| Ancor Com... | ACI23D2 | ASUS VS239   | 1920x1080 | 510x290mm  | 23.1 | 2014 | A6758 |
| Ancor Com... | ACI23D2 | ASUS VS239   | 1920x1080 | 510x290mm  | 23.1 | 2013 | 9A923 |
| Ancor Com... | ACI23D2 | ASUS VS239   | 1920x1080 | 510x290mm  | 23.1 | 2011 | 039C3 |
| Ancor Com... | ACI23D3 | ASUS VG23A   | 1920x1080 | 530x310mm  | 24.2 | 2012 | 05392 |
| Ancor Com... | ACI23E1 | VX239        | 1920x1080 | 510x290mm  | 23.1 | 2014 | 20975 |
| Ancor Com... | ACI23E1 | VX239        | 1920x1080 | 510x290mm  | 23.1 | 2013 | FA9CC |
| Ancor Com... | ACI23F1 | ASUS VH232   | 1920x1080 | 520x290mm  | 23.4 | 2009 | 9DA5E |
| Ancor Com... | ACI23F1 | ASUS VH232H  | 1920x1080 | 520x290mm  | 23.4 | 2009 | C66A3 |
| Ancor Com... | ACI23F2 | ASUS VH236H  | 1920x1080 | 520x290mm  | 23.4 | 2011 | CB010 |
| Ancor Com... | ACI23F2 | ASUS VH236H  | 1920x1080 | 520x290mm  | 23.4 | 2009 | 482B0 |
| Ancor Com... | ACI23F5 | ASUS MS238   | 1920x1080 | 510x290mm  | 23.1 | 2010 | 0A8B4 |
| Ancor Com... | ACI23F7 | ASUS VG236   | 1920x1080 | 510x290mm  | 23.1 | 2010 | 1786B |
| Ancor Com... | ACI23F8 | ASUS ML239   | 1920x1080 | 510x290mm  | 23.1 | 2011 | 2509B |
| Ancor Com... | ACI23F9 | ASUS VH238   | 1920x1080 | 510x290mm  | 23.1 | 2011 | 382BB |
| Ancor Com... | ACI23FA | ASUS VS238   | 1920x1080 | 510x290mm  | 23.1 | 2015 | 2B56D |
| Ancor Com... | ACI23FA | ASUS VS238   | 1920x1080 | 510x290mm  | 23.1 | 2013 | ACE65 |
| Ancor Com... | ACI2491 | ML248        | 1920x1080 | 530x300mm  | 24.0 | 2011 | 29687 |
| Ancor Com... | ACI2493 | VE247        | 1920x1080 | 530x300mm  | 24.0 | 2016 | 74865 |
| Ancor Com... | ACI2493 | VE247        | 1920x1080 | 530x300mm  | 24.0 | 2015 | 43F68 |
| Ancor Com... | ACI2493 | VE247        | 1920x1080 | 530x300mm  | 24.0 | 2014 | CAAFA |
| Ancor Com... | ACI2493 | VE247        | 1920x1080 | 530x300mm  | 24.0 | 2013 | 097FE |
| Ancor Com... | ACI2493 | VE247        | 1920x1080 | 530x300mm  | 24.0 | 2012 | 9D112 |
| Ancor Com... | ACI2494 | VE248        | 1920x1080 | 530x300mm  | 24.0 | 2017 | 1A13D |
| Ancor Com... | ACI2494 | VE248        | 1920x1080 | 530x300mm  | 24.0 | 2012 | 6C5F6 |
| Ancor Com... | ACI2496 | ASUS VW247   | 1920x1080 | 530x300mm  | 24.0 | 2011 | 59C93 |
| Ancor Com... | ACI2498 | VS248        | 1920x1080 | 530x300mm  | 24.0 | 2018 | 0BC89 |
| Ancor Com... | ACI2498 | VS248        | 1920x1080 | 530x300mm  | 24.0 | 2017 | 1BC31 |
| Ancor Com... | ACI2498 | VS248        | 1920x1080 | 530x300mm  | 24.0 | 2016 | 07EAB |
| Ancor Com... | ACI2498 | VS248        | 1920x1080 | 530x300mm  | 24.0 | 2015 | 58ED9 |
| Ancor Com... | ACI2498 | VS248        | 1920x1080 | 530x300mm  | 24.0 | 2014 | 4A70D |
| Ancor Com... | ACI2498 | VS248        | 1920x1080 | 530x300mm  | 24.0 | 2013 | C2F9F |
| Ancor Com... | ACI2498 | VS248        | 1920x1080 | 530x300mm  | 24.0 | 2012 | 50A20 |
| Ancor Com... | ACI2498 | VS248        | 1920x1080 | 530x300mm  | 24.0 | 2011 | 80B40 |
| Ancor Com... | ACI2499 | ML249        | 1920x1080 | 530x300mm  | 24.0 | 2013 | 4518A |
| Ancor Com... | ACI2499 | ML249        | 1920x1080 | 530x300mm  | 24.0 | 2012 | 5EB8D |
| Ancor Com... | ACI2499 | ML249        | 1920x1080 | 530x300mm  | 24.0 | 2011 | 10D48 |
| Ancor Com... | ACI249A | ASUS VS247   | 1920x1080 | 520x290mm  | 23.4 | 2018 | 62364 |
| Ancor Com... | ACI249A | ASUS VS247   | 1920x1080 | 520x290mm  | 23.4 | 2017 | 4B9ED |
| Ancor Com... | ACI249A | ASUS VS247   | 1920x1080 | 520x290mm  | 23.4 | 2016 | 0DECD |
| Ancor Com... | ACI249A | ASUS VS247   | 1920x1080 | 520x290mm  | 23.4 | 2015 | 2E94D |
| Ancor Com... | ACI249A | ASUS VS247   | 1920x1080 | 520x290mm  | 23.4 | 2014 | 3DA14 |
| Ancor Com... | ACI249A | ASUS VS247   | 1920x1080 | 520x290mm  | 23.4 | 2013 | 577AD |
| Ancor Com... | ACI249A | ASUS VS247   | 1920x1080 | 520x290mm  | 23.4 | 2012 | 2C294 |
| Ancor Com... | ACI249A | ASUS VS247   | 1920x1080 | 520x290mm  | 23.4 | 2011 | 1D52D |
| Ancor Com... | ACI24A1 | ASUS MK241   | 1920x1200 | 550x350mm  | 25.7 | 2008 | 0F071 |
| Ancor Com... | ACI24A3 | PB248        | 1920x1200 | 520x320mm  | 24.0 | 2015 | BB6CC |
| Ancor Com... | ACI24A3 | PB248        | 1920x1200 | 520x320mm  | 24.0 | 2014 | 8C26E |
| Ancor Com... | ACI24A3 | PB248        | 1920x1200 | 520x320mm  | 24.0 | 2013 | EEEBF |
| Ancor Com... | ACI24A4 | VG248        | 1920x1080 | 530x300mm  | 24.0 | 2017 | FFA62 |
| Ancor Com... | ACI24A4 | VG248        | 1920x1080 | 530x300mm  | 24.0 | 2015 | DE3C6 |
| Ancor Com... | ACI24A4 | VG248        | 1920x1080 | 530x300mm  | 24.0 | 2014 | 7896C |
| Ancor Com... | ACI24A5 | VG248        | 1920x1080 | 530x300mm  | 24.0 | 2017 | 86771 |
| Ancor Com... | ACI24A5 | VG248        | 1920x1080 | 530x300mm  | 24.0 | 2014 | A9D2C |
| Ancor Com... | ACI24A5 | VG248        | 1920x1080 | 530x300mm  | 24.0 | 2013 | C1F4E |
| Ancor Com... | ACI24AE | MG248        | 1920x1080 | 530x300mm  | 24.0 | 2016 | 3449D |
| Ancor Com... | ACI24B1 | PA248        | 1920x1200 | 550x350mm  | 25.7 | 2012 | 7DD55 |
| Ancor Com... | ACI24B2 | PA249        | 1920x1200 | 520x320mm  | 24.0 | 2013 | 76CB5 |
| Ancor Com... | ACI24C1 | VK248        | 1920x1080 | 530x300mm  | 24.0 | 2013 | B5017 |
| Ancor Com... | ACI24C1 | VK248        | 1920x1080 | 530x300mm  | 24.0 | 2012 | FFDDB |
| Ancor Com... | ACI24C3 | ASUS VN247   | 1920x1080 | 520x290mm  | 23.4 | 2015 | ACB41 |
| Ancor Com... | ACI24C3 | ASUS VN247   | 1920x1080 | 520x290mm  | 23.4 | 2014 | BEF70 |
| Ancor Com... | ACI24C3 | ASUS VN247   | 1920x1080 | 520x290mm  | 23.4 | 2013 | B48DC |
| Ancor Com... | ACI24C4 | ASUS VN248   | 1920x1080 | 530x300mm  | 24.0 | 2016 | 109B0 |
| Ancor Com... | ACI24C4 | ASUS VN248   | 1920x1080 | 530x300mm  | 24.0 | 2013 | 5A28A |
| Ancor Com... | ACI24C5 | VX248        | 1920x1080 | 530x300mm  | 24.0 | 2017 | 56EE6 |
| Ancor Com... | ACI24C5 | VX248        | 1920x1080 | 530x300mm  | 24.0 | 2015 | 41A9C |
| Ancor Com... | ACI24C7 | ASUS VP247   | 1920x1080 | 520x290mm  | 23.4 | 2017 | 568D0 |
| Ancor Com... | ACI24C7 | ASUS VP247   | 1920x1080 | 520x290mm  | 23.4 | 2016 | 8344E |
| Ancor Com... | ACI24D1 | VS24A        | 1920x1200 | 520x320mm  | 24.0 | 2016 | DA3A7 |
| Ancor Com... | ACI24E1 | VG248        | 1920x1080 | 530x300mm  | 24.0 | 2019 | 169CD |
| Ancor Com... | ACI24E1 | VG248        | 1920x1080 | 530x300mm  | 24.0 | 2016 | 297AC |
| Ancor Com... | ACI24E1 | VG248        | 1920x1080 | 530x300mm  | 24.0 | 2015 | 38478 |
| Ancor Com... | ACI24E1 | VG248        | 1920x1080 | 530x300mm  | 24.0 | 2013 | 2700F |
| Ancor Com... | ACI24E1 | VG248        | 1920x1080 | 530x300mm  | 24.0 | 2012 | 67D3F |
| Ancor Com... | ACI24F1 | VK246        | 1920x1080 | 530x300mm  | 24.0 | 2010 | 481D7 |
| Ancor Com... | ACI24F1 | VK246        | 1920x1080 | 530x300mm  | 24.0 | 2008 | 2D285 |
| Ancor Com... | ACI24F2 | VW246        | 1920x1080 | 530x300mm  | 24.0 | 2010 | 7D8C5 |
| Ancor Com... | ACI24F2 | VW246        | 1920x1080 | 530x300mm  | 24.0 | 2009 | 00F61 |
| Ancor Com... | ACI24F2 | VW246        | 1920x1080 | 530x300mm  | 24.0 | 2008 | DAB50 |
| Ancor Com... | ACI24F3 | ASUS VH242H  | 1920x1080 | 520x290mm  | 23.4 | 2012 | 6EB13 |
| Ancor Com... | ACI24F3 | ASUS VH242H  | 1920x1080 | 520x290mm  | 23.4 | 2011 | CE6F4 |
| Ancor Com... | ACI24F3 | ASUS VH242H  | 1920x1080 | 520x290mm  | 23.4 | 2010 | 1F3B2 |
| Ancor Com... | ACI24F3 | ASUS VH242H  | 1920x1080 | 520x290mm  | 23.4 | 2008 | 1EC1D |
| Ancor Com... | ACI24F4 | ASUS 24T1E   | 1920x1080 | 520x290mm  | 23.4 | 2010 | 672D7 |
| Ancor Com... | ACI24F6 | LS246        | 1920x1080 | 530x300mm  | 24.0 | 2010 | BF9C6 |
| Ancor Com... | ACI24F8 | VE245        | 1920x1080 | 530x300mm  | 24.0 | 2010 | 847EB |
| Ancor Com... | ACI24FA | ASUS VH242   | 1920x1080 | 520x290mm  | 23.4 | 2012 | 0F761 |
| Ancor Com... | ACI24FA | ASUS VH242   | 1920x1080 | 520x290mm  | 23.4 | 2010 | 36E73 |
| Ancor Com... | ACI24FE | LS248        | 1920x1080 | 530x300mm  | 24.0 | 2011 | 04B6D |
| Ancor Com... | ACI25A1 | ASUS PB258   | 2560x1440 | 550x310mm  | 24.9 | 2017 | C6F38 |
| Ancor Com... | ACI25A1 | ASUS PB258   | 2560x1440 | 550x310mm  | 24.9 | 2016 | 55033 |
| Ancor Com... | ACI25F1 | ASUS VE258   | 1920x1080 | 550x310mm  | 24.9 | 2012 | B938C |
| Ancor Com... | ACI25F1 | ASUS VE258   | 1920x1080 | 550x310mm  | 24.9 | 2011 | 77466 |
| Ancor Com... | ACI26A3 | ASUS VK266H  | 1920x1200 | 550x340mm  | 25.5 | 2009 | 76FA0 |
| Ancor Com... | ACI26A4 | ASUS VW266H  | 1920x1200 | 550x340mm  | 25.5 | 2009 | 41AD7 |
| Ancor Com... | ACI2725 | ASUS VG278HE | 1920x1080 | 600x340mm  | 27.2 | 2014 | 1D348 |
| Ancor Com... | ACI27A1 | VS278        | 1920x1080 | 600x340mm  | 27.2 | 2018 | 8E19F |
| Ancor Com... | ACI27A1 | VS278        | 1920x1080 | 600x340mm  | 27.2 | 2017 | 575AB |
| Ancor Com... | ACI27A1 | VS278        | 1920x1080 | 600x340mm  | 27.2 | 2016 | B7A70 |
| Ancor Com... | ACI27A1 | VS278        | 1920x1080 | 600x340mm  | 27.2 | 2015 | DC017 |
| Ancor Com... | ACI27A1 | VS278        | 1920x1080 | 600x340mm  | 27.2 | 2014 | 74787 |
| Ancor Com... | ACI27A3 | ASUS PB278   | 2560x1440 | 600x340mm  | 27.2 | 2014 | AFA07 |
| Ancor Com... | ACI27A3 | ASUS PB278   | 2560x1440 | 600x340mm  | 27.2 | 2013 | 33425 |
| Ancor Com... | ACI27A3 | ASUS PB278   | 2560x1440 | 600x340mm  | 27.2 | 2012 | 28040 |
| Ancor Com... | ACI27A5 | MX27AQ       | 2560x1440 | 600x340mm  | 27.2 | 2015 | 813A4 |
| Ancor Com... | ACI27A6 | ASUS PB279   | 3840x2160 | 600x340mm  | 27.2 | 2015 | F6DA7 |
| Ancor Com... | ACI27A7 | ASUS MG279   | 2560x1440 | 600x340mm  | 27.2 | 2016 | 4CD28 |
| Ancor Com... | ACI27A7 | ASUS MG279   | 2560x1440 | 600x340mm  | 27.2 | 2015 | 3344B |
| Ancor Com... | ACI27B2 | PA279        | 2560x1440 | 600x340mm  | 27.2 | 2013 | 347BE |
| Ancor Com... | ACI27C2 | ASUS VG27A   | 1920x1080 | 600x340mm  | 27.2 | 2012 | 34167 |
| Ancor Com... | ACI27C3 | MX279        | 1920x1080 | 600x340mm  | 27.2 | 2017 | F7E6C |
| Ancor Com... | ACI27C3 | MX279        | 1920x1080 | 600x340mm  | 27.2 | 2014 | 0A4CD |
| Ancor Com... | ACI27C3 | MX279        | 1920x1080 | 600x340mm  | 27.2 | 2013 | 2A800 |
| Ancor Com... | ACI27C3 | MX279        | 1920x1080 | 600x340mm  | 27.2 | 2012 | 9980E |
| Ancor Com... | ACI27C4 | VC279        | 1920x1080 | 600x340mm  | 27.2 | 2017 | A09BF |
| Ancor Com... | ACI27C4 | VC279        | 1920x1080 | 600x340mm  | 27.2 | 2016 | 24016 |
| Ancor Com... | ACI27C8 | ASUS VP278   | 1920x1080 | 600x340mm  | 27.2 | 2018 | 9430E |
| Ancor Com... | ACI27C8 | ASUS VP278   | 1920x1080 | 600x340mm  | 27.2 | 2016 | AFFDF |
| Ancor Com... | ACI27E2 | ASUS VG278HR | 1920x1080 | 600x340mm  | 27.2 | 2013 | 25A7D |
| Ancor Com... | ACI27E3 | ASUS VG278HE | 1920x1080 | 600x340mm  | 27.2 | 2012 | 10976 |
| Ancor Com... | ACI27E4 | VX279        | 1920x1080 | 600x340mm  | 27.2 | 2015 | 77BA9 |
| Ancor Com... | ACI27E4 | ASUS VX279   | 1920x1080 | 600x340mm  | 27.2 | 2015 | ECC5D |
| Ancor Com... | ACI27E4 | ASUS VX279   | 1920x1080 | 600x340mm  | 27.2 | 2014 | 03D25 |
| Ancor Com... | ACI27E4 | VX279        | 1920x1080 | 600x340mm  | 27.2 | 2014 | 432F2 |
| Ancor Com... | ACI27E4 | VX279        | 1920x1080 | 600x340mm  | 27.2 | 2013 | 52D87 |
| Ancor Com... | ACI27F4 | ASUS VE276   | 1920x1080 | 600x340mm  | 27.2 | 2011 | 17BDC |
| Ancor Com... | ACI27F6 | ASUS VE278   | 1920x1080 | 600x340mm  | 27.2 | 2014 | D942F |
| Ancor Com... | ACI27F6 | ASUS VE278   | 1920x1080 | 600x340mm  | 27.2 | 2013 | 36A1D |
| Ancor Com... | ACI27F6 | ASUS VE278   | 1920x1080 | 600x340mm  | 27.2 | 2011 | 67D32 |
| Ancor Com... | ACI27F7 | ASUS VK278   | 1920x1080 | 600x340mm  | 27.2 | 2013 | D811D |
| Ancor Com... | ACI27F7 | ASUS VK278   | 1920x1080 | 600x340mm  | 27.2 | 2012 | EF5B3 |
| Ancor Com... | ACI27F7 | ASUS VK278   | 1920x1080 | 600x340mm  | 27.2 | 2011 | 43084 |
| Ancor Com... | ACI28A1 | ASUS VN289   | 1920x1080 | 620x340mm  | 27.8 | 2014 | 0180C |
| Ancor Com... | ACI28A1 | ASUS VN289   | 1920x1080 | 620x340mm  | 27.8 | 2013 | 4DEA9 |
| Ancor Com... | ACI28A3 | ASUS PB287Q  | 3840x2160 | 620x340mm  | 27.8 | 2017 | 7A8DB |
| Ancor Com... | ACI28A3 | ASUS PB287Q  | 3840x2160 | 620x340mm  | 27.8 | 2015 | 439C9 |
| Ancor Com... | ACI28A3 | ASUS PB287Q  | 3840x2160 | 620x340mm  | 27.8 | 2014 | DCFCA |
| Ancor Com... | ACI28A7 | ASUS MG28U   | 3840x2160 | 620x340mm  | 27.8 | 2017 | BA368 |
| Ancor Com... | ACI28A7 | ASUS MG28U   | 3840x2160 | 620x340mm  | 27.8 | 2016 | FF38A |
| Ancor Com... | ACI2931 | ASUS MX299   | 2560x1080 | 670x280mm  | 28.6 | 2013 | AEFE9 |
| Ancor Com... | ACI2932 | ASUS PB298   | 2560x1080 | 670x280mm  | 28.6 | 2013 | 03778 |
| Ancor Com... | ACI32A5 | PB328        | 2560x1440 | 710x400mm  | 32.1 | 2017 | 84C32 |
| Ancor Com... | ACI32A5 | PB328        | 2560x1440 | 710x400mm  | 32.1 | 2016 | 1B713 |
| Ancor Com... | ACI3433 | ROG PG348Q   | 3440x1440 | 800x330mm  | 34.1 | 2017 | 74EDB |
| Ancor Com... | ACIA056 | ET2702       | 2560x1440 | 600x340mm  | 27.2 | 2013 | 1CC6D |
| Ancor Com... | ACIE310 | ET2400I      | 1920x1080 | 520x300mm  | 23.6 | 2010 | 65B3A |
| Ancor Com... | ACIE330 | ET2032I_FHD  | 1920x1080 | 480x270mm  | 21.7 | 2013 | 4D061 |
| Ancor Com... | ACIE330 | ET2221A      | 1920x1080 | 480x270mm  | 21.7 | 2012 | 45A78 |
| Ancor Com... | ACIE340 | ET2700I      | 1920x1080 | 590x330mm  | 26.6 | 2010 | 8F16B |
| Ancor Com... | ACIE340 | ET2400IGTS   | 1920x1080 | 520x300mm  | 23.6 | 2010 | A75B7 |
| Ancor Com... | ACIFF24 | PA246        | 1920x1200 | 520x320mm  | 24.0 | 2011 | 1909A |
| Apple        | APP9219 | Cinema Di... | 1680x1050 | 430x270mm  | 20.0 |      | D693E |
| Apple        | APP921B | Cinema       | 1680x1050 | 430x270mm  | 20.0 | 2008 | 2EB00 |
| Apple        | APP921C | Cinema HD    | 1920x1200 | 490x310mm  | 22.8 | 2007 | 5C2C2 |
| Apple        | APP921D | Cinema Di... | 1680x1050 | 430x270mm  | 20.0 |      | 37783 |
| Apple        | APP921E | Cinema HD... | 1920x1200 | 490x310mm  | 22.8 |      | 6C0EF |
| Apple        | APP921F | Cinema HD... | 1920x1200 | 490x310mm  | 22.8 |      | 0427E |
| Apple        | APP9221 | Cinema HD    | 2560x1600 | 640x400mm  | 29.7 | 2007 | CBFEA |
| Apple        | APP9222 | Cinema       | 1680x1050 | 430x270mm  | 20.0 | 2006 | D54A3 |
| Apple        | APP9223 | Cinema HD    | 1920x1200 | 490x310mm  | 22.8 | 2006 | 588EE |
| Apple        | APP9223 | Cinema HD    | 1920x1200 | 490x310mm  | 22.8 |      | 95B81 |
| Apple        | APP9226 | LED Cinema   | 2560x1440 | 600x340mm  | 27.2 | 2013 | 85A4F |
| Apple        | APP9226 | LED Cinema   | 2560x1440 | 600x340mm  | 27.2 | 2011 | 20567 |
| Apple        | APP9227 | Thunderbolt  | 2560x1440 | 600x340mm  | 27.2 | 2013 | A90F2 |
| Apple        | APP9232 | Cinema HD    | 2560x1600 | 640x400mm  | 29.7 | 2006 | 63AEE |
| Apple        | APP9236 | LED Cinema   | 1920x1200 | 520x320mm  | 24.0 | 2009 | 10EA0 |
| Apple        | APP9C56 | LTN154X7 ... | 1440x900  | 340x220mm  | 15.9 | 2006 | 91D30 |
| Apple        | APP9C59 | LM201W01-... | 1680x1050 | 430x270mm  | 20.0 |      | 5C75D |
| Apple        | APP9C5A | LTM201M1 ... | 1680x1050 | 430x270mm  | 20.0 |      | C6F5F |
| Apple        | APP9C5B | LTN133W1 ... | 1280x800  | 290x180mm  | 13.4 | 2006 | 05537 |
| Apple        | APP9C5C | B133EW01 ... | 1280x800  | 290x180mm  | 13.4 | 2006 | 74C83 |
| Apple        | APP9C5E | N133I1-L0... | 1280x800  | 290x190mm  | 13.6 | 2006 | 4A0B5 |
| Apple        | APP9C5F | LP133WX1-... | 1280x800  | 290x180mm  | 13.4 | 2006 | 9DC2A |
| Apple        | APP9C60 | B154PW01 ... | 1440x900  | 340x220mm  | 15.9 | 2006 | 03B52 |
| Apple        | APP9C6A | LM201WE3-... | 1680x1050 | 430x270mm  | 20.0 | 2006 | FDDC6 |
| Apple        | APP9C6B | M201EW02 ... | 1680x1050 | 430x270mm  | 20.0 | 2006 | BAEB1 |
| Apple        | APP9C6C | LM240WU2-... | 1920x1200 | 520x320mm  | 24.0 | 2007 | A6C09 |
| Apple        | APP9C73 | B133EW03 ... | 1280x800  | 290x180mm  | 13.4 | 2007 | 0F1B9 |
| Apple        | APP9C84 | LP154WP3-... | 1440x900  | 330x210mm  | 15.4 | 2008 | 3D1D4 |
| Apple        | APP9C89 | LP133WX2-... | 1280x800  | 290x180mm  | 13.4 | 2008 | 98C47 |
| Apple        | APP9C8B | N133I6-L0... | 1280x800  | 290x190mm  | 13.6 | 2008 | 5C423 |
| Apple        | APP9C8C | B133EW04 ... | 1280x800  | 290x180mm  | 13.4 | 2008 | 7EA43 |
| Apple        | APP9C8E | LM240WU2-... | 1920x1200 | 520x320mm  | 24.0 | 2007 | 48CC8 |
| Apple        | APP9C93 | LM201WE3-... | 1680x1050 | 430x270mm  | 20.0 | 2008 | F57E3 |
| Apple        | APP9C96 | LM240WU2-... | 1920x1200 | 520x320mm  | 24.0 | 2008 | 79D97 |
| Apple        | APP9C98 | LP171WU6-... | 1920x1200 | 370x230mm  | 17.2 | 2008 | 3E3B9 |
| Apple        | APP9CA0 | N133I6-L0... | 1280x800  | 290x190mm  | 13.6 | 2009 | EA742 |
| Apple        | APP9CA3 | LP154WP4-... | 1440x900  | 330x210mm  | 15.4 | 2009 | C585F |
| Apple        | APP9CA4 | LTN154BT0... | 1440x900  | 330x210mm  | 15.4 | 2009 | 2D9F3 |
| Apple        | APP9CB5 | Color LCD    | 2560x1440 | 600x340mm  | 27.2 | 2009 | 3C4B2 |
| Apple        | APP9CB7 | LTN154MT0... | 1680x1050 | 330x210mm  | 15.4 | 2009 | EC7BD |
| Apple        | APP9CBC | LM215WF3-... | 1920x1080 | 480x270mm  | 21.7 | 2009 | DA53C |
| Apple        | APP9CBD | LP133WX3-... | 1280x800  | 290x180mm  | 13.4 | 2009 | 4F271 |
| Apple        | APP9CBE | LP133WX2-... | 1280x800  | 290x180mm  | 13.4 | 2009 | 18F6E |
| Apple        | APP9CC0 | LTN133AT0... | 1280x800  | 290x180mm  | 13.4 | 2009 | A4EC2 |
| Apple        | APP9CC3 | LP133WX3-... | 1280x800  | 290x180mm  | 13.4 | 2009 | 5CA75 |
| Apple        | APP9CC5 | LP133WX3-... | 1280x800  | 290x180mm  | 13.4 | 2009 | 2FE6B |
| Apple        | APP9CC7 | LTN133AT0... | 1280x800  | 290x180mm  | 13.4 | 2009 | B8DBE |
| Apple        | APP9CCB | B133EW07 ... | 1280x800  | 290x180mm  | 13.4 | 2009 | EEFC1 |
| Apple        | APP9CCD | LP171WU6-... | 1920x1200 | 370x230mm  | 17.2 | 2009 | 59553 |
| Apple        | APP9CD1 | B133EW04 ... | 1280x800  | 290x180mm  | 13.4 | 2009 | EF4B2 |
| Apple        | APP9CD7 | Color LCD    | 2560x1440 | 600x340mm  | 27.2 | 2010 | 33C1A |
| Apple        | APP9CDF | LP133WP1-... | 1440x900  | 290x180mm  | 13.4 | 2011 | C3CC2 |
| Apple        | APP9CDF | LP133WP1-... | 1440x900  | 290x180mm  | 13.4 | 2010 | 728B1 |
| Apple        | APP9CF0 | LSN133BT0... | 1440x900  | 290x180mm  | 13.4 | 2012 | BB006 |
| Apple        | APP9CF0 | LTH133BT0... | 1440x900  | 290x180mm  | 13.4 | 2011 | 6A049 |
| Apple        | APP9CF0 | LTH133BT0... | 1440x900  | 290x180mm  | 13.4 | 2010 | 8504C |
| Apple        | APP9CF2 | LP116WH4-... | 1366x768  | 260x140mm  | 11.6 | 2011 | 25CD5 |
| Apple        | APP9CF3 | LTH116AT0... | 1366x768  | 260x140mm  | 11.6 | 2011 | 9DAA6 |
| Apple        | APP9CF3 | LTH116AT0... | 1366x768  | 260x140mm  | 11.6 | 2010 | 7CA85 |
| Apple        | APPA005 | LP154WE3-... | 1680x1050 | 330x210mm  | 15.4 | 2010 | 82D50 |
| Apple        | APPA007 | iMac         | 2560x1440 | 600x340mm  | 27.2 | 2010 | 59AC1 |
| Apple        | APPA00C | iMac         | 1920x1080 | 480x270mm  | 21.7 | 2010 | 78938 |
| Apple        | APPA00E | Color LCD    | 2880x1800 | 330x210mm  | 15.4 | 2011 | 99461 |
| Apple        | APPA00F | Color LCD    | 2880x1800 | 330x210mm  | 15.4 | 2011 | A83DF |
| Apple        | APPA010 | B116XW05 ... | 1366x768  | 260x140mm  | 11.6 | 2011 | 4F827 |
| Apple        | APPA010 | B116XW05 ... | 1366x768  | 260x140mm  | 11.6 | 2011 | 7A549 |
| Apple        | APPA012 | iMac         | 1920x1080 | 480x270mm  | 21.7 | 2012 | E7F2A |
| Apple        | APPA018 | Color LCD    | 2560x1600 | 290x180mm  | 13.4 | 2012 | FD836 |
| Apple        | APPA019 | Color LCD    | 2880x1800 | 330x210mm  | 15.4 | 2012 | BD1FC |
| Apple        | APPA01B | NT133WGB-... | 1440x900  | 290x180mm  | 13.4 | 2015 | 8A133 |
| Apple        | APPA020 | Color LCD    | 2560x1600 | 290x180mm  | 13.4 | 2013 | 60734 |
| Apple        | APPA022 | Color LCD    | 2880x1800 | 330x210mm  | 15.4 | 2013 | 3BB95 |
| Apple        | APPA029 | Color LCD    | 2560x1600 | 290x180mm  | 13.4 | 2013 | 13406 |
| Apple        | APPA02A | Color LCD    | 2560x1600 | 290x180mm  | 13.4 | 2014 | 2259E |
| Apple        | APPA02E | Color LCD    | 2880x1800 | 330x210mm  | 15.4 | 2015 | 6AF0F |
| Apple        | APPA02F | Color LCD    | 2880x1800 | 330x210mm  | 15.4 | 2015 | 237D5 |
| Apple        | APPA030 | Color LCD    | 2880x1800 | 330x210mm  | 15.4 | 2015 | EA896 |
| Apple        | APPA034 | Color LCD    | 2880x1800 | 290x180mm  | 13.4 | 2015 | 90D29 |
| Apple        | APPAE01 | iMac         | 3840x2160 | 600x340mm  | 27.2 | 2014 | 6E9D2 |
| Apple        | APPC106 | 097L1JY02... | 2048x1536 | 200x150mm  | 9.8  | 2011 | CE6C8 |
| Arnos Ins... | AIC0023 | H-W22 DVI    | 1680x1050 | 470x300mm  | 22.0 | 2007 | 47E29 |
| Arnos Ins... | AIC1005 | L-W27        | 1920x1080 | 600x340mm  | 27.2 | 2014 | 1787F |
| Arnos Ins... | AIC1014 | LE-24        | 1920x1080 | 520x290mm  | 23.4 | 2016 | D9C92 |
| Arnos Ins... | AIC2274 | E-171 DVI    | 1280x1024 | 340x270mm  | 17.1 | 2006 | C0A24 |
| Arnos Ins... | AIC4190 | F-419        | 1280x1024 | 370x300mm  | 18.8 |      | CB5A1 |
| Ativa        | ATV0000 | MT27         | 1920x1080 | 820x460mm  | 37.0 | 2012 | F069A |
| BBK          | BBK0000 | TV           | 1920x1080 | 575x323mm  | 26.0 | 2015 | 9D65A |
| BBK          | BBK0000 | LCD          | 1920x1080 | 760x450mm  | 34.8 | 2009 | 138FE |
| BBK          | BBK0030 | TV           | 1920x540  | 1150x650mm | 52.0 | 2012 | 31BA3 |
| BBK          | BBK0B01 | TV           | 1920x540  | 700x390mm  | 31.5 | 2010 | 440EB |
| BBK          | BBK0B01 | TV           | 1920x540  | 700x390mm  | 31.5 | 2009 | F087C |
| BBK          | BBK2075 | LCD          | 1680x1050 | 430x270mm  | 20.0 | 2008 | FF32D |
| BBK          | BBK531A | TV           | 1920x1080 | 930x530mm  | 42.1 | 2014 | C3E83 |
| BBK          | BBK9202 | TV           | 1360x768  |            |      | 2014 | 6D38F |
| BOE          | BOE0000 | 18.5JL3      | 1366x768  | 410x230mm  | 18.5 | 2018 | D2DF6 |
| BOE          | BOE03F2 |              | 1280x800  | 220x140mm  | 10.3 | 2015 | 41641 |
| BOE          | BOE0582 | OT HT156W... | 1366x768  | 340x190mm  | 15.3 | 2009 | ABA85 |
| BOE          | BOE0586 | OT HT140W... | 1366x768  | 310x170mm  | 13.9 | 2009 | 7B278 |
| BOE          | BOE0587 | H018T        | 1024x600  | 220x130mm  | 10.1 | 2009 | 7DF8D |
| BOE          | BOE0590 | OT HT140W... | 1366x768  | 310x170mm  | 13.9 | 2010 | 3D700 |
| BOE          | BOE059D | OT HT140W... | 1366x768  | 310x170mm  | 13.9 | 2010 | 7221A |
| BOE          | BOE059E | 1W3CW        | 1366x768  | 310x170mm  | 13.9 | 2010 | 4CC1E |
| BOE          | BOE059F | OT HT140W... | 1366x768  | 310x170mm  | 13.9 | 2010 | 0CC33 |
| BOE          | BOE05A1 | OT HT140W... | 1366x768  | 310x170mm  | 13.9 | 2010 | 6760C |
| BOE          | BOE05B0 | D3MR5        | 1366x768  | 310x170mm  | 13.9 | 2011 | 431B4 |
| BOE          | BOE05B1 | HF HB140W... | 1366x768  | 310x170mm  | 13.9 | 2011 | 0471D |
| BOE          | BOE05B3 | 0GTN1        | 1366x768  | 340x190mm  | 15.3 | 2012 | 94F22 |
| BOE          | BOE05B5 | HF HB140W... | 1366x768  | 310x170mm  | 13.9 | 2012 | 6B628 |
| BOE          | BOE05B9 | HF BA101W... | 1024x600  | 220x130mm  | 10.1 | 2012 | 2E8B0 |
| BOE          | BOE05BA | HF HB140W... | 1366x768  | 310x170mm  | 13.9 | 2012 | 7BE1F |
| BOE          | BOE05BB | HF HB156W... | 1366x768  | 340x190mm  | 15.3 | 2012 | DF9A2 |
| BOE          | BOE05BC | HF HB156W... | 1366x768  | 340x190mm  | 15.3 | 2012 | 0EBD1 |
| BOE          | BOE05C2 | J7P58        | 1366x768  | 310x170mm  | 13.9 | 2012 | 5CB00 |
| BOE          | BOE05C7 | HF HB140W... | 1366x768  | 310x170mm  | 13.9 | 2012 | E309E |
| BOE          | BOE05C9 | HF HB140W... | 1366x768  | 310x170mm  | 13.9 | 2012 | 719D8 |
| BOE          | BOE05D7 | HF HN133W... | 1920x1080 | 290x170mm  | 13.2 | 2012 | EEAEB |
| BOE          | BOE05DA | 31R70        | 1366x768  | 280x160mm  | 12.7 | 2013 | D7A41 |
| BOE          | BOE05DF | F9RHP        | 1366x768  | 290x160mm  | 13.0 | 2014 | 20848 |
| BOE          | BOE05DF | HF HB133W... | 1366x768  | 290x160mm  | 13.0 | 2012 | 042BE |
| BOE          | BOE05E3 | HF HN133W... | 1920x1080 | 290x170mm  | 13.2 | 2013 | 80B1B |
| BOE          | BOE05E5 | OYMOW        | 1366x768  | 310x170mm  | 13.9 | 2013 | 3D356 |
| BOE          | BOE05E9 | HF HN116W... | 1366x768  | 250x140mm  | 11.3 | 2012 | 40572 |
| BOE          | BOE05EA | DT HB140W... | 1366x768  | 310x170mm  | 13.9 | 2013 | 3AE78 |
| BOE          | BOE05EC | DT HB140W... | 1366x768  | 310x170mm  | 13.9 | 2013 | 96531 |
| BOE          | BOE05EF | 58F5Y        | 1366x768  | 310x170mm  | 13.9 | 2013 | 773B1 |
| BOE          | BOE05F0 | DT HB140W... | 1366x768  | 310x170mm  | 13.9 | 2014 | 3FB8E |
| BOE          | BOE05F1 | W7GVR        | 1366x768  | 310x170mm  | 13.9 | 2013 | 5F948 |
| BOE          | BOE05F4 | Y2HM9        | 1366x768  | 280x160mm  | 12.7 | 2014 | 45242 |
| BOE          | BOE05F6 | 9TWF0        | 1366x768  | 310x170mm  | 13.9 | 2013 | EB929 |
| BOE          | BOE05FE | DT HB140W... | 1366x768  | 310x170mm  | 13.9 | 2013 | 8A95A |
| BOE          | BOE0600 | DT HB140W... | 1366x768  | 310x170mm  | 13.9 | 2014 | 67642 |
| BOE          | BOE0602 |              | 1366x768  | 310x170mm  | 13.9 | 2014 | 13B71 |
| BOE          | BOE0607 |              | 1366x768  | 260x150mm  | 11.8 | 2014 | D58A2 |
| BOE          | BOE0608 | DT NT116W... | 1366x768  | 260x150mm  | 11.8 | 2014 | 48C5E |
| BOE          | BOE0609 | DT NT116W... | 1366x768  | 260x150mm  | 11.8 | 2015 | 5FD1C |
| BOE          | BOE0610 | HF HB156F... | 1920x1080 | 340x190mm  | 15.3 | 2014 | 4B38F |
| BOE          | BOE0614 |              | 1366x768  | 340x190mm  | 15.3 | 2014 | 38640 |
| BOE          | BOE0615 | 1W7NH        | 1366x768  | 340x190mm  | 15.3 | 2014 | 85FCD |
| BOE          | BOE0615 | 2GC9W        | 1366x768  | 340x190mm  | 15.3 | 2014 | E7715 |
| BOE          | BOE0618 |              | 1366x768  | 340x190mm  | 15.3 | 2014 | CDA71 |
| BOE          | BOE061C |              | 1366x768  | 260x150mm  | 11.8 | 2014 | 89859 |
| BOE          | BOE061D | HF NT156W... | 1366x768  | 340x190mm  | 15.3 | 2014 | 66784 |
| BOE          | BOE061E | HF NT156W... | 1366x768  | 340x190mm  | 15.3 | 2014 | 57665 |
| BOE          | BOE0620 | PDJJH        | 1366x768  | 340x190mm  | 15.3 | 2014 | 26B5D |
| BOE          | BOE0623 | RX4T9        | 1366x768  | 260x150mm  | 11.8 | 2015 | A6D56 |
| BOE          | BOE0628 | DT NT116W... | 1366x768  | 260x150mm  | 11.8 | 2014 | CD3D6 |
| BOE          | BOE0629 | 60K2M        | 1366x768  | 310x170mm  | 13.9 | 2014 | 1786F |
| BOE          | BOE0629 | 9YHM5        | 1366x768  | 310x170mm  | 13.9 | 2014 | 2A6C0 |
| BOE          | BOE0629 | KT5M8        | 1366x768  | 310x170mm  | 13.9 | 2014 | FC206 |
| BOE          | BOE062B | HF HB156F... | 1920x1080 | 340x190mm  | 15.3 | 2014 | AF513 |
| BOE          | BOE062F | YHDGT        | 1920x1080 | 340x190mm  | 15.3 | 2015 | 222F7 |
| BOE          | BOE0630 | HF NV156F... | 1920x1080 | 340x190mm  | 15.3 | 2015 | D3D62 |
| BOE          | BOE0632 | VMX8X        | 1920x1080 | 340x190mm  | 15.3 | 2015 | A1D5C |
| BOE          | BOE0637 | DT NV140F... | 1920x1080 | 310x170mm  | 13.9 | 2014 | B886A |
| BOE          | BOE0638 |              | 1920x1080 | 310x170mm  | 13.9 | 2015 | 2815D |
| BOE          | BOE0639 | GN887        | 1920x1080 | 310x170mm  | 13.9 | 2015 | C1154 |
| BOE          | BOE063B |              | 1366x768  | 340x190mm  | 15.3 | 2014 | A62A2 |
| BOE          | BOE0641 |              | 1920x1080 | 340x190mm  | 15.3 | 2014 | FF298 |
| BOE          | BOE0644 |              | 1366x768  | 310x170mm  | 13.9 | 2015 | A70DD |
| BOE          | BOE0648 | CGR5T        | 1920x1080 | 340x190mm  | 15.3 | 2015 | F7420 |
| BOE          | BOE0651 | DT HB140W... | 1366x768  | 310x170mm  | 13.9 | 2015 | D85AD |
| BOE          | BOE0652 | 905VH        | 1920x1080 | 310x170mm  | 13.9 | 2015 | 2D7C9 |
| BOE          | BOE0653 | 6J1Y3        | 1920x1080 | 310x170mm  | 13.9 | 2015 | AD984 |
| BOE          | BOE0654 | HF NT156W... | 1366x768  | 340x190mm  | 15.3 | 2014 | 371E0 |
| BOE          | BOE065D | HF NV156F... | 1920x1080 | 340x190mm  | 15.3 | 2015 | C76E1 |
| BOE          | BOE065E | HF NV156F... | 1920x1080 | 340x190mm  | 15.3 | 2015 | 91005 |
| BOE          | BOE065F | HF NV156F... | 1920x1080 | 340x190mm  | 15.3 | 2015 | DF354 |
| BOE          | BOE0660 |              | 1600x900  | 380x210mm  | 17.1 | 2015 | C5573 |
| BOE          | BOE0661 | VJJ1P        | 1366x768  | 340x190mm  | 15.3 | 2016 | 4EA8D |
| BOE          | BOE0669 | 2DC5R        | 1920x1080 | 290x170mm  | 13.2 | 2015 | 64D09 |
| BOE          | BOE066E | CQ NT156W... | 1366x768  | 340x190mm  | 15.3 | 2015 | AEEBF |
| BOE          | BOE0671 | NCH65        | 1366x768  | 340x190mm  | 15.3 | 2016 | 6C02C |
| BOE          | BOE0672 | CQ NT156W... | 1366x768  | 340x190mm  | 15.3 | 2015 | 82C14 |
| BOE          | BOE0674 | FVGPP        | 1366x768  | 340x190mm  | 15.3 | 2016 | 537C4 |
| BOE          | BOE0675 | CQ NT156W... | 1366x768  | 340x190mm  | 15.3 | 2015 | F55D8 |
| BOE          | BOE0679 |              | 1920x1080 | 340x190mm  | 15.3 | 2015 | 7CCBD |
| BOE          | BOE0684 |              | 1600x900  | 380x210mm  | 17.1 | 2015 | 4A69C |
| BOE          | BOE0685 |              | 1600x900  | 380x210mm  | 17.1 | 2015 | D4C79 |
| BOE          | BOE0686 | CQ NT156F... | 1920x1080 | 340x190mm  | 15.3 | 2015 | D432D |
| BOE          | BOE0687 |              | 1920x1080 | 340x190mm  | 15.3 | 2015 | B6A39 |
| BOE          | BOE068A | CQ NT156F... | 1920x1080 | 340x190mm  | 15.3 | 2016 | AAF07 |
| BOE          | BOE0690 | C1JFR        | 1920x1080 | 340x190mm  | 15.3 | 2015 | 429BC |
| BOE          | BOE0690 | CQ NT156F... | 1920x1080 | 340x190mm  | 15.3 | 2015 | 8CD77 |
| BOE          | BOE0690 | 07TVD        | 1920x1080 | 340x190mm  | 15.3 | 2015 | D3AE0 |
| BOE          | BOE0691 |              | 1920x1080 | 280x160mm  | 12.7 | 2015 | E5E85 |
| BOE          | BOE0694 |              | 1920x1080 | 380x210mm  | 17.1 | 2015 | 429E7 |
| BOE          | BOE0695 | 99D49        | 1920x1080 | 380x210mm  | 17.1 | 2016 | 4A163 |
| BOE          | BOE0696 |              | 1366x768  | 310x170mm  | 13.9 | 2015 | BA4B5 |
| BOE          | BOE0697 |              | 1366x768  | 310x170mm  | 13.9 | 2015 | 8DB31 |
| BOE          | BOE0698 | CQ NT140W... | 1366x768  | 310x170mm  | 13.9 | 2015 | 2B0E3 |
| BOE          | BOE069A |              | 1366x768  | 310x170mm  | 13.9 | 2015 | 79770 |
| BOE          | BOE069B |              | 1600x900  | 380x210mm  | 17.1 | 2015 | 164AA |
| BOE          | BOE069C | CQ NT156F... | 1920x1080 | 340x190mm  | 15.3 | 2015 | C924C |
| BOE          | BOE069E | 8VPR0        | 1600x900  | 380x210mm  | 17.1 | 2016 | A8829 |
| BOE          | BOE06A4 |              | 1366x768  | 340x190mm  | 15.3 | 2015 | 731BA |
| BOE          | BOE06A4 | NT156WHM-N32 | 1366x768  | 340x190mm  | 15.3 | 2015 | AAFF2 |
| BOE          | BOE06A5 | NT156WHM-N42 | 1366x768  | 340x190mm  | 15.3 | 2015 | 22B6B |
| BOE          | BOE06A5 | CQ NT156W... | 1366x768  | 340x190mm  | 15.3 | 2015 | 582AE |
| BOE          | BOE06A5 |              | 1366x768  | 340x190mm  | 15.3 | 2015 | 852DB |
| BOE          | BOE06A6 | 3N1N0        | 1366x768  | 310x170mm  | 13.9 | 2015 | CB154 |
| BOE          | BOE06A7 | 6NKDX        | 1920x1080 | 290x170mm  | 13.2 | 2015 | 21DDD |
| BOE          | BOE06A7 | 4F5HT        | 1920x1080 | 290x170mm  | 13.2 | 2015 | F1638 |
| BOE          | BOE06A9 | X0KFK        | 1920x1080 | 340x190mm  | 15.3 | 2016 | BE634 |
| BOE          | BOE06A9 | 4561N        | 1920x1080 | 340x190mm  | 15.3 | 2015 | 797F2 |
| BOE          | BOE06AC | DT NV140F... | 1920x1080 | 310x170mm  | 13.9 | 2015 | E1F60 |
| BOE          | BOE06B3 | CQ NV140F... | 1920x1080 |            |      | 2016 | 01ABC |
| BOE          | BOE06B3 | CQ NT140W... | 1366x768  | 310x170mm  | 13.9 | 2015 | 0DC8C |
| BOE          | BOE06B4 | 84V7R        | 1920x1080 | 340x190mm  | 15.3 | 2016 | 1162D |
| BOE          | BOE06B6 | 0C8WJ        | 1366x768  | 310x170mm  | 13.9 | 2016 | 6A98C |
| BOE          | BOE06B7 | CQ NV133F... | 1920x1080 | 290x170mm  | 13.2 | 2016 | 11ACD |
| BOE          | BOE06B8 | HF NV133F... | 1920x1080 | 290x170mm  | 13.2 | 2016 | 3F7A1 |
| BOE          | BOE06B9 | HF NV156F... | 1920x1080 | 340x190mm  | 15.3 | 2016 | 4E3EE |
| BOE          | BOE06BA | CQ NT156F... | 1920x1080 | 340x190mm  | 15.3 | 2016 | FD430 |
| BOE          | BOE06BB | CQ NT140F... | 1920x1080 | 310x170mm  | 13.9 | 2016 | 3E6A2 |
| BOE          | BOE06BD | CQ NT140W... | 1366x768  | 310x170mm  | 13.9 | 2016 | 649C3 |
| BOE          | BOE06BE | 2M9wH        | 1920x1080 | 290x170mm  | 13.2 | 2016 | 70310 |
| BOE          | BOE06C0 | 186GC        | 1920x1080 | 280x160mm  | 12.7 | 2016 | 2B119 |
| BOE          | BOE06C3 |              | 3840x2160 | 340x190mm  | 15.3 | 2016 | A73DE |
| BOE          | BOE06C6 | CQ NV156F... | 1920x1080 | 340x190mm  | 15.3 | 2016 | 0BA4C |
| BOE          | BOE06D0 | HF NV126A... | 2880x1920 | 260x170mm  | 12.2 | 2016 | A5BED |
| BOE          | BOE06D3 | CQ NT156W... | 1366x768  | 340x190mm  | 15.3 | 2016 | 3D19B |
| BOE          | BOE06D7 | HF NV156Q... | 3840x2160 | 350x190mm  | 15.7 | 2016 | 2557A |
| BOE          | BOE06DC | 0KK8X        | 1920x1280 | 260x170mm  | 12.2 | 2016 | 72359 |
| BOE          | BOE06DF | CQ HV140F... | 1920x1080 | 310x170mm  | 13.9 | 2016 | 1574D |
| BOE          | BOE06E1 | CQ NV133F... | 1920x1080 | 290x170mm  | 13.2 | 2016 | FBD44 |
| BOE          | BOE06E2 | CJ5JM        | 1920x1080 | 310x170mm  | 13.9 | 2016 | 3D08C |
| BOE          | BOE06E4 | 1RHN9        | 1366x768  | 260x140mm  | 11.6 | 2016 | 6D24A |
| BOE          | BOE06E7 | VKCV8        | 1366x768  | 310x170mm  | 13.9 | 2016 | 3FB3C |
| BOE          | BOE06EB |              | 1920x1080 | 310x170mm  | 13.9 | 2016 | 8CDAD |
| BOE          | BOE06EE | 6HY1W        | 1920x1080 | 310x170mm  | 13.9 | 2016 | 66C17 |
| BOE          | BOE06F0 |              | 1366x768  | 340x190mm  | 15.3 | 2016 | 7FA71 |
| BOE          | BOE06F1 | MH98N        | 1920x1080 | 340x190mm  | 15.3 | 2016 | 352FC |
| BOE          | BOE06F2 | CQ NV140F... | 1920x1080 | 310x170mm  | 13.9 | 2016 | D7D89 |
| BOE          | BOE06F3 | CQ NV140F... | 1920x1080 | 310x170mm  | 13.9 | 2016 | D8027 |
| BOE          | BOE06F4 | HF NV156Q... | 3840x2160 | 350x190mm  | 15.7 | 2016 | 6EB6E |
| BOE          | BOE06F9 |              | 1920x1080 | 340x190mm  | 15.3 | 2016 | EA911 |
| BOE          | BOE06FA | CQ NV133F... | 1920x1080 | 290x170mm  | 13.2 | 2017 | 4141A |
| BOE          | BOE06FB | CQ NV156F... | 1920x1080 | 340x190mm  | 15.3 | 2016 | C794A |
| BOE          | BOE06FF | CQ NV156F... | 1920x1080 | 340x190mm  | 15.3 | 2016 | 32812 |
| BOE          | BOE0700 | CQ NV156F... | 1920x1080 | 340x190mm  | 15.3 | 2016 | F8A3B |
| BOE          | BOE0702 | YG0NR        | 1366x768  | 340x190mm  | 15.3 | 2016 | A1AEC |
| BOE          | BOE0703 | CQ NV156F... | 1920x1080 | 340x190mm  | 15.3 | 2016 | 75A2F |
| BOE          | BOE0704 | CQ NT156W... | 1366x768  | 340x190mm  | 15.3 | 2016 | CFAC7 |
| BOE          | BOE0705 | CQ NT140W... | 1366x768  | 310x170mm  | 13.9 | 2016 | 10A05 |
| BOE          | BOE070C | CQ TV156F... | 1920x1080 | 340x190mm  | 15.3 | 2016 | 19B89 |
| BOE          | BOE070D | NM2N3        | 1366x768  | 310x170mm  | 13.9 | 2016 | 380A7 |
| BOE          | BOE070E |              | 1920x1080 | 290x170mm  | 13.2 | 2016 | E4F8C |
| BOE          | BOE0713 | 7J92R        | 1920x1080 | 340x190mm  | 15.3 | 2016 | 87C25 |
| BOE          | BOE0714 |              | 1920x1080 | 310x170mm  | 13.9 | 2016 | 821B8 |
| BOE          | BOE0718 | NV140FHM-N62 | 1920x1080 | 310x170mm  | 13.9 | 2016 | BDB13 |
| BOE          | BOE071A | G64PY        | 1920x1080 | 290x170mm  | 13.2 | 2017 | 98BC8 |
| BOE          | BOE071F | HF NV156Q... | 3840x2160 | 340x190mm  | 15.3 | 2016 | 2C730 |
| BOE          | BOE0729 | CQ NT156F... | 1920x1080 | 340x190mm  | 15.3 | 2017 | 412B7 |
| BOE          | BOE072A | HJGCP        | 1366x768  | 310x170mm  | 13.9 | 2016 | 094C1 |
| BOE          | BOE072C |              | 1920x1080 | 310x170mm  | 13.9 | 2017 | 9E32E |
| BOE          | BOE0730 |              | 3840x2160 | 340x190mm  | 15.3 | 2016 | 584D7 |
| BOE          | BOE0731 | CQ NT116W... | 1366x768  | 260x140mm  | 11.6 | 2017 | 514EC |
| BOE          | BOE0739 |              | 1920x1080 | 340x190mm  | 15.3 | 2017 | 304C8 |
| BOE          | BOE0741 | NT116WHM-N44 | 1366x768  | 250x140mm  | 11.3 | 2017 | C028E |
| BOE          | BOE0742 | CQ NV140F... | 1920x1080 | 310x170mm  | 13.9 | 2017 | 8E6DF |
| BOE          | BOE0747 | CQ NV156F... | 1920x1080 | 340x190mm  | 15.3 | 2017 | FA75C |
| BOE          | BOE074A |              | 1920x1080 | 290x170mm  | 13.2 | 2017 | 29263 |
| BOE          | BOE074E |              | 1920x1080 | 340x190mm  | 15.3 | 2017 | 40B74 |
| BOE          | BOE0759 | 97G39        | 1366x768  | 340x190mm  | 15.3 | 2017 | AC8AE |
| BOE          | BOE0761 | KRMYH        | 1920x1080 | 340x190mm  | 15.3 | 2017 | EADE4 |
| BOE          | BOE076E |              | 1366x768  | 340x190mm  | 15.3 | 2017 | DC164 |
| BOE          | BOE076F |              | 1366x768  | 340x190mm  | 15.3 | 2017 | CF2F9 |
| BOE          | BOE0783 |              | 1366x768  | 300x170mm  | 13.6 | 2018 | C44FD |
| BOE          | BOE078B | CQ NT156W... | 1366x768  | 340x190mm  | 15.3 | 2017 | DBC24 |
| BOE          | BOE0791 | CQ NV140F... | 1920x1080 | 310x170mm  | 13.9 | 2017 | E3075 |
| BOE          | BOE0792 | CQ NV156F... | 1920x1080 | 340x190mm  | 15.3 | 2017 | CC2DE |
| BOE          | BOE07A1 |              | 1920x1080 | 340x190mm  | 15.3 | 2016 | B5BBF |
| BOE          | BOE07A3 | CQ NT156F... | 1920x1080 | 340x190mm  | 15.3 | 2017 | 96A66 |
| BOE          | BOE07A8 | 60F1N        | 1366x768  | 260x140mm  | 11.6 | 2017 | 646B7 |
| BOE          | BOE07A9 | CQ NV156F... | 1920x1080 | 340x190mm  | 15.3 | 2017 | 340DC |
| BOE          | BOE07B0 | 12CJJ        | 1920x1080 | 340x190mm  | 15.3 | 2018 | 6A872 |
| BOE          | BOE07B4 | CQ NT156W... | 1366x768  | 340x190mm  | 15.3 | 2018 | 9078D |
| BOE          | BOE07B5 |              | 1366x768  | 300x170mm  | 13.6 | 2018 | 9B283 |
| BOE          | BOE07B6 | HF NV173F... | 1920x1080 | 380x210mm  | 17.1 | 2018 | DB9AD |
| BOE          | BOE07C5 | CQ NV156F... | 1920x1080 | 340x190mm  | 15.3 | 2018 | 52A9F |
| BOE          | BOE07C9 | CQ NV140F... | 1920x1080 | 300x170mm  | 13.6 | 2018 | FE577 |
| BOE          | BOE07CE | CQ NT156W... | 1366x768  | 340x190mm  | 15.3 | 2018 | F3772 |
| BOE          | BOE07D8 | CQ NV156F... | 1920x1080 | 340x190mm  | 15.3 | 2018 | CDAB5 |
| BOE          | BOE07F6 | CQ NT140F... | 1920x1080 | 310x170mm  | 13.9 | 2018 | 7CB87 |
| BOE          | BOE0802 | M9P74        | 1920x1080 | 340x190mm  | 15.3 | 2018 | D1588 |
| BOE          | BOE0808 | 9CV35        | 1366x768  | 340x190mm  | 15.3 | 2018 | 60351 |
| BOE          | BOE0810 | 1RN29        | 1920x1080 | 310x170mm  | 13.9 | 2018 | 81EBE |
| BOE          | BOE0826 | 908N0        | 1920x1080 | 340x190mm  | 15.3 | 2018 | 5ED6E |
| BOE          | BOE083C | CQ TV140F... | 1920x1080 | 310x170mm  | 13.9 | 2018 | FB8DE |
| BOE          | BOE084E | NV173FHM-N49 | 1920x1080 | 380x210mm  | 17.1 | 2018 | 1FF03 |
| BOE          | BOE0899 | HYDIS HT1... | 1024x768  | 240x180mm  | 11.8 |      | 7BB27 |
| BOE          | BOE089B | HYDIS HV1... | 1280x800  | 260x160mm  | 12.0 | 2007 | CE7E9 |
| BOE          | BOE08A0 |              | 1280x800  | 260x160mm  | 12.0 | 2010 | C89A8 |
| BOE          | BOE08A0 |              | 1280x800  | 260x160mm  | 12.0 | 2009 | 131F9 |
| Beko         | BEK3233 | B22WVJAZ1    | 1680x1050 | 470x270mm  | 21.3 |      | 1145D |
| Beko         | BEK3233 | VJAZ1        | 1280x720  | 700x400mm  | 31.7 |      | 268E5 |
| Belinea      | MAX05DE | B101556 M... | 1024x768  | 300x230mm  | 14.9 |      | 4989D |
| Belinea      | MAX06B6 |              | 1280x1024 | 340x270mm  | 17.1 |      | 9A276 |
| Belinea      | MAX077F | B101920      | 1280x1024 | 370x300mm  | 18.8 |      | FDBB2 |
| Belinea      | MAX0792 | B1980G1      | 1280x1024 | 380x310mm  | 19.3 | 2007 | 80ADB |
| Belinea      | MAX0793 |              | 1280x1024 | 380x300mm  | 19.1 | 2007 | D9978 |
| Belinea      | MAX07D8 | 2080S1 MA... | 1600x1200 | 410x310mm  | 20.2 | 2006 | 46BCE |
| Belinea      | MAX07D9 | B2080S2      | 1600x1200 | 410x310mm  | 20.2 | 2007 | 862E0 |
| Belinea      | MAX0835 | MX-26X3      | 1920x1080 | 580x320mm  | 26.1 |      | D8189 |
| Belinea      | MAX089F |              | 1680x1050 | 470x300mm  | 22.0 | 2007 | A5B36 |
| BenQ         | BNQ08C1 | SE2241       | 1920x1080 | 480x270mm  | 21.7 | 2009 | 9D37A |
| BenQ         | BNQ4002 | PJ           | 1600x1200 |            |      | 2013 | 858CB |
| BenQ         | BNQ7587 | ML2241       | 1920x1080 | 480x270mm  | 21.7 | 2010 | 021D3 |
| BenQ         | BNQ7588 | ML2441       | 1920x1080 | 530x300mm  | 24.0 | 2010 | 19568 |
| BenQ         | BNQ7591 | MK2443       | 1920x1080 | 530x300mm  | 24.0 | 2010 | 0CA22 |
| BenQ         | BNQ75A1 |              | 1920x1080 | 530x300mm  | 24.0 | 2011 | 5722C |
| BenQ         | BNQ7668 | FP783        | 1280x1024 | 340x270mm  | 17.1 |      | 12D14 |
| BenQ         | BNQ7675 | T903         | 1280x1024 | 380x300mm  | 19.1 |      | AB319 |
| BenQ         | BNQ7678 | FP785        | 1280x1024 | 340x270mm  | 17.1 |      | A1A3D |
| BenQ         | BNQ7685 | FP937s       | 1280x1024 | 380x300mm  | 19.1 |      | 4D87F |
| BenQ         | BNQ7697 | FP91G        | 1280x1024 | 380x300mm  | 19.1 |      | 23231 |
| BenQ         | BNQ7699 | FP937s+      | 1280x1024 | 380x300mm  | 19.1 |      | A1B2C |
| BenQ         | BNQ76A6 | FP91G+       | 1280x1024 | 380x300mm  | 19.1 | 2006 | 57462 |
| BenQ         | BNQ76A6 | FP91G+       | 1280x1024 | 380x300mm  | 19.1 |      | 1E8BC |
| BenQ         | BNQ76B1 | T90X         | 1280x1024 | 380x300mm  | 19.1 | 2006 | 31C46 |
| BenQ         | BNQ76B1 | T90X         | 1280x1024 | 380x300mm  | 19.1 |      | 7FC5A |
| BenQ         | BNQ76BA | FP92G+       | 1280x1024 | 380x300mm  | 19.1 |      | 08948 |
| BenQ         | BNQ76BC | FP91GP       | 1280x1024 | 380x300mm  | 19.1 |      | 2011F |
| BenQ         | BNQ76BE | FP91GX       | 1280x1024 | 380x300mm  | 19.1 | 2006 | 1133C |
| BenQ         | BNQ76BE | FP91GX       | 1280x1024 | 380x300mm  | 19.1 |      | 4E8C5 |
| BenQ         | BNQ76BF | FP71G+       | 1280x1024 | 340x270mm  | 17.1 | 2006 | 10E4B |
| BenQ         | BNQ76C1 | FP71G X      | 1280x1024 | 340x270mm  | 17.1 |      | 105B4 |
| BenQ         | BNQ76C3 | FP202W       | 1680x1050 | 430x270mm  | 20.0 | 2007 | B74EA |
| BenQ         | BNQ76C3 | FP202W       | 1680x1050 | 430x270mm  | 20.0 | 2006 | 10812 |
| BenQ         | BNQ76C8 | FP93G        | 1280x1024 | 380x300mm  | 19.1 | 2007 | 19392 |
| BenQ         | BNQ76CC | FP95G        | 1280x1024 | 380x300mm  | 19.1 | 2007 | 8EF53 |
| BenQ         | BNQ76CE | FP222WH      | 1680x1050 | 470x300mm  | 22.0 | 2007 | BB3CA |
| BenQ         | BNQ76D0 | FP94VW       | 1440x900  | 400x250mm  | 18.6 | 2007 | 20CA7 |
| BenQ         | BNQ76D1 | FP73G        | 1280x1024 | 340x270mm  | 17.1 | 2007 | EDABA |
| BenQ         | BNQ76D5 | FP93GX       | 1280x1024 | 380x300mm  | 19.1 | 2007 | 36362 |
| BenQ         | BNQ76D5 | FP93GX       | 1280x1024 | 380x300mm  | 19.1 | 2006 | 2C7C2 |
| BenQ         | BNQ76D6 | FP93E        | 1280x1024 | 380x300mm  | 19.1 | 2006 | 545F8 |
| BenQ         | BNQ76FC | FP91G+U      | 1280x1024 | 380x300mm  | 19.1 | 2008 | 380C8 |
| BenQ         | BNQ76FC | FP91G+U      | 1280x1024 | 380x300mm  | 19.1 | 2007 | 12620 |
| BenQ         | BNQ7701 | FP93GS       | 1280x1024 | 380x300mm  | 19.1 | 2007 | 18CFC |
| BenQ         | BNQ7701 | FP93GS       | 1280x1024 | 380x300mm  | 19.1 | 2006 | 8C7A3 |
| BenQ         | BNQ7703 | FP93GP       | 1280x1024 | 380x300mm  | 19.1 | 2007 | 274E7 |
| BenQ         | BNQ7706 | FP92Wa       | 1440x900  | 400x250mm  | 18.6 | 2006 | F90E5 |
| BenQ         | BNQ770D | FP93ES       | 1280x1024 | 380x300mm  | 19.1 | 2007 | C31FB |
| BenQ         | BNQ7726 | T2200HD      | 1920x1080 | 480x270mm  | 21.7 | 2010 | 0027F |
| BenQ         | BNQ7726 | T2200HD      | 1920x1080 | 480x270mm  | 21.7 | 2009 | F3A45 |
| BenQ         | BNQ7726 | T2200HD      | 1920x1080 | 480x270mm  | 21.7 | 2008 | 779E8 |
| BenQ         | BNQ7802 | G700         | 1280x1024 | 340x270mm  | 17.1 | 2007 | 8434E |
| BenQ         | BNQ7804 | G900         | 1280x1024 | 380x300mm  | 19.1 | 2008 | 48AE0 |
| BenQ         | BNQ780A | G2400W       | 1920x1200 | 520x320mm  | 24.0 | 2008 | 8E8A2 |
| BenQ         | BNQ780A | G2400W       | 1920x1200 | 520x320mm  | 24.0 | 2007 | 7B03C |
| BenQ         | BNQ780E | G2200W       | 1680x1050 | 470x300mm  | 22.0 | 2010 | 4D6BE |
| BenQ         | BNQ780E | G2200W       | 1680x1050 | 470x300mm  | 22.0 | 2009 | 65C1F |
| BenQ         | BNQ780E | G2200W       | 1680x1050 | 470x300mm  | 22.0 | 2008 | 0101D |
| BenQ         | BNQ781F | G2020HD      | 1600x900  | 440x250mm  | 19.9 | 2012 | F3448 |
| BenQ         | BNQ781F | G2020HD      | 1600x900  | 440x250mm  | 19.9 | 2011 | 24198 |
| BenQ         | BNQ781F | G2020HD      | 1600x900  | 440x250mm  | 19.9 | 2010 | FB262 |
| BenQ         | BNQ781F | G2020HD      | 1600x900  | 440x250mm  | 19.9 | 2009 | 89B83 |
| BenQ         | BNQ7820 |              | 1920x1080 | 480x270mm  | 21.7 | 2010 | 32ECF |
| BenQ         | BNQ7821 | G2220HD      | 1920x1080 | 480x270mm  | 21.7 | 2011 | 3597B |
| BenQ         | BNQ7821 | G2220HD      | 1920x1080 | 480x270mm  | 21.7 | 2010 | 6761B |
| BenQ         | BNQ7821 | G2220HD      | 1920x1080 | 480x270mm  | 21.7 | 2009 | 282C9 |
| BenQ         | BNQ7827 | G2320HD      | 1920x1080 | 510x290mm  | 23.1 | 2009 | 2E78B |
| BenQ         | BNQ7829 |              | 1920x1080 | 510x290mm  | 23.1 | 2009 | 0C059 |
| BenQ         | BNQ783B | G2412HD      | 1920x1080 | 520x290mm  | 23.4 | 2009 | 0CAB4 |
| BenQ         | BNQ783F | G2420HD      | 1920x1080 | 530x300mm  | 24.0 | 2011 | 7DED2 |
| BenQ         | BNQ7840 | G2420HD      | 1920x1080 | 530x300mm  | 24.0 | 2012 | 070F4 |
| BenQ         | BNQ7840 | G2420HD      | 1920x1080 | 530x300mm  | 24.0 | 2011 | 711AB |
| BenQ         | BNQ7840 | G2420HD      | 1920x1080 | 530x300mm  | 24.0 | 2010 | 09585 |
| BenQ         | BNQ7840 | G2420HD      | 1920x1080 | 530x300mm  | 24.0 | 2009 | 2D2C0 |
| BenQ         | BNQ7842 |              | 1920x1080 | 530x290mm  | 23.8 | 2011 | 19A80 |
| BenQ         | BNQ7842 |              | 1920x1080 | 530x290mm  | 23.8 | 2010 | 032C5 |
| BenQ         | BNQ7842 |              | 1920x1080 | 530x290mm  | 23.8 | 2009 | B7339 |
| BenQ         | BNQ7850 | G2210W       | 1680x1050 | 470x300mm  | 22.0 | 2009 | A5EDC |
| BenQ         | BNQ7857 | G920WL       | 1440x900  | 410x230mm  | 18.5 | 2010 | 4715D |
| BenQ         | BNQ785A | BenQG2222HDL | 1920x1080 | 480x270mm  | 21.7 | 2011 | 72611 |
| BenQ         | BNQ785A | BenQG2222HDL | 1920x1080 | 480x270mm  | 21.7 | 2010 | 3C667 |
| BenQ         | BNQ785A | BenQG2222HDL | 1920x1080 | 480x270mm  | 21.7 | 2009 | 66151 |
| BenQ         | BNQ785E | G2420HDBL    | 1920x1080 | 530x290mm  | 23.8 | 2010 | 1E80B |
| BenQ         | BNQ785F | G2420HDBL    | 1920x1080 | 530x290mm  | 23.8 | 2011 | 07FAF |
| BenQ         | BNQ785F | G2420HDBL    | 1920x1080 | 530x290mm  | 23.8 | 2010 | 6ADFE |
| BenQ         | BNQ785F | G2420HDBL    | 1920x1080 | 530x290mm  | 23.8 | 2009 | AE455 |
| BenQ         | BNQ7867 | GL2030       | 1600x900  | 440x250mm  | 19.9 | 2011 | 62BBA |
| BenQ         | BNQ787E | BenQGL2040M  | 1600x900  | 440x250mm  | 19.9 | 2011 | 134BC |
| BenQ         | BNQ7881 | GL941        | 1440x900  | 410x260mm  | 19.1 | 2011 | A33EE |
| BenQ         | BNQ7884 | GL940        | 1366x768  | 410x230mm  | 18.5 | 2011 | 08F4B |
| BenQ         | BNQ7887 | GL2240       | 1920x1080 | 480x270mm  | 21.7 | 2011 | 159F6 |
| BenQ         | BNQ7887 | GL2240       | 1920x1080 | 480x270mm  | 21.7 | 2010 | 1C909 |
| BenQ         | BNQ7889 | GL2440H      | 1920x1080 | 530x300mm  | 24.0 | 2010 | E9214 |
| BenQ         | BNQ789A | GL2250       | 1920x1080 | 480x270mm  | 21.7 | 2016 | DF970 |
| BenQ         | BNQ789A | GL2250       | 1920x1080 | 480x270mm  | 21.7 | 2012 | BA72C |
| BenQ         | BNQ789B | GL2250       | 1920x1080 | 480x270mm  | 21.7 | 2016 | 383F0 |
| BenQ         | BNQ789B | GL2250       | 1920x1080 | 480x270mm  | 21.7 | 2014 | 0404D |
| BenQ         | BNQ789B | GL2250       | 1920x1080 | 480x270mm  | 21.7 | 2012 | C41FF |
| BenQ         | BNQ789D | G2250        | 1920x1080 | 480x270mm  | 21.7 | 2013 | 5DB56 |
| BenQ         | BNQ789F | G2251        | 1680x1050 | 470x300mm  | 22.0 | 2011 | 5A1E9 |
| BenQ         | BNQ78A1 | GL2250H      | 1920x1080 | 480x270mm  | 21.7 | 2016 | 2EC73 |
| BenQ         | BNQ78A1 | GL2250H      | 1920x1080 | 480x270mm  | 21.7 | 2014 | 9C49E |
| BenQ         | BNQ78A5 | GL2450       | 1920x1080 | 530x300mm  | 24.0 | 2016 | AF8B9 |
| BenQ         | BNQ78A5 | GL2450       | 1920x1080 | 530x300mm  | 24.0 | 2015 | 0A255 |
| BenQ         | BNQ78A5 | GL2450       | 1920x1080 | 530x300mm  | 24.0 | 2014 | 380D5 |
| BenQ         | BNQ78A5 | GL2450       | 1920x1080 | 530x300mm  | 24.0 | 2013 | 55ACF |
| BenQ         | BNQ78A5 | GL2450       | 1920x1080 | 530x300mm  | 24.0 | 2012 | 3FA05 |
| BenQ         | BNQ78A7 | GL2450H      | 1920x1080 | 530x300mm  | 24.0 | 2017 | A6390 |
| BenQ         | BNQ78A7 | GL2450H      | 1920x1080 | 530x300mm  | 24.0 | 2016 | 00EA4 |
| BenQ         | BNQ78A7 | GL2450H      | 1920x1080 | 530x300mm  | 24.0 | 2014 | 47073 |
| BenQ         | BNQ78A7 | GL2450H      | 1920x1080 | 530x300mm  | 24.0 | 2013 | 58E8B |
| BenQ         | BNQ78A7 | GL2450H      | 1920x1080 | 530x300mm  | 24.0 | 2012 | 0F1CB |
| BenQ         | BNQ78A7 | GL2450H      | 1920x1080 | 530x300mm  | 24.0 | 2011 | FE876 |
| BenQ         | BNQ78A9 | G2450        | 1920x1080 | 530x300mm  | 24.0 | 2012 | 13EA2 |
| BenQ         | BNQ78B1 | G2750        | 1920x1080 | 600x340mm  | 27.2 | 2012 | 3DEB2 |
| BenQ         | BNQ78B3 | G2320HDBL    | 1920x1080 | 510x290mm  | 23.1 | 2012 | EAA11 |
| BenQ         | BNQ78BB | GW2250       | 1920x1080 | 480x270mm  | 21.7 | 2012 | 78170 |
| BenQ         | BNQ78BD | GW2250H      | 1920x1080 | 480x270mm  | 21.7 | 2012 | 29FF4 |
| BenQ         | BNQ78BF | GW2450       | 1920x1080 | 530x300mm  | 24.0 | 2012 | E8984 |
| BenQ         | BNQ78C1 | GW2450H      | 1920x1080 | 530x300mm  | 24.0 | 2013 | 3B06D |
| BenQ         | BNQ78C1 | GW2450H      | 1920x1080 | 530x300mm  | 24.0 | 2012 | 056FD |
| BenQ         | BNQ78C3 | GW2750H      | 1920x1080 | 600x340mm  | 27.2 | 2013 | 45092 |
| BenQ         | BNQ78C3 | GW2750H      | 1920x1080 | 600x340mm  | 27.2 | 2012 | 65D6E |
| BenQ         | BNQ78C4 | GW2260       | 1920x1080 | 480x270mm  | 21.7 | 2013 | 98813 |
| BenQ         | BNQ78C4 | GW2260       | 1920x1080 | 480x270mm  | 21.7 | 2012 | C1A37 |
| BenQ         | BNQ78C5 | GW2460       | 1920x1080 | 530x300mm  | 24.0 | 2013 | 1EB4E |
| BenQ         | BNQ78C5 | GW2460       | 1920x1080 | 530x300mm  | 24.0 | 2012 | 4DA3F |
| BenQ         | BNQ78C6 | GW2760       | 1920x1080 | 600x340mm  | 27.2 | 2015 | D84DB |
| BenQ         | BNQ78CA |              | 1920x1080 | 600x340mm  | 27.2 | 2017 | CC55F |
| BenQ         | BNQ78CA |              | 1920x1080 | 600x340mm  | 27.2 | 2016 | 571FF |
| BenQ         | BNQ78CA |              | 1920x1080 | 600x340mm  | 27.2 | 2015 | 067E6 |
| BenQ         | BNQ78CA |              | 1920x1080 | 600x340mm  | 27.2 | 2014 | 7FFB0 |
| BenQ         | BNQ78CA |              | 1920x1080 | 600x340mm  | 27.2 | 2013 | 0119C |
| BenQ         | BNQ78CD | GW2255       | 1920x1080 | 480x270mm  | 21.7 | 2014 | 31864 |
| BenQ         | BNQ78CD | GW2255       | 1920x1080 | 480x270mm  | 21.7 | 2013 | 42E7B |
| BenQ         | BNQ78CE | GL2460       | 1920x1080 | 530x300mm  | 24.0 | 2018 | 0D6F7 |
| BenQ         | BNQ78CE | GL2460       | 1920x1080 | 530x300mm  | 24.0 | 2016 | 3B6E5 |
| BenQ         | BNQ78CE | GL2460       | 1920x1080 | 530x300mm  | 24.0 | 2015 | 00F64 |
| BenQ         | BNQ78CE | GL2460       | 1920x1080 | 530x300mm  | 24.0 | 2014 | 0D792 |
| BenQ         | BNQ78CE | GL2460       | 1920x1080 | 530x300mm  | 24.0 | 2013 | 35EFB |
| BenQ         | BNQ78CE | GL2460       | 1920x1080 | 530x300mm  | 24.0 | 2012 | 3E1CB |
| BenQ         | BNQ78D1 | GW2265       | 1920x1080 | 480x270mm  | 21.7 | 2015 | 6D453 |
| BenQ         | BNQ78D1 | GW2265       | 1920x1080 | 480x270mm  | 21.7 | 2014 | 2590E |
| BenQ         | BNQ78D1 | GW2265       | 1920x1080 | 480x270mm  | 21.7 | 2013 | 0FB91 |
| BenQ         | BNQ78D3 | GW2320       | 1920x1080 | 510x290mm  | 23.1 | 2013 | AAE38 |
| BenQ         | BNQ78D5 | GL2760       | 1920x1080 | 600x340mm  | 27.2 | 2016 | B265B |
| BenQ         | BNQ78D5 | GL2760       | 1920x1080 | 600x340mm  | 27.2 | 2015 | 25C9D |
| BenQ         | BNQ78D6 | GW2765       | 2560x1440 | 600x340mm  | 27.2 | 2018 | 94492 |
| BenQ         | BNQ78D6 | GW2765       | 2560x1440 | 600x340mm  | 27.2 | 2017 | 56F0E |
| BenQ         | BNQ78D6 | GW2765       | 2560x1440 | 600x340mm  | 27.2 | 2016 | 38815 |
| BenQ         | BNQ78D6 | GW2765       | 2560x1440 | 600x340mm  | 27.2 | 2015 | 07DCE |
| BenQ         | BNQ78D6 | GW2765       | 2560x1440 | 600x340mm  | 27.2 | 2014 | 8D03D |
| BenQ         | BNQ78D8 | GW2455       | 1920x1080 | 520x290mm  | 23.4 | 2016 | 55236 |
| BenQ         | BNQ78D9 | GW2470       | 1920x1080 | 530x300mm  | 24.0 | 2017 | 2D4DE |
| BenQ         | BNQ78D9 | GW2470       | 1920x1080 | 530x300mm  | 24.0 | 2016 | 18E8F |
| BenQ         | BNQ78DB | GW2270       | 1920x1080 | 480x270mm  | 21.7 | 2018 | 90DCB |
| BenQ         | BNQ78DB | GW2270       | 1920x1080 | 480x270mm  | 21.7 | 2017 | 44B4C |
| BenQ         | BNQ78DB | GW2270       | 1920x1080 | 480x270mm  | 21.7 | 2016 | 1102E |
| BenQ         | BNQ78DB | GW2270       | 1920x1080 | 480x270mm  | 21.7 | 2015 | 7CD58 |
| BenQ         | BNQ78DD | GC2870       | 1920x1080 | 620x340mm  | 27.8 | 2016 | 3A17B |
| BenQ         | BNQ78E1 | GW2406Z      | 1920x1080 | 530x300mm  | 24.0 | 2017 | 1F8EB |
| BenQ         | BNQ78E1 | GW2406Z      | 1920x1080 | 530x300mm  | 24.0 | 2016 | 767B2 |
| BenQ         | BNQ78E4 | GW2470       | 1920x1080 | 530x300mm  | 24.0 | 2018 | 4A13E |
| BenQ         | BNQ78E4 | GW2470       | 1920x1080 | 530x300mm  | 24.0 | 2017 | 3A143 |
| BenQ         | BNQ78E5 | GL2580       | 1920x1080 | 540x300mm  | 24.3 | 2018 | 19523 |
| BenQ         | BNQ78E6 | GW2780       | 1920x1080 | 600x340mm  | 27.2 | 2018 | B80EE |
| BenQ         | BNQ78E7 | GW2480       | 1920x1080 | 530x300mm  | 24.0 | 2017 | 8E1F5 |
| BenQ         | BNQ78E8 | GW2280       | 1920x1080 | 480x270mm  | 21.7 | 2018 | 41D3A |
| BenQ         | BNQ790C | E2200HD      | 1920x1080 | 470x260mm  | 21.1 | 2009 | 03967 |
| BenQ         | BNQ790C | E2200HD      | 1920x1080 | 470x260mm  | 21.1 | 2008 | 6AC4F |
| BenQ         | BNQ7913 | E2220HD      | 1920x1080 | 480x270mm  | 21.7 | 2010 | 5785E |
| BenQ         | BNQ7917 | E2420HD      | 1920x1080 | 530x300mm  | 24.0 | 2010 | D09B7 |
| BenQ         | BNQ7917 | E2420HD      | 1920x1080 | 530x300mm  | 24.0 | 2009 | 2E275 |
| BenQ         | BNQ791B | E910         | 1280x1024 | 380x300mm  | 19.1 | 2009 | B44F0 |
| BenQ         | BNQ7923 | EW2420       | 1920x1080 | 530x300mm  | 24.0 | 2011 | 4B77F |
| BenQ         | BNQ7923 | EW2420       | 1920x1080 | 530x300mm  | 24.0 | 2010 | 49995 |
| BenQ         | BNQ7925 | EW2430       | 1920x1080 | 530x300mm  | 24.0 | 2013 | 93A79 |
| BenQ         | BNQ7925 | EW2430       | 1920x1080 | 530x300mm  | 24.0 | 2011 | A381A |
| BenQ         | BNQ7927 | EW2730       | 1920x1080 | 600x340mm  | 27.2 | 2013 | E766C |
| BenQ         | BNQ7927 | EW2730       | 1920x1080 | 600x340mm  | 27.2 | 2012 | B89B9 |
| BenQ         | BNQ7927 | EW2730       | 1920x1080 | 600x340mm  | 27.2 | 2011 | E0C75 |
| BenQ         | BNQ7937 | EW2740L      | 1920x1080 | 600x340mm  | 27.2 | 2014 | 8B79D |
| BenQ         | BNQ7938 | EW2440L      | 1920x1080 | 530x300mm  | 24.0 | 2015 | 84150 |
| BenQ         | BNQ793D |              | 1920x1080 | 600x340mm  | 27.2 | 2016 | 75F9E |
| BenQ         | BNQ793D |              | 1920x1080 | 600x340mm  | 27.2 | 2015 | 6D047 |
| BenQ         | BNQ7944 |              | 1920x1080 | 600x340mm  | 27.2 | 2018 | 04077 |
| BenQ         | BNQ7945 |              | 2560x1440 | 710x400mm  | 32.1 | 2016 | 32720 |
| BenQ         | BNQ7948 |              | 1920x1080 | 600x340mm  | 27.2 | 2017 | DD046 |
| BenQ         | BNQ7950 | EW3270U      | 3840x2160 | 700x390mm  | 31.5 | 2018 | CCB4A |
| BenQ         | BNQ7A04 | X900W        | 1440x900  | 410x260mm  | 19.1 | 2008 | 985D8 |
| BenQ         | BNQ7B0A | V920         | 1366x768  | 410x230mm  | 18.5 | 2010 | 1AB58 |
| BenQ         | BNQ7B0C | V2220        | 1920x1080 | 480x270mm  | 21.7 | 2011 | 907D4 |
| BenQ         | BNQ7B0C | V2220        | 1920x1080 | 480x270mm  | 21.7 | 2010 | 51075 |
| BenQ         | BNQ7B0E | V2220H       | 1920x1080 | 480x270mm  | 21.7 | 2010 | 0ABFB |
| BenQ         | BNQ7B14 | V2420H       | 1920x1080 | 530x300mm  | 24.0 | 2010 | 0FC18 |
| BenQ         | BNQ7B22 | V2320H       | 1920x1080 | 510x290mm  | 23.1 | 2011 | 218EF |
| BenQ         | BNQ7B26 | VW2420H      | 1920x1080 | 530x300mm  | 24.0 | 2011 | 72B45 |
| BenQ         | BNQ7B26 | VW2420H      | 1920x1080 | 530x300mm  | 24.0 | 2010 | 2F1C9 |
| BenQ         | BNQ7B27 | VW2220       | 1920x1080 | 480x270mm  | 21.7 | 2011 | E8EA0 |
| BenQ         | BNQ7B28 | VW2220       | 1920x1080 | 480x270mm  | 21.7 | 2011 | 02ADF |
| BenQ         | BNQ7B2D | VW2230       | 1920x1080 | 480x270mm  | 21.7 | 2013 | 032F6 |
| BenQ         | BNQ7B2E | VW2430       | 1920x1080 | 530x300mm  | 24.0 | 2014 | 11368 |
| BenQ         | BNQ7B2E | VW2430       | 1920x1080 | 530x300mm  | 24.0 | 2012 | 73621 |
| BenQ         | BNQ7B32 | VW2245Z      | 1920x1080 | 480x270mm  | 21.7 | 2014 | EA9DF |
| BenQ         | BNQ7B3B | VZ2470H      | 1920x1080 | 530x300mm  | 24.0 | 2017 | 9687E |
| BenQ         | BNQ7B3C | VZ2770H      | 1920x1080 | 600x340mm  | 27.2 | 2017 | 48015 |
| BenQ         | BNQ7C04 | M2200HD      | 1920x1080 | 470x260mm  | 21.1 | 2008 | 01617 |
| BenQ         | BNQ7C06 | M2700HD      | 1920x1080 | 600x340mm  | 27.2 | 2011 | E0DC3 |
| BenQ         | BNQ7C06 | M2700HD      | 1920x1080 | 600x340mm  | 27.2 | 2010 | 4315A |
| BenQ         | BNQ7D01 |              | 1920x1080 | 530x300mm  | 24.0 | 2010 | 456BF |
| BenQ         | BNQ7D02 |              | 1920x1080 | 530x300mm  | 24.0 | 2009 | 2D8A9 |
| BenQ         | BNQ7D04 |              | 1920x1080 | 480x270mm  | 21.7 | 2010 | 6FA05 |
| BenQ         | BNQ7D04 |              | 1920x1080 | 480x270mm  | 21.7 | 2009 | 17F31 |
| BenQ         | BNQ7F02 | XL2410T      | 1920x1080 | 520x290mm  | 23.4 | 2011 | 3D868 |
| BenQ         | BNQ7F02 | XL2410T      | 1920x1080 | 520x290mm  | 23.4 | 2010 | 50B5E |
| BenQ         | BNQ7F04 | XL2420T      | 1920x1080 | 530x300mm  | 24.0 | 2012 | 85CDC |
| BenQ         | BNQ7F05 | XL2420T      | 1920x1080 | 530x300mm  | 24.0 | 2013 | 6C966 |
| BenQ         | BNQ7F08 |              | 1920x1080 | 530x300mm  | 24.0 | 2013 | 6374B |
| BenQ         | BNQ7F0C | RL2240H      | 1920x1080 | 480x270mm  | 21.7 | 2012 | 1D71A |
| BenQ         | BNQ7F0E | RL2450H      | 1920x1080 | 530x300mm  | 24.0 | 2011 | 46B7B |
| BenQ         | BNQ7F10 | XL2411T      | 1920x1080 | 530x300mm  | 24.0 | 2012 | 31FAE |
| BenQ         | BNQ7F1C | RL2455       | 1920x1080 | 530x300mm  | 24.0 | 2016 | 1D3A5 |
| BenQ         | BNQ7F1C | RL2455       | 1920x1080 | 530x300mm  | 24.0 | 2015 | 4E045 |
| BenQ         | BNQ7F1C | RL2455       | 1920x1080 | 530x300mm  | 24.0 | 2014 | 3F648 |
| BenQ         | BNQ7F1E | xl2411t      | 1920x1080 | 530x300mm  | 24.0 | 2013 | D5255 |
| BenQ         | BNQ7F21 | xl2420t      | 1920x1080 | 530x300mm  | 24.0 | 2013 | DE2F9 |
| BenQ         | BNQ7F22 | xl2420t      | 1920x1080 | 530x300mm  | 24.0 | 2013 | 52681 |
| BenQ         | BNQ7F23 | xl2420t      | 1920x1080 | 530x300mm  | 24.0 | 2013 | AB8B3 |
| BenQ         | BNQ7F31 | ZOWIE XL LCD | 1920x1080 | 530x300mm  | 24.0 | 2017 | 81E24 |
| BenQ         | BNQ7F31 | ZOWIE XL LCD | 1920x1080 | 530x300mm  | 24.0 | 2016 | 12474 |
| BenQ         | BNQ7F31 | XL2411Z      | 1920x1080 | 530x300mm  | 24.0 | 2015 | 7EB4F |
| BenQ         | BNQ7F31 | XL2411Z      | 1920x1080 | 530x300mm  | 24.0 | 2014 | FA43D |
| BenQ         | BNQ7F32 | ZOWIE XL LCD | 1920x1080 | 530x300mm  | 24.0 | 2017 | 77AD6 |
| BenQ         | BNQ7F34 | RL2460H      | 1920x1080 | 530x300mm  | 24.0 | 2014 | 098ED |
| BenQ         | BNQ7F3D | ZOWIE XL LCD | 1920x1080 | 530x300mm  | 24.0 | 2017 | D8A09 |
| BenQ         | BNQ7F3D | XL2430T      | 1920x1080 | 530x300mm  | 24.0 | 2014 | 55098 |
| BenQ         | BNQ7F3E | XL2430T      | 1920x1080 | 530x300mm  | 24.0 | 2014 | B154C |
| BenQ         | BNQ7F3F | ZOWIE XL LCD | 1920x1080 | 530x300mm  | 24.0 | 2017 | FB2F0 |
| BenQ         | BNQ7F41 | RL2755       | 1920x1080 | 600x340mm  | 27.2 | 2016 | 5AADB |
| BenQ         | BNQ7F58 | ZOWIE XL LCD | 1920x1080 | 540x300mm  | 24.3 | 2017 | C43B6 |
| BenQ         | BNQ7F59 | ZOWIE XL LCD | 1920x1080 | 540x300mm  | 24.3 | 2017 | A19F1 |
| BenQ         | BNQ8002 | BL2400       | 1920x1080 | 530x300mm  | 24.0 | 2011 | 0F973 |
| BenQ         | BNQ8004 | BL2201       | 1680x1050 | 480x300mm  | 22.3 | 2012 | B8FA5 |
| BenQ         | BNQ8004 | BL2201       | 1680x1050 | 480x300mm  | 22.3 | 2010 | 0B8FA |
| BenQ         | BNQ8008 | BL902        | 1280x1024 | 380x300mm  | 19.1 | 2013 | F1B51 |
| BenQ         | BNQ8008 | BL902        | 1280x1024 | 380x300mm  | 19.1 | 2012 | 10BA2 |
| BenQ         | BNQ8011 | BL2411       | 1920x1200 | 520x320mm  | 24.0 | 2014 | 0314D |
| BenQ         | BNQ8012 | BL2710       | 2560x1440 | 600x340mm  | 27.2 | 2016 | ED98E |
| BenQ         | BNQ8014 | BL912        | 1280x1024 | 380x300mm  | 19.1 | 2017 | 45FD4 |
| BenQ         | BNQ8017 | BL3200       | 2560x1440 | 710x400mm  | 32.1 | 2016 | 4842A |
| BenQ         | BNQ8017 | BL3200       | 2560x1440 | 710x400mm  | 32.1 | 2015 | 51851 |
| BenQ         | BNQ8017 | BL3200       | 2560x1440 | 710x400mm  | 32.1 | 2014 | D09CA |
| BenQ         | BNQ8018 | BL2700       | 1920x1080 | 600x340mm  | 27.2 | 2016 | B95D7 |
| BenQ         | BNQ8018 | BL2700       | 1920x1080 | 600x340mm  | 27.2 | 2015 | 4C4AE |
| BenQ         | BNQ801B | LCD          | 2560x1440 | 530x300mm  | 24.0 | 2018 | 35E18 |
| BenQ         | BNQ801B | LCD          | 2560x1440 | 530x300mm  | 24.0 | 2017 | C69D4 |
| BenQ         | BNQ801B | LCD          | 2560x1440 | 530x300mm  | 24.0 | 2016 | 370C9 |
| BenQ         | BNQ801E | LCD          | 3840x2160 | 600x340mm  | 27.2 | 2017 | 048E9 |
| BenQ         | BNQ801E | LCD          | 3840x2160 | 600x340mm  | 27.2 | 2016 | 97B01 |
| BenQ         | BNQ8022 | LCD          | 1920x1080 | 480x270mm  | 21.7 | 2016 | D4337 |
| BenQ         | BNQ802A | PD2500Q      | 2560x1440 | 550x310mm  | 24.9 | 2017 | C5208 |
| BenQ         | BNQ802B | BL2780       | 1920x1080 | 600x340mm  | 27.2 | 2017 | 9F4AA |
| BenQ         | BNQ8102 | T420         | 1920x1080 | 930x520mm  | 41.9 | 2013 | 4E0F7 |
| BenQ         | BNQ8301 | BL2410       | 1920x1080 | 530x300mm  | 24.0 | 2015 | 1B1CB |
| BenQ         | BNQ8301 | BL2410       | 1920x1080 | 530x300mm  | 24.0 | 2012 | 73941 |
| BenQ         | BNQ8403 | DL2215       | 1920x1080 | 480x270mm  | 21.7 | 2014 | D005C |
| BenQ         | BNQ9275 | VP2212       | 1680x1050 | 480x300mm  | 22.3 | 2010 | 7BDDD |
| Bose         | BSE0101 | LS           | 1920x1080 |            |      |      | B5E16 |
| COMPAL       | WOR2435 |              | 1920x1080 | 530x300mm  | 24.0 | 2018 | 10D13 |
| COMPAL       | WOR2455 | TERRA 2455W  | 1920x1080 | 520x290mm  | 23.4 | 2013 | 33151 |
| COMPAL       | WOR2750 | TERRA 2750W  | 1920x1080 | 600x340mm  | 27.2 | 2013 | 59C48 |
| CPT          | CPT04C4 | CLAA102NA0A  | 1024x600  | 230x140mm  | 10.6 | 2009 | 275BC |
| CPT          | CPT04C4 | CLAA102NA0A  | 1024x600  | 230x140mm  | 10.6 | 2008 | 3262A |
| CPT          | CPT04C4 | CLAA102NA0A  | 1024x768  | 230x140mm  | 10.6 | 2008 | B0503 |
| CPT          | CPT04CE | CLAA102NA1B  | 1024x600  | 230x140mm  | 10.6 | 2009 | B2A67 |
| CPT          | CPT0BB9 | CLAA150XH01  | 1024x768  | 300x230mm  | 14.9 |      | C7588 |
| CPT          | CPT1393 | CLAA154WA02A | 1280x800  | 330x210mm  | 15.4 |      | F046A |
| CPT          | CPT13A6 | Y53611154WA4 | 1280x800  | 330x210mm  | 15.4 |      | 84E1B |
| CPT          | CPT13B0 | CLAA154WA05  | 1280x800  | 330x210mm  | 15.4 | 2006 | 7FF20 |
| CPT          | CPT13B1 | CLAA154WA05A | 1280x800  | 330x210mm  | 15.4 | 2007 | 8F681 |
| CPT          | CPT13B1 | CLAA154WA05A | 1280x800  | 330x210mm  | 15.4 | 2006 | 1D808 |
| CPT          | CPT13B1 | CLAA154WA05A | 1280x800  | 330x210mm  | 15.4 |      | 1B189 |
| CPT          | CPT13EE | 154WA01AQ    | 1280x800  | 330x210mm  | 15.4 |      | 9CAA4 |
| CPT          | CPT1401 | CLAA154WB03A | 1280x800  | 330x210mm  | 15.4 | 2010 | 0A92B |
| CPT          | CPT1401 | CLAA154WB03A | 1280x800  | 330x210mm  | 15.4 | 2008 | 1971A |
| CPT          | CPT1401 | CLAA154WB03A | 1280x800  | 330x210mm  | 15.4 | 2007 | 28073 |
| CPT          | CPT1401 | CLAA154WB03A | 1280x800  | 330x210mm  | 15.4 |      | 3E9A7 |
| CPT          | CPT140A | FD1630154... | 1280x800  | 330x210mm  | 15.4 | 2007 | E0F9D |
| CPT          | CPT1415 | CLAA154WB05A | 1280x800  | 330x210mm  | 15.4 | 2008 | 27F22 |
| CPT          | CPT1415 | CLAA154WB05A | 1280x800  | 330x210mm  | 15.4 | 2007 | 1B28B |
| CPT          | CPT1415 | CLAA154WB05A | 1280x800  | 330x210mm  | 15.4 |      | 6A0E0 |
| CPT          | CPT141F | R784G (7BHn  | 1280x800  | 330x210mm  | 15.4 | 2008 | B8D32 |
| CPT          | CPT141F | TM1210154... | 1280x800  | 330x210mm  | 15.4 | 2007 | 62554 |
| CPT          | CPT141F | TM1210154... | 1280x800  | 330x210mm  | 15.4 | 2007 | FDEF4 |
| CPT          | CPT1429 | RM522        | 1280x800  | 330x210mm  | 15.4 | 2008 | 0BABC |
| CPT          | CPT1465 | CLAA154WP05A | 1440x900  | 330x210mm  | 15.4 | 2008 | 158DC |
| CPT          | CPT14B5 | CLAA156WA01A | 1366x768  | 340x190mm  | 15.3 | 2009 | 03D6B |
| CPT          | CPT14BF | CLAA156WA11A | 1366x768  | 340x190mm  | 15.3 | 2010 | ADADA |
| CPT          | CPT14BF | CLAA156WA11A | 1366x768  | 340x190mm  | 15.3 | 2009 | 3F835 |
| CPT          | CPT14C5 | NG510        | 1366x768  | 340x190mm  | 15.3 | 2010 | E1919 |
| CPT          | CPT14C7 |              | 1366x768  | 340x190mm  | 15.3 | 2011 | 1FDD9 |
| CPT          | CPT14C7 |              | 1366x768  | 340x190mm  | 15.3 | 2010 | 79835 |
| CPT          | CPT14C8 | VXW7V        | 1366x768  | 340x190mm  | 15.3 | 2011 | 15895 |
| CPT          | CPT1775 | PY676        | 1280x800  | 300x190mm  | 14.0 | 2008 | 80B58 |
| CPT          | CPT1785 | CLAA141WB05A | 1280x800  | 300x190mm  | 14.0 | 2008 | 15E9E |
| CPT          | CPT1785 | CLAA141WB05A | 1280x800  | 300x190mm  | 14.0 | 2007 | 7E258 |
| CPT          | CPT17A9 |              | 1366x768  | 300x170mm  | 13.6 | 2011 | A5AE0 |
| CPT          | CPT17A9 |              | 1366x768  | 300x170mm  | 13.6 | 2010 | CE153 |
| CPT          | CPT17AB |              | 1366x768  | 300x170mm  | 13.6 | 2011 | 1467E |
| CPT          | CPT17AB |              | 1366x768  | 300x170mm  | 13.6 | 2010 | ACC4B |
| CPT          | CPT17D5 | CLAA133WA01A | 1366x768  | 290x160mm  | 13.0 | 2010 | 609B1 |
| CPT          | CPT17D8 | CLAA133WB01A | 1366x768  | 290x160mm  | 13.0 | 2011 | 240FA |
| CPT          | CPT1BBE | CLAA101NB01A | 1024x600  | 220x120mm  | 9.9  | 2010 | 23168 |
| CPT          | CPT1BC0 | CLAA101NB03A | 1024x600  | 220x120mm  | 9.9  | 2010 | 2E62E |
| CPT          | CPT1BC7 | CLAA101NC05  | 1024x600  | 220x120mm  | 9.9  | 2012 | 5F186 |
| CPT          | CPT1BC7 | CLAA101NC05  | 1024x600  | 220x120mm  | 9.9  | 2011 | 24721 |
| CPT          | CPT1C85 | CLAA101WA01A | 1366x768  | 220x120mm  | 9.9  | 2011 | EE67C |
| CPT          | CPT1F42 | CLAA173UA01A | 1600x900  | 380x210mm  | 17.1 | 2011 | 199B8 |
| CPT          | CPT1F42 | CLAA173UA01A | 1600x900  | 380x210mm  | 17.1 | 2010 | 6FB83 |
| CPT          | CPT22CE | CLAA089NA0A  | 1024x600  | 210x120mm  | 9.5  | 2008 | 1ECCD |
| CTL          | CTL2362 | IP2362       | 1920x1080 | 520x290mm  | 23.4 |      | 306ED |
| Chi Mei O... | CMO0209 | K862H        | 1024x600  | 210x120mm  | 9.5  | 2008 | 3E7EC |
| Chi Mei O... | CMO0209 | N089L6-L02   | 1024x600  | 210x120mm  | 9.5  | 2008 | AAC18 |
| Chi Mei O... | CMO0309 | N089L6-L03   | 1024x600  | 210x120mm  | 9.5  | 2008 | 95505 |
| Chi Mei O... | CMO0903 | N089L6-L03   | 1024x600  | 210x120mm  | 9.5  | 2008 | 2A41C |
| Chi Mei O... | CMO1004 | J1J93        | 1024x600  | 220x120mm  | 9.9  | 2009 | 23ACE |
| Chi Mei O... | CMO1004 | N101L6-L02   | 1024x600  | 220x120mm  | 9.9  | 2009 | B69B4 |
| Chi Mei O... | CMO1006 | D038T        | 1024x600  | 220x120mm  | 9.9  | 2009 | 07A4F |
| Chi Mei O... | CMO1007 | N101L6-L01   | 1024x600  | 220x120mm  | 9.9  | 2009 | A63B0 |
| Chi Mei O... | CMO1015 | N101L6-L0A   | 1024x600  | 220x120mm  | 9.9  | 2009 | E9FB4 |
| Chi Mei O... | CMO1016 | N101L6-L0B   | 1024x600  | 220x120mm  | 9.9  | 2009 | E7E5B |
| Chi Mei O... | CMO1018 | N101L6-L0D   | 1024x600  | 220x120mm  | 9.9  | 2009 | 111B1 |
| Chi Mei O... | CMO1020 | N101L6-L0A   | 1024x600  | 220x120mm  | 9.9  | 2009 | 25893 |
| Chi Mei O... | CMO1025 | N101L6-L0A   | 1024x600  | 220x120mm  | 9.9  | 2009 | A9025 |
| Chi Mei O... | CMO1028 | 234FR        | 1366x768  | 220x120mm  | 9.9  | 2010 | 8C412 |
| Chi Mei O... | CMO1030 | N101LGE-L21  | 1024x600  | 220x120mm  | 9.9  | 2009 | 577A0 |
| Chi Mei O... | CMO1031 | N101LGE-L41  | 1024x600  | 220x120mm  | 9.9  | 2009 | 104CE |
| Chi Mei O... | CMO1032 | N101LGE-L11  | 1024x600  | 220x120mm  | 9.9  | 2009 | 534FC |
| Chi Mei O... | CMO1100 | N116B6-L02   | 1366x768  | 250x140mm  | 11.3 | 2009 | 54CCB |
| Chi Mei O... | CMO1107 | N116B6-L04   | 1366x768  | 250x140mm  | 11.3 | 2009 | 5CD74 |
| Chi Mei O... | CMO1111 | HF9D2        | 1366x768  | 250x140mm  | 11.3 | 2010 | B5A5C |
| Chi Mei O... | CMO1113 | N116BGE-L41  | 1366x768  | 260x140mm  | 11.6 | 2009 | 9526A |
| Chi Mei O... | CMO1204 | N121X5 Co... | 1024x768  | 250x180mm  | 12.1 |      | 87185 |
| Chi Mei O... | CMO1210 | N121I3-L01   | 1280x800  | 260x160mm  | 12.0 | 2006 | 7F602 |
| Chi Mei O... | CMO1227 | N121IB-L01   | 1280x800  | 260x170mm  | 12.2 | 2008 | 62C31 |
| Chi Mei O... | CMO1233 | N121IB-L06   | 1280x800  | 260x170mm  | 12.2 | 2008 | B96A3 |
| Chi Mei O... | CMO1305 | N133I1-L05   | 1280x800  | 290x180mm  | 13.4 | 2006 | 63417 |
| Chi Mei O... | CMO1312 | N133I7-L01   | 1280x800  | 290x180mm  | 13.4 |      | D35E6 |
| Chi Mei O... | CMO1316 | N134B6-L04   | 1366x768  | 290x160mm  | 13.0 | 2009 | 31942 |
| Chi Mei O... | CMO1317 | N134B6-L01   | 1366x768  | 290x160mm  | 13.0 | 2009 | 8C448 |
| Chi Mei O... | CMO1321 | N133B6-L02   | 1366x768  | 290x160mm  | 13.0 | 2009 | 52593 |
| Chi Mei O... | CMO1324 | N133B6-L01   | 1366x768  | 290x160mm  | 13.0 | 2009 | 8DECB |
| Chi Mei O... | CMO1332 | 65MJF        | 1366x768  | 290x160mm  | 13.0 | 2011 | 23551 |
| Chi Mei O... | CMO1332 | N133BGE-L31  | 1366x768  | 290x160mm  | 13.0 | 2010 | 5F6B8 |
| Chi Mei O... | CMO1333 | N133BGE-L41  | 1366x768  | 290x160mm  | 13.0 | 2010 | 19AD7 |
| Chi Mei O... | CMO1409 | N141I1-L03   | 1280x800  | 300x190mm  | 14.0 |      | 8BECE |
| Chi Mei O... | CMO1422 | N141C2-L01   | 1440x900  | 300x190mm  | 14.0 | 2006 | 9881D |
| Chi Mei O... | CMO1424 | N141I3-L01   | 1280x800  | 300x190mm  | 14.0 | 2007 | 7446C |
| Chi Mei O... | CMO1425 | N141I3-L02   | 1280x800  | 300x190mm  | 14.0 | 2007 | 9C46A |
| Chi Mei O... | CMO1425 | N141I3-L02   | 1280x800  | 300x190mm  | 14.0 | 2006 | 73A3B |
| Chi Mei O... | CMO1426 | R767G8N14... | 1280x800  | 300x190mm  | 14.0 | 2007 | B009F |
| Chi Mei O... | CMO1426 | GR5517N14... | 1280x800  | 300x190mm  | 14.0 | 2007 | F09EA |
| Chi Mei O... | CMO1428 | PY7267N14... | 1440x900  | 300x190mm  | 14.0 |      | 83049 |
| Chi Mei O... | CMO1430 | N141C3-L01   | 1440x900  | 300x190mm  | 14.0 | 2007 | 1FC82 |
| Chi Mei O... | CMO1431 | N141C3-L02   | 1440x900  | 300x190mm  | 14.0 |      | 03182 |
| Chi Mei O... | CMO1433 | YY2721N14... | 1440x900  | 300x190mm  | 14.0 |      | 96618 |
| Chi Mei O... | CMO1441 | N141I6-L01   | 1280x800  | 300x190mm  | 14.0 | 2006 | 19C8F |
| Chi Mei O... | CMO1443 | N140B6-L01   | 1366x768  | 310x170mm  | 13.9 | 2008 | BE76F |
| Chi Mei O... | CMO1444 | N140B6-L02   | 1366x768  | 310x170mm  | 13.9 | 2008 | 7C303 |
| Chi Mei O... | CMO1451 | N140B6-L06   | 1366x768  | 310x170mm  | 13.9 | 2009 | 757C3 |
| Chi Mei O... | CMO1453 | N140B6-L08   | 1366x768  | 310x170mm  | 13.9 | 2009 | 03153 |
| Chi Mei O... | CMO1456 |              | 1366x768  | 310x170mm  | 13.9 | 2009 | 1299A |
| Chi Mei O... | CMO1457 | N140B6-L24   | 1366x768  | 310x170mm  | 13.9 | 2010 | 9F40C |
| Chi Mei O... | CMO1461 | N140B6-L02   | 1366x768  | 310x170mm  | 13.9 | 2008 | 56A00 |
| Chi Mei O... | CMO1462 | JXCN8        | 1280x800  | 300x190mm  | 14.0 | 2010 | 68456 |
| Chi Mei O... | CMO1464 | KJ303        | 1366x768  | 300x170mm  | 13.6 | 2011 | 96B52 |
| Chi Mei O... | CMO1464 | N140BGE-L11  | 1366x768  | 310x180mm  | 14.1 | 2010 | F5372 |
| Chi Mei O... | CMO1465 | N140BGE-L21  | 1366x768  | 310x180mm  | 14.1 | 2010 | 5F127 |
| Chi Mei O... | CMO1467 |              | 1366x768  | 310x180mm  | 14.1 | 2010 | 78725 |
| Chi Mei O... | CMO1467 | N140BGE-L31  | 1366x768  | 310x180mm  | 14.1 | 2010 | CAB41 |
| Chi Mei O... | CMO1469 | 7JRT9        | 1366x768  | 310x170mm  | 13.9 | 2012 | CC5D0 |
| Chi Mei O... | CMO1504 | N154I1-L09   | 1280x800  | 330x210mm  | 15.4 |      | F9CAB |
| Chi Mei O... | CMO1508 | N150P5-L01   | 1400x1050 | 300x230mm  | 14.9 |      | B0F24 |
| Chi Mei O... | CMO1510 | N150P5-L02   | 1400x1050 | 300x230mm  | 14.9 |      | CF9ED |
| Chi Mei O... | CMO1511 | N150X3-L08   | 1024x768  | 300x230mm  | 14.9 |      | 23D40 |
| Chi Mei O... | CMO1513 | N150X3-L09   | 1024x768  | 300x230mm  | 14.9 |      | E526D |
| Chi Mei O... | CMO1514 | N154I1-L0B   | 1280x800  | 330x210mm  | 15.4 |      | F1900 |
| Chi Mei O... | CMO1516 | N154I1-L0C   | 1280x800  | 330x210mm  | 15.4 |      | FE89B |
| Chi Mei O... | CMO1520 | N154Z1-L01   | 1680x1050 | 330x200mm  | 15.2 |      | 59ED8 |
| Chi Mei O... | CMO1523 | N154Z1-L02   | 1680x1050 | 330x200mm  | 15.2 |      | 00CE7 |
| Chi Mei O... | CMO1526 | N154I2-L02   | 1280x800  | 330x210mm  | 15.4 | 2006 | 6379A |
| Chi Mei O... | CMO1541 | N154C3-L02   | 1440x900  | 330x200mm  | 15.2 | 2006 | 147D1 |
| Chi Mei O... | CMO1545 | N154C1-L01   | 1440x900  | 330x210mm  | 15.4 | 2006 | 3C597 |
| Chi Mei O... | CMO1552 | N154I2-L05   | 1280x800  | 330x210mm  | 15.4 |      | DBEAE |
| Chi Mei O... | CMO1553 | N154I3-L02   | 1280x800  | 330x210mm  | 15.4 | 2007 | CA391 |
| Chi Mei O... | CMO1554 | N154I3-L03   | 1280x800  | 330x210mm  | 15.4 | 2007 | 4B8F6 |
| Chi Mei O... | CMO1557 | G732G        | 1366x768  | 340x190mm  | 15.3 | 2008 | C4BCE |
| Chi Mei O... | CMO1557 | N156B3-L01   | 1366x768  | 350x190mm  | 15.7 | 2007 | 6E3D3 |
| Chi Mei O... | CMO1558 | K647H        | 1366x768  | 340x190mm  | 15.3 | 2008 | E6B73 |
| Chi Mei O... | CMO1558 | N156B3-L02   | 1366x768  | 350x190mm  | 15.7 | 2007 | 8E2B0 |
| Chi Mei O... | CMO1561 | N154I6-L03   | 1280x800  | 330x210mm  | 15.4 | 2007 | 21003 |
| Chi Mei O... | CMO1570 | N156B6-D07   | 1366x768  | 350x190mm  | 15.7 | 2009 | 855DF |
| Chi Mei O... | CMO1570 | N156B6-L03   | 1366x768  | 350x190mm  | 15.7 | 2008 | 71E77 |
| Chi Mei O... | CMO1571 | N156B6-L04   | 1366x768  | 350x190mm  | 15.7 | 2008 | 2CE2A |
| Chi Mei O... | CMO1581 | N156B3-L04   | 1366x768  | 350x190mm  | 15.7 | 2009 | C84FB |
| Chi Mei O... | CMO1590 | N156B6-L0D   | 1366x768  | 340x190mm  | 15.3 | 2009 | 2C2FB |
| Chi Mei O... | CMO1591 | N156B6-L0A   | 1366x768  | 350x190mm  | 15.7 | 2009 | 8B029 |
| Chi Mei O... | CMO1592 | N156B6-L0B   | 1366x768  | 350x190mm  | 15.7 | 2009 | 4B2C0 |
| Chi Mei O... | CMO1593 | N156B3-L0B   | 1366x768  | 340x190mm  | 15.3 | 2010 | 16FAA |
| Chi Mei O... | CMO1598 |              | 1366x768  | 340x190mm  | 15.3 | 2009 | 6D50F |
| Chi Mei O... | CMO1599 | N156B3-L0B   | 1366x768  | 340x190mm  | 15.3 | 2010 | 5518E |
| Chi Mei O... | CMO15A1 |              | 1366x768  | 340x190mm  | 15.3 | 2011 | A5393 |
| Chi Mei O... | CMO15A1 | N156B6-L0A   | 1366x768  | 350x190mm  | 15.7 | 2009 | 33E7C |
| Chi Mei O... | CMO15A2 | N156B6-L0B   | 1366x768  | 350x190mm  | 15.7 | 2011 | 36D7B |
| Chi Mei O... | CMO15A2 |              | 1366x768  | 340x190mm  | 15.3 | 2011 | F8061 |
| Chi Mei O... | CMO15A2 | N156B6-L0B   | 1366x768  | 350x190mm  | 15.7 | 2009 | 097FA |
| Chi Mei O... | CMO15A3 | 5FNJX        | 1366x768  | 340x190mm  | 15.3 | 2011 | 5D05B |
| Chi Mei O... | CMO15A3 | N156BGE-L11  | 1366x768  | 350x190mm  | 15.7 | 2010 | 39FEC |
| Chi Mei O... | CMO15A4 | XM5XG        | 1366x768  | 340x190mm  | 15.3 | 2010 | 0972D |
| Chi Mei O... | CMO15A7 | N156BGE-L21  | 1366x768  | 350x190mm  | 15.7 | 2010 | 1DD96 |
| Chi Mei O... | CMO15AB | N156BGE-L41  | 1366x768  | 340x190mm  | 15.3 | 2010 | 16990 |
| Chi Mei O... | CMO15AB | 7F4TK        | 1366x768  | 340x190mm  | 15.3 | 2010 | AF705 |
| Chi Mei O... | CMO15AE | N156BGE-L52  | 1366x768  | 350x190mm  | 15.7 | 2011 | 1A76F |
| Chi Mei O... | CMO1600 | N164HGE-L11  | 1920x1080 | 370x190mm  | 16.4 | 2011 | 83AB3 |
| Chi Mei O... | CMO1601 | N164HGE-L12  | 1920x1080 | 370x190mm  | 16.4 | 2011 | 64577 |
| Chi Mei O... | CMO1680 | N156B6-L06   | 1366x768  | 350x190mm  | 15.7 | 2009 | 3FDE7 |
| Chi Mei O... | CMO1701 | N170C1-L02   | 1440x900  | 370x230mm  | 17.2 |      | 3F452 |
| Chi Mei O... | CMO1702 | N170C2-L01   | 1440x900  | 370x230mm  | 17.2 | 2006 | 4D2F8 |
| Chi Mei O... | CMO1703 | N170C2-L02   | 1440x900  | 370x230mm  | 17.2 | 2006 | ADFE2 |
| Chi Mei O... | CMO1711 | N173O6-L02   | 1600x900  | 390x220mm  | 17.6 | 2009 | 1F201 |
| Chi Mei O... | CMO1718 | KPMRT        | 1600x900  | 380x220mm  | 17.3 | 2010 | 93E33 |
| Chi Mei O... | CMO1719 | N173O6-L02   | 1600x900  | 390x220mm  | 17.6 | 2009 | 338FC |
| Chi Mei O... | CMO1720 | N173HGE-L11  | 1920x1080 | 380x210mm  | 17.1 | 2011 | 682C6 |
| Chi Mei O... | CMO1720 | HC9GK        | 1920x1080 | 380x210mm  | 17.1 | 2011 | C13AA |
| Chi Mei O... | CMO1721 | N173FGE-L21  | 1600x900  | 390x220mm  | 17.6 | 2009 | 3ACD9 |
| Chi Mei O... | CMO1726 | N173HGE-L21  | 1920x1080 | 380x210mm  | 17.1 | 2011 | 634C6 |
| Chi Mei O... | CMO1800 | N184H4-L01   | 1920x1080 | 410x230mm  | 18.5 | 2008 | 82A32 |
| Chi Mei O... | CMO1803 | N184H4-L04   | 1920x1080 | 410x230mm  | 18.5 | 2008 | 1E739 |
| Chi Mei O... | CMO1807 | N184H6-L02   | 1920x1080 | 410x230mm  | 18.5 | 2009 | 938E2 |
| Chi Mei O... | CMO1808 | N184HGE-L21  | 1920x1080 | 410x230mm  | 18.5 | 2011 | E3CE9 |
| Chi Mei O... | CMO2228 | CMC 22 W     | 1680x1050 | 470x300mm  | 22.0 | 2007 | BF148 |
| Chi Mei O... | CMO9C3F | N141XB Co... | 1024x768  | 280x210mm  | 13.8 |      | 7C3C9 |
| Chimei In... | CMN1040 | N101BGE-L31  | 1366x768  | 220x130mm  | 10.1 | 2013 | D9199 |
| Chimei In... | CMN1118 | N116BGE-L32  | 1366x768  | 260x140mm  | 11.6 | 2014 | 3FE88 |
| Chimei In... | CMN1118 | 9GTPJ        | 1366x768  | 260x140mm  | 11.6 | 2013 | E4413 |
| Chimei In... | CMN1118 | N116BGE-L32  | 1366x768  | 260x140mm  | 11.6 | 2011 | 8AB24 |
| Chimei In... | CMN1118 |              | 1366x768  | 260x140mm  | 11.6 | 2011 | C68FA |
| Chimei In... | CMN1119 | N116BGE-L42  | 1366x768  | 260x140mm  | 11.6 | 2011 | CC55C |
| Chimei In... | CMN1121 | N116BGE-LB1  | 1366x768  | 260x140mm  | 11.6 | 2011 | E4AD1 |
| Chimei In... | CMN1122 | N116BGE-L11  | 1366x768  | 260x140mm  | 11.6 | 2011 | D60E3 |
| Chimei In... | CMN1123 | N144NGE-E41  | 1792x768  | 330x140mm  | 14.1 | 2012 | A34B2 |
| Chimei In... | CMN1124 | N116HSE-EA1  | 1920x1080 | 260x140mm  | 11.6 | 2011 | C688A |
| Chimei In... | CMN1127 | N116HSG-WJ1  | 1920x1080 | 260x140mm  | 11.6 | 2012 | 35B80 |
| Chimei In... | CMN1128 | N116BGE-E42  | 1366x768  | 260x140mm  | 11.6 | 2014 | D4EE6 |
| Chimei In... | CMN1130 | N116BGE-EB2  | 1366x768  | 260x140mm  | 11.6 | 2013 | F14F3 |
| Chimei In... | CMN1132 | TCP4G        | 1366x768  | 260x140mm  | 11.6 | 2014 | 28969 |
| Chimei In... | CMN1132 | N116BGE-EA2  | 1366x768  | 260x140mm  | 11.6 | 2013 | 4C2D2 |
| Chimei In... | CMN1133 | N116HSE-EA2  | 1920x1080 | 260x140mm  | 11.6 | 2013 | DA24D |
| Chimei In... | CMN1137 | N116HSE-EBC  | 1920x1080 | 260x140mm  | 11.6 | 2014 | 6FB33 |
| Chimei In... | CMN1239 | N125HCE-GN1  | 1920x1080 | 280x160mm  | 12.7 | 2015 | 63213 |
| Chimei In... | CMN1338 | N34H6        | 1366x768  | 290x160mm  | 13.0 | 2011 | FE7E0 |
| Chimei In... | CMN1340 | N133FGE-L31  | 1600x900  | 290x160mm  | 13.0 | 2012 | 1AFF4 |
| Chimei In... | CMN1341 | N133BGE-LB1  | 1366x768  | 290x160mm  | 13.0 | 2012 | C3D92 |
| Chimei In... | CMN1343 | N133HSE-EA1  | 1920x1080 | 290x170mm  | 13.2 | 2012 | A03A9 |
| Chimei In... | CMN1343 | N133HSE-EA1  | 1920x1080 | 280x160mm  | 12.7 | 2011 | 9AEF0 |
| Chimei In... | CMN1345 | DFTH4        | 1920x1080 | 290x170mm  | 13.2 | 2013 | AA9CE |
| Chimei In... | CMN1345 | VKWJC        | 1920x1080 | 290x170mm  | 13.2 | 2012 | 79111 |
| Chimei In... | CMN1348 | N133HSG-F31  | 1920x1080 | 280x160mm  | 12.7 | 2012 | 4E07D |
| Chimei In... | CMN1352 | 90N37        | 1366x768  | 290x170mm  | 13.2 | 2013 | A9508 |
| Chimei In... | CMN1357 | N133HSE-EB3  | 1920x1080 | 290x170mm  | 13.2 | 2014 | 6952A |
| Chimei In... | CMN1357 |              | 1920x1080 | 290x170mm  | 13.2 | 2013 | 4CA65 |
| Chimei In... | CMN1361 | N133HSE-EA3  | 1920x1080 | 290x170mm  | 13.2 | 2014 | 9344C |
| Chimei In... | CMN1362 | N133BGE-EAB  | 1366x768  | 290x160mm  | 13.0 | 2014 | 32DBF |
| Chimei In... | CMN1363 | CYWXX        | 1920x1080 | 290x170mm  | 13.2 | 2014 | FB9C9 |
| Chimei In... | CMN1365 |              | 1920x1080 | 290x170mm  | 13.2 | 2014 | BD385 |
| Chimei In... | CMN1367 |              | 1920x1080 | 290x170mm  | 13.2 | 2015 | F8C22 |
| Chimei In... | CMN1371 | 0CKHP        | 1920x1080 | 290x170mm  | 13.2 | 2016 | 088CB |
| Chimei In... | CMN1372 | N133HCE-EN1  | 1920x1080 | 290x170mm  | 13.2 | 2016 | 2009A |
| Chimei In... | CMN1375 |              | 1920x1080 | 290x170mm  | 13.2 | 2017 | 85159 |
| Chimei In... | CMN1375 | DG7J1        | 1920x1080 | 290x170mm  | 13.2 | 2016 | 88998 |
| Chimei In... | CMN1376 |              | 1920x1080 | 290x170mm  | 13.2 | 2015 | 39634 |
| Chimei In... | CMN1379 |              | 1920x1080 | 290x170mm  | 13.2 | 2017 | 32400 |
| Chimei In... | CMN1380 |              | 1920x1080 | 290x170mm  | 13.2 | 2017 | 44D24 |
| Chimei In... | CMN1384 | G6G62        | 1920x1080 | 290x170mm  | 13.2 | 2017 | 64F00 |
| Chimei In... | CMN1469 |              | 1366x768  | 310x170mm  | 13.9 | 2011 | 571E2 |
| Chimei In... | CMN1469 | N140BGE-L12  | 1366x768  | 310x170mm  | 13.9 | 2011 | E9A48 |
| Chimei In... | CMN1470 |              | 1366x768  | 310x170mm  | 13.9 | 2011 | A1613 |
| Chimei In... | CMN1470 | N140BGE-L22  | 1366x768  | 300x170mm  | 13.6 | 2011 | A8C21 |
| Chimei In... | CMN1471 | N140BGE-L32  | 1366x768  | 310x170mm  | 13.9 | 2012 | F21F3 |
| Chimei In... | CMN1471 | JCGRY        | 1366x768  | 310x170mm  | 13.9 | 2011 | 14295 |
| Chimei In... | CMN1471 | N140BGE-L32  | 1366x768  | 310x170mm  | 13.9 | 2011 | 56BEC |
| Chimei In... | CMN1472 | N140BGE-L42  | 1366x768  | 310x170mm  | 13.9 | 2011 | 6D701 |
| Chimei In... | CMN1475 |              | 1366x768  | 310x170mm  | 13.9 | 2011 | C1CE9 |
| Chimei In... | CMN1476 | Y9P7N        | 1366x768  | 310x170mm  | 13.9 | 2011 | 42CC6 |
| Chimei In... | CMN1476 | N140BGE-LB2  | 1366x768  | 310x170mm  | 13.9 | 2011 | 81D89 |
| Chimei In... | CMN1476 | T4PHC        | 1366x768  | 310x170mm  | 13.9 | 2011 | AB448 |
| Chimei In... | CMN1476 |              | 1366x768  | 310x170mm  | 13.9 | 2011 | C5F6D |
| Chimei In... | CMN1477 | N140BGE-LA2  | 1366x768  | 310x170mm  | 13.9 | 2012 | E05A7 |
| Chimei In... | CMN1477 |              | 1366x768  | 310x170mm  | 13.9 | 2012 | F9F59 |
| Chimei In... | CMN1480 | N140BGE-L23  | 1366x768  | 310x170mm  | 13.9 | 2012 | B7464 |
| Chimei In... | CMN1481 | 9R5K4        | 1600x900  | 310x170mm  | 13.9 | 2012 | EEC66 |
| Chimei In... | CMN1482 | N140FGE-EA2  | 1600x900  | 310x170mm  | 13.9 | 2012 | 7DD24 |
| Chimei In... | CMN1484 |              | 1600x900  | 310x170mm  | 13.9 | 2012 | E785E |
| Chimei In... | CMN1487 |              | 1366x768  | 310x170mm  | 13.9 | 2014 | 5B0A2 |
| Chimei In... | CMN1487 | N140BGE-EB3  | 1366x768  | 310x170mm  | 13.9 | 2013 | 770CD |
| Chimei In... | CMN1487 | N140BGE-EB3  | 1366x768  | 310x170mm  | 13.9 | 2012 | 0269A |
| Chimei In... | CMN1489 | 9F0DP        | 1366x768  | 310x170mm  | 13.9 | 2014 | 1B843 |
| Chimei In... | CMN1490 |              | 1366x768  | 310x170mm  | 13.9 | 2014 | AA8C3 |
| Chimei In... | CMN1490 | 6761Y        | 1366x768  | 310x170mm  | 13.9 | 2014 | FCF35 |
| Chimei In... | CMN1490 | 4T17W        | 1366x768  | 310x170mm  | 13.9 | 2013 | 506A9 |
| Chimei In... | CMN1490 | N140BGE-EA3  | 1366x768  | 310x170mm  | 13.9 | 2013 | 70C9B |
| Chimei In... | CMN1491 | N140BGE-L43  | 1366x768  | 310x170mm  | 13.9 | 2014 | 27CD3 |
| Chimei In... | CMN1491 | N140BGE-L32  | 1366x768  | 310x170mm  | 13.9 | 2012 | 00D3A |
| Chimei In... | CMN1491 | N140BGE-L43  | 1366x768  | 310x170mm  | 13.9 | 2012 | BD96D |
| Chimei In... | CMN1492 | N140BGE-E33  | 1366x768  | 310x170mm  | 13.9 | 2013 | 336A0 |
| Chimei In... | CMN1492 | 08HH2        | 1366x768  | 310x170mm  | 13.9 | 2012 | E2236 |
| Chimei In... | CMN1493 | N140BGE-E... | 1366x768  | 310x170mm  | 13.9 | 2014 | 5B1F4 |
| Chimei In... | CMN1493 | N140BGE-E43  | 1366x768  | 310x170mm  | 13.9 | 2013 | 767A8 |
| Chimei In... | CMN1493 | N140BGE-E43  | 1366x768  | 310x170mm  | 13.9 | 2012 | 40274 |
| Chimei In... | CMN1494 |              | 1366x768  | 310x170mm  | 13.9 | 2013 | 5AD5E |
| Chimei In... | CMN1495 |              | 1366x768  | 310x170mm  | 13.9 | 2013 | 6BD3D |
| Chimei In... | CMN1495 |              | 1366x768  | 310x170mm  | 13.9 | 2012 | 9E907 |
| Chimei In... | CMN1496 | 17WNW        | 1366x768  | 310x170mm  | 13.9 | 2014 | D05BD |
| Chimei In... | CMN1496 | Y0G9F        | 1366x768  | 310x170mm  | 13.9 | 2013 | 0D0F6 |
| Chimei In... | CMN1498 |              | 1366x768  | 310x170mm  | 13.9 | 2012 | 32960 |
| Chimei In... | CMN1499 |              | 1366x768  | 310x170mm  | 13.9 | 2013 | 9A0A2 |
| Chimei In... | CMN1499 |              | 1366x768  | 310x170mm  | 13.9 | 2012 | 0A386 |
| Chimei In... | CMN14A1 |              | 1366x768  | 310x170mm  | 13.9 | 2013 | C16E7 |
| Chimei In... | CMN14A1 |              | 1366x768  | 310x170mm  | 13.9 | 2012 | CB974 |
| Chimei In... | CMN14A3 |              | 1600x900  | 310x170mm  | 13.9 | 2012 | 9E69F |
| Chimei In... | CMN14A4 |              | 1366x768  | 310x170mm  | 13.9 | 2014 | 1B55F |
| Chimei In... | CMN14A4 | N140BGE-LA3  | 1366x768  | 310x170mm  | 13.9 | 2014 | 55B4C |
| Chimei In... | CMN14A7 | N140HGE-EAA  | 1920x1080 | 310x170mm  | 13.9 | 2015 | F1F31 |
| Chimei In... | CMN14A7 |              | 1920x1080 | 310x170mm  | 13.9 | 2014 | A748A |
| Chimei In... | CMN14A7 | N140HGE-EAA  | 1920x1080 | 310x170mm  | 13.9 | 2013 | 2113B |
| Chimei In... | CMN14A8 | N140HGE-EA1  | 1920x1080 | 310x170mm  | 13.9 | 2013 | ACDD6 |
| Chimei In... | CMN14A9 | N140HGE-EBA  | 1920x1080 | 310x170mm  | 13.9 | 2013 | 95479 |
| Chimei In... | CMN14B1 | N140HCE-EAA  | 1920x1080 | 310x170mm  | 13.9 | 2015 | D78CD |
| Chimei In... | CMN14B1 | N140HCE-EAA  | 1920x1080 | 310x170mm  | 13.9 | 2014 | 96D3C |
| Chimei In... | CMN14B6 |              | 1920x1080 | 310x170mm  | 13.9 | 2015 | 7D487 |
| Chimei In... | CMN14C0 |              | 1920x1080 | 310x170mm  | 13.9 | 2015 | 0843B |
| Chimei In... | CMN14C3 | JH0CN        | 1366x768  | 310x170mm  | 13.9 | 2016 | AE053 |
| Chimei In... | CMN14C3 |              | 1366x768  | 310x170mm  | 13.9 | 2016 | F92E3 |
| Chimei In... | CMN14C3 | N140BGA-EA3  | 1366x768  | 310x170mm  | 13.9 | 2015 | E0448 |
| Chimei In... | CMN14C4 | N140BGA-EB3  | 1366x768  | 310x170mm  | 13.9 | 2015 | 07E9C |
| Chimei In... | CMN14C4 |              | 1366x768  | 310x170mm  | 13.9 | 2015 | CEB71 |
| Chimei In... | CMN14C8 | 0C1RV        | 1920x1080 | 310x170mm  | 13.9 | 2016 | ED21A |
| Chimei In... | CMN14C9 | N4YGV        | 1920x1080 | 310x170mm  | 13.9 | 2017 | F887A |
| Chimei In... | CMN14C9 | N140HCA-EAB  | 1920x1080 | 310x170mm  | 13.9 | 2016 | 5601E |
| Chimei In... | CMN14D2 | N140HCE-EN1  | 1920x1080 | 310x170mm  | 13.9 | 2016 | DF71E |
| Chimei In... | CMN14D4 | 189YJ        | 1920x1080 | 310x170mm  | 13.9 | 2017 | 5885A |
| Chimei In... | CMN14D4 |              | 1920x1080 | 310x170mm  | 13.9 | 2017 | 6A3E3 |
| Chimei In... | CMN14D4 | N140HCA-EAC  | 1920x1080 | 310x170mm  | 13.9 | 2016 | 407A0 |
| Chimei In... | CMN14D5 | N140HCE-EN2  | 1920x1080 | 310x170mm  | 13.9 | 2016 | 3181E |
| Chimei In... | CMN14D6 |              | 1366x768  | 310x170mm  | 13.9 | 2017 | D7E21 |
| Chimei In... | CMN14D6 | N140BGA-EA4  | 1366x768  | 310x170mm  | 13.9 | 2016 | 05A72 |
| Chimei In... | CMN14E7 | 8KN8F        | 1920x1080 | 310x170mm  | 13.9 | 2018 | 70683 |
| Chimei In... | CMN14F2 | N140HCG-GQ2  | 1920x1080 | 310x170mm  | 13.9 | 2018 | 79BCB |
| Chimei In... | CMN15A7 | N156BGE-L21  | 1366x768  | 340x190mm  | 15.3 | 2013 | 2E617 |
| Chimei In... | CMN15A9 | N156BGE-L11  | 1366x768  | 340x190mm  | 15.3 | 2013 | A9E27 |
| Chimei In... | CMN15A9 | 53DC3        | 1366x768  | 340x190mm  | 15.3 | 2011 | 0B704 |
| Chimei In... | CMN15AA | 19X7W        | 1366x768  | 340x190mm  | 15.3 | 2011 | C88DB |
| Chimei In... | CMN15AB |              | 1366x768  | 340x190mm  | 15.3 | 2013 | 6AEDB |
| Chimei In... | CMN15AB | N156BGE-L41  | 1366x768  | 340x190mm  | 15.3 | 2013 | E69F2 |
| Chimei In... | CMN15AB | N156BGE-L41  | 1366x768  | 340x190mm  | 15.3 | 2012 | B028A |
| Chimei In... | CMN15AB |              | 1366x768  | 350x190mm  | 15.7 | 2011 | 13BB6 |
| Chimei In... | CMN15B1 | VCM8X        | 1920x1080 | 340x190mm  | 15.3 | 2012 | DC447 |
| Chimei In... | CMN15B4 | N156BGE-E11  | 1366x768  | 350x190mm  | 15.7 | 2011 | 0F36A |
| Chimei In... | CMN15B4 | N156BGE-E21  | 1366x768  | 350x190mm  | 15.7 | 2011 | CEC8F |
| Chimei In... | CMN15B5 | N156BGE-LA1  | 1366x768  | 340x190mm  | 15.3 | 2012 | 6C37A |
| Chimei In... | CMN15B6 | R33H8        | 1366x768  | 340x190mm  | 15.3 | 2013 | 90784 |
| Chimei In... | CMN15B6 | 7GYF0        | 1366x768  | 340x190mm  | 15.3 | 2012 | 14842 |
| Chimei In... | CMN15B6 | R2VC8        | 1366x768  | 340x190mm  | 15.3 | 2012 | 1A4EA |
| Chimei In... | CMN15B6 |              | 1366x768  | 340x190mm  | 15.3 | 2012 | 4CC52 |
| Chimei In... | CMN15B6 | 8D7T0        | 1366x768  | 340x190mm  | 15.3 | 2012 | 863A1 |
| Chimei In... | CMN15B6 | N156BGE-LB1  | 1366x768  | 340x190mm  | 15.3 | 2012 | D7807 |
| Chimei In... | CMN15B7 | N156BGE-EB1  | 1366x768  | 340x190mm  | 15.3 | 2013 | 8D21D |
| Chimei In... | CMN15B7 | N156BGE-EB1  | 1366x768  | 340x190mm  | 15.3 | 2012 | 5D735 |
| Chimei In... | CMN15B8 | N156BGE-L31  | 1366x768  | 340x190mm  | 15.3 | 2013 | E2959 |
| Chimei In... | CMN15B8 | N156BGE-L31  | 1366x768  | 340x190mm  | 15.3 | 2012 | 10DBE |
| Chimei In... | CMN15B9 |              | 1920x1080 | 340x190mm  | 15.3 | 2013 | 90DAA |
| Chimei In... | CMN15B9 | N156HGE-LA1  | 1920x1080 | 340x190mm  | 15.3 | 2012 | 789EC |
| Chimei In... | CMN15BA | 9FN4Y        | 1920x1080 | 340x190mm  | 15.3 | 2012 | D9DE5 |
| Chimei In... | CMN15BB |              | 1920x1080 | 340x190mm  | 15.3 | 2012 | 0D0C9 |
| Chimei In... | CMN15BB | N156HGE-LB1  | 1920x1080 | 340x190mm  | 15.3 | 2012 | 5893B |
| Chimei In... | CMN15BC |              | 1366x768  | 340x190mm  | 15.3 | 2013 | A0B6D |
| Chimei In... | CMN15BC |              | 1366x768  | 350x190mm  | 15.7 | 2012 | FB735 |
| Chimei In... | CMN15BD | N156BGE-EA1  | 1366x768  | 340x190mm  | 15.3 | 2013 | 32BCB |
| Chimei In... | CMN15BD | N156BGE-EA1  | 1366x768  | 340x190mm  | 15.3 | 2012 | 93D19 |
| Chimei In... | CMN15BE |              | 1366x768  | 340x190mm  | 15.3 | 2013 | 40A9A |
| Chimei In... | CMN15BE | N156BGE-E31  | 1366x768  | 340x190mm  | 15.3 | 2013 | 8EBB9 |
| Chimei In... | CMN15BE | N156BGE-E31  | 1366x768  | 340x190mm  | 15.3 | 2012 | 6575A |
| Chimei In... | CMN15BE |              | 1366x768  | 340x190mm  | 15.3 | 2012 | AAA00 |
| Chimei In... | CMN15BF | N156BGE-E41  | 1366x768  | 340x190mm  | 15.3 | 2013 | 7D161 |
| Chimei In... | CMN15BF | KYK75        | 1366x768  | 340x190mm  | 15.3 | 2013 | D153D |
| Chimei In... | CMN15BF | N156BGE-E41  | 1366x768  | 340x190mm  | 15.3 | 2012 | 1E428 |
| Chimei In... | CMN15C0 | N156HGE-EA1  | 1920x1080 | 340x190mm  | 15.3 | 2013 | E04CE |
| Chimei In... | CMN15C2 |              | 1920x1080 | 340x190mm  | 15.3 | 2013 | D78BA |
| Chimei In... | CMN15C3 |              | 1920x1080 | 340x190mm  | 15.3 | 2014 | CAD73 |
| Chimei In... | CMN15C3 | N156HGE-EA2  | 1920x1080 | 340x190mm  | 15.3 | 2013 | 92A63 |
| Chimei In... | CMN15C4 | 8KV42        | 1920x1080 | 340x190mm  | 15.3 | 2015 | 77DBD |
| Chimei In... | CMN15C4 | N156HGE-EAB  | 1920x1080 | 340x190mm  | 15.3 | 2013 | 7FB93 |
| Chimei In... | CMN15C5 |              | 1366x768  | 340x190mm  | 15.3 | 2015 | FD714 |
| Chimei In... | CMN15C5 |              | 1366x768  | 340x190mm  | 15.3 | 2014 | 2BFD1 |
| Chimei In... | CMN15C5 | F4X6Y        | 1366x768  | 340x190mm  | 15.3 | 2014 | 7CB72 |
| Chimei In... | CMN15C5 | N156BGE-EA2  | 1366x768  | 340x190mm  | 15.3 | 2013 | 8C0EB |
| Chimei In... | CMN15C6 | 1TT80        | 1366x768  | 340x190mm  | 15.3 | 2016 | 436D8 |
| Chimei In... | CMN15C6 |              | 1366x768  | 340x190mm  | 15.3 | 2015 | 47211 |
| Chimei In... | CMN15C6 | N156BGE-EB2  | 1366x768  | 340x190mm  | 15.3 | 2013 | D9874 |
| Chimei In... | CMN15C9 | N156BGE-E32  | 1366x768  | 340x190mm  | 15.3 | 2013 | D72B5 |
| Chimei In... | CMN15CA | 53MPX        | 1366x768  | 340x190mm  | 15.3 | 2014 | 29D08 |
| Chimei In... | CMN15CA |              | 1366x768  | 340x190mm  | 15.3 | 2014 | 6FA06 |
| Chimei In... | CMN15CA | N156BGE-E42  | 1366x768  | 340x190mm  | 15.3 | 2014 | F6E16 |
| Chimei In... | CMN15CB | N156HGE-EBB  | 1920x1080 | 340x190mm  | 15.3 | 2014 | AC959 |
| Chimei In... | CMN15CC |              | 1366x768  | 340x190mm  | 15.3 | 2015 | 5E9CE |
| Chimei In... | CMN15D2 | N156HGE-EAL  | 1920x1080 | 340x190mm  | 15.3 | 2015 | F25D8 |
| Chimei In... | CMN15D3 |              | 1920x1080 | 340x190mm  | 15.3 | 2015 | C687A |
| Chimei In... | CMN15D3 | N156HCE-EAA  | 1920x1080 | 340x190mm  | 15.3 | 2015 | C8705 |
| Chimei In... | CMN15D5 | N156HGA-EAB  | 1920x1080 | 340x190mm  | 15.3 | 2015 | B4D20 |
| Chimei In... | CMN15D6 | N156HGA-EAL  | 1920x1080 | 340x190mm  | 15.3 | 2015 | 2CAD2 |
| Chimei In... | CMN15D7 | N156HCA-EA1  | 1920x1080 | 340x190mm  | 15.3 | 2015 | A2E9D |
| Chimei In... | CMN15D9 | N156HGA-EBB  | 1920x1080 | 340x190mm  | 15.3 | 2015 | 73D63 |
| Chimei In... | CMN15DB | JMC9X        | 1366x768  | 340x190mm  | 15.3 | 2016 | 679F8 |
| Chimei In... | CMN15DB | N156BGA-EA2  | 1366x768  | 340x190mm  | 15.3 | 2015 | 5BD82 |
| Chimei In... | CMN15DB |              | 1366x768  | 340x190mm  | 15.3 | 2015 | B5CF6 |
| Chimei In... | CMN15DC | N156BGA-EB2  | 1366x768  | 340x190mm  | 15.3 | 2015 | 2FFC1 |
| Chimei In... | CMN15DC |              | 1366x768  | 340x190mm  | 15.3 | 2015 | AFC96 |
| Chimei In... | CMN15E0 | N156HCA-EBA  | 1920x1080 | 340x190mm  | 15.3 | 2017 | 1C8E2 |
| Chimei In... | CMN15E0 | 52KF6        | 1920x1080 | 340x190mm  | 15.3 | 2016 | 37FA6 |
| Chimei In... | CMN15E2 | N156HGE-DAB  | 1920x1080 | 340x190mm  | 15.3 | 2016 | 5767F |
| Chimei In... | CMN15E3 |              | 1920x1080 | 340x190mm  | 15.3 | 2015 | 916EA |
| Chimei In... | CMN15E5 | CP7VM        | 1920x1080 | 340x190mm  | 15.3 | 2018 | 442A2 |
| Chimei In... | CMN15E5 | N156HCA-EAA  | 1920x1080 | 340x190mm  | 15.3 | 2016 | 93F5A |
| Chimei In... | CMN15E6 | N156BGA-EA3  | 1366x768  | 340x190mm  | 15.3 | 2016 | 1E8FA |
| Chimei In... | CMN15E7 |              | 1920x1080 | 340x190mm  | 15.3 | 2017 | 4DD38 |
| Chimei In... | CMN15E8 | N156HCE-EN1  | 1920x1080 | 340x190mm  | 15.3 | 2016 | 61E75 |
| Chimei In... | CMN15E9 |              | 1920x1080 | 340x190mm  | 15.3 | 2016 | 792C7 |
| Chimei In... | CMN15F4 | N156HHE-GA1  | 1920x1080 | 340x190mm  | 15.3 | 2017 | 32490 |
| Chimei In... | CMN15F5 | N156HGA-EA3  | 1920x1080 | 340x190mm  | 15.3 | 2017 | 5108D |
| Chimei In... | CMN15FD |              | 1366x768  | 340x190mm  | 15.3 | 2017 | 0A39D |
| Chimei In... | CMN1602 | N161HCA-GA1  | 1920x1080 | 360x200mm  | 16.2 | 2018 | 65A4D |
| Chimei In... | CMN1720 | HDKPV        | 1920x1080 | 380x210mm  | 17.1 | 2011 | 6348A |
| Chimei In... | CMN1728 | XJ11G        | 1600x900  | 380x220mm  | 17.3 | 2012 | 55C4C |
| Chimei In... | CMN1728 | J00VV        | 1600x900  | 380x220mm  | 17.3 | 2012 | 6DB8D |
| Chimei In... | CMN1728 | N173FGE-L23  | 1600x900  | 390x220mm  | 17.6 | 2011 | 46FDC |
| Chimei In... | CMN1729 | N173FGE-L... | 1600x900  | 380x210mm  | 17.1 | 2014 | 0B59A |
| Chimei In... | CMN1731 |              | 1600x900  | 390x220mm  | 17.6 | 2012 | 27DCF |
| Chimei In... | CMN1732 |              | 1600x900  | 390x220mm  | 17.6 | 2012 | 3ECCB |
| Chimei In... | CMN1733 |              | 1600x900  | 390x220mm  | 17.6 | 2012 | CCFDE |
| Chimei In... | CMN1734 | N173FGE-E23  | 1600x900  | 380x210mm  | 17.1 | 2014 | 702F8 |
| Chimei In... | CMN1734 | M6RGM        | 1600x900  | 380x210mm  | 17.1 | 2013 | 6A88D |
| Chimei In... | CMN1735 | N173HGE-E11  | 1920x1080 | 380x220mm  | 17.3 | 2014 | 85077 |
| Chimei In... | CMN1735 | N173HGE-E11  | 1920x1080 | 380x210mm  | 17.1 | 2014 | 94E51 |
| Chimei In... | CMN1735 | 9Y6GJ        | 1920x1080 | 380x220mm  | 17.3 | 2014 | CD36F |
| Chimei In... | CMN1735 | N173HGE-E11  | 1920x1080 | 380x210mm  | 17.1 | 2012 | ABAE1 |
| Chimei In... | CMN1737 | N173HGE-E... | 1920x1080 | 380x210mm  | 17.1 | 2014 | CEE07 |
| Chimei In... | CMN1737 | N173HGE-E21  | 1920x1080 | 380x210mm  | 17.1 | 2014 | D5E92 |
| Chimei In... | CMN1738 | 0Y9WG        | 1920x1080 | 380x210mm  | 17.1 | 2018 | AB329 |
| Chimei In... | CMN1738 |              | 1920x1080 | 380x210mm  | 17.1 | 2015 | 5CE9C |
| Chimei In... | CMN1738 | XWCYC        | 1920x1080 | 380x210mm  | 17.1 | 2015 | 9166A |
| Chimei In... | CMN1738 | N1YPX        | 1920x1080 | 380x210mm  | 17.1 | 2015 | D336C |
| Chimei In... | CMN1738 | N173HCE-E31  | 1920x1080 | 380x210mm  | 17.1 | 2014 | 4340A |
| Chimei In... | CMN1740 |              | 1600x900  | 380x210mm  | 17.1 | 2014 | 94859 |
| Chimei In... | CMN1745 | N173FGA-E34  | 1600x900  | 380x210mm  | 17.1 | 2016 | 51264 |
| Chimei In... | CMN1745 |              | 1600x900  | 380x210mm  | 17.1 | 2016 | 89C79 |
| Chimei In... | CMN1746 |              | 1600x900  | 380x210mm  | 17.1 | 2016 | 706A3 |
| Chimei In... | CMN1747 | N173HHE-G32  | 1920x1080 | 380x210mm  | 17.1 | 2016 | C9B36 |
| Chimei In... | CMN175C | N173HCE-G33  | 1920x1080 | 380x210mm  | 17.1 | 2018 | B883F |
| Chimei In... | CMN8201 | P130ZFA-BA1  | 2160x1440 | 280x180mm  | 13.1 | 2018 | 92818 |
| Chimei In... | CMN8201 | P130ZDZ-EF1  | 2160x1440 | 280x180mm  | 13.1 | 2016 | E0140 |
| Chimei In... | CMNAE0D | CMN N173F... | 1600x900  | 390x220mm  | 17.6 | 2012 | D2F90 |
| DENON        | DON0035 | AVR          | 1920x1080 | 600x340mm  | 27.2 | 2013 | 97A9C |
| DENON        | DON003D | AVR          | 1920x1080 | 640x360mm  | 28.9 | 2014 | 5AC35 |
| DENON        | DON0046 | AVR          | 3840x2160 |            |      | 2015 | 3D23E |
| DENON        | DON0053 | AVR          | 1920x1080 | 1210x680mm | 54.6 | 2017 | BDE5C |
| DENON        | DON0054 | AVR          | 3840x2160 | 1110x620mm | 50.1 | 2017 | 7A51E |
| DENON        | DON0055 | AVR          | 1920x540  | 800x450mm  | 36.1 | 2017 | 1C4B1 |
| DNS          | DNS2461 | J279         | 1920x1080 | 600x340mm  | 27.2 | 2010 | 77720 |
| DNS          | DNS2465 | J240         | 1920x1080 | 520x300mm  | 23.6 | 2010 | 68734 |
| DNS          | DNS2510 | JL270U       | 1920x1080 | 600x340mm  | 27.2 | 2012 | 338DF |
| DNS          | DNS2522 | JL220        | 1920x1080 | 480x270mm  | 21.7 | 2013 | 47E1F |
| Daewoo       | DWE0195 | 19.5 monitor | 1600x900  | 430x240mm  | 19.4 | 2013 | DF0EF |
| Daewoo       | DWE0215 | 21.5 monitor | 1920x1080 | 480x270mm  | 21.7 | 2013 | 9D9F3 |
| Daewoo       | DWE0236 |              | 1920x1080 | 520x290mm  | 23.4 | 2014 | 313D8 |
| Dell         | DEL06CC |              | 1600x900  | 440x240mm  | 19.7 | 2014 | 3E8BA |
| Dell         | DEL074A |              | 1920x1080 | 530x300mm  | 24.0 | 2015 | 601AE |
| Dell         | DEL2004 | D1918H       | 1366x768  | 410x230mm  | 18.5 | 2017 | 82BE1 |
| Dell         | DEL200B | D3218HN      | 1920x1080 | 700x390mm  | 31.5 | 2018 | 2AD26 |
| Dell         | DEL3006 | 1702FP       | 1280x1024 | 340x270mm  | 17.1 |      | B71DE |
| Dell         | DEL300D | 1504FP       | 1024x768  | 300x220mm  | 14.6 |      | 3253A |
| Dell         | DEL3011 | 1703FP       | 1280x1024 | 340x270mm  | 17.1 |      | 1B3AA |
| Dell         | DEL3016 | 1704FPV      | 1280x1024 | 340x270mm  | 17.1 |      | 15862 |
| Dell         | DEL3018 | 1706FPV      | 1280x1024 | 340x270mm  | 17.1 |      | 3E8CB |
| Dell         | DEL4001 | 1901FP       | 1280x1024 | 380x310mm  | 19.3 |      | F25F7 |
| Dell         | DEL4004 | 1704FPT      | 1280x1024 | 340x270mm  | 17.1 |      | DD3D1 |
| Dell         | DEL4005 | 1704FPT      | 1280x1024 | 340x270mm  | 17.1 |      | 2D55E |
| Dell         | DEL4006 | 1505FP       | 1024x768  | 300x230mm  | 14.9 |      | BD2CF |
| Dell         | DEL400D | 1905FP       | 1280x1024 | 380x310mm  | 19.3 |      | A332B |
| Dell         | DEL4013 | 1707FP       | 1280x1024 | 340x270mm  | 17.1 | 2006 | 33C4A |
| Dell         | DEL4015 | 1907FP       | 1280x1024 | 380x300mm  | 19.1 | 2007 | CA106 |
| Dell         | DEL4015 | 1907FP       | 1280x1024 | 380x300mm  | 19.1 | 2006 | 223B7 |
| Dell         | DEL4020 | 1907FPV      | 1280x1024 | 380x300mm  | 19.1 | 2009 | 3AFD8 |
| Dell         | DEL4020 | 1907FPV      | 1280x1024 | 380x300mm  | 19.1 | 2007 | 03B55 |
| Dell         | DEL4024 | 1708FP       | 1280x1024 | 340x270mm  | 17.1 | 2007 | 8793A |
| Dell         | DEL4026 | 1908FP       | 1280x1024 | 380x300mm  | 19.1 | 2008 | B3003 |
| Dell         | DEL4026 | 1908FP       | 1280x1024 | 380x300mm  | 19.1 | 2007 | 07D6E |
| Dell         | DEL4035 |              | 2560x1600 | 650x410mm  | 30.3 | 2008 | 6635E |
| Dell         | DEL4036 | 3008WFP      | 2560x1600 | 650x410mm  | 30.3 | 2009 | B82B9 |
| Dell         | DEL4039 | SP2208WFP    | 1680x1050 | 470x290mm  | 21.7 | 2008 | 854BD |
| Dell         | DEL403C | 2208WFP      | 1680x1050 | 470x300mm  | 22.0 | 2009 | 9F015 |
| Dell         | DEL403C | 2208WFP      | 1680x1050 | 470x300mm  | 22.0 | 2008 | 24F53 |
| Dell         | DEL4042 | 2009W        | 1680x1050 | 430x270mm  | 20.0 | 2008 | 20C8E |
| Dell         | DEL404B | G2410        | 1920x1080 | 530x300mm  | 24.0 | 2009 | CFA68 |
| Dell         | DEL404D | P2210        | 1680x1050 | 470x300mm  | 22.0 | 2012 | 4ACC0 |
| Dell         | DEL404D | P2210        | 1680x1050 | 470x300mm  | 22.0 | 2010 | F7C59 |
| Dell         | DEL404D | P2210        | 1680x1050 | 470x300mm  | 22.0 | 2009 | DE04C |
| Dell         | DEL404E | P2210        | 1680x1050 | 470x300mm  | 22.0 | 2012 | 47770 |
| Dell         | DEL404E | P2210        | 1680x1050 | 470x300mm  | 22.0 | 2011 | 56884 |
| Dell         | DEL404E | P2210        | 1680x1050 | 470x300mm  | 22.0 | 2010 | 68394 |
| Dell         | DEL404E | P2210        | 1680x1050 | 470x300mm  | 22.0 | 2009 | C9453 |
| Dell         | DEL4059 | P170S        | 1280x1024 | 340x270mm  | 17.1 | 2011 | 3F758 |
| Dell         | DEL405A | P190S        | 1280x1024 | 380x300mm  | 19.1 | 2011 | 178CE |
| Dell         | DEL405B | P190S        | 1280x1024 | 380x300mm  | 19.1 | 2011 | 32320 |
| Dell         | DEL405B | P190S        | 1280x1024 | 380x300mm  | 19.1 | 2010 | D6E4C |
| Dell         | DEL405C |              | 1920x1080 | 510x290mm  | 23.1 | 2012 | F9A7C |
| Dell         | DEL405E | U2211H       | 1920x1080 | 480x270mm  | 21.7 | 2010 | 8AC1A |
| Dell         | DEL405F | U2211H       | 1920x1080 | 480x270mm  | 21.7 | 2010 | AEB7C |
| Dell         | DEL4061 | P2211H       | 1920x1080 | 480x270mm  | 21.7 | 2011 | 1D336 |
| Dell         | DEL4061 | P2211H       | 1920x1080 | 480x270mm  | 21.7 | 2010 | 3D8FA |
| Dell         | DEL4063 | U3011        | 2560x1600 | 640x400mm  | 29.7 | 2011 | 6655B |
| Dell         | DEL406A | P2011H       | 1600x900  | 440x250mm  | 19.9 | 2011 | D8066 |
| Dell         | DEL406C | E2011H       | 1600x900  | 440x250mm  | 19.9 | 2012 | 4514D |
| Dell         | DEL4072 | U2312HM      | 1920x1080 | 510x290mm  | 23.1 | 2013 | 03FE9 |
| Dell         | DEL4072 | U2312HM      | 1920x1080 | 510x290mm  | 23.1 | 2012 | 01F23 |
| Dell         | DEL4072 | U2312HM      | 1920x1080 | 510x290mm  | 23.1 | 2011 | 07FC9 |
| Dell         | DEL4073 | U2312HM      | 1920x1080 | 510x290mm  | 23.1 | 2012 | DF769 |
| Dell         | DEL4073 | U2312HM      | 1920x1080 | 510x290mm  | 23.1 | 2011 | 2833F |
| Dell         | DEL407E | U2713HM      | 2560x1440 | 600x340mm  | 27.2 | 2014 | 1EC97 |
| Dell         | DEL407E | U2713HM      | 2560x1440 | 600x340mm  | 27.2 | 2013 | 1159D |
| Dell         | DEL407F | U2713HM      | 1920x1080 | 600x340mm  | 27.2 | 2012 | B660B |
| Dell         | DEL4080 | U2713HM      | 2560x1440 | 600x340mm  | 27.2 | 2014 | A4ACD |
| Dell         | DEL4080 | U2713HM      | 2560x1440 | 600x340mm  | 27.2 | 2013 | AA0E0 |
| Dell         | DEL4080 | U2713HM      | 2560x1440 | 600x340mm  | 27.2 | 2012 | A73C4 |
| Dell         | DEL4084 | S2340T       | 1920x1080 | 510x290mm  | 23.1 | 2013 | DFBDD |
| Dell         | DEL4085 | S2340T       | 1920x1080 | 510x290mm  | 23.1 | 2013 | 193E6 |
| Dell         | DEL408A | U2913WM      | 2560x1080 | 670x280mm  | 28.6 | 2016 | C1D44 |
| Dell         | DEL408B | U2913WM      | 2560x1080 | 670x280mm  | 28.6 | 2015 | D2BF5 |
| Dell         | DEL408B | U2913WM      | 2560x1080 | 670x280mm  | 28.6 | 2013 | 4C361 |
| Dell         | DEL408E |              | 1600x900  | 440x250mm  | 19.9 | 2013 | B23CB |
| Dell         | DEL408E |              | 1600x900  | 440x250mm  | 19.9 | 2012 | 80E2B |
| Dell         | DEL4090 | E2414H       | 1920x1080 | 530x300mm  | 24.0 | 2015 | 175CC |
| Dell         | DEL4091 | E2414H       | 1920x1080 | 530x300mm  | 24.0 | 2015 | 69196 |
| Dell         | DEL4091 | E2414H       | 1920x1080 | 530x300mm  | 24.0 | 2014 | 4E852 |
| Dell         | DEL4091 | E2414H       | 1920x1080 | 530x300mm  | 24.0 | 2013 | 6594F |
| Dell         | DEL4098 | P2314H       | 1920x1080 | 510x290mm  | 23.1 | 2014 | 8E3ED |
| Dell         | DEL4098 | P2314H       | 1920x1080 | 510x290mm  | 23.1 | 2013 | 26F1C |
| Dell         | DEL4099 | P2314H       | 1920x1080 | 510x290mm  | 23.1 | 2015 | 0CA44 |
| Dell         | DEL4099 | P2314H       | 1920x1080 | 510x290mm  | 23.1 | 2014 | C709D |
| Dell         | DEL4099 | P2314H       | 1920x1080 | 510x290mm  | 23.1 | 2013 | 5C2CA |
| Dell         | DEL40AD | E2014T       | 1600x900  | 430x240mm  | 19.4 | 2014 | 842E9 |
| Dell         | DEL40B8 | UP2715K      | 3840x2160 | 600x340mm  | 27.2 | 2015 | C2BA8 |
| Dell         | DEL40BB | S2715H       | 1920x1080 | 600x340mm  | 27.2 | 2017 | 5F50D |
| Dell         | DEL40BD | P2715Q       | 3840x2160 | 600x340mm  | 27.2 | 2016 | 7A11F |
| Dell         | DEL40BD | P2715Q       | 3840x2160 | 600x340mm  | 27.2 | 2015 | 44CE3 |
| Dell         | DEL40BF | P2715Q       | 3840x2160 | 600x340mm  | 27.2 | 2015 | 12605 |
| Dell         | DEL40CA |              | 1920x1080 | 530x300mm  | 24.0 | 2014 | F3206 |
| Dell         | DEL40DE | UP2716D      | 2560x1440 | 600x340mm  | 27.2 | 2015 | 3C53C |
| Dell         | DEL40E7 | U2417H       | 1920x1080 | 530x300mm  | 24.0 | 2018 | E3022 |
| Dell         | DEL40E7 | U2417H       | 1920x1080 | 530x300mm  | 24.0 | 2016 | 66979 |
| Dell         | DEL40E8 | U2417H       | 1920x1080 | 530x300mm  | 24.0 | 2018 | 0ADB0 |
| Dell         | DEL40E8 | U2417H       | 1920x1080 | 530x300mm  | 24.0 | 2017 | 6B1C1 |
| Dell         | DEL40EE | S2817Q       | 3840x2160 | 620x340mm  | 27.8 | 2018 | 1EF32 |
| Dell         | DEL40EE | S2817Q       | 3840x2160 | 620x340mm  | 27.8 | 2017 | 86846 |
| Dell         | DEL40F2 | P2317H       | 1920x1080 | 510x290mm  | 23.1 | 2017 | FB2DF |
| Dell         | DEL40F3 | P2317H       | 1920x1080 | 510x290mm  | 23.1 | 2016 | D7898 |
| Dell         | DEL40F4 | P2317H       | 1920x1080 | 510x290mm  | 23.1 | 2018 | 1FC8B |
| Dell         | DEL40F4 | P2317H       | 1920x1080 | 510x290mm  | 23.1 | 2017 | 386E6 |
| Dell         | DEL40F4 | P2317H       | 1920x1080 | 510x290mm  | 23.1 | 2016 | 8FC7B |
| Dell         | DEL40F7 | P2717H       | 1920x1080 | 600x340mm  | 27.2 | 2017 | 5465E |
| Dell         | DEL40F7 | P2717H       | 1920x1080 | 600x340mm  | 27.2 | 2016 | 02E3E |
| Dell         | DEL40FA | UP3017       | 2560x1600 | 640x400mm  | 29.7 | 2016 | 67859 |
| Dell         | DEL4101 |              | 1920x1080 | 530x300mm  | 24.0 | 2015 | D77E6 |
| Dell         | DEL4113 | P2418HT      | 1920x1080 | 530x300mm  | 24.0 | 2017 | 5739B |
| Dell         | DEL4114 | P2418HT      | 1920x1080 | 530x300mm  | 24.0 | 2017 | 040FE |
| Dell         | DEL4120 | S2418H/HX    | 1920x1080 | 530x300mm  | 24.0 | 2017 | 0F53E |
| Dell         | DEL4123 | S2418HN/NX   | 1920x1080 | 530x300mm  | 24.0 | 2017 | 2B54B |
| Dell         | DEL4124 | S2418HN/NX   | 1920x1080 | 530x300mm  | 24.0 | 2017 | 33485 |
| Dell         | DEL413A | U2518D       | 2560x1440 | 550x310mm  | 24.9 | 2017 | 3756B |
| Dell         | DEL4145 |              | 1920x1080 | 540x310mm  | 24.5 | 2016 | 3D1E5 |
| Dell         | DEL4166 | S2719DM      | 2560x1440 | 600x340mm  | 27.2 | 2018 | 7421C |
| Dell         | DEL4167 | S2719DM      | 2560x1440 | 600x340mm  | 27.2 | 2018 | 31301 |
| Dell         | DEL4168 | U2419HC      | 1920x1080 | 530x300mm  | 24.0 | 2018 | 53797 |
| Dell         | DEL417E | U2719DC      | 2560x1440 | 600x340mm  | 27.2 | 2018 | 9AFCD |
| Dell         | DEL4185 | P2719H       | 1920x1080 | 600x340mm  | 27.2 | 2018 | 6EC29 |
| Dell         | DEL4192 | S2719HS      | 1920x1080 | 600x340mm  | 27.2 | 2018 | 24CDF |
| Dell         | DEL93E4 |              | 1920x1080 | 510x290mm  | 23.1 | 2012 | DB188 |
| Dell         | DEL93E8 |              | 1920x1080 | 510x290mm  | 23.1 | 2013 | 32007 |
| Dell         | DEL93F1 |              | 1600x900  | 430x240mm  | 19.4 | 2014 | 42781 |
| Dell         | DEL93F8 |              | 1920x1080 | 510x290mm  | 23.1 | 2017 | F2073 |
| Dell         | DEL93F9 |              | 1920x1080 | 530x300mm  | 24.0 | 2017 | ED9B8 |
| Dell         | DELA008 | 2001FP       | 1600x1200 | 410x310mm  | 20.2 | 2006 | 861CD |
| Dell         | DELA008 | 2001FP       | 1600x1200 | 410x310mm  | 20.2 |      | A75F4 |
| Dell         | DELA00A | E172FP       | 1280x1024 | 340x270mm  | 17.1 |      | AEA66 |
| Dell         | DELA010 | 2405FPW      | 1920x1200 | 520x330mm  | 24.2 | 2006 | C6424 |
| Dell         | DELA010 | 2405FPW      | 1920x1200 | 520x330mm  | 24.2 |      | 0EC69 |
| Dell         | DELA016 | 2407WFP      | 1920x1200 | 520x330mm  | 24.2 | 2007 | 13239 |
| Dell         | DELA017 | 2407WFP      | 1920x1200 | 520x330mm  | 24.2 | 2007 | 09CAD |
| Dell         | DELA017 | 2407WFP      | 1920x1200 | 520x330mm  | 24.2 | 2006 | 08DE1 |
| Dell         | DELA019 | 2007WFP      | 1680x1050 | 430x270mm  | 20.0 | 2008 | 71F3A |
| Dell         | DELA019 | 2007WFP      | 1680x1050 | 430x270mm  | 20.0 | 2007 | 5689C |
| Dell         | DELA019 | 2007WFP      | 1680x1050 | 430x270mm  | 20.0 | 2006 | B1CE7 |
| Dell         | DELA021 | 2007FP       | 1600x1200 | 410x310mm  | 20.2 | 2009 | 663E5 |
| Dell         | DELA021 | 2007FP       | 1600x1200 | 410x310mm  | 20.2 | 2008 | 26180 |
| Dell         | DELA021 | 2007FP       | 1600x1200 | 410x310mm  | 20.2 | 2006 | BEDF5 |
| Dell         | DELA026 |              | 1920x1200 | 520x330mm  | 24.2 | 2008 | 82F88 |
| Dell         | DELA027 | E178FP       | 1280x1024 | 340x270mm  | 17.1 | 2007 | 20421 |
| Dell         | DELA028 | E198FP       | 1280x1024 | 380x300mm  | 19.1 | 2008 | 408C3 |
| Dell         | DELA02A | 2408WFP      | 1920x1200 | 520x320mm  | 24.0 | 2008 | 47112 |
| Dell         | DELA02C | 2408WFP      | 1920x1200 | 520x320mm  | 24.0 | 2008 | 0CFE5 |
| Dell         | DELA02E | E248WFP      | 1920x1200 | 520x320mm  | 24.0 | 2008 | 03B80 |
| Dell         | DELA030 | 2709W        | 1920x1200 | 580x360mm  | 26.9 | 2008 | 09AB9 |
| Dell         | DELA038 | S2409W       | 1920x1080 | 530x300mm  | 24.0 | 2008 | 19B1D |
| Dell         | DELA039 | S2409W       | 1920x1080 | 530x300mm  | 24.0 | 2009 | 75D1D |
| Dell         | DELA039 | S2409W       | 1920x1080 | 530x300mm  | 24.0 | 2008 | F595E |
| Dell         | DELA03D | 1909W        | 1440x900  | 410x260mm  | 19.1 | 2010 | 57D04 |
| Dell         | DELA03D | 1909W        | 1440x900  | 410x260mm  | 19.1 | 2009 | B630F |
| Dell         | DELA043 | S2209W       | 1920x1080 | 480x270mm  | 21.7 | 2009 | E20E9 |
| Dell         | DELA04C | IN1910N      | 1366x768  | 410x230mm  | 18.5 | 2009 | 79F03 |
| Dell         | DELA055 | U2711        | 2560x1440 | 600x340mm  | 27.2 | 2012 | 2D5E2 |
| Dell         | DELA055 | U2711        | 2560x1440 | 600x340mm  | 27.2 | 2011 | CBF49 |
| Dell         | DELA056 | U2711        | 1920x1080 | 600x340mm  | 27.2 | 2011 | 55AD7 |
| Dell         | DELA05F | U2311H       | 1920x1080 | 510x290mm  | 23.1 | 2011 | 1F332 |
| Dell         | DELA05F | U2311H       | 1920x1080 | 510x290mm  | 23.1 | 2010 | 220AF |
| Dell         | DELA064 | ST2220L      | 1920x1080 | 480x270mm  | 21.7 | 2010 | B4C54 |
| Dell         | DELA068 | ST2420L      | 1920x1080 | 530x300mm  | 24.0 | 2011 | 0880E |
| Dell         | DELA06C | ST2421L      | 1920x1080 | 530x300mm  | 24.0 | 2013 | 085CA |
| Dell         | DELA06C | ST2421L      | 1920x1080 | 530x300mm  | 24.0 | 2011 | B82B4 |
| Dell         | DELA072 | E2211H       | 1920x1080 | 480x270mm  | 21.7 | 2012 | FF6B8 |
| Dell         | DELA074 | P1911        | 1440x900  | 410x260mm  | 19.1 | 2012 | CC70F |
| Dell         | DELA079 | U2412M       | 1920x1200 | 520x320mm  | 24.0 | 2013 | 241E0 |
| Dell         | DELA079 | U2412M       | 1920x1200 | 520x320mm  | 24.0 | 2012 | 80AF1 |
| Dell         | DELA07A | U2412M       | 1920x1200 | 520x320mm  | 24.0 | 2016 | C7E6A |
| Dell         | DELA07A | U2412M       | 1920x1200 | 520x320mm  | 24.0 | 2015 | C7EFD |
| Dell         | DELA07A | U2412M       | 1920x1200 | 520x320mm  | 24.0 | 2014 | 06AC3 |
| Dell         | DELA07A | U2412M       | 1920x1200 | 520x320mm  | 24.0 | 2013 | 0693C |
| Dell         | DELA07A | U2412M       | 1920x1200 | 520x320mm  | 24.0 | 2012 | 05CE8 |
| Dell         | DELA07A | U2412M       | 1920x1200 | 520x320mm  | 24.0 | 2011 | 01C43 |
| Dell         | DELA07B | U2412M       | 1920x1200 | 520x320mm  | 24.0 | 2017 | E8214 |
| Dell         | DELA07B | U2412M       | 1920x1200 | 520x320mm  | 24.0 | 2015 | 1E8A4 |
| Dell         | DELA07B | U2412M       | 1920x1200 | 520x320mm  | 24.0 | 2014 | 5A422 |
| Dell         | DELA07B | U2412M       | 1920x1200 | 520x320mm  | 24.0 | 2013 | A2211 |
| Dell         | DELA07B | U2412M       | 1920x1200 | 520x320mm  | 24.0 | 2012 | 00A31 |
| Dell         | DELA07B | U2412M       | 1920x1200 | 520x320mm  | 24.0 | 2011 | 95F9A |
| Dell         | DELA07D | P2412H       | 1920x1080 | 530x300mm  | 24.0 | 2013 | CB588 |
| Dell         | DELA07D | P2412H       | 1920x1080 | 530x300mm  | 24.0 | 2012 | 866BE |
| Dell         | DELA085 | P1913S       | 1280x1024 | 380x300mm  | 19.1 | 2013 | 9B821 |
| Dell         | DELA088 | P1913        | 1440x900  | 410x260mm  | 19.1 | 2014 | C11E1 |
| Dell         | DELA08B | S2440L       | 1920x1080 | 530x300mm  | 24.0 | 2014 | 38646 |
| Dell         | DELA08B | S2440L       | 1920x1080 | 530x300mm  | 24.0 | 2013 | 36E11 |
| Dell         | DELA08B | S2440L       | 1920x1080 | 530x300mm  | 24.0 | 2012 | CC86C |
| Dell         | DELA08D | S2740L       | 1920x1080 | 600x340mm  | 27.2 | 2013 | 8F892 |
| Dell         | DELA08D | S2740L       | 1920x1080 | 600x340mm  | 27.2 | 2012 | 18FAA |
| Dell         | DELA08E | S2740L       | 1920x1080 | 600x340mm  | 27.2 | 2012 | 743BC |
| Dell         | DELA090 | E2213H       | 1920x1080 | 480x270mm  | 21.7 | 2013 | 56145 |
| Dell         | DELA093 | U2713H       | 2560x1440 | 600x340mm  | 27.2 | 2013 | F6A00 |
| Dell         | DELA095 | S2240T       | 1920x1080 | 480x270mm  | 21.7 | 2017 | 22F5B |
| Dell         | DELA096 | S2240T       | 1920x1080 | 480x270mm  | 21.7 | 2014 | FE094 |
| Dell         | DELA097 | P2214H       | 1920x1080 | 480x270mm  | 21.7 | 2015 | 036C9 |
| Dell         | DELA098 | P2214H       | 1920x1080 | 480x270mm  | 21.7 | 2014 | 5BD85 |
| Dell         | DELA09A | P2414H       | 1920x1080 | 530x300mm  | 24.0 | 2016 | 91290 |
| Dell         | DELA09A | P2414H       | 1920x1080 | 530x300mm  | 24.0 | 2015 | 0747E |
| Dell         | DELA09A | P2414H       | 1920x1080 | 530x300mm  | 24.0 | 2014 | 4B247 |
| Dell         | DELA09B | P2414H       | 1920x1080 | 530x300mm  | 24.0 | 2016 | 58E1F |
| Dell         | DELA09B | P2414H       | 1920x1080 | 530x300mm  | 24.0 | 2015 | 7DC49 |
| Dell         | DELA09C | P2414H       | 1920x1080 | 530x300mm  | 24.0 | 2015 | E997B |
| Dell         | DELA09D | E2214H       | 1920x1080 | 480x270mm  | 21.7 | 2014 | B03EA |
| Dell         | DELA09E | E2214H       | 1920x1080 | 480x270mm  | 21.7 | 2015 | 94B14 |
| Dell         | DELA09E | E2214H       | 1920x1080 | 480x270mm  | 21.7 | 2014 | 1E91B |
| Dell         | DELA0A2 | U2414H       | 1920x1080 | 530x300mm  | 24.0 | 2016 | 500CD |
| Dell         | DELA0A2 | U2414H       | 1920x1080 | 530x300mm  | 24.0 | 2014 | 4F196 |
| Dell         | DELA0A2 | U2414H       | 1920x1080 | 530x300mm  | 24.0 | 2013 | 83776 |
| Dell         | DELA0A3 | U2414H       | 1920x1080 | 530x300mm  | 24.0 | 2017 | 53D1C |
| Dell         | DELA0A3 | U2414H       | 1920x1080 | 530x300mm  | 24.0 | 2016 | 53483 |
| Dell         | DELA0A3 | U2414H       | 1920x1080 | 530x300mm  | 24.0 | 2015 | 69336 |
| Dell         | DELA0A3 | U2414H       | 1920x1080 | 530x300mm  | 24.0 | 2014 | 5F9D0 |
| Dell         | DELA0A4 | U2414H       | 1920x1080 | 530x300mm  | 24.0 | 2018 | 692C6 |
| Dell         | DELA0A4 | U2414H       | 1920x1080 | 530x300mm  | 24.0 | 2017 | 6AF02 |
| Dell         | DELA0A4 | U2414H       | 1920x1080 | 530x300mm  | 24.0 | 2015 | C28B4 |
| Dell         | DELA0A4 | U2414H       | 1920x1080 | 530x300mm  | 24.0 | 2014 | 0BA9E |
| Dell         | DELA0A6 | U3415W       | 3440x1440 | 800x330mm  | 34.1 | 2015 | BB284 |
| Dell         | DELA0B2 | U2414H       | 1920x1080 | 530x300mm  | 24.0 | 2015 | 204C0 |
| Dell         | DELA0B2 | U2414H       | 1920x1080 | 530x300mm  | 24.0 | 2014 | 3D309 |
| Dell         | DELA0B2 | U2414H       | 1920x1080 | 530x300mm  | 24.0 | 2013 | 53F07 |
| Dell         | DELA0B5 | S2415H       | 1920x1080 | 530x300mm  | 24.0 | 2016 | 70DB1 |
| Dell         | DELA0B5 | S2415H       | 1920x1080 | 530x300mm  | 24.0 | 2015 | F14CF |
| Dell         | DELA0B8 | U2415        | 1920x1200 | 520x320mm  | 24.0 | 2016 | 8BDF2 |
| Dell         | DELA0B9 | U2415        | 1920x1200 | 520x320mm  | 24.0 | 2018 | E2254 |
| Dell         | DELA0B9 | U2415        | 1920x1200 | 520x320mm  | 24.0 | 2016 | 3285E |
| Dell         | DELA0B9 | U2415        | 1920x1200 | 520x320mm  | 24.0 | 2014 | C54A2 |
| Dell         | DELA0BA | U2415        | 1920x1200 | 520x320mm  | 24.0 | 2017 | 0DDDB |
| Dell         | DELA0BA | U2415        | 1920x1200 | 520x320mm  | 24.0 | 2015 | 36D9C |
| Dell         | DELA0BA | U2415        | 1920x1200 | 520x320mm  | 24.0 | 2014 | 7B6A8 |
| Dell         | DELA0BC | U2415        | 1920x1200 | 520x320mm  | 24.0 | 2016 | 0B695 |
| Dell         | DELA0BC | U2415        | 1920x1200 | 520x320mm  | 24.0 | 2014 | 434D4 |
| Dell         | DELA0BE | P2415Q       | 3840x2160 | 530x300mm  | 24.0 | 2016 | 3CC1C |
| Dell         | DELA0BE | P2415Q       | 3840x2160 | 530x300mm  | 24.0 | 2015 | 4174F |
| Dell         | DELA0BF | P2415Q       | 3840x2160 | 530x300mm  | 24.0 | 2016 | 4EEBD |
| Dell         | DELA0C3 | P2416D       | 2560x1440 | 530x300mm  | 24.0 | 2016 | 1F35B |
| Dell         | DELA0C3 | P2416D       | 2560x1440 | 530x300mm  | 24.0 | 2015 | 144BE |
| Dell         | DELA0C4 | P2416D       | 2560x1440 | 530x300mm  | 24.0 | 2016 | 00745 |
| Dell         | DELA0C7 | E2016H       | 1600x900  | 430x240mm  | 19.4 | 2018 | 6E752 |
| Dell         | DELA0C8 | E2016H       | 1600x900  | 430x240mm  | 19.4 | 2015 | AEDD0 |
| Dell         | DELA0D1 | S2716DG      | 2560x1440 | 600x340mm  | 27.2 | 2015 | D40DA |
| Dell         | DELA0D8 | P2217H       | 1920x1080 | 480x270mm  | 21.7 | 2017 | 462F8 |
| Dell         | DELA0D8 | P2217H       | 1920x1080 | 480x270mm  | 21.7 | 2016 | 97F32 |
| Dell         | DELA0D9 | P2217H       | 1920x1080 | 480x270mm  | 21.7 | 2018 | C7AF4 |
| Dell         | DELA0D9 | P2217H       | 1920x1080 | 480x270mm  | 21.7 | 2016 | 2F1F8 |
| Dell         | DELA0DB | P2417H       | 1920x1080 | 530x300mm  | 24.0 | 2018 | 9E35D |
| Dell         | DELA0DB | P2417H       | 1920x1080 | 530x300mm  | 24.0 | 2017 | 11178 |
| Dell         | DELA0DC | P2417H       | 1920x1080 | 530x300mm  | 24.0 | 2017 | 76511 |
| Dell         | DELA0DC | P2417H       | 1920x1080 | 530x300mm  | 24.0 | 2016 | 3DFDA |
| Dell         | DELA0DD | U3417W       | 3440x1440 | 800x330mm  | 34.1 | 2017 | 3FF02 |
| Dell         | DELA0E2 | E2417H       | 1920x1080 | 530x300mm  | 24.0 | 2017 | F47FE |
| Dell         | DELA0E9 | U2718Q       | 1920x1080 | 610x350mm  | 27.7 | 2018 | BEB62 |
| Dell         | DELA0F0 | U3818DW      | 3840x1600 | 880x370mm  | 37.6 | 2018 | 8BD53 |
| Dell         | DELA0F3 | U3818DW      | 3840x1600 | 880x370mm  | 37.6 | 2017 | 0CAEC |
| Dell         | DELA105 | E2418HN      | 1920x1080 | 530x300mm  | 24.0 | 2018 | 93720 |
| Dell         | DELB123 | INSPIRON ONE | 1920x1080 | 510x290mm  | 23.1 | 2013 | 90D21 |
| Dell         | DELB123 | 23" AIO      | 1920x1080 | 510x290mm  | 23.1 | 2011 | 44BA9 |
| Dell         | DELB123 |              | 1920x1080 | 510x290mm  | 23.1 | 2010 | C72FC |
| Dell         | DELD011 | E207WFP      | 1680x1050 | 430x270mm  | 20.0 | 2008 | ABCA2 |
| Dell         | DELD011 | E207WFP      | 1680x1050 | 430x270mm  | 20.0 | 2007 | 06DCB |
| Dell         | DELD011 | E207WFP      | 1680x1050 | 430x270mm  | 20.0 | 2006 | 397C5 |
| Dell         | DELD015 | E228WFP      | 1680x1050 | 470x300mm  | 22.0 | 2008 | 26148 |
| Dell         | DELD015 | E228WFP      | 1680x1050 | 470x300mm  | 22.0 | 2007 | 24496 |
| Dell         | DELD01A | SP2009W      | 1680x1050 | 430x270mm  | 20.0 | 2008 | AF018 |
| Dell         | DELD01C | SP2309W      | 2048x1152 | 510x290mm  | 23.1 | 2009 | B08EB |
| Dell         | DELD022 | E1709W       | 1440x900  | 370x230mm  | 17.2 | 2010 | F1A65 |
| Dell         | DELD031 | E2210H       | 1920x1080 | 480x270mm  | 21.7 | 2009 | 28ECA |
| Dell         | DELD035 | E1910        | 1440x900  | 410x260mm  | 19.1 | 2010 | 37C86 |
| Dell         | DELD037 | E2210        | 1680x1050 | 470x300mm  | 22.0 | 2010 | 39AA8 |
| Dell         | DELD037 | E2210        | 1680x1050 | 470x300mm  | 22.0 | 2009 | DF13F |
| Dell         | DELD039 | D2201        | 1920x1080 | 480x270mm  | 21.7 | 2011 | C02C8 |
| Dell         | DELD03A | E1914H       | 1366x768  | 410x230mm  | 18.5 | 2014 | 82BB2 |
| Dell         | DELD03C | E2014H       | 1600x900  | 430x240mm  | 19.4 | 2014 | 01750 |
| Dell         | DELD043 | ST2220T      | 1920x1080 | 480x270mm  | 21.7 | 2011 | 01A32 |
| Dell         | DELD047 | U2212HM      | 1920x1080 | 480x270mm  | 21.7 | 2013 | 09FA6 |
| Dell         | DELD047 | U2212HM      | 1920x1080 | 480x270mm  | 21.7 | 2012 | C6B8C |
| Dell         | DELD047 | U2212HM      | 1920x1080 | 480x270mm  | 21.7 | 2011 | AF7C6 |
| Dell         | DELD048 | U2212HM      | 1920x1080 | 480x270mm  | 21.7 | 2013 | 036AB |
| Dell         | DELD048 | U2212HM      | 1920x1080 | 480x270mm  | 21.7 | 2011 | E4C73 |
| Dell         | DELD04A | S2330MX      | 1920x1080 | 510x290mm  | 23.1 | 2011 | 9035F |
| Dell         | DELD054 | S2240L       | 1920x1080 | 480x270mm  | 21.7 | 2015 | 79239 |
| Dell         | DELD054 | S2240L       | 1920x1080 | 480x270mm  | 21.7 | 2014 | 963B9 |
| Dell         | DELD054 | S2240L       | 1920x1080 | 480x270mm  | 21.7 | 2013 | 59177 |
| Dell         | DELD054 | S2240L       | 1920x1080 | 480x270mm  | 21.7 | 2012 | B7BC9 |
| Dell         | DELD058 | S2340L       | 1920x1080 | 510x290mm  | 23.1 | 2015 | 2CACE |
| Dell         | DELD058 | S2340L       | 1920x1080 | 510x290mm  | 23.1 | 2014 | 0A867 |
| Dell         | DELD058 | S2340L       | 1920x1080 | 510x290mm  | 23.1 | 2013 | 0D312 |
| Dell         | DELD05E | P2714H       | 1920x1080 | 600x340mm  | 27.2 | 2015 | DE108 |
| Dell         | DELD05F | P2714H       | 1920x1080 | 600x340mm  | 27.2 | 2015 | 5FFF1 |
| Dell         | DELD065 | U2715H       | 2560x1440 | 600x340mm  | 27.2 | 2014 | 1508C |
| Dell         | DELD066 | U2715H       | 2560x1440 | 600x340mm  | 27.2 | 2018 | 0D9F6 |
| Dell         | DELD066 | U2715H       | 2560x1440 | 600x340mm  | 27.2 | 2016 | 0F558 |
| Dell         | DELD067 | U2715H       | 2560x1440 | 600x340mm  | 27.2 | 2018 | BB8A0 |
| Dell         | DELD067 | U2715H       | 2560x1440 | 600x340mm  | 27.2 | 2016 | 0A24E |
| Dell         | DELD067 | U2715H       | 2560x1440 | 600x340mm  | 27.2 | 2015 | 20581 |
| Dell         | DELD06E | U2515H       | 2560x1440 | 550x310mm  | 24.9 | 2017 | 50368 |
| Dell         | DELD06E | U2515H       | 2560x1440 | 550x310mm  | 24.9 | 2016 | 5C528 |
| Dell         | DELD06E | U2515H       | 2560x1440 | 550x310mm  | 24.9 | 2015 | 1FE49 |
| Dell         | DELD06E | U2515H       | 2560x1440 | 550x310mm  | 24.9 | 2014 | 48FDB |
| Dell         | DELD06F | U2515H       | 2560x1440 | 550x310mm  | 24.9 | 2017 | 48B9B |
| Dell         | DELD06F | U2515H       | 2560x1440 | 550x310mm  | 24.9 | 2015 | 206AC |
| Dell         | DELD06F | U2515H       | 2560x1440 | 550x310mm  | 24.9 | 2014 | 791E9 |
| Dell         | DELD070 | U2515H       | 2560x1440 | 550x310mm  | 24.9 | 2016 | 6A408 |
| Dell         | DELD070 | U2515H       | 2560x1440 | 550x310mm  | 24.9 | 2015 | 66933 |
| Dell         | DELD072 | U2515H       | 2560x1440 | 550x310mm  | 24.9 | 2015 | 1F558 |
| Dell         | DELD079 | S2216H       | 1920x1080 | 480x270mm  | 21.7 | 2015 | 394DB |
| Dell         | DELD07A | S2216H       | 1920x1080 | 480x270mm  | 21.7 | 2016 | 02C2B |
| Dell         | DELD07A | S2216H       | 1920x1080 | 480x270mm  | 21.7 | 2015 | 121D2 |
| Dell         | DELD07E | S2316H       | 1920x1080 | 510x290mm  | 23.1 | 2016 | 10501 |
| Dell         | DELD07E | S2316H       | 1920x1080 | 510x290mm  | 23.1 | 2015 | 1D1CA |
| Dell         | DELD082 | SE2416H      | 1920x1080 | 530x300mm  | 24.0 | 2018 | 2DDBF |
| Dell         | DELD082 | SE2416H      | 1920x1080 | 530x300mm  | 24.0 | 2015 | 6D3D6 |
| Dell         | DELD084 | P4317Q       | 3840x2160 | 940x530mm  | 42.5 | 2016 | 4D2DC |
| Dell         | DELD08A | D2216H       | 1920x1080 | 480x270mm  | 21.7 | 2017 | D34B0 |
| Dell         | DELD092 | P1917S       | 1280x1024 | 380x300mm  | 19.1 | 2017 | C9D36 |
| Dell         | DELD093 | P1917S       | 1280x1024 | 380x300mm  | 19.1 | 2018 | 35FC1 |
| Dell         | DELD099 | P2217        | 1680x1050 | 470x300mm  | 22.0 | 2016 | D5DCC |
| Dell         | DELD0A1 | SE2717H/HX   | 1920x1080 | 600x340mm  | 27.2 | 2018 | 72F0A |
| Dell         | DELD0A1 | SE2717H/HX   | 1920x1080 | 600x340mm  | 27.2 | 2017 | 5E235 |
| Dell         | DELD0A1 | SE2717H/HX   | 1920x1080 | 600x340mm  | 27.2 | 2016 | 063AC |
| Dell         | DELD0B2 | S2718H/HX    | 1920x1080 | 600x340mm  | 27.2 | 2017 | 21F5E |
| Dell         | DELD0B8 | S2218H       | 1920x1080 | 480x270mm  | 21.7 | 2018 | FA37A |
| Dell         | DELD0BA | S2218M       | 1920x1080 | 480x270mm  | 21.7 | 2017 | A9CD1 |
| Dell         | DELD0C0 | S2318HN/NX   | 1920x1080 | 510x290mm  | 23.1 | 2017 | 7C155 |
| Dell         | DELD0C1 | P2418D       | 2560x1440 | 530x300mm  | 24.0 | 2018 | 6E01A |
| Dell         | DELD0C2 | P2418D       | 2560x1440 | 530x300mm  | 24.0 | 2018 | 9A6B6 |
| Dell         | DELD0CD | S2719H       | 1920x1080 | 600x340mm  | 27.2 | 2018 | 876A7 |
| Dell         | DELD0D6 | P2319H       | 1920x1080 | 510x290mm  | 23.1 | 2019 | D1078 |
| Dell         | DELD0D7 | P2319H       | 1920x1080 | 510x290mm  | 23.1 | 2019 | 4B089 |
| Dell         | DELD0D9 | P2419H       | 1920x1080 | 530x300mm  | 24.0 | 2018 | F820F |
| Dell         | DELD0DA | P2419H       | 1920x1080 | 530x300mm  | 24.0 | 2018 | 98699 |
| Dell         | DELE005 | 1801FP       | 1280x1024 | 360x290mm  | 18.2 |      | 340AB |
| Dell         | DELE009 | 2005FPW      | 1680x1050 | 430x270mm  | 20.0 | 2006 | 1C55D |
| Dell         | DELF004 |              | 1440x900  | 410x260mm  | 19.1 | 2008 | 23688 |
| Dell         | DELF004 |              | 1440x900  | 410x260mm  | 19.1 | 2007 | 0EDE1 |
| Dell         | DELF006 | E198WFP      | 1440x900  | 410x260mm  | 19.1 | 2008 | 61D2D |
| Dell         | DELF008 | 1908WFP      | 1440x900  | 410x260mm  | 19.1 | 2008 | 75149 |
| Dell         | DELF00A | S199WFP      | 1440x900  | 410x260mm  | 19.1 | 2007 | B10E0 |
| Dell         | DELF00C | S1909WX      | 1440x900  | 410x260mm  | 19.1 | 2009 | 7BD47 |
| Dell         | DELF00D | E1909W       | 1440x900  | 410x260mm  | 19.1 | 2009 | 490B7 |
| Dell         | DELF00E | E1909W       | 1440x900  | 410x260mm  | 19.1 | 2009 | ED29E |
| Dell         | DELF011 | 2209WA       | 1680x1050 | 470x300mm  | 22.0 | 2010 | 4B7D5 |
| Dell         | DELF011 | 2209WA       | 1680x1050 | 470x300mm  | 22.0 | 2009 | 0F4B8 |
| Dell         | DELF015 | U2410        | 1920x1200 | 520x320mm  | 24.0 | 2012 | A88EA |
| Dell         | DELF015 | U2410        | 1920x1200 | 520x320mm  | 24.0 | 2011 | 055B5 |
| Dell         | DELF015 | U2410        | 1920x1200 | 520x320mm  | 24.0 | 2010 | 6B952 |
| Dell         | DELF016 | U2410        | 1920x1200 | 520x320mm  | 24.0 | 2012 | 5E73C |
| Dell         | DELF016 | U2410        | 1920x1200 | 520x320mm  | 24.0 | 2011 | AE02C |
| Dell         | DELF016 | U2410        | 1920x1200 | 520x320mm  | 24.0 | 2010 | C7007 |
| Dell         | DELF017 | U2410        | 1920x1200 | 520x320mm  | 24.0 | 2011 | 89CC5 |
| Dell         | DELF017 | U2410        | 1920x1200 | 520x320mm  | 24.0 | 2010 | 042BD |
| Dell         | DELF023 | ST2320L      | 1920x1080 | 510x290mm  | 23.1 | 2011 | C1BFA |
| Dell         | DELF035 | E2311H       | 1920x1080 | 510x290mm  | 23.1 | 2012 | C8452 |
| Dell         | DELF035 | E2311H       | 1920x1080 | 510x290mm  | 23.1 | 2011 | B43F9 |
| Dell         | DELF039 | SR2320L      | 1920x1080 | 510x290mm  | 23.1 | 2011 | 89E7B |
| Dell         | DELF042 | P2213        | 1680x1050 | 470x300mm  | 22.0 | 2012 | 531FC |
| Dell         | DELF043 | P2213        | 1680x1050 | 470x300mm  | 22.0 | 2012 | 5A3D8 |
| Dell         | DELF046 | U2413        | 1920x1200 | 520x320mm  | 24.0 | 2013 | 14526 |
| Dell         | DELF047 | U2413        | 1920x1200 | 520x320mm  | 24.0 | 2013 | A4287 |
| Dell         | DELF048 | U2413        | 1920x1200 | 520x320mm  | 24.0 | 2013 | F0CF9 |
| Dell         | DELF04D | E2314H       | 1920x1080 | 510x290mm  | 23.1 | 2015 | A7500 |
| Dell         | DELF056 | UZ2315H      | 1920x1080 | 510x290mm  | 23.1 | 2014 | 76F9E |
| Dell         | DELF05C | P2815Q       | 3840x2160 | 620x340mm  | 27.8 | 2014 | 060BE |
| Dell         | DELF068 | E2216H       | 1920x1080 | 480x270mm  | 21.7 | 2017 | 25156 |
| Dell         | DELF068 | E2216H       | 1920x1080 | 480x270mm  | 21.7 | 2015 | EE23D |
| Dell         | DELF069 | E2216H       | 1920x1080 | 480x270mm  | 21.7 | 2017 | 20982 |
| Dell         | DELF069 | E2216H       | 1920x1080 | 480x270mm  | 21.7 | 2016 | 50DD1 |
| Dell         | DELF069 | E2216H       | 1920x1080 | 480x270mm  | 21.7 | 2015 | 21B77 |
| Dell         | DELF06E | E2016HV      | 1600x900  | 430x240mm  | 19.4 | 2018 | 55FCA |
| Dell         | DELF06F | E2216HV      | 1920x1080 | 480x270mm  | 21.7 | 2017 | 2257E |
| Dell         | DELF071 | SE2216H      | 1920x1080 | 480x270mm  | 21.7 | 2018 | 0A57A |
| Dell         | DELF071 | SE2216H      | 1920x1080 | 480x270mm  | 21.7 | 2017 | E96F1 |
| Dell         | DELF071 | SE2216H      | 1920x1080 | 480x270mm  | 21.7 | 2016 | DF15C |
| Dell         | DELF071 | SE2216H      | 1920x1080 | 480x270mm  | 21.7 | 2015 | D9200 |
| Dell         | DELF093 | E2318H       | 1920x1080 | 510x290mm  | 23.1 | 2018 | F65F9 |
| Dell         | DELF093 | E2318H       | 1920x1080 | 510x290mm  | 23.1 | 2017 | F796F |
| Dell         | DELF095 | E2318HN      | 1920x1080 | 510x290mm  | 23.1 | 2018 | A8EB3 |
| Dell         | DELF109 | SE2419H      | 1920x1080 | 530x300mm  | 24.0 | 2018 | 3C5BA |
| Dell         | DELF112 | 20           | 1600x900  | 440x250mm  | 19.9 | 2011 | 1872C |
| Doffler      | LRN0226 | M226HA       | 1920x1080 | 480x270mm  | 21.7 | 2018 | E0103 |
| ELSA         | ELA3200 | E32B700BD    | 3840x2160 | 710x400mm  | 32.1 | 2016 | B8BFB |
| Eizo         | ENC1650 | L565         | 1280x1024 | 340x270mm  | 17.1 |      | 031A1 |
| Eizo         | ENC1674 | L985EX       | 1600x1200 | 440x330mm  | 21.7 |      | 0DE2B |
| Eizo         | ENC1687 | L767         | 1280x1024 | 380x300mm  | 19.1 |      | 15A0C |
| Eizo         | ENC1714 | L550         | 1280x1024 | 340x280mm  | 17.3 |      | 6D1D0 |
| Eizo         | ENC1731 | L788         | 1280x1024 | 380x300mm  | 19.1 | 2007 | 6B2D0 |
| Eizo         | ENC1731 | L788         | 1280x1024 | 380x300mm  | 19.1 |      | 78832 |
| Eizo         | ENC1749 | L997         | 1600x1200 | 440x330mm  | 21.7 | 2006 | 92A9B |
| Eizo         | ENC1786 | S1910        | 1280x1024 | 380x300mm  | 19.1 | 2006 | EBB38 |
| Eizo         | ENC1848 | S2111W       | 1680x1050 | 460x290mm  | 21.4 | 2007 | 47D89 |
| Eizo         | ENC1887 | S2431W       | 1920x1200 | 520x330mm  | 24.2 | 2008 | E07B2 |
| Eizo         | ENC1940 | S1932        | 1280x1024 | 380x300mm  | 19.1 | 2008 | 7B22C |
| Eizo         | ENC1978 | HD2442W      | 1920x1200 | 520x330mm  | 24.2 | 2008 | 1D4ED |
| Eizo         | ENC2001 | S2232W       | 1680x1050 | 480x300mm  | 22.3 | 2010 | 1AD75 |
| Eizo         | ENC2036 | FX2431       | 1920x1200 | 520x330mm  | 24.2 | 2009 | 44BA1 |
| Eizo         | ENC2060 | EV2303W      | 1920x1080 | 510x290mm  | 23.1 | 2009 | 80077 |
| Eizo         | ENC2069 | EV2333W      | 1920x1080 | 510x290mm  | 23.1 | 2010 | D3D61 |
| Eizo         | ENC2110 | S2433W       | 1920x1200 | 520x330mm  | 24.2 | 2010 | 9C602 |
| Eizo         | ENC2111 | S2433W       | 1920x1200 | 520x330mm  | 24.2 | 2009 | 8F2DC |
| Eizo         | ENC2139 | S2243W       | 1920x1200 | 480x300mm  | 22.3 | 2010 | 29E16 |
| Eizo         | ENC2143 | CG223W       | 1680x1050 | 480x300mm  | 22.3 | 2010 | 3FF42 |
| Eizo         | ENC2342 | FS2333       | 1920x1080 | 510x290mm  | 23.1 | 2012 | DFF66 |
| Eizo         | ENC2382 | EV2736W      | 2560x1440 | 600x340mm  | 27.2 | 2013 | FC54D |
| Eizo         | ENC2384 | EV2436W      | 1920x1200 | 520x330mm  | 24.2 | 2015 | 7CA39 |
| Eizo         | ENC2384 | EV2436W      | 1920x1200 | 520x330mm  | 24.2 | 2013 | 713D6 |
| Eizo         | ENC2457 | FG2421       | 1920x1080 | 530x300mm  | 24.0 | 2013 | 90D60 |
| Eizo         | ENC2530 | EV2450       | 1920x1080 | 530x300mm  | 24.0 | 2016 | E6535 |
| Eizo         | ENC2531 | EV2450       | 1920x1080 | 530x300mm  | 24.0 | 2017 | 8C925 |
| Eizo         | ENC2534 | EV2455       | 1920x1200 | 520x330mm  | 24.2 | 2016 | 6F8E3 |
| Eizo         | ENC2569 | EV2455       | 1920x1200 | 520x330mm  | 24.2 | 2017 | 6CC83 |
| Eizo         | ENC2634 | FS2434       | 1920x1080 | 530x300mm  | 24.0 | 2015 | 7EB02 |
| Eizo         | ENC2644 | CS240        | 1920x1200 | 520x330mm  | 24.2 | 2014 | ED9A6 |
| Eizo         | ENC2682 | EV2750       | 2560x1440 | 600x340mm  | 27.2 | 2016 | FB493 |
| Eizo         | ENC2684 | EV2750       | 2560x1440 | 600x340mm  | 27.2 | 2016 | A75A7 |
| Eizo         | ENC2691 | CS270        | 2560x1440 | 600x340mm  | 27.2 | 2015 | 5E86F |
| Element      | ELE0000 | ELEFW328X    | 1920x1080 | 700x390mm  | 31.5 | 2017 | 227A4 |
| Element      | ELE0000 | ELEFW248     | 1366x768  | 520x290mm  | 23.4 | 2017 | B4892 |
| Element      | ELE0000 | ELEFW328     | 1366x768  | 700x390mm  | 31.5 | 2015 | 65396 |
| Element      | ELE0000 | ELEFW328     | 1360x768  | 700x390mm  | 31.5 | 2015 | 93B46 |
| Element      | ELE078A | ELEFW195     | 1360x768  | 410x230mm  | 18.5 | 2014 | 52808 |
| Element      | ELE1911 | ELEFW195     | 1366x768  | 410x230mm  | 18.5 | 2014 | 75638 |
| Elo Touch    | ELO2201 | 2201L        | 1920x1080 | 470x260mm  | 21.1 | 2016 | F51DB |
| Envision     | EPI1944 | G918w1       | 1440x900  | 410x260mm  | 19.1 | 2007 | 4EE5E |
| Envision     | EPI4044 | LE40F1465/25 | 1920x1080 | 890x500mm  | 40.2 | 2017 | 228AE |
| Envision ... | ENV1851 | LCD851       | 1366x768  | 410x230mm  | 18.5 | 2010 | 82202 |
| Envision ... | ENV1976 | LED H976WDL  | 1366x768  | 410x230mm  | 18.5 | 2012 | A7816 |
| Envision ... | ENV2071 | LCD2071      | 1600x900  | 440x250mm  | 19.9 | 2011 | 2695B |
| Envision ... | ENV2261 | LCD2261      | 1920x1080 | 480x270mm  | 21.7 | 2011 | 0C2CD |
| Envision ... | ENV2261 | LCD2261      | 1920x1080 | 480x270mm  | 21.7 | 2010 | 20F4B |
| Envision ... | ENV2271 | LED 2271wh   | 1920x1080 | 470x260mm  | 21.1 | 2012 | A296C |
| Envision ... | ENV2273 | LED 2273w    | 1920x1080 | 480x270mm  | 21.7 | 2011 | 477B8 |
| Envision ... | ENV2361 | LCD2361      | 1920x1080 | 520x290mm  | 23.4 | 2010 | 83CA4 |
| Envision ... | ENV2373 | LED 2373d    | 1920x1080 | 510x290mm  | 23.1 | 2012 | 0DD36 |
| Envision ... | ENV2460 | LCD2460      | 1920x1080 | 520x290mm  | 23.4 | 2010 | 5F35A |
| Envision ... | ENV2461 | LCD2461      | 1920x1080 | 520x290mm  | 23.4 | 2011 | 09D71 |
| Envision ... | ENV2461 | LCD2461      | 1920x1080 | 520x290mm  | 23.4 | 2010 | 0EBB8 |
| Envision ... | ENV2471 | LED 2471h    | 1920x1080 | 530x300mm  | 24.0 | 2012 | 4D330 |
| Envision ... | ENV2471 | LED 2471h    | 1920x1080 | 530x300mm  | 24.0 | 2011 | 69AD1 |
| Envision ... | ENV2473 | LED 2473w    | 1920x1080 | 520x290mm  | 23.4 | 2011 | 531F0 |
| Envision ... | ENV2478 |              | 1920x1080 | 530x300mm  | 24.0 | 2012 | C046B |
| Envision ... | ENV2770 | LED2770h     | 1920x1080 | 600x340mm  | 27.2 | 2012 | 92AEF |
| Envision ... | ENV2770 | LED2770h     | 1920x1080 | 600x340mm  | 27.2 | 2011 | B6071 |
| Envision ... | ENV2770 | LCD2770      | 1920x1080 | 600x340mm  | 27.2 | 2010 | 076B1 |
| Fujitsu S... | FUS0620 | A17-2 DVI    | 1280x1024 | 340x270mm  | 17.1 | 2007 | 77D6C |
| Fujitsu S... | FUS077A | L24W-2       | 1920x1200 | 520x320mm  | 24.0 | 2009 | 8FBFF |
| Fujitsu S... | FUS077A | L24W-2       | 1920x1200 | 520x320mm  | 24.0 | 2008 | 699FB |
| Fujitsu S... | FUS0780 |              | 1680x1050 | 470x300mm  | 22.0 | 2012 | D20E0 |
| Fujitsu S... | FUS078B | A19-3 DVI    | 1280x1024 | 380x300mm  | 19.1 | 2008 | 696A9 |
| Fujitsu S... | FUS078F | E19-8        | 1280x1024 | 380x300mm  | 19.1 | 2008 | F93BA |
| Fujitsu S... | FUS0796 | S3260W       | 1920x1200 | 550x340mm  | 25.5 | 2008 | 24E36 |
| Fujitsu S... | FUS07A7 | LSL 3230T    | 1920x1080 | 510x290mm  | 23.1 | 2008 | F03D2 |
| Fujitsu S... | FUS07C4 | B22W-5 ECO   | 1680x1050 | 470x300mm  | 22.0 | 2009 | FD2A1 |
| Fujitsu S... | FUS07D9 | L22W-1       | 1680x1050 | 470x300mm  | 22.0 | 2011 | 36D05 |
| Fujitsu S... | FUS07E1 | SL22W-1 LED  | 1680x1050 | 470x300mm  | 22.0 | 2011 | 9CB23 |
| Fujitsu S... | FUS07E4 | SL27T-1 LED  | 1920x1080 | 600x340mm  | 27.2 | 2012 | 803B3 |
| Fujitsu S... | FUS07E6 | SL27T-1 LED  | 1920x1080 | 600x340mm  | 27.2 | 2011 | 70213 |
| Fujitsu S... | FUS07F9 | SL23T-1 LED  | 1920x1080 | 510x290mm  | 23.1 | 2012 | 03D4A |
| Fujitsu S... | FUS07FD | B23T-6 LED   | 1920x1080 | 510x290mm  | 23.1 | 2011 | 66E24 |
| Fujitsu S... | FUS07FD | B23T-6 LED   | 1920x1080 | 510x290mm  | 23.1 | 2010 | 9910A |
| Fujitsu S... | FUS080A | L22T-3 LED   | 1920x1080 | 480x270mm  | 21.7 | 2011 | E7B07 |
| Fujitsu S... | FUS0821 | P27T-7 LED   | 2560x1440 | 600x340mm  | 27.2 | 2013 | D7F81 |
| Fujitsu S... | FUS0839 | E22T-7 LED   | 1920x1080 | 480x270mm  | 21.7 | 2013 | 6E5DD |
| Fujitsu S... | FUS0856 | B23T-7 LED   | 1920x1080 | 510x290mm  | 23.1 | 2015 | 12557 |
| Fujitsu S... | FUS0859 | E20T-7 LED   | 1600x900  | 430x240mm  | 19.4 | 2017 | 4A40A |
| Fujitsu S... | FUS085B | E24T-7 LED   | 1920x1080 | 530x300mm  | 24.0 | 2015 | DDF12 |
| Fujitsu S... | FUS085C | E24T-7 LED   | 1920x1080 | 530x300mm  | 24.0 | 2015 | 08B69 |
| Fujitsu S... | FUS087E | B24-8 TS Pro | 1920x1080 | 530x300mm  | 24.0 | 2018 | C82FB |
| GABA         | GBA2700 | GL-2701FHD   | 1920x1080 | 600x330mm  | 27.0 | 2018 | 8E30A |
| Gateway      | GWY088A | FPD2185W     | 1680x1050 | 450x280mm  | 20.9 |      | D103B |
| Gateway      | GWY08AF | HD2201       | 1680x1050 | 470x300mm  | 22.0 | 2009 | 85BA4 |
| Gateway      | GWY096B | FHD2400      | 1920x1200 | 520x320mm  | 24.0 | 2007 | D3488 |
| Gateway      | GWY09B6 | FPD2485W     | 1920x1200 | 520x330mm  | 24.2 | 2007 | 92544 |
| Gericom      | QMX2421 | Q24          | 1920x1080 | 520x300mm  | 23.6 | 2010 | 2A09F |
| Gericom      | QMX2421 | Q24          | 1920x1080 | 520x300mm  | 23.6 | 2009 | 49757 |
| Gericom      | QMX2472 |              | 1920x1080 | 480x270mm  | 21.7 | 2010 | 6DE54 |
| Goldstar     | GSM0000 | LG TV        | 1360x768  | 1150x650mm | 52.0 | 2009 | 721AB |
| Goldstar     | GSM0001 | LG TV        | 3840x2160 |            |      | 2018 | 0444F |
| Goldstar     | GSM0001 | LG TV        | 1920x1080 |            |      | 2018 | 915EC |
| Goldstar     | GSM0001 | LG TV        | 1920x1080 |            |      | 2017 | 29AD1 |
| Goldstar     | GSM0001 | LG TV        | 1360x768  |            |      | 2017 | 38BE4 |
| Goldstar     | GSM0001 | LG TV        | 3840x2160 |            |      | 2017 | 41275 |
| Goldstar     | GSM0001 | LG TV        | 3840x2160 |            |      | 2016 | 0D669 |
| Goldstar     | GSM0001 | LG TV        | 1360x768  |            |      | 2016 | 11E3E |
| Goldstar     | GSM0001 | LG TV        | 1920x1080 |            |      | 2016 | 18677 |
| Goldstar     | GSM0001 | LG TV        | 1920x1080 |            |      | 2015 | 0D663 |
| Goldstar     | GSM0001 | LG TV RVU    | 1920x1080 |            |      | 2015 | 15757 |
| Goldstar     | GSM0001 | LG TV        | 1360x768  |            |      | 2015 | 47CA5 |
| Goldstar     | GSM0001 | LG TV        | 1920x1080 |            |      | 2014 | 0666E |
| Goldstar     | GSM0001 | LG TV        | 1366x768  | 690x390mm  | 31.2 | 2014 | 8AEBD |
| Goldstar     | GSM0001 | LG TV        | 1360x768  |            |      | 2014 | B03F9 |
| Goldstar     | GSM0001 | LG TV        | 1024x768  |            |      | 2014 | C40CA |
| Goldstar     | GSM0001 | LG TV        | 1920x1080 |            |      | 2013 | 0F8E9 |
| Goldstar     | GSM0001 | LG TV        | 1360x768  |            |      | 2013 | 19C23 |
| Goldstar     | GSM0001 | LG TV        | 1920x1080 |            |      | 2012 | 16ED7 |
| Goldstar     | GSM0001 | LG TV        | 1024x768  |            |      | 2012 | 2D395 |
| Goldstar     | GSM0001 | LG TV        | 1360x768  |            |      | 2012 | 30DB6 |
| Goldstar     | GSM0001 | LG TV        | 1360x768  |            |      | 2011 | 0381D |
| Goldstar     | GSM0001 | LG TV        | 1920x1080 |            |      | 2011 | 13EFA |
| Goldstar     | GSM0001 | LG TV        | 1360x768  | 1150x650mm | 52.0 | 2011 | 18813 |
| Goldstar     | GSM0001 | LG TV        | 1024x768  |            |      | 2011 | 922C2 |
| Goldstar     | GSM0001 | LG TV        | 1360x768  | 700x390mm  | 31.5 | 2011 | B571E |
| Goldstar     | GSM0001 | LG TV        | 1920x1080 |            |      | 2010 | 1E11B |
| Goldstar     | GSM0001 | LG TV        | 1280x720  |            |      | 2010 | 48834 |
| Goldstar     | GSM0001 | LG TV        | 1360x768  | 1150x650mm | 52.0 | 2010 | 50EDB |
| Goldstar     | GSM0001 | LG TV        | 1920x1080 | 1150x650mm | 52.0 | 2009 | 3506B |
| Goldstar     | GSM0001 | LG TV        | 1360x768  | 1150x650mm | 52.0 | 2009 | 598A2 |
| Goldstar     | GSM0001 | LG TV        | 1360x768  | 1150x650mm | 52.0 | 2008 | 91ED5 |
| Goldstar     | GSM0002 | LG TV        | 1920x1080 | 1150x650mm | 52.0 | 2009 | 8F5FE |
| Goldstar     | GSM2412 | LG TV        | 1920x1080 | 940x530mm  | 42.5 | 2017 | 2F35E |
| Goldstar     | GSM2412 | LG TV        | 1920x1080 | 940x530mm  | 42.5 | 2016 | 4666D |
| Goldstar     | GSM3AD7 | LG TV        | 1280x1024 | 930x520mm  | 41.9 |      | 763F1 |
| Goldstar     | GSM3B97 | L1530B       | 1024x768  | 300x230mm  | 14.9 |      | 543DD |
| Goldstar     | GSM3E89 | W1642C       | 1366x768  | 350x200mm  | 15.9 | 2009 | 154E6 |
| Goldstar     | GSM4357 | L1710S       | 1280x1024 | 340x270mm  | 17.1 |      | 3403A |
| Goldstar     | GSM438F | L1730B       | 1280x1024 | 340x270mm  | 17.1 |      | AFAAE |
| Goldstar     | GSM4396 | F720B        | 1280x1024 | 330x250mm  | 16.3 |      | 2C6AF |
| Goldstar     | GSM439E | L1730P       | 1280x1024 | 340x270mm  | 17.1 |      | 264E1 |
| Goldstar     | GSM43BB | L1780U       | 1280x1024 | 340x270mm  | 17.1 |      | C9D47 |
| Goldstar     | GSM43D0 | L1740P       | 1280x1024 | 340x270mm  | 17.1 |      | 64015 |
| Goldstar     | GSM43E1 | L1780Q       | 1280x1024 | 340x270mm  | 17.1 |      | 4176C |
| Goldstar     | GSM43E7 | L1740PQ      | 1280x1024 | 340x270mm  | 17.1 | 2006 | F96C5 |
| Goldstar     | GSM43E7 | L1740PQ      | 1280x1024 | 340x270mm  | 17.1 |      | B6BCC |
| Goldstar     | GSM43F1 | L1751SQ      | 1280x1024 | 340x270mm  | 17.1 |      | 0B884 |
| Goldstar     | GSM43FF | L1717S       | 1280x1024 | 340x270mm  | 17.1 | 2006 | FA310 |
| Goldstar     | GSM4407 | L1750B       | 1280x1024 | 340x270mm  | 17.1 | 2006 | DDEC7 |
| Goldstar     | GSM4413 | L1732S       | 1280x1024 | 340x270mm  | 17.1 |      | 181C8 |
| Goldstar     | GSM4432 | L1752S       | 1280x1024 | 340x270mm  | 17.1 | 2006 | D5610 |
| Goldstar     | GSM4434 | L1752T       | 1280x1024 | 340x270mm  | 17.1 | 2006 | E0CFE |
| Goldstar     | GSM4458 | L1752HQ      | 1280x1024 | 340x270mm  | 17.1 | 2006 | F2472 |
| Goldstar     | GSM445E | L1760TR      | 1280x1024 | 340x270mm  | 17.1 | 2007 | 3D905 |
| Goldstar     | GSM445E | L1760TR      | 1280x1024 | 340x270mm  | 17.1 | 2006 | 97C3C |
| Goldstar     | GSM4460 | L1752HR      | 1280x1024 | 340x270mm  | 17.1 | 2006 | FF8D7 |
| Goldstar     | GSM4462 | L1752TR      | 1280x1024 | 340x270mm  | 17.1 | 2006 | D736E |
| Goldstar     | GSM446F | L1753S       | 1280x1024 | 340x270mm  | 17.1 | 2007 | 48837 |
| Goldstar     | GSM4477 | L1753T       | 1280x1024 | 340x270mm  | 17.1 | 2007 | 57CE6 |
| Goldstar     | GSM4490 | W1752        | 1440x900  | 380x230mm  | 17.5 | 2008 | 8F2FF |
| Goldstar     | GSM4491 | W1752        | 1440x900  | 380x230mm  | 17.5 | 2009 | 16250 |
| Goldstar     | GSM4495 | L1734        | 1280x1024 | 340x270mm  | 17.1 | 2008 | 1B6BA |
| Goldstar     | GSM449B | L1742        | 1280x1024 | 340x270mm  | 17.1 | 2009 | 49674 |
| Goldstar     | GSM4A7C | L1920P       | 1280x1024 | 380x300mm  | 19.1 | 2006 | DA9F9 |
| Goldstar     | GSM4A90 | L1915S       | 1280x1024 | 380x300mm  | 19.1 |      | 4380A |
| Goldstar     | GSM4AD1 | L1940PQ      | 1280x1024 | 380x300mm  | 19.1 |      | 88C79 |
| Goldstar     | GSM4ADE | M1917TM      | 1280x1024 | 380x300mm  | 19.1 | 2006 | 77310 |
| Goldstar     | GSM4AE2 | L1952T       | 1280x1024 | 380x300mm  | 19.1 | 2006 | 022DE |
| Goldstar     | GSM4AE9 | L1970HR      | 1280x1024 | 380x300mm  | 19.1 | 2006 | 5C539 |
| Goldstar     | GSM4AED | L1953H       | 1280x1024 | 380x300mm  | 19.1 | 2007 | BEF9D |
| Goldstar     | GSM4B00 | L1900E       | 1280x1024 | 380x300mm  | 19.1 | 2007 | 6E4A8 |
| Goldstar     | GSM4B06 | L194WT       | 1440x900  | 410x260mm  | 19.1 | 2008 | E400E |
| Goldstar     | GSM4B06 | L194WT       | 1440x900  | 410x260mm  | 19.1 | 2007 | 14100 |
| Goldstar     | GSM4B09 | L1952HQ      | 1280x1024 | 380x300mm  | 19.1 | 2006 | 2EE7D |
| Goldstar     | GSM4B0B | L1982U       | 1280x1024 | 380x300mm  | 19.1 | 2008 | 36388 |
| Goldstar     | GSM4B17 | L1952TQ      | 1280x1024 | 380x300mm  | 19.1 | 2006 | 1D09D |
| Goldstar     | GSM4B21 | L1960TR      | 1280x1024 | 380x300mm  | 19.1 | 2007 | 2EEF9 |
| Goldstar     | GSM4B31 | L1918S       | 1280x1024 | 380x300mm  | 19.1 | 2007 | B6F96 |
| Goldstar     | GSM4B36 | M198WA       | 1440x900  | 410x260mm  | 19.1 | 2007 | 96B42 |
| Goldstar     | GSM4B3D | L1953H       | 1280x1024 | 340x270mm  | 17.1 |      | 795B4 |
| Goldstar     | GSM4B42 | L1953HR      | 1280x1024 | 380x300mm  | 19.1 | 2007 | 06CAC |
| Goldstar     | GSM4B44 | L1953TR      | 1280x1024 | 340x270mm  | 17.1 | 2008 | 52781 |
| Goldstar     | GSM4B44 | L1953TR      | 1280x1024 | 380x300mm  | 19.1 | 2007 | 0A364 |
| Goldstar     | GSM4B44 | L1953TR      | 1280x1024 | 340x270mm  | 17.1 | 2007 | A0A14 |
| Goldstar     | GSM4B4F | L196WTQ      | 1440x900  | 410x260mm  | 19.1 | 2007 | BBB78 |
| Goldstar     | GSM4B50 | L196WTQ      | 1440x900  | 410x260mm  | 19.1 | 2008 | C1ED2 |
| Goldstar     | GSM4B50 | L196WTQ      | 1440x900  | 410x260mm  | 19.1 | 2007 | 8FC77 |
| Goldstar     | GSM4B58 | M1921TA      | 1280x1024 | 380x300mm  | 19.1 | 2008 | 6F056 |
| Goldstar     | GSM4B65 | L1954        | 1280x1024 | 380x300mm  | 19.1 | 2008 | E7AFB |
| Goldstar     | GSM4B6B | L194W        | 1440x900  | 410x260mm  | 19.1 | 2007 | ADA74 |
| Goldstar     | GSM4B6D | L197W        | 1440x900  | 410x260mm  | 19.1 | 2008 | 6C6C0 |
| Goldstar     | GSM4B6F | W1942        | 1440x900  | 410x260mm  | 19.1 | 2008 | 0E9C2 |
| Goldstar     | GSM4B70 | W1942        | 1440x900  | 410x260mm  | 19.1 | 2008 | 04825 |
| Goldstar     | GSM4B71 | 19LS4D-ZB    | 1920x1080 | 470x300mm  | 22.0 | 2008 | 31669 |
| Goldstar     | GSM4B78 | W1952        | 1440x900  | 410x260mm  | 19.1 | 2009 | F3964 |
| Goldstar     | GSM4B78 | W1952        | 1440x900  | 410x260mm  | 19.1 | 2008 | 6B1AA |
| Goldstar     | GSM4B7A | W1934        | 1440x900  | 410x260mm  | 19.1 | 2008 | 468C5 |
| Goldstar     | GSM4B8A | M1994D-PZ    | 1680x1050 | 400x250mm  | 18.6 | 2008 | 14A6F |
| Goldstar     | GSM4BAC | M197WA       | 1360x768  | 410x230mm  | 18.5 | 2009 | 3FB1B |
| Goldstar     | GSM4BAD | W1943        | 1360x768  | 410x230mm  | 18.5 | 2011 | 954E8 |
| Goldstar     | GSM4BAD | W1943        | 1360x768  | 410x230mm  | 18.5 | 2010 | 79B3A |
| Goldstar     | GSM4BAD | W1943        | 1360x768  | 410x230mm  | 18.5 | 2009 | 81464 |
| Goldstar     | GSM4BBF | M1962D       | 1360x768  | 410x230mm  | 18.5 | 2009 | 8B58F |
| Goldstar     | GSM4BD6 | E1940        | 1360x768  | 410x230mm  | 18.5 | 2010 | 9945F |
| Goldstar     | GSM4BEB | E1910        | 1280x1024 | 370x300mm  | 18.8 | 2011 | 57050 |
| Goldstar     | GSM4BF9 | E1911        | 1366x768  | 410x230mm  | 18.5 | 2011 | F6BAA |
| Goldstar     | GSM4C09 | E1942        | 1366x768  | 410x230mm  | 18.5 | 2012 | 3D3FD |
| Goldstar     | GSM4C1F | 19EB13       | 1366x768  | 410x230mm  | 18.5 | 2014 | C93FA |
| Goldstar     | GSM4E3A | L2000C       | 1600x1200 | 410x310mm  | 20.2 | 2008 | 6003C |
| Goldstar     | GSM4E3A | L2000C       | 1600x1200 | 410x310mm  | 20.2 | 2006 | BAC7E |
| Goldstar     | GSM4E48 | L204WT       | 1680x1050 | 430x270mm  | 20.0 | 2007 | 07EF9 |
| Goldstar     | GSM4E48 | L204WT       | 1680x1050 | 430x270mm  | 20.0 | 2006 | 9DB7A |
| Goldstar     | GSM4E53 | L2000CN      | 1600x1200 | 410x310mm  | 20.2 | 2007 | 4CD94 |
| Goldstar     | GSM4E62 | M208WA       | 1680x1050 | 430x270mm  | 20.0 | 2008 | 4F7C7 |
| Goldstar     | GSM4E62 | M208WA       | 1680x1050 | 430x270mm  | 20.0 | 2007 | D4665 |
| Goldstar     | GSM4E6A | L206WTQ      | 1680x1050 | 430x270mm  | 20.0 | 2007 | B8A10 |
| Goldstar     | GSM4E7D | L204W        | 1680x1050 | 430x270mm  | 20.0 | 2007 | 39788 |
| Goldstar     | GSM4E7F | W2042        | 1680x1050 | 430x270mm  | 20.0 | 2008 | 31A84 |
| Goldstar     | GSM4E8E | WX2052       | 1680x1050 | 470x300mm  | 22.0 | 2008 | 8E7E6 |
| Goldstar     | GSM4E9D | W2043        | 1600x900  | 450x250mm  | 20.3 | 2010 | 15265 |
| Goldstar     | GSM4E9E | W2043        | 1600x900  | 450x250mm  | 20.3 | 2010 | 0B2CB |
| Goldstar     | GSM4E9E | W2043        | 1600x900  | 450x250mm  | 20.3 | 2009 | 68389 |
| Goldstar     | GSM4EAB | W2046        | 1600x900  | 450x250mm  | 20.3 | 2010 | FBF4B |
| Goldstar     | GSM4EB4 | E2040        | 1600x900  | 450x250mm  | 20.3 | 2010 | 8D2E5 |
| Goldstar     | GSM4EB7 | M2080D       | 1920x1080 | 440x250mm  | 19.9 | 2010 | EDA9D |
| Goldstar     | GSM4EC0 | E2060        | 1600x900  | 450x250mm  | 20.3 | 2011 | AB9D3 |
| Goldstar     | GSM4EC9 | E2041        | 1600x900  | 450x250mm  | 20.3 | 2011 | FD1CE |
| Goldstar     | GSM4ED4 | E2011        | 1600x900  | 450x250mm  | 20.3 | 2014 | 14A36 |
| Goldstar     | GSM4ED4 | E2011        | 1600x900  | 450x250mm  | 20.3 | 2013 | 27B1B |
| Goldstar     | GSM4EE1 | 20EN33       | 1600x900  | 440x250mm  | 19.9 | 2014 | 339BA |
| Goldstar     | GSM4EE2 | 20EN33       | 1600x900  | 440x250mm  | 19.9 | 2013 | FB628 |
| Goldstar     | GSM562A | 26LC3R-ZJ    | 720x480   | 920x520mm  | 41.6 | 2007 | 69FBE |
| Goldstar     | GSM5635 | L226WA       | 1680x1050 | 470x290mm  | 21.7 | 2007 | 03858 |
| Goldstar     | GSM5637 | L226WA       | 1680x1050 | 470x290mm  | 21.7 | 2007 | 90A0A |
| Goldstar     | GSM563D | M228WA       | 1680x1050 | 470x300mm  | 22.0 | 2008 | 4C9D7 |
| Goldstar     | GSM563D | M228WA       | 1680x1050 | 470x300mm  | 22.0 | 2007 | 3C483 |
| Goldstar     | GSM5663 | L225W        | 1680x1050 | 470x300mm  | 22.0 | 2008 | 00DF5 |
| Goldstar     | GSM5669 | L206W        | 1680x1050 | 430x270mm  | 20.0 | 2007 | 361CA |
| Goldstar     | GSM566B | L226W        | 1680x1050 | 490x320mm  | 23.0 | 2008 | 3BADF |
| Goldstar     | GSM566B | L226W        | 1680x1050 | 490x320mm  | 23.0 | 2007 | 5ADDF |
| Goldstar     | GSM566F | L227W        | 1680x1050 | 490x320mm  | 23.0 | 2009 | FB6D8 |
| Goldstar     | GSM566F | L227W        | 1680x1050 | 490x320mm  | 23.0 | 2008 | 3D2FA |
| Goldstar     | GSM5671 | M228WD-BZ    | 1680x1050 | 470x300mm  | 22.0 | 2008 | 03640 |
| Goldstar     | GSM5673 | 26LB75       | 1920x1080 | 700x390mm  | 31.5 | 2007 | 89EDA |
| Goldstar     | GSM5675 | W2600        | 1920x1200 | 550x340mm  | 25.5 | 2009 | 91B67 |
| Goldstar     | GSM5677 | W2242        | 1680x1050 | 490x320mm  | 23.0 | 2009 | 28048 |
| Goldstar     | GSM5678 | W2242        | 1680x1050 | 490x320mm  | 23.0 | 2010 | 72B98 |
| Goldstar     | GSM5678 | W2242        | 1680x1050 | 490x320mm  | 23.0 | 2009 | 0A735 |
| Goldstar     | GSM5678 | W2242        | 1680x1050 | 490x320mm  | 23.0 | 2008 | 07E08 |
| Goldstar     | GSM567A | 22LS4D-ZB    | 1920x1080 | 470x300mm  | 22.0 | 2008 | 3EF19 |
| Goldstar     | GSM567E | W2252        | 1680x1050 | 490x320mm  | 23.0 | 2009 | 3B5CE |
| Goldstar     | GSM567E | W2252        | 1680x1050 | 490x320mm  | 23.0 | 2008 | 055B5 |
| Goldstar     | GSM5688 | 26LB76       | 1920x1080 | 700x390mm  | 31.5 | 2008 | 50927 |
| Goldstar     | GSM5694 | W2452        | 1920x1200 | 520x320mm  | 24.0 | 2008 | 2C74B |
| Goldstar     | GSM5698 | W2284        | 1680x1050 | 490x320mm  | 23.0 | 2009 | 32CBE |
| Goldstar     | GSM5698 | W2284        | 1680x1050 | 490x320mm  | 23.0 | 2008 | 9198E |
| Goldstar     | GSM569D | L246WHX      | 1920x1200 | 520x320mm  | 24.0 | 2009 | 5FFBD |
| Goldstar     | GSM56B3 | W2241        | 1680x1050 | 490x320mm  | 23.0 | 2008 | A4320 |
| Goldstar     | GSM56B4 | W2241        | 1680x1050 | 490x320mm  | 23.0 | 2008 | 18692 |
| Goldstar     | GSM56CC | W2442        | 1920x1080 | 530x300mm  | 24.0 | 2010 | CF835 |
| Goldstar     | GSM56CE | W2261        | 1920x1080 | 480x270mm  | 21.7 | 2009 | B1D3D |
| Goldstar     | GSM56CF | W2261        | 1920x1080 | 530x300mm  | 24.0 | 2009 | D188D |
| Goldstar     | GSM56D4 | M227WD       | 1920x1080 | 480x270mm  | 21.7 | 2009 | 4A222 |
| Goldstar     | GSM56D9 | W2442        | 1920x1080 | 530x300mm  | 24.0 | 2009 | 88BD1 |
| Goldstar     | GSM56DC | W2253        | 1920x1080 | 490x320mm  | 23.0 | 2010 | 25711 |
| Goldstar     | GSM56DC | W2253        | 1920x1080 | 490x320mm  | 23.0 | 2009 | 3E72D |
| Goldstar     | GSM56DC | W2253        | 1920x1080 | 480x270mm  | 21.7 | 2009 | 7D2BB |
| Goldstar     | GSM56EE | W2353        | 1920x1080 | 510x290mm  | 23.1 | 2010 | EB7DB |
| Goldstar     | GSM56EF | W2353        | 1920x1080 | 510x290mm  | 23.1 | 2010 | 98F0A |
| Goldstar     | GSM56EF | W2353        | 1920x1080 | 510x290mm  | 23.1 | 2009 | 1B595 |
| Goldstar     | GSM56F0 | W2353        | 1920x1080 | 510x290mm  | 23.1 | 2010 | FDBC7 |
| Goldstar     | GSM56F4 | W2453        | 1920x1080 | 530x300mm  | 24.0 | 2010 | 959AE |
| Goldstar     | GSM56F7 | W2753        | 1920x1080 | 600x340mm  | 27.2 | 2010 | E5381 |
| Goldstar     | GSM56FE | W2243        | 1920x1080 | 480x270mm  | 21.7 | 2009 | 8B15D |
| Goldstar     | GSM56FF | W2243        | 1920x1080 | 480x270mm  | 21.7 | 2010 | A1187 |
| Goldstar     | GSM56FF | W2243        | 1920x1080 | 480x270mm  | 21.7 | 2009 | D1147 |
| Goldstar     | GSM5700 | W2343        | 1920x1080 | 510x290mm  | 23.1 | 2011 | A1886 |
| Goldstar     | GSM5701 | W2343        | 1920x1080 | 510x290mm  | 23.1 | 2011 | 281AA |
| Goldstar     | GSM570C | M227WA       | 1920x1080 | 480x270mm  | 21.7 | 2009 | BF799 |
| Goldstar     | GSM570D | M227WA       | 1920x1080 | 480x270mm  | 21.7 | 2009 | 8F36C |
| Goldstar     | GSM5723 | W2486        | 1920x1080 | 530x300mm  | 24.0 | 2010 | A3EDE |
| Goldstar     | GSM5725 | M237WA       | 1920x1080 | 510x280mm  | 22.9 | 2011 | C568A |
| Goldstar     | GSM5725 | M237WA       | 1920x1080 | 510x280mm  | 22.9 | 2009 | EA66B |
| Goldstar     | GSM5726 | M237WA       | 1920x1080 | 510x280mm  | 22.9 | 2009 | 4A5B8 |
| Goldstar     | GSM5729 | W2486        | 1920x1080 | 530x300mm  | 24.0 | 2009 | 0788B |
| Goldstar     | GSM572E | W2363        | 1920x1080 | 510x290mm  | 23.1 | 2010 | 8DE45 |
| Goldstar     | GSM5738 | W2262        | 1920x1080 | 480x270mm  | 21.7 | 2009 | 993E6 |
| Goldstar     | GSM576E | M2762D       | 1920x1080 | 600x340mm  | 27.2 | 2009 | 65B2D |
| Goldstar     | GSM5775 | M227WDP      | 1920x1080 | 480x270mm  | 21.7 | 2009 | 48B07 |
| Goldstar     | GSM5776 | M227WDP      | 1920x1080 | 480x270mm  | 21.7 | 2009 | 89AB5 |
| Goldstar     | GSM5779 | M237WDP      | 1920x1080 | 510x290mm  | 23.1 | 2009 | FD424 |
| Goldstar     | GSM5784 | W2246        | 1920x1080 | 480x270mm  | 21.7 | 2010 | 27CB7 |
| Goldstar     | GSM5786 | W2346        | 1920x1080 | 510x290mm  | 23.1 | 2010 | 4245F |
| Goldstar     | GSM5787 | W2363D       | 1920x1080 | 510x280mm  | 22.9 | 2010 | 1CB0E |
| Goldstar     | GSM578D | E2250        | 1920x1080 | 480x270mm  | 21.7 | 2011 | 62A0C |
| Goldstar     | GSM578D | E2250        | 1920x1080 | 480x270mm  | 21.7 | 2010 | 4A67E |
| Goldstar     | GSM578E | E2250        | 1920x1080 | 480x270mm  | 21.7 | 2009 | 998A1 |
| Goldstar     | GSM578F | E2350        | 1920x1080 | 510x290mm  | 23.1 | 2011 | 379AD |
| Goldstar     | GSM578F | E2350        | 1920x1080 | 510x290mm  | 23.1 | 2010 | 14100 |
| Goldstar     | GSM5790 | E2350        | 1920x1080 | 510x290mm  | 23.1 | 2010 | 052B8 |
| Goldstar     | GSM5791 | E2350        | 1920x1080 | 510x290mm  | 23.1 | 2010 | 22F8E |
| Goldstar     | GSM5791 | E2350        | 1920x1080 | 510x290mm  | 23.1 | 2009 | 992A3 |
| Goldstar     | GSM57A1 | W2240        | 1920x1080 | 480x270mm  | 21.7 | 2011 | 9666A |
| Goldstar     | GSM57A3 | E2240        | 1920x1080 | 480x270mm  | 21.7 | 2010 | C9BC7 |
| Goldstar     | GSM57A4 | E2240        | 1920x1080 | 480x270mm  | 21.7 | 2010 | 25C02 |
| Goldstar     | GSM57A6 | E2340        | 1920x1080 | 510x290mm  | 23.1 | 2010 | 7C2F7 |
| Goldstar     | GSM57A8 | W2340        | 1920x1080 | 510x290mm  | 23.1 | 2010 | D1354 |
| Goldstar     | GSM57B9 | M2280D       | 1920x1080 | 480x270mm  | 21.7 | 2010 | 6E15F |
| Goldstar     | GSM57BC | M2380D       | 1920x1080 | 510x290mm  | 23.1 | 2010 | 45CA4 |
| Goldstar     | GSM57C7 | E2340        | 1920x1080 | 510x290mm  | 23.1 | 2009 | 7F10F |
| Goldstar     | GSM57D0 | E2380        | 1920x1080 | 510x290mm  | 23.1 | 2010 | 82B0B |
| Goldstar     | GSM57E0 | E2260        | 1920x1080 | 480x270mm  | 21.7 | 2010 | 7AAE0 |
| Goldstar     | GSM57E1 | E2260        | 1920x1080 | 480x270mm  | 21.7 | 2011 | 5992A |
| Goldstar     | GSM57E3 | E2360        | 1920x1080 | 510x290mm  | 23.1 | 2010 | D4510 |
| Goldstar     | GSM57E4 | E2360        | 1920x1080 | 510x290mm  | 23.1 | 2011 | 09A63 |
| Goldstar     | GSM57E4 | E2360        | 1920x1080 | 510x290mm  | 23.1 | 2010 | 2110D |
| Goldstar     | GSM57EC | M2280A       | 1920x1080 | 480x270mm  | 21.7 | 2010 | 93C99 |
| Goldstar     | GSM57EE | M2380A       | 1920x1080 | 510x280mm  | 22.9 | 2010 | B0E5A |
| Goldstar     | GSM57FB | E2210        | 1680x1050 | 470x290mm  | 21.7 | 2010 | 430F9 |
| Goldstar     | GSM5806 |              | 1920x1080 | 480x270mm  | 21.7 | 2012 | 216A0 |
| Goldstar     | GSM5806 |              | 1920x1080 | 480x270mm  | 21.7 | 2011 | 08E04 |
| Goldstar     | GSM5807 |              | 1920x1080 | 480x270mm  | 21.7 | 2009 | 5C1EE |
| Goldstar     | GSM5809 | E2290        | 1920x1080 | 480x270mm  | 21.7 | 2011 | 31BAF |
| Goldstar     | GSM5809 | E2290        | 1920x1080 | 480x270mm  | 21.7 | 2010 | 65ED4 |
| Goldstar     | GSM580C |              | 1920x1080 | 510x290mm  | 23.1 | 2012 | 18C55 |
| Goldstar     | GSM580C |              | 1920x1080 | 510x290mm  | 23.1 | 2011 | 606B6 |
| Goldstar     | GSM580C |              | 1920x1080 | 510x290mm  | 23.1 | 2009 | 322BE |
| Goldstar     | GSM580D |              | 1920x1080 | 510x290mm  | 23.1 | 2009 | 1612F |
| Goldstar     | GSM580F | E2370        | 1920x1080 | 510x290mm  | 23.1 | 2011 | 68968 |
| Goldstar     | GSM5816 |              | 1920x1080 | 510x290mm  | 23.1 | 2011 | CB1C6 |
| Goldstar     | GSM5817 |              | 1920x1080 | 510x290mm  | 23.1 | 2014 | 08D88 |
| Goldstar     | GSM5818 | E2241        | 1920x1080 | 480x270mm  | 21.7 | 2011 | 7E9B6 |
| Goldstar     | GSM5819 | E2241        | 1920x1080 | 480x270mm  | 21.7 | 2011 | 0D906 |
| Goldstar     | GSM581A | E2241        | 1920x1080 | 480x270mm  | 21.7 | 2014 | 0C771 |
| Goldstar     | GSM581A | E2241        | 1920x1080 | 480x270mm  | 21.7 | 2011 | 3F871 |
| Goldstar     | GSM581C | E2341        | 1920x1080 | 510x290mm  | 23.1 | 2011 | A23F7 |
| Goldstar     | GSM581F | E2441        | 1920x1080 | 530x300mm  | 24.0 | 2011 | 092F7 |
| Goldstar     | GSM5820 | E2441        | 1920x1080 | 530x300mm  | 24.0 | 2010 | D7BA2 |
| Goldstar     | GSM5826 | E2281        | 1920x1080 | 480x270mm  | 21.7 | 2011 | F4569 |
| Goldstar     | GSM583B | E2411        | 1920x1080 | 530x300mm  | 24.0 | 2013 | D86EF |
| Goldstar     | GSM583B | E2411        | 1920x1080 | 530x300mm  | 24.0 | 2011 | 227A6 |
| Goldstar     | GSM583B | E2411        | 1920x1080 | 530x300mm  | 24.0 | 2010 | 4F74A |
| Goldstar     | GSM5841 | D2342P       | 1920x1080 | 510x290mm  | 23.1 | 2012 | 03617 |
| Goldstar     | GSM5841 | D2342P       | 1920x1080 | 510x290mm  | 23.1 | 2011 | A8ECD |
| Goldstar     | GSM5842 | D2342P       | 1920x1080 | 510x290mm  | 23.1 | 2009 | 87EFE |
| Goldstar     | GSM5867 | DM2350D      | 1920x1080 | 510x290mm  | 23.1 | 2011 | 6F505 |
| Goldstar     | GSM586D | E2251        | 1920x1080 | 480x270mm  | 21.7 | 2011 | 8A799 |
| Goldstar     | GSM586E | E2251        | 1920x1080 | 480x270mm  | 21.7 | 2011 | 011A9 |
| Goldstar     | GSM586F | E2251        | 1920x1080 | 480x270mm  | 21.7 | 2011 | 2CA66 |
| Goldstar     | GSM5871 | E2351        | 1920x1080 | 510x290mm  | 23.1 | 2012 | B36F6 |
| Goldstar     | GSM5871 | E2351        | 1920x1080 | 510x290mm  | 23.1 | 2011 | 1E97E |
| Goldstar     | GSM5872 | E2351        | 1920x1080 | 510x290mm  | 23.1 | 2012 | 3E691 |
| Goldstar     | GSM587A | IPS225       | 1920x1080 | 510x290mm  | 23.1 | 2012 | 1FDDA |
| Goldstar     | GSM587B | IPS225       | 1920x1080 | 510x290mm  | 23.1 | 2012 | 76264 |
| Goldstar     | GSM587D | IPS235       | 1920x1080 | 510x290mm  | 23.1 | 2012 | 03968 |
| Goldstar     | GSM587D | IPS235       | 1920x1080 | 510x290mm  | 23.1 | 2011 | 41F4D |
| Goldstar     | GSM5890 | D2542P       | 1920x1080 | 560x320mm  | 25.4 | 2011 | 2CB7B |
| Goldstar     | GSM589F | DM2780D      | 1920x1080 | 600x340mm  | 27.2 | 2011 | 2ABDC |
| Goldstar     | GSM58BF | E2242        | 1920x1080 | 480x270mm  | 21.7 | 2013 | DFFD3 |
| Goldstar     | GSM58BF | E2242        | 1920x1080 | 480x270mm  | 21.7 | 2012 | 4160A |
| Goldstar     | GSM58C1 | E2342        | 1920x1080 | 510x290mm  | 23.1 | 2012 | 9A683 |
| Goldstar     | GSM58C5 | E2442        | 1920x1080 | 530x300mm  | 24.0 | 2012 | 322DB |
| Goldstar     | GSM58D6 | IPS224       | 1920x1080 | 480x270mm  | 21.7 | 2013 | 1C360 |
| Goldstar     | GSM58D6 | IPS224       | 1920x1080 | 480x270mm  | 21.7 | 2012 | 0C7F4 |
| Goldstar     | GSM58D7 | IPS224       | 1920x1080 | 480x270mm  | 21.7 | 2011 | E6EC1 |
| Goldstar     | GSM58D9 | IPS234       | 1920x1080 | 510x290mm  | 23.1 | 2013 | A887C |
| Goldstar     | GSM58D9 | IPS234       | 1920x1080 | 510x290mm  | 23.1 | 2012 | 0BC7B |
| Goldstar     | GSM58D9 | IPS234       | 1920x1080 | 510x290mm  | 23.1 | 2011 | BD562 |
| Goldstar     | GSM58DA | IPS234       | 1920x1080 | 510x290mm  | 23.1 | 2013 | 71406 |
| Goldstar     | GSM58DA | IPS234       | 1920x1080 | 510x290mm  | 23.1 | 2011 | AD3F6 |
| Goldstar     | GSM58EB | DM2752       | 1920x1080 | 510x290mm  | 23.1 | 2012 | 84126 |
| Goldstar     | GSM5901 | IPS237       | 1920x1080 | 510x290mm  | 23.1 | 2013 | 09D19 |
| Goldstar     | GSM5901 | IPS237       | 1920x1080 | 510x290mm  | 23.1 | 2012 | 2467B |
| Goldstar     | GSM5903 | IPS277       | 1920x1080 | 600x340mm  | 27.2 | 2016 | 1C040 |
| Goldstar     | GSM5903 | IPS277       | 1920x1080 | 600x340mm  | 27.2 | 2013 | 46004 |
| Goldstar     | GSM5903 | IPS277       | 1920x1080 | 600x340mm  | 27.2 | 2012 | 1EC54 |
| Goldstar     | GSM5929 | D2343        | 1920x1080 | 500x280mm  | 22.6 | 2013 | 513B8 |
| Goldstar     | GSM592A | D2343        | 1920x1080 | 500x280mm  | 22.6 | 2012 | FE77E |
| Goldstar     | GSM5936 | M2451DS      | 1920x1080 | 530x300mm  | 24.0 | 2012 | DE4FE |
| Goldstar     | GSM5974 | 29EA93       | 2560x1080 | 670x280mm  | 28.6 | 2012 | 1630C |
| Goldstar     | GSM597A | V320         | 1920x1080 | 510x290mm  | 23.1 | 2012 | 3658E |
| Goldstar     | GSM597C | 22EN33       | 1920x1080 | 480x270mm  | 21.7 | 2013 | 97165 |
| Goldstar     | GSM5988 | 23EA73       | 1920x1080 | 510x290mm  | 23.1 | 2013 | 63B06 |
| Goldstar     | GSM598A | 27EA63       | 1920x1080 | 600x340mm  | 27.2 | 2013 | 65DFF |
| Goldstar     | GSM598D | 23EA63       | 1920x1080 | 510x290mm  | 23.1 | 2013 | 233B1 |
| Goldstar     | GSM598E | 23EA63       | 1920x1080 | 510x290mm  | 23.1 | 2012 | 74057 |
| Goldstar     | GSM5990 | 22EA63       | 1920x1080 | 480x270mm  | 21.7 | 2013 | E21DE |
| Goldstar     | GSM59A4 | 22EA53       | 1920x1080 | 480x270mm  | 21.7 | 2013 | 6A977 |
| Goldstar     | GSM59A5 | 22EA53       | 1920x1080 | 480x270mm  | 21.7 | 2013 | 03209 |
| Goldstar     | GSM59A8 | 23EA53       | 1920x1080 | 510x290mm  | 23.1 | 2013 | 29DEE |
| Goldstar     | GSM59A9 | 23EA53       | 1920x1080 | 510x290mm  | 23.1 | 2013 | 6E3C6 |
| Goldstar     | GSM59AB | 24EA53       | 1920x1080 | 510x290mm  | 23.1 | 2013 | 0E79F |
| Goldstar     | GSM59AC | 24EA53       | 1920x1080 | 510x290mm  | 23.1 | 2013 | 291D6 |
| Goldstar     | GSM59AC | 24EA53       | 1920x1080 | 510x290mm  | 23.1 | 2012 | 29FD1 |
| Goldstar     | GSM59B4 | 24EB23       | 1920x1200 | 520x330mm  | 24.2 | 2013 | 8D491 |
| Goldstar     | GSM59BC | 27EA33       | 1920x1080 | 600x340mm  | 27.2 | 2013 | 13795 |
| Goldstar     | GSM59BD | 27EA33       | 1920x1080 | 600x340mm  | 27.2 | 2013 | 22B8C |
| Goldstar     | GSM59C2 | 3D FHD LG TV | 1920x1080 | 510x290mm  | 23.1 | 2013 | F7E78 |
| Goldstar     | GSM59C6 | 2D FHD LG TV | 1920x1080 | 510x290mm  | 23.1 | 2016 | DC189 |
| Goldstar     | GSM59C6 | 2D FHD LG TV | 1920x1080 | 510x290mm  | 23.1 | 2015 | 5B3EF |
| Goldstar     | GSM59C6 | 2D FHD LG TV | 1920x1080 | 510x290mm  | 23.1 | 2013 | 1A075 |
| Goldstar     | GSM59C6 | 2D FHD LG TV | 1920x1080 | 480x270mm  | 21.7 | 2013 | 3BE77 |
| Goldstar     | GSM59CA | 2D HD LG TV  | 1366x768  | 510x290mm  | 23.1 | 2017 | 6E2C7 |
| Goldstar     | GSM59CA | 2D HD LG TV  | 1366x768  | 510x290mm  | 23.1 | 2016 | 7A37D |
| Goldstar     | GSM59CA | 2D HD LG TV  | 1366x768  | 510x290mm  | 23.1 | 2015 | 63011 |
| Goldstar     | GSM59CA | 2D HD LG TV  | 1366x768  | 510x290mm  | 23.1 | 2013 | 49884 |
| Goldstar     | GSM59CA | 2D HD LG TV  | 1920x1080 | 520x290mm  | 23.4 | 2013 | 9FEE6 |
| Goldstar     | GSM59CA | 2D HD LG TV  | 1920x1080 | 520x290mm  | 23.4 | 2012 | E8FBB |
| Goldstar     | GSM59D6 | 24EN33       | 1920x1080 | 530x300mm  | 24.0 | 2013 | C7319 |
| Goldstar     | GSM59D8 | 22EN43       | 1920x1080 | 480x270mm  | 21.7 | 2013 | FBF66 |
| Goldstar     | GSM59D9 | 22EN43       | 1920x1080 | 480x270mm  | 21.7 | 2012 | F8C1A |
| Goldstar     | GSM59DC | 23EN43       | 1920x1080 | 510x290mm  | 23.1 | 2012 | 0A817 |
| Goldstar     | GSM59DF | 24EN43       | 1920x1080 | 510x290mm  | 23.1 | 2012 | A8556 |
| Goldstar     | GSM59F0 | LG ULTRAWIDE | 2560x1080 | 670x280mm  | 28.6 | 2013 | D8D53 |
| Goldstar     | GSM59F1 | LG ULTRAWIDE | 2560x1080 | 670x280mm  | 28.6 | 2018 | 40972 |
| Goldstar     | GSM59F1 | LG ULTRAWIDE | 2560x1080 | 670x280mm  | 28.6 | 2017 | 12A4C |
| Goldstar     | GSM59F1 | LG ULTRAWIDE | 2560x1080 | 800x340mm  | 34.2 | 2017 | 3D724 |
| Goldstar     | GSM59F1 | LG ULTRAWIDE | 2560x1080 | 670x280mm  | 28.6 | 2016 | 19445 |
| Goldstar     | GSM59F1 | LG ULTRAWIDE | 2560x1080 | 800x340mm  | 34.2 | 2016 | B7390 |
| Goldstar     | GSM59F1 | LG ULTRAWIDE | 2560x1080 | 800x340mm  | 34.2 | 2015 | 39E77 |
| Goldstar     | GSM59F1 | LG ULTRAWIDE | 2560x1080 | 670x280mm  | 28.6 | 2015 | 6424D |
| Goldstar     | GSM59F1 | LG ULTRAWIDE | 2560x1080 | 580x240mm  | 24.7 | 2014 | 536A2 |
| Goldstar     | GSM59F1 | LG ULTRAWIDE | 2560x1080 | 800x340mm  | 34.2 | 2014 | E6DD3 |
| Goldstar     | GSM59F1 | LG ULTRAWIDE | 2560x1080 | 580x240mm  | 24.7 | 2013 | 17E05 |
| Goldstar     | GSM59F1 | LG ULTRAWIDE | 2560x1080 | 670x280mm  | 28.6 | 2013 | B652E |
| Goldstar     | GSM59F2 | LG ULTRAWIDE | 2560x1080 | 800x340mm  | 34.2 | 2016 | 6BF70 |
| Goldstar     | GSM59F2 | LG ULTRAWIDE | 2560x1080 | 800x340mm  | 34.2 | 2014 | 4A795 |
| Goldstar     | GSM5A1D | 27MP55       | 1920x1080 | 600x340mm  | 27.2 | 2013 | CEE54 |
| Goldstar     | GSM5A1F | 24MP55       | 1920x1080 | 510x290mm  | 23.1 | 2014 | 0BD69 |
| Goldstar     | GSM5A20 | 24MP55       | 1920x1080 | 510x290mm  | 23.1 | 2014 | 381BB |
| Goldstar     | GSM5A20 | 24MP55       | 1920x1080 | 510x290mm  | 23.1 | 2013 | 125E4 |
| Goldstar     | GSM5A22 | 23MP55       | 1920x1080 | 510x290mm  | 23.1 | 2014 | 20392 |
| Goldstar     | GSM5A23 | 23MP55       | 1920x1080 | 510x290mm  | 23.1 | 2016 | 0EBCE |
| Goldstar     | GSM5A23 | 23MP55       | 1920x1080 | 510x290mm  | 23.1 | 2015 | FACFE |
| Goldstar     | GSM5A23 | 23MP55       | 1920x1080 | 510x290mm  | 23.1 | 2014 | 46EC6 |
| Goldstar     | GSM5A23 | 23MP55       | 1920x1080 | 510x290mm  | 23.1 | 2013 | EBA2B |
| Goldstar     | GSM5A25 | 22MP55       | 1920x1080 | 480x270mm  | 21.7 | 2014 | 6E807 |
| Goldstar     | GSM5A26 | 22MP55       | 1920x1080 | 480x270mm  | 21.7 | 2017 | 6AD89 |
| Goldstar     | GSM5A26 | 22MP55       | 1920x1080 | 480x270mm  | 21.7 | 2015 | A7F32 |
| Goldstar     | GSM5A26 | 22MP55       | 1920x1080 | 480x270mm  | 21.7 | 2014 | DAB52 |
| Goldstar     | GSM5A2E | 22MB65       | 1680x1050 | 480x300mm  | 22.3 | 2016 | 5DE62 |
| Goldstar     | GSM5A2F | 22MB65       | 1680x1050 | 480x300mm  | 22.3 | 2014 | 23DD1 |
| Goldstar     | GSM5A34 | 22M45        | 1920x1080 | 480x270mm  | 21.7 | 2014 | 031C0 |
| Goldstar     | GSM5A39 | 22MP56       | 1920x1080 | 510x290mm  | 23.1 | 2015 | F4433 |
| Goldstar     | GSM5A39 | 22MP56       | 1920x1080 | 510x290mm  | 23.1 | 2014 | 6490C |
| Goldstar     | GSM5A3B | 22MP65       | 1920x1080 | 480x270mm  | 21.7 | 2014 | 5D980 |
| Goldstar     | GSM5A43 | 23MP65       | 1920x1080 | 510x290mm  | 23.1 | 2014 | D9C4F |
| Goldstar     | GSM5A44 | 23MP65       | 1920x1080 | 510x290mm  | 23.1 | 2014 | 9CBF5 |
| Goldstar     | GSM5A45 | 23MP65       | 1920x1080 | 510x290mm  | 23.1 | 2014 | 0B5B8 |
| Goldstar     | GSM5A47 | 23MP75       | 1920x1080 | 510x290mm  | 23.1 | 2014 | 0B1A6 |
| Goldstar     | GSM5A4C | 24MB65       | 1920x1200 | 520x330mm  | 24.2 | 2016 | F7111 |
| Goldstar     | GSM5A4C | 24MB65       | 1920x1200 | 520x330mm  | 24.2 | 2014 | EA82C |
| Goldstar     | GSM5A50 | 24M35        | 1920x1080 | 530x300mm  | 24.0 | 2014 | 6AEDF |
| Goldstar     | GSM5A56 | 24MP56       | 1920x1080 | 510x290mm  | 23.1 | 2015 | 1E92B |
| Goldstar     | GSM5A56 | 24MP56       | 1920x1080 | 510x290mm  | 23.1 | 2014 | 05C7A |
| Goldstar     | GSM5A5B | 27MP35       | 1920x1080 | 600x340mm  | 27.2 | 2012 | A141B |
| Goldstar     | GSM5A5F | 27MP65       | 1920x1080 | 600x340mm  | 27.2 | 2014 | 74FC8 |
| Goldstar     | GSM5A5F | 27MP65       | 1920x1080 | 600x340mm  | 27.2 | 2013 | 9E7A5 |
| Goldstar     | GSM5A61 | 27MP75       | 1920x1080 | 600x340mm  | 27.2 | 2015 | 4E7B4 |
| Goldstar     | GSM5A65 | LG ULTRAWIDE | 2560x1080 | 670x280mm  | 28.6 | 2013 | 687EC |
| Goldstar     | GSM5A6A | LG ULTRAWIDE | 2560x1080 | 580x240mm  | 24.7 | 2013 | 14817 |
| Goldstar     | GSM5A6B | LG ULTRAWIDE | 2560x1080 | 580x240mm  | 24.7 | 2013 | 5FB2D |
| Goldstar     | GSM5A86 | 27MP35       | 1920x1080 | 600x340mm  | 27.2 | 2014 | 92E62 |
| Goldstar     | GSM5A90 | 24GM77       | 1920x1080 | 530x300mm  | 24.0 | 2014 | 1D328 |
| Goldstar     | GSM5A92 | 24GM77       | 1920x1080 | 530x300mm  | 24.0 | 2014 | 92545 |
| Goldstar     | GSM5AB6 |              | 1920x1080 | 480x270mm  | 21.7 | 2017 | 9FF82 |
| Goldstar     | GSM5AB6 |              | 1920x1080 | 480x270mm  | 21.7 | 2016 | F3283 |
| Goldstar     | GSM5AB7 |              | 1920x1080 | 480x270mm  | 21.7 | 2017 | 467F1 |
| Goldstar     | GSM5AB7 |              | 1920x1080 | 480x270mm  | 21.7 | 2016 | 98CFC |
| Goldstar     | GSM5AB7 |              | 1920x1080 | 480x270mm  | 21.7 | 2015 | 079D3 |
| Goldstar     | GSM5AB8 |              | 1920x1080 | 480x270mm  | 21.7 | 2017 | 83F21 |
| Goldstar     | GSM5AB8 |              | 1920x1080 | 480x270mm  | 21.7 | 2016 | 47C62 |
| Goldstar     | GSM5AB8 |              | 1920x1080 | 480x270mm  | 21.7 | 2015 | 3C761 |
| Goldstar     | GSM5AB8 |              | 1920x1080 | 480x270mm  | 21.7 | 2014 | 26195 |
| Goldstar     | GSM5AB9 | LG FULL HD   | 1920x1080 | 480x270mm  | 21.7 | 2017 | 06763 |
| Goldstar     | GSM5AB9 | LG FULL HD   | 1920x1080 | 480x270mm  | 21.7 | 2015 | 38000 |
| Goldstar     | GSM5ABB | LG FULL HD   | 1920x1080 | 480x270mm  | 21.7 | 2015 | 37AA7 |
| Goldstar     | GSM5ABB | LG FULL HD   | 1920x1080 | 480x270mm  | 21.7 | 2014 | C65DD |
| Goldstar     | GSM5AC6 | LG HD PLUS   | 1600x900  | 440x250mm  | 19.9 | 2015 | 6B22D |
| Goldstar     | GSM5ACB | LG HD        | 1366x768  | 410x230mm  | 18.5 | 2018 | C650E |
| Goldstar     | GSM5ACB | LG HD        | 1366x768  | 410x230mm  | 18.5 | 2014 | DBE4C |
| Goldstar     | GSM5ADB | 27MB67       | 1920x1080 | 600x340mm  | 27.2 | 2015 | FA61C |
| Goldstar     | GSM5ADE | LG AIO       | 1920x1080 | 510x290mm  | 23.1 | 2015 | DB626 |
| Goldstar     | GSM5AE2 | LG ULTRAWIDE | 3440x1440 | 800x340mm  | 34.2 | 2017 | 3126E |
| Goldstar     | GSM5AE2 | LG ULTRAWIDE | 3440x1440 | 800x340mm  | 34.2 | 2013 | 70E49 |
| Goldstar     | GSM5AE4 | LG ULTRAWIDE | 2560x1080 | 670x280mm  | 28.6 | 2013 | 0F7D5 |
| Goldstar     | GSM5AEC | 24MB37       | 1920x1080 | 510x290mm  | 23.1 | 2017 | BACF7 |
| Goldstar     | GSM5AFB | LG ULTRAWIDE | 2560x1080 | 800x340mm  | 34.2 | 2017 | 57BDF |
| Goldstar     | GSM5AFB | LG ULTRAWIDE | 2560x1080 | 800x340mm  | 34.2 | 2016 | 94293 |
| Goldstar     | GSM5B08 | LG Ultra HD  | 3840x2160 | 600x340mm  | 27.2 | 2017 | 47666 |
| Goldstar     | GSM5B08 | LG Ultra HD  | 3840x2160 | 600x340mm  | 27.2 | 2016 | B3DDF |
| Goldstar     | GSM5B09 | LG Ultra HD  | 3840x2160 | 600x340mm  | 27.2 | 2019 | 8BAE3 |
| Goldstar     | GSM5B09 | LG Ultra HD  | 3840x2160 | 600x340mm  | 27.2 | 2018 | 7CFD1 |
| Goldstar     | GSM5B09 | LG Ultra HD  | 3840x2160 | 600x340mm  | 27.2 | 2017 | 14EE0 |
| Goldstar     | GSM5B1A | 27MB35       | 1920x1080 | 600x340mm  | 27.2 | 2017 | 226A6 |
| Goldstar     | GSM5B34 | MP59G        | 1920x1080 | 480x270mm  | 21.7 | 2018 | 4310D |
| Goldstar     | GSM5B34 | MP59G        | 1920x1080 | 480x270mm  | 21.7 | 2017 | 64B1D |
| Goldstar     | GSM5B34 | MP59G        | 1920x1080 | 480x270mm  | 21.7 | 2015 | 4245F |
| Goldstar     | GSM5B38 | 24GM79G      | 1920x1080 | 530x300mm  | 24.0 | 2018 | 4046E |
| Goldstar     | GSM5B39 | 24GM79G      | 1920x1080 | 530x300mm  | 24.0 | 2018 | 9FDE0 |
| Goldstar     | GSM5B44 | MP59HT       | 1920x1080 | 480x270mm  | 21.7 | 2015 | 17899 |
| Goldstar     | GSM5B46 | 24BK55WV     | 1920x1200 | 520x330mm  | 24.2 | 2017 | 64121 |
| Goldstar     | GSM5B55 | LG FULL HD   | 1920x1080 | 480x270mm  | 21.7 | 2018 | C9962 |
| Goldstar     | GSM5B55 | LG FULL HD   | 1920x1080 | 480x270mm  | 21.7 | 2016 | AD160 |
| Goldstar     | GSM60AC | M2252D       | 1920x1080 | 530x300mm  | 24.0 | 2012 | 5A2B6 |
| Goldstar     | GSM75A2 | LG TV        | 1360x768  | 700x390mm  | 31.5 | 2007 | 29E48 |
| Goldstar     | GSM75A4 | LG TV        | 1360x768  | 700x390mm  | 31.5 | 2007 | ECCB6 |
| Goldstar     | GSM75CB | 37LF65       | 1920x1080 | 700x390mm  | 31.5 | 2007 | D3756 |
| Goldstar     | GSM75F0 | LG TV        | 1920x1080 | 700x390mm  | 31.5 | 2009 | 81A8C |
| Goldstar     | GSM75F0 | 32LG3000     | 1920x1080 | 700x390mm  | 31.5 | 2008 | 7630A |
| Goldstar     | GSM7666 | ITE6604      | 1920x1200 | 700x390mm  | 31.5 | 2012 | FE91A |
| Goldstar     | GSM76E3 | LG ULTRAWIDE | 3440x1440 | 800x340mm  | 34.2 | 2015 | 6B685 |
| Goldstar     | GSM76E4 | LG ULTRAWIDE | 3440x1440 | 800x340mm  | 34.2 | 2017 | 3FCF8 |
| Goldstar     | GSM76F6 | LG ULTRAWIDE | 3440x1440 | 800x340mm  | 34.2 | 2019 | 450E9 |
| Goldstar     | GSM76F6 | LG ULTRAWIDE | 3440x1440 | 800x340mm  | 34.2 | 2016 | 24533 |
| Goldstar     | GSM76F9 | LG ULTRAWIDE | 2560x1080 | 800x340mm  | 34.2 | 2019 | 4C675 |
| Goldstar     | GSM76F9 | LG ULTRAWIDE | 2560x1080 | 800x340mm  | 34.2 | 2018 | 16A3B |
| Goldstar     | GSM76F9 | LG ULTRAWIDE | 2560x1080 | 800x340mm  | 34.2 | 2017 | 0BAFA |
| Goldstar     | GSM76FA | LG ULTRAWIDE | 2560x1080 | 800x340mm  | 34.2 | 2018 | 58C53 |
| Goldstar     | GSM76FA | LG ULTRAWIDE | 2560x1080 | 800x340mm  | 34.2 | 2017 | 6E91C |
| Goldstar     | GSM76FC | LG ULTRAWIDE | 3840x1600 | 870x370mm  | 37.2 | 2017 | A2C3C |
| Goldstar     | GSM7700 | LG 32 FHD    | 1920x1080 | 700x390mm  | 31.5 | 2014 | 5BFD0 |
| Goldstar     | GSM7706 | LG HDR 4K    | 3840x2160 | 600x340mm  | 27.2 | 2018 | 67C08 |
| Goldstar     | GSM7707 | LG HDR 4K    | 3840x2160 | 600x340mm  | 27.2 | 2019 | 60101 |
| Goldstar     | GSM7707 | LG HDR 4K    | 3840x2160 | 600x340mm  | 27.2 | 2018 | 01248 |
| Goldstar     | GSM7714 | LG HDR WFHD  | 2560x1080 | 800x340mm  | 34.2 | 2018 | 11EE2 |
| Goldstar     | GSM7715 | LG HDR WFHD  | 2560x1080 | 800x340mm  | 34.2 | 2018 | EFD9C |
| Goldstar     | GSM7715 | LG HDR WFHD  | 2560x1080 | 800x340mm  | 34.2 | 2017 | E59F2 |
| Goldstar     | GSM9CF6 | LG TV        | 1360x768  | 700x390mm  | 31.5 | 2013 | E04A3 |
| Goldstar     | GSM9E76 | LG SIGNAGE   | 1920x1080 | 1220x690mm | 55.2 | 2013 | 573FA |
| Goldstar     | GSMC3A2 | LG TV        | 1920x1080 | 700x390mm  | 31.5 | 2008 | 42768 |
| Goldstar     | GSMC468 | LG TV        | 1920x1080 | 700x390mm  | 31.5 | 2009 | DCEEF |
| Goldstar     | GSMFFFF | LG TV        | 1280x720  | 700x390mm  | 31.5 | 2009 | 871EA |
| Grundig      | GRU4448 | UHD          | 3840x2160 | 1210x680mm | 54.6 | 2018 | D817C |
| Grundig      | GRU4448 |              | 1920x1080 |            |      | 2011 | D36A2 |
| Grundig      | GRU4448 | WXGA         | 1600x1200 |            |      | 2009 | 3051A |
| Grundig      | GRU4448 | G2 1080p dig | 1920x1080 |            |      | 2006 | 9CBF0 |
| HCL          | HCMAE1A | HCMELWBT11   | 1366x768  | 430x260mm  | 19.8 | 2009 | E5BE0 |
| HKC          | HKC0000 |              | 1920x1080 | 520x300mm  | 23.6 | 2017 | 5A11C |
| HKC          | HKC0000 |              | 1920x1080 | 530x300mm  | 24.0 | 2015 | 0DEBF |
| HKC          | HKC0000 | TV           | 1360x768  | 575x323mm  | 26.0 | 2015 | 30700 |
| HKC          | HKC0000 | L23600WHS    | 1920x1080 | 520x290mm  | 23.4 | 2015 | 96202 |
| HKC          | HKC0000 |              | 1920x1080 | 575x323mm  | 26.0 | 2014 | B276A |
| HKC          | HKC1850 |              | 1360x768  | 410x230mm  | 18.5 | 2010 | 73B91 |
| HKC          | HKC2160 |              | 1920x1080 | 480x270mm  | 21.7 | 2013 | DDB36 |
| HKC          | HKC2160 |              | 1920x1080 | 480x270mm  | 21.7 | 2011 | 0BBB5 |
| HKC          | HKC2160 |              | 1920x1080 | 300x230mm  | 14.9 | 2011 | 2AC3F |
| HKC          | HKC9000 | TV           | 1920x1080 |            |      | 2014 | 36C1A |
| HKC          | HKC9000 | TV           | 1360x768  |            |      | 2013 | 18914 |
| HKC          | HKCB34C | C340         | 3440x1440 | 800x330mm  | 34.1 | 2018 | 18F9F |
| HP           | HPN335A | V213a        | 1920x1080 | 460x260mm  | 20.8 | 2016 | 7421C |
| HP           | HPN3378 |              | 2560x1440 | 710x400mm  | 32.1 | 2016 | 744F1 |
| HP           | HPN337B | 24o          | 1920x1080 | 530x300mm  | 24.0 | 2017 | 71710 |
| HP           | HPN337C | 24o          | 1920x1080 | 530x300mm  | 24.0 | 2017 | 0FB23 |
| HP           | HPN337C | 24o          | 1920x1080 | 530x300mm  | 24.0 | 2016 | 0CE20 |
| HP           | HPN3395 | 27ea         | 1920x1080 | 530x300mm  | 24.0 | 2016 | F636A |
| HP           | HPN3421 |              | 2560x1440 | 600x340mm  | 27.2 | 2017 | 3C9DB |
| HP           | HPN3424 | 27 Curved    | 1920x1080 | 600x340mm  | 27.2 | 2017 | 13334 |
| HP           | HPN3425 |              | 1920x1080 | 540x300mm  | 24.3 | 2017 | 0AA3F |
| HP           | HPN3426 |              | 1920x1080 | 540x300mm  | 24.3 | 2018 | B542C |
| HP           | HPN342E | 22w          | 1920x1080 | 480x270mm  | 21.7 | 2018 | 7073D |
| HP           | HPN342E | 22w          | 1920x1080 | 480x270mm  | 21.7 | 2017 | 94C11 |
| HP           | HPN3431 | 24w          | 1920x1080 | 530x300mm  | 24.0 | 2018 | 1253A |
| HP           | HPN3431 | 24w          | 1920x1080 | 530x300mm  | 24.0 | 2017 | 3324B |
| HP           | HPN3438 | Z43          | 3840x2160 | 940x530mm  | 42.5 | 2018 | A8CDF |
| HP           | HPN3449 | V273a        | 1920x1080 | 600x340mm  | 27.2 | 2018 | AE95D |
| HP           | HPN345C | E223         | 1920x1080 | 480x270mm  | 21.7 | 2018 | D447C |
| HP           | HPN345C | E223         | 1920x1080 | 480x270mm  | 21.7 | 2017 | DF15E |
| HP           | HPN345F | E233         | 1920x1080 | 510x290mm  | 23.1 | 2018 | C1FBE |
| HP           | HPN3460 | E233         | 1920x1080 | 510x290mm  | 23.1 | 2018 | 23005 |
| HP           | HPN3462 | E243i        | 1920x1200 | 520x320mm  | 24.0 | 2018 | 862F7 |
| HP           | HPN3462 | E243i        | 1920x1200 | 520x320mm  | 24.0 | 2017 | CDEC9 |
| HP           | HPN3468 | E243         | 1920x1080 | 530x300mm  | 24.0 | 2018 | 7C92F |
| HP           | HPN346E | E273m        | 1920x1080 | 600x340mm  | 27.2 | 2017 | 4C1DC |
| HP           | HPN347A | Z23n G2      | 1920x1080 | 510x290mm  | 23.1 | 2018 | 7199F |
| HP           | HPN3499 | VH240a       | 1920x1080 | 530x300mm  | 24.0 | 2017 | 03964 |
| HP           | HPN3504 | 24y          | 1920x1080 | 530x300mm  | 24.0 | 2018 | BA53E |
| HP           | HPN3515 | ANX7530      | 2880x1440 | 60x50mm    | 3.1  | 2017 | EB800 |
| HP           | HPN352F | S270n        | 3840x2160 | 600x340mm  | 27.2 | 2018 | EA6AE |
| HP           | HPN3541 | 22f          | 1920x1080 | 500x300mm  | 23.0 | 2018 | 11512 |
| HP           | HPN3545 | 24f          | 1920x1080 | 530x300mm  | 24.0 | 2018 | 37FD8 |
| HP           | HPN3545 | 24fw         | 1920x1080 | 530x300mm  | 24.0 | 2018 | 633C2 |
| HP           | HPN354B | 27fw         | 1920x1080 | 600x340mm  | 27.2 | 2018 | 63DB3 |
| HP           | HPN354B | 27f          | 1920x1080 | 600x340mm  | 27.2 | 2018 | B7466 |
| HP           | HPN3564 | 27q          | 2560x1440 | 600x340mm  | 27.2 | 2018 | 4322E |
| HP           | HPN3573 | 27 FHD       | 1920x1080 | 600x340mm  | 27.2 | 2018 | F3130 |
| HP           | HPN4018 |              | 1920x1080 | 530x300mm  | 24.0 | 2017 | B0874 |
| HP           | HPN401D |              | 1920x1080 | 480x270mm  | 21.7 | 2017 | 8A8A9 |
| HP           | HPN4027 |              | 2560x1440 | 600x340mm  | 27.2 | 2018 | 63273 |
| HP           | HWP0A72 | LP2065       | 1600x1200 | 410x310mm  | 20.2 | 2007 | 3DDD9 |
| HP           | HWP0A72 | LP2065       | 1600x1200 | 410x310mm  | 20.2 | 2006 | A6B7A |
| HP           | HWP1001 | Omni         | 1920x1080 | 510x290mm  | 23.1 | 2010 | 9B5FB |
| HP           | HWP1080 | HPQ 800 A... | 1920x1080 | 510x290mm  | 23.1 | 2013 | AA0AB |
| HP           | HWP12DA | f70          | 1280x1024 | 340x270mm  | 17.1 |      | 5A780 |
| HP           | HWP259B | L1925        | 1280x1024 | 370x300mm  | 18.8 |      | 91954 |
| HP           | HWP260E | L1730        | 1280x1024 | 340x270mm  | 17.1 |      | 494A3 |
| HP           | HWP2615 | L2335        | 1920x1200 | 500x310mm  | 23.2 |      | 1B261 |
| HP           | HWP261A | f2304        | 1920x1200 | 500x310mm  | 23.2 |      | 90ADB |
| HP           | HWP262F | L1940        | 1280x1024 | 380x300mm  | 19.1 |      | 3806D |
| HP           | HWP2649 | L1740        | 1280x1024 | 330x270mm  | 16.8 | 2006 | 02005 |
| HP           | HWP2649 | L1740        | 1280x1024 | 330x270mm  | 16.8 |      | 1A628 |
| HP           | HWP264B | L1755        | 1280x1024 | 330x270mm  | 16.8 | 2006 | 7C99A |
| HP           | HWP264F | f1905        | 1280x1024 | 380x300mm  | 19.1 |      | 0C1C6 |
| HP           | HWP265C | L1706        | 1280x1024 | 340x270mm  | 17.1 | 2007 | 03894 |
| HP           | HWP265C | L1706        | 1280x1024 | 340x270mm  | 17.1 | 2006 | 0BA9B |
| HP           | HWP2676 | LP2465       | 1920x1200 | 520x330mm  | 24.2 | 2007 | C0F96 |
| HP           | HWP2679 | w19          | 1440x900  | 400x250mm  | 18.6 | 2007 | 094E3 |
| HP           | HWP2683 | L1940T       | 1280x1024 | 380x300mm  | 19.1 | 2007 | 1FD80 |
| HP           | HWP2683 | L1940T       | 1280x1024 | 380x300mm  | 19.1 | 2006 | 9BBCE |
| HP           | HWP2690 | LP3065       | 2560x1600 | 640x400mm  | 29.7 | 2006 | 69822 |
| HP           | HWP2693 | LP1965       | 1280x1024 | 380x300mm  | 19.1 | 2008 | 29DCB |
| HP           | HWP2693 | LP1965       | 1280x1024 | 380x300mm  | 19.1 | 2007 | 1C597 |
| HP           | HWP2695 | L2045w       | 1680x1050 | 430x270mm  | 20.0 | 2007 | 4A8F0 |
| HP           | HWP2695 | L2045w       | 1680x1050 | 430x270mm  | 20.0 | 2006 | 63F7C |
| HP           | HWP26A2 | w1907        | 1440x900  | 410x260mm  | 19.1 | 2007 | 2CD0F |
| HP           | HWP26A3 | w1907        | 1440x900  | 410x260mm  | 19.1 | 2008 | 272AD |
| HP           | HWP26A3 | w1907        | 1440x900  | 410x260mm  | 19.1 | 2007 | 21162 |
| HP           | HWP26A7 | w2007        | 1680x1050 | 430x270mm  | 20.0 | 2007 | EE9DE |
| HP           | HWP26A9 | w2207        | 1680x1050 | 470x300mm  | 22.0 | 2008 | 7D68A |
| HP           | HWP26A9 | w2207        | 1680x1050 | 470x300mm  | 22.0 | 2007 | 29947 |
| HP           | HWP26AE | w22          | 1680x1050 | 470x300mm  | 22.0 | 2006 | 6A6A2 |
| HP           | HWP26CF | w2408        | 1920x1200 | 520x320mm  | 24.0 | 2008 | 17E64 |
| HP           | HWP26CF | w2408        | 1920x1200 | 520x320mm  | 24.0 | 2007 | 4CD4A |
| HP           | HWP26E8 | L1950        | 1280x1024 | 380x300mm  | 19.1 | 2008 | 159B7 |
| HP           | HWP26EA | L1750        | 1280x1024 | 340x270mm  | 17.1 | 2008 | 040F5 |
| HP           | HWP26F7 | LP2475w      | 1920x1200 | 540x350mm  | 25.3 | 2011 | 776E7 |
| HP           | HWP26F7 | LP2475w      | 1920x1200 | 540x350mm  | 25.3 | 2009 | 962C9 |
| HP           | HWP26F7 | LP2475w      | 1920x1200 | 540x350mm  | 25.3 | 2008 | 54D39 |
| HP           | HWP26F8 | LP2475w      | 1920x1200 | 540x350mm  | 25.3 | 2010 | 7207A |
| HP           | HWP26F8 | LP2475w      | 1920x1200 | 540x350mm  | 25.3 | 2009 | 71952 |
| HP           | HWP26F8 | LP2475w      | 1920x1200 | 540x350mm  | 25.3 | 2008 | 792B0 |
| HP           | HWP26FB | L2245w       | 1680x1050 | 470x300mm  | 22.0 | 2008 | 4D83C |
| HP           | HWP26FC | L2245w       | 1680x1050 | 470x300mm  | 22.0 | 2009 | 077BE |
| HP           | HWP26FC | L2245w       | 1680x1050 | 470x300mm  | 22.0 | 2008 | 56CCA |
| HP           | HWP2801 | w1707        | 1440x900  | 370x230mm  | 17.2 | 2008 | 23A04 |
| HP           | HWP2805 | L1945w       | 1440x900  | 410x260mm  | 19.1 | 2009 | EDFC5 |
| HP           | HWP2807 | LP2275w      | 1680x1050 | 470x300mm  | 22.0 | 2008 | 12A46 |
| HP           | HWP280F | L2445w       | 1920x1200 | 520x320mm  | 24.0 | 2008 | E6806 |
| HP           | HWP2812 | w2228h       | 1680x1050 | 480x300mm  | 22.3 | 2008 | 7EA9D |
| HP           | HWP2814 | w2448h       | 1920x1200 | 520x320mm  | 24.0 | 2008 | E2429 |
| HP           | HWP2817 | w2558hc      | 1920x1200 | 550x350mm  | 25.7 | 2008 | 3263C |
| HP           | HWP281A | w1858        | 1366x768  | 410x230mm  | 18.5 | 2010 | DBA7E |
| HP           | HWP281C | w2338h       | 1920x1080 | 510x290mm  | 23.1 | 2009 | 409E8 |
| HP           | HWP2820 | v185w        | 1366x768  | 410x230mm  | 18.5 | 2009 | EC2D6 |
| HP           | HWP2823 | 2309         | 1920x1080 | 510x290mm  | 23.1 | 2009 | 3CEC6 |
| HP           | HWP2828 | 2009         | 1600x900  | 440x250mm  | 19.9 | 2009 | 48B84 |
| HP           | HWP282B | 2159         | 1920x1080 | 480x270mm  | 21.7 | 2009 | DA3DC |
| HP           | HWP283A | 2509         | 1920x1080 | 550x310mm  | 24.9 | 2010 | 8C548 |
| HP           | HWP283B | 2509         | 1920x1080 | 550x310mm  | 24.9 | 2009 | 1036A |
| HP           | HWP2845 | LA1905       | 1440x900  | 410x260mm  | 19.1 | 2010 | 75C21 |
| HP           | HWP2849 | LA2205       | 1680x1050 | 470x300mm  | 22.0 | 2010 | DDA93 |
| HP           | HWP284B | LA2405       | 1920x1200 | 520x320mm  | 24.0 | 2012 | DE032 |
| HP           | HWP284B | LA2405       | 1920x1200 | 520x320mm  | 24.0 | 2011 | 1ED49 |
| HP           | HWP284B | LA2405       | 1920x1200 | 520x320mm  | 24.0 | 2009 | BBDE2 |
| HP           | HWP284C | LA2405       | 1920x1200 | 520x320mm  | 24.0 | 2011 | A32F6 |
| HP           | HWP284C | LA2405       | 1920x1200 | 520x320mm  | 24.0 | 2010 | 51C8B |
| HP           | HWP284E | LE1901w      | 1440x900  | 410x260mm  | 19.1 | 2010 | 7A37B |
| HP           | HWP2851 |              | 1366x768  | 410x230mm  | 18.5 | 2009 | 0B06E |
| HP           | HWP2854 | 2229h        | 1680x1050 | 480x300mm  | 22.3 | 2009 | 376F9 |
| HP           | HWP285A | LA1951       | 1280x1024 | 380x300mm  | 19.1 | 2010 | 979C0 |
| HP           | HWP285A | M215HW01     | 1920x1080 | 470x270mm  | 21.3 | 2009 | 98A35 |
| HP           | HWP285B | LA1951       | 1280x1024 | 380x300mm  | 19.1 | 2012 | 6EB54 |
| HP           | HWP285B | LA1951       | 1280x1024 | 380x300mm  | 19.1 | 2010 | DFE75 |
| HP           | HWP285B | LA1951       | 1280x1024 | 380x300mm  | 19.1 | 2009 | 7E5F0 |
| HP           | HWP2866 | ZR22w        | 1920x1080 | 480x270mm  | 21.7 | 2011 | 8B17D |
| HP           | HWP2867 | ZR22w        | 1920x1080 | 480x270mm  | 21.7 | 2010 | 6D3D1 |
| HP           | HWP286A | ZR24w        | 1920x1200 | 540x350mm  | 25.3 | 2011 | 1011E |
| HP           | HWP286A | ZR24w        | 1920x1200 | 520x320mm  | 24.0 | 2011 | 5176C |
| HP           | HWP286E | ZR30w        | 2560x1600 | 640x400mm  | 29.7 | 2012 | 07CA0 |
| HP           | HWP287F | L2445m       | 1920x1200 | 520x320mm  | 24.0 | 2009 | 92086 |
| HP           | HWP2888 | 2010         | 1600x900  | 440x250mm  | 19.9 | 2009 | 5D858 |
| HP           | HWP288E | 2310e        | 1920x1080 | 510x290mm  | 23.1 | 2010 | 393E8 |
| HP           | HWP288E | 2310         | 1920x1080 | 510x290mm  | 23.1 | 2010 | 5EDF6 |
| HP           | HWP2903 | S2031        | 1600x900  | 440x250mm  | 19.9 | 2010 | 06495 |
| HP           | HWP2905 | S2231        | 1920x1080 | 480x270mm  | 21.7 | 2011 | 04B61 |
| HP           | HWP2905 | S2231        | 1920x1080 | 480x270mm  | 21.7 | 2010 | 28357 |
| HP           | HWP2907 | S2331        | 1920x1080 | 510x290mm  | 23.1 | 2011 | 12F04 |
| HP           | HWP2907 | S2331        | 1920x1080 | 510x290mm  | 23.1 | 2010 | BE5CD |
| HP           | HWP2908 | S2331        | 1920x1080 | 510x290mm  | 23.1 | 2011 | FA5E9 |
| HP           | HWP290F | x20LED       | 1600x900  | 440x250mm  | 19.9 | 2012 | C8436 |
| HP           | HWP290F | x20LED       | 1600x900  | 440x250mm  | 19.9 | 2011 | 923B3 |
| HP           | HWP290F | x20LED       | 1600x900  | 440x250mm  | 19.9 | 2010 | 9C46B |
| HP           | HWP2934 | 2011         | 1600x900  | 440x250mm  | 19.9 | 2012 | 85EDC |
| HP           | HWP2934 | 2011         | 1600x900  | 440x250mm  | 19.9 | 2011 | 00088 |
| HP           | HWP2939 | 2311         | 1920x1080 | 510x290mm  | 23.1 | 2011 | DDB7F |
| HP           | HWP293A | 2311x        | 1920x1080 | 510x290mm  | 23.1 | 2011 | 18164 |
| HP           | HWP293A | 2311         | 1920x1080 | 510x290mm  | 23.1 | 2011 | 57F2D |
| HP           | HWP293E | 2511         | 1920x1080 | 550x310mm  | 24.9 | 2013 | 7A768 |
| HP           | HWP2941 | 2711         | 1920x1080 | 600x340mm  | 27.2 | 2011 | 9FDED |
| HP           | HWP2943 | LA2006       | 1600x900  | 440x250mm  | 19.9 | 2010 | 49C29 |
| HP           | HWP2944 | LA2006       | 1600x900  | 440x250mm  | 19.9 | 2011 | 6BDA3 |
| HP           | HWP294A | LA2306       | 1920x1080 | 510x290mm  | 23.1 | 2013 | 8E15A |
| HP           | HWP294B | LA2306       | 1920x1080 | 510x290mm  | 23.1 | 2011 | E11CD |
| HP           | HWP2950 | ZR2240w      | 1920x1080 | 480x270mm  | 21.7 | 2011 | 14174 |
| HP           | HWP2951 | ZR2240w      | 1920x1080 | 480x270mm  | 21.7 | 2011 | B843B |
| HP           | HWP2952 | ZR2240w      | 1920x1080 | 480x270mm  | 21.7 | 2011 | 5500D |
| HP           | HWP2954 | ZR2440w      | 1920x1200 | 520x320mm  | 24.0 | 2013 | F219F |
| HP           | HWP2955 | ZR2440w      | 1920x1200 | 520x320mm  | 24.0 | 2013 | DD510 |
| HP           | HWP2956 | ZR2440w      | 1920x1200 | 520x320mm  | 24.0 | 2012 | F6D04 |
| HP           | HWP2958 | ZR2740w      | 2560x1440 | 600x340mm  | 27.2 | 2012 | 26579 |
| HP           | HWP2972 | x2301        | 1920x1080 | 510x290mm  | 23.1 | 2012 | 15120 |
| HP           | HWP2973 | x2301        | 1920x1080 | 510x290mm  | 23.1 | 2012 | CF28B |
| HP           | HWP2975 | x2301        | 1920x1080 | 510x290mm  | 23.1 | 2011 | 45ABF |
| HP           | HWP2981 | 2311gt       | 1920x1080 | 510x290mm  | 23.1 | 2011 | 10ED8 |
| HP           | HWP299C | W2071d       | 1600x900  | 440x250mm  | 19.9 | 2014 | 84218 |
| HP           | HWP299F | W2072a       | 1600x900  | 440x250mm  | 19.9 | 2013 | 0A3EE |
| HP           | HWP302E | 22xi         | 1920x1080 | 480x270mm  | 21.7 | 2013 | 101F0 |
| HP           | HWP302F | 22xi         | 1920x1080 | 480x270mm  | 21.7 | 2015 | 67EED |
| HP           | HWP302F | 22xi         | 1920x1080 | 480x270mm  | 21.7 | 2014 | 63E33 |
| HP           | HWP302F | 22xi         | 1920x1080 | 480x270mm  | 21.7 | 2013 | 29EAE |
| HP           | HWP3031 | 23xi         | 1920x1080 | 510x290mm  | 23.1 | 2014 | D6CF3 |
| HP           | HWP3031 | 23xi         | 1920x1080 | 510x290mm  | 23.1 | 2013 | 9D2B5 |
| HP           | HWP3032 | 23xi         | 1920x1080 | 510x290mm  | 23.1 | 2014 | C4324 |
| HP           | HWP3037 | 27xi         | 1920x1080 | 600x340mm  | 27.2 | 2014 | 29C77 |
| HP           | HWP3038 | 27xi         | 1920x1080 | 600x340mm  | 27.2 | 2013 | 595D0 |
| HP           | HWP303C | x2401        | 1920x1080 | 530x300mm  | 24.0 | 2012 | 2DC57 |
| HP           | HWP303E | x2401        | 1920x1080 | 530x300mm  | 24.0 | 2012 | 510DB |
| HP           | HWP3040 | ENVY 27      | 1920x1080 | 600x340mm  | 27.2 | 2013 | 34563 |
| HP           | HWP3047 | 22bw         | 1920x1080 | 480x270mm  | 21.7 | 2013 | 00326 |
| HP           | HWP3048 | 22bw         | 1920x1080 | 480x270mm  | 21.7 | 2014 | 05BB5 |
| HP           | HWP3055 | P201         | 1600x900  | 440x250mm  | 19.9 | 2013 | DBCB2 |
| HP           | HWP305E | E201         | 1600x900  | 440x250mm  | 19.9 | 2013 | 8C991 |
| HP           | HWP3061 | E221         | 1920x1080 | 500x290mm  | 22.8 | 2015 | CEB20 |
| HP           | HWP3064 | E231         | 1920x1080 | 510x290mm  | 23.1 | 2014 | C6C76 |
| HP           | HWP3064 | E231         | 1920x1080 | 510x290mm  | 23.1 | 2013 | 08FE9 |
| HP           | HWP3075 | 23fi         | 1920x1080 | 510x290mm  | 23.1 | 2013 | 25113 |
| HP           | HWP3092 | E221c        | 1920x1080 | 500x290mm  | 22.8 | 2014 | 23EF2 |
| HP           | HWP309F | Z24i         | 1920x1200 | 520x320mm  | 24.0 | 2017 | 678F6 |
| HP           | HWP309F | Z24i         | 1920x1200 | 520x320mm  | 24.0 | 2015 | 4354A |
| HP           | HWP3106 | E271i        | 1920x1080 | 600x340mm  | 27.2 | 2013 | 86574 |
| HP           | HWP310B | ENVY 23      | 1920x1080 | 510x290mm  | 23.1 | 2013 | 593F0 |
| HP           | HWP3110 | 23tm         | 1920x1080 | 510x290mm  | 23.1 | 2013 | 6DC28 |
| HP           | HWP3122 | E241i        | 1920x1200 | 520x320mm  | 24.0 | 2015 | 8084F |
| HP           | HWP3123 | E241i        | 1920x1200 | 520x320mm  | 24.0 | 2015 | 46F9E |
| HP           | HWP314F | E221i        | 1920x1080 | 480x270mm  | 21.7 | 2014 | B174A |
| HP           | HWP3151 | E231i        | 1920x1080 | 510x290mm  | 23.1 | 2014 | 7F427 |
| HP           | HWP315F | ENVY 32      | 2560x1440 | 710x400mm  | 32.1 | 2015 | 5AC15 |
| HP           | HWP3183 | 22cwa        | 1920x1080 | 480x270mm  | 21.7 | 2016 | 37559 |
| HP           | HWP318A | 23xw         | 1920x1080 | 510x290mm  | 23.1 | 2015 | 2B8A1 |
| HP           | HWP3195 | 27cw         | 1920x1080 | 600x340mm  | 27.2 | 2015 | D74D5 |
| HP           | HWP3198 | 27xw         | 1920x1080 | 600x340mm  | 27.2 | 2015 | 7D79B |
| HP           | HWP319E | 27c          | 1920x1080 | 600x340mm  | 27.2 | 2014 | 6142A |
| HP           | HWP3204 | ENVY 34c     | 3440x1440 | 800x330mm  | 34.1 | 2016 | 9B25D |
| HP           | HWP3205 | ENVY 34c     | 3440x1440 | 800x330mm  | 34.1 | 2015 | 6AF3D |
| HP           | HWP320E | Z24n         | 1920x1200 | 520x320mm  | 24.0 | 2016 | 177BE |
| HP           | HWP322A | P202         | 1600x900  | 440x250mm  | 19.9 | 2016 | 9DFC0 |
| HP           | HWP3237 | P242va       | 1920x1080 | 530x300mm  | 24.0 | 2015 | AA65A |
| HP           | HWP323A | Z24nq        | 2560x1440 | 530x300mm  | 24.0 | 2016 | C2B9B |
| HP           | HWP3257 | 24cw         | 1920x1080 | 530x300mm  | 24.0 | 2015 | A1345 |
| HP           | HWP326E | E242         | 1920x1200 | 520x320mm  | 24.0 | 2017 | 47005 |
| HP           | HWP326F | E242         | 1920x1200 | 520x320mm  | 24.0 | 2016 | A52C8 |
| HP           | HWP327E | E240c        | 1920x1080 | 510x290mm  | 23.1 | 2016 | F58BD |
| HP           | HWP3288 | 27sv         | 1920x1080 | 600x340mm  | 27.2 | 2018 | 315EF |
| HP           | HWP3306 | P240va       | 1920x1080 | 530x300mm  | 24.0 | 2018 | 835DE |
| HP           | HWP3316 | V198bz       | 1366x768  | 410x230mm  | 18.5 | 2016 | 003A4 |
| HP           | HWP3318 | V225hz       | 1920x1080 | 600x340mm  | 27.2 | 2017 | B639D |
| HP           | HWP3319 | V225hz       | 1920x1080 | 510x290mm  | 23.1 | 2016 | 34294 |
| HP           | HWP331B | 22er         | 1920x1080 | 500x300mm  | 23.0 | 2018 | 5036B |
| HP           | HWP331B | 22es         | 1920x1080 | 500x300mm  | 23.0 | 2018 | 96170 |
| HP           | HWP331B | 22es         | 1920x1080 | 500x300mm  | 23.0 | 2017 | BE05D |
| HP           | HWP331B | 22er         | 1920x1080 | 500x300mm  | 23.0 | 2017 | F9025 |
| HP           | HWP331B | 22es         | 1920x1080 | 500x300mm  | 23.0 | 2016 | 60A8B |
| HP           | HWP331E | 23er         | 1920x1080 | 510x290mm  | 23.1 | 2018 | F407A |
| HP           | HWP331E | 23es         | 1920x1080 | 510x290mm  | 23.1 | 2016 | 2E5C4 |
| HP           | HWP331E | 23er         | 1920x1080 | 510x290mm  | 23.1 | 2016 | A8289 |
| HP           | HWP3320 | 24er         | 1920x1080 | 530x300mm  | 24.0 | 2017 | 382C8 |
| HP           | HWP3320 | 24es         | 1920x1080 | 530x300mm  | 24.0 | 2017 | 92F47 |
| HP           | HWP3320 | 24es         | 1920x1080 | 530x300mm  | 24.0 | 2016 | D3303 |
| HP           | HWP3322 | 25es         | 1920x1080 | 550x310mm  | 24.9 | 2017 | 6438A |
| HP           | HWP3325 | 27es         | 1920x1080 | 600x340mm  | 27.2 | 2018 | F2CF5 |
| HP           | HWP3325 | 27es         | 1920x1080 | 600x340mm  | 27.2 | 2017 | D957C |
| HP           | HWP3325 | 27er         | 1920x1080 | 600x340mm  | 27.2 | 2016 | 481B3 |
| HP           | HWP3325 | 27es         | 1920x1080 | 600x340mm  | 27.2 | 2016 | 7B383 |
| HP           | HWP3326 | 27es         | 1920x1080 | 600x340mm  | 27.2 | 2017 | 07E09 |
| HP           | HWP332B | 22kd         | 1920x1080 | 480x270mm  | 21.7 | 2016 | DB776 |
| HP           | HWP3339 |              | 2560x1440 | 710x400mm  | 32.1 | 2018 | B8B4D |
| HP           | HWP3339 |              | 2560x1440 | 710x400mm  | 32.1 | 2017 | 5A1A3 |
| HP           | HWP3354 | 27wm         | 1920x1080 | 600x340mm  | 27.2 | 2016 | 8C4C5 |
| HP           | HWP3561 | Z1           | 2560x1440 | 600x340mm  | 27.2 | 2011 | 963B7 |
| HP           | HWP4000 |              | 1920x1080 | 510x290mm  | 23.1 | 2009 | 6FDDE |
| HP           | HWP4101 | Omni/Pro ... | 1920x1080 | 470x270mm  | 21.3 | 2010 | 4A350 |
| HP           | HWP4101 | M215HW01 V6  | 1920x1080 | 470x270mm  | 21.3 | 2009 | 91E33 |
| HP           | HWP4108 | Omni/Pro ... | 1920x1080 | 470x270mm  | 21.3 | 2010 | 94E21 |
| HP           | HWP4109 | SAMSUNG L... | 1600x900  | 440x250mm  | 19.9 | 2009 | FD085 |
| HP           | HWP410E | Compaq CQ... | 1366x768  | 410x230mm  | 18.5 | 2011 | B850B |
| HP           | HWP4211 | All-in-One   | 1920x1080 | 510x290mm  | 23.1 | 2013 | 7CD70 |
| HP           | HWP4211 | All-in-One   | 1920x1080 | 510x290mm  | 23.1 | 2012 | 3E6BB |
| HP           | HWP4211 |              | 1920x1080 | 510x290mm  | 23.1 | 2011 | D9835 |
| HP           | HWP4211 | TouchSmart   | 1920x1080 | 510x290mm  | 23.1 | 2010 | 89D4E |
| HP           | HWP4218 | All-in-One   | 1600x900  | 440x250mm  | 19.9 | 2012 | 9AA2B |
| HP           | HWP4218 | Omni 120 ... | 1600x900  | 440x250mm  | 19.9 | 2009 | 8687F |
| HP           | HWP4218 | LTM200KT10   | 1600x900  | 440x250mm  | 19.9 | 2009 | D28A6 |
| HP           | HWP4218 |              | 1600x900  | 440x250mm  | 19.9 | 2009 | F0B16 |
| HP           | HWP4219 |              | 1600x900  | 440x250mm  | 19.9 | 2009 | BDC08 |
| HP           | HWP421A | LM230WF3-... | 1920x1080 | 500x280mm  | 22.6 | 2013 | 4E848 |
| HP           | HWP421A | LM215WF3-... | 1920x1080 | 470x260mm  | 21.1 | 2013 | E8479 |
| HP           | HWP421A | All-in-One   | 1600x900  | 440x250mm  | 19.9 | 2012 | 9187E |
| HP           | HWP4224 |              | 1920x1080 | 510x290mm  | 23.1 | 2015 | CAA0B |
| HP           | HWP4224 | SDC          | 1920x1080 | 540x340mm  | 25.1 | 2014 | 7C7CF |
| HP           | HWP422A | All-in-One   | 1920x1080 | 510x290mm  | 23.1 | 2013 | 2060E |
| HP           | HWP422A | All-in-One   | 1920x1080 | 510x290mm  | 23.1 | 2012 | 0A17F |
| HP           | HWP4244 |              | 1600x900  | 430x240mm  | 19.4 | 2015 | E5125 |
| HP           | HWP4244 | All-in-One   | 1600x900  | 430x240mm  | 19.4 | 2013 | E9D6B |
| HP           | HWP4245 |              | 1600x900  | 430x240mm  | 19.4 | 2015 | D4E14 |
| HP           | HWP4245 | All-in-One   | 1600x900  | 430x240mm  | 19.4 | 2013 | 23E77 |
| HP           | HWP424E | All-in-One   | 1920x1080 | 480x270mm  | 21.7 | 2015 | C33B3 |
| HP           | HWP4253 | All-in-One   | 1920x1080 | 510x290mm  | 23.1 | 2014 | 77867 |
| HP           | HWP4255 |              | 1920x1080 | 530x300mm  | 24.0 | 2016 | 45A7F |
| HP           | HWP4255 |              | 1920x1080 | 530x300mm  | 24.0 | 2015 | 6373C |
| HP           | HWP4256 |              | 1920x1080 | 530x300mm  | 24.0 | 2017 | 6D82B |
| HP           | HWP425D |              | 1920x1080 | 530x300mm  | 24.0 | 2016 | 33444 |
| HVR          | HVRAA01 | HTC-VIVE     | 2160x1200 | 122x68mm   | 5.5  |      | 60868 |
| Haier        | HAI17FC | LED39C800F   | 1920x1080 | 1150x650mm | 52.0 | 2012 | 02E99 |
| Haier        | HAR2206 | LE22Z6       | 1920x1080 | 480x270mm  | 21.7 | 2010 | D057A |
| Haier        | HAR221C | LT22M1CW     | 1360x768  | 470x290mm  | 21.7 | 2009 | 32108 |
| Haier        | HAR3210 | LT32A1       | 1360x768  | 700x390mm  | 31.5 | 2010 | A4F38 |
| Haier        | HRE0030 |              | 1920x1080 | 1150x650mm | 52.0 | 2014 | 62694 |
| HannStar     | HSD0000 | HP247        | 1920x1080 | 520x290mm  | 23.4 | 2017 | 5DEDC |
| HannStar     | HSD0000 | HP195DCB     | 1366x768  | 410x230mm  | 18.5 | 2013 | B5ADF |
| HannStar     | HSD0013 |              | 1280x1024 | 380x300mm  | 19.1 | 2007 | 2438F |
| HannStar     | HSD00BF |              | 1280x1024 | 380x300mm  | 19.1 | 2007 | F8C8F |
| HannStar     | HSD00C2 |              | 1280x1024 | 380x300mm  | 19.1 | 2007 | E1B17 |
| HannStar     | HSD0325 |              | 1024x600  | 200x110mm  | 9.0  | 2012 | B1D1C |
| HannStar     | HSD03E9 | HSD100IFW1   | 1024x600  | 220x130mm  | 10.1 | 2012 | 576DA |
| HannStar     | HSD03E9 | HSD101PFW2   | 1024x600  | 220x130mm  | 10.1 | 2012 | 9F1B0 |
| HannStar     | HSD03E9 | HSD101PFW2   | 1024x600  | 220x130mm  | 10.1 | 2011 | 1101F |
| HannStar     | HSD03E9 | HSD100IFW1   | 1024x600  | 220x130mm  | 10.1 | 2011 | 1B38E |
| HannStar     | HSD03E9 |              | 1024x600  | 220x130mm  | 10.1 | 2011 | 1D5FA |
| HannStar     | HSD03E9 |              | 1024x600  | 220x130mm  | 10.1 | 2010 | 07969 |
| HannStar     | HSD03E9 | HSD101PFW2   | 1024x600  | 220x130mm  | 10.1 | 2010 | 08CD6 |
| HannStar     | HSD03E9 | HSD100IFW1   | 1024x600  | 220x130mm  | 10.1 | 2010 | 10551 |
| HannStar     | HSD03E9 |              | 1024x600  | 220x130mm  | 10.1 | 2009 | 0A48F |
| HannStar     | HSD03E9 | HSD100IFW1   | 1024x600  | 220x130mm  | 10.1 | 2009 | 27FF0 |
| HannStar     | HSD03E9 | HSD101PFW2   | 1024x600  | 220x130mm  | 10.1 | 2009 | 35DAB |
| HannStar     | HSD03E9 |              | 1024x600  | 220x130mm  | 10.1 | 2008 | 24683 |
| HannStar     | HSD03E9 | HSD100IFW1   | 1024x600  | 220x130mm  | 10.1 | 2008 | D9154 |
| HannStar     | HSD03EA | HSD101PFW3D  | 1024x600  | 220x130mm  | 10.1 | 2011 | 9017F |
| HannStar     | HSD03EC | HSD101PWW1A  | 1280x800  | 220x140mm  | 10.3 | 2012 | 832E8 |
| HannStar     | HSD03EC | HSD101PWW1A  | 1280x800  | 220x140mm  | 10.3 | 2011 | 32932 |
| HannStar     | HSD03ED | HSD101PFW4A  | 1024x600  | 220x130mm  | 10.1 | 2012 | 1B3BE |
| HannStar     | HSD03ED | HSD101PFW4B  | 1024x600  | 220x130mm  | 10.1 | 2012 | 5C3B5 |
| HannStar     | HSD03ED | HSD101PFW4A  | 1024x600  | 220x130mm  | 10.1 | 2011 | 0770A |
| HannStar     | HSD03ED | HSD101PFW4A  | 1024x600  | 220x130mm  | 10.1 | 2008 | CF321 |
| HannStar     | HSD03EE | HSD100IFW4A  | 1024x600  | 220x130mm  | 10.1 | 2013 | 49EA4 |
| HannStar     | HSD03EE | HSD100IFW4A  | 1024x600  | 220x130mm  | 10.1 | 2012 | 973B9 |
| HannStar     | HSD03EE | HSD100IFW4A  | 1024x600  | 220x130mm  | 10.1 | 2011 | 100E7 |
| HannStar     | HSD04B6 | HSD121PHW1   | 1366x768  | 270x150mm  | 12.2 | 2011 | 4D4FC |
| HannStar     | HSD04B6 | HSD121PHW1   | 1366x768  | 270x150mm  | 12.2 | 2010 | 064D0 |
| HannStar     | HSD04B6 | HSD121PHW1   | 1366x768  | 270x150mm  | 12.2 | 2009 | 3248C |
| HannStar     | HSD0583 | HSD140PHW1   | 1366x768  | 310x170mm  | 13.9 | 2010 | 42DAD |
| HannStar     | HSD0583 | HSD140PHW1   | 1366x768  | 310x170mm  | 13.9 | 2009 | 03823 |
| HannStar     | HSD05D3 | HSD140PHW1   | 1366x768  | 310x170mm  | 13.9 | 2010 | BADFF |
| HannStar     | HSD0640 | HSD160PHW1   | 1366x768  | 350x200mm  | 15.9 | 2010 | 76C1C |
| HannStar     | HSD0640 | HSD160PHW1   | 1366x768  | 350x200mm  | 15.9 | 2009 | 176EE |
| HannStar     | HSD0640 | HSD160PHW1   | 1366x768  | 350x200mm  | 15.9 | 2006 | 0C04E |
| HannStar     | HSD06A5 | HSD173PUW1   | 1920x1080 | 390x230mm  | 17.8 | 2011 | 6093A |
| HannStar     | HSD06A5 | HSD173PUW1   | 1920x1080 | 390x230mm  | 17.8 | 2010 | 37142 |
| HannStar     | HSD06A5 | HSD173PUW1   | 1920x1080 | 390x230mm  | 17.8 | 2009 | 5C935 |
| HannStar     | HSD08C1 | HB191        | 1440x900  | 410x260mm  | 19.1 | 2007 | B09D3 |
| HannStar     | HSD0CC6 | JC198D       | 1280x1024 | 380x300mm  | 19.1 | 2006 | F77E5 |
| HannStar     | HSD1512 |              | 1024x768  | 320x240mm  | 15.7 |      | C316A |
| HannStar     | HSD1531 | HSD150PK14-B | 1400x1050 | 300x230mm  | 14.9 |      | 9C381 |
| HannStar     | HSD15C6 | HX191D       | 1280x1024 | 380x300mm  | 19.1 | 2007 | 8F9DA |
| HannStar     | HSD1940 | HC194D       | 1280x1024 | 380x300mm  | 19.1 | 2006 | B16A1 |
| HannStar     | HSD1A41 | HF257        | 1920x1080 | 540x300mm  | 24.3 | 2009 | 1C3A5 |
| HannStar     | HSD1AC3 | HF237        | 1920x1080 | 510x290mm  | 23.1 | 2008 | 1C269 |
| HannStar     | HSD1CA3 | HG216        | 1680x1050 | 470x290mm  | 21.7 | 2008 | FE3E1 |
| HannStar     | HSD208B |              | 1920x1080 | 480x270mm  | 21.7 | 2010 | E021C |
| HannStar     | HSD20E5 | HH281        | 1920x1200 | 590x370mm  | 27.4 | 2009 | B7E72 |
| HannStar     | HSD2211 | HH251        | 1920x1080 | 540x300mm  | 24.3 | 2010 | C40EF |
| HannStar     | HSD2214 | iH252        | 1920x1080 | 540x300mm  | 24.3 | 2009 | B1E32 |
| HannStar     | HSD2469 |              | 1680x1050 | 470x300mm  | 22.0 | 2009 | 1792F |
| HannStar     | HSD47D8 | HE225DPB     | 1920x1080 | 480x270mm  | 21.7 | 2012 | 8560D |
| HannStar     | HSD49F3 | HT225HPB     | 1920x1080 | 480x270mm  | 21.7 | 2016 | 3AFD3 |
| HannStar     | HSD5173 | HT231        | 1920x1080 | 510x280mm  | 22.9 | 2016 | F7792 |
| HannStar     | HSD5173 | HT231        | 1920x1080 | 510x280mm  | 22.9 | 2013 | 304BB |
| HannStar     | HSD6311 | HL225D       | 1920x1080 | 480x270mm  | 21.7 | 2010 | 8DBFD |
| HannStar     | HSD6325 | HL229DPB     | 1920x1080 | 480x270mm  | 21.7 | 2011 | 78D18 |
| HannStar     | HSD6505 | HL249DPB     | 1920x1080 | 520x290mm  | 23.4 | 2011 | F2B50 |
| HannStar     | HSD6537 | HL272HPB     | 1920x1080 | 590x330mm  | 26.6 | 2011 | 951EB |
| HannStar     | HSD66C7 | HZ221D       | 1680x1050 | 470x300mm  | 22.0 | 2010 | 092A6 |
| HannStar     | HSD6735 | HZ281H       | 1920x1200 | 590x370mm  | 27.4 | 2011 | 5026B |
| HannStar     | HSD8991 | HW191D       | 1440x900  | 410x260mm  | 19.1 | 2007 | 45A8F |
| HannStar     | HSD899A | HU196D       | 1280x1024 | 380x300mm  | 19.1 | 2006 | 987C6 |
| HannStar     | HSP001C | HSG1141      | 1920x1080 | 590x370mm  | 27.4 | 2010 | 51E54 |
| HannStar     | HSP001C | HSG1075      | 1920x1200 | 590x370mm  | 27.4 | 2010 | 56E89 |
| HannStar     | HSP0226 | U3 26        | 1360x768  | 630x370mm  | 28.8 | 2007 | 7B958 |
| Hitachi      | HEC0000 | 55R6+        | 3840x2160 | 1220x680mm | 55.0 | 2018 | 06E69 |
| Hitachi      | HEC0029 | HDMI         | 1920x1080 | 1150x650mm | 52.0 | 2014 | 5CFE0 |
| Hitachi      | HEC002F | HISENSE      | 3840x2160 | 1150x650mm | 52.0 | 2015 | 0291D |
| Hitachi      | HEC0030 | HDMI         | 1920x1080 | 1150x650mm | 52.0 | 2015 | 5CA71 |
| Hitachi      | HEC0030 | HDMI         | 1920x1080 | 1150x650mm | 52.0 | 2010 | 07E93 |
| Hitachi      | HEC0088 | HDMI         | 1920x540  | 1100x560mm | 48.6 | 2013 | 8D1F4 |
| Hitachi      | HEC0088 | HDMI         | 1920x540  | 1100x560mm | 48.6 | 2008 | 78686 |
| Hitachi      | HEC3206 | LCD3206NEU   | 1280x1024 | 820x460mm  | 37.0 | 2006 | FD607 |
| Hitachi      | HEC3233 | LCD3233NEU   | 1280x1024 | 820x460mm  | 37.0 | 2006 | 65E09 |
| Hitachi      | HIT6D0D | N91W DVI     | 1440x900  | 410x260mm  | 19.1 | 2007 | 8B528 |
| Hitachi      | HIT6D10 | X91D DVI     | 1280x1024 | 380x300mm  | 19.1 | 2007 | F3783 |
| Hitachi      | HTC0087 | FPD          | 1920x540  |            |      | 2007 | 84F65 |
| Hitachi      | HTC010B | LE55G508     | 1440x900  |            |      | 2014 | E84AF |
| Hitachi      | HTC1C6D | TX39D99VC1FA | 1680x1050 | 330x210mm  | 15.4 |      | 86F0C |
| Hitachi      | HTC6606 | PROJECTOR    | 1600x1200 |            |      | 2006 | B978F |
| Hyundai I... | HIQ5003 | L72D DVI     | 1280x1024 | 330x270mm  | 16.8 |      | C2170 |
| Hyundai I... | IQT90DD | L90D Digital | 1280x1024 | 370x300mm  | 18.8 |      | 7B5A9 |
| Hyundai I... | IQT9D0A | HDIT 24W DVI | 1920x1080 | 520x300mm  | 23.6 | 2011 | CB596 |
| IBM          | IBM2373 | LTN154X3-L04 | 1280x800  | 330x210mm  | 15.4 |      | D554C |
| IBM          | IBM2374 | N154I1       | 1280x800  | 330x210mm  | 15.4 |      | D8C00 |
| IBM          | IBM2887 | LP154W02-... | 1680x1050 | 330x210mm  | 15.4 |      | B080A |
| IBM          | IBM2887 | LTN154P2-L05 | 1680x1050 | 330x210mm  | 15.4 |      | C5CBF |
| IBM          | IBM4160 |              | 1600x1200 | 410x310mm  | 20.2 |      | 316D8 |
| IBM          | IBM686E | L170p        | 1280x1024 | 340x270mm  | 17.1 |      | 82BD3 |
| Iiyama       | IVM0007 | PL4071UH     | 3840x2160 | 880x490mm  | 39.7 | 2016 | D4050 |
| Iiyama       | IVM0009 | PL4071UH     | 3840x2160 | 880x490mm  | 39.7 | 2015 | C2209 |
| Iiyama       | IVM46E4 | HDMI         | 1280x1024 | 330x270mm  | 16.8 | 2007 | 18390 |
| Iiyama       | IVM4829 | PLE483       | 1280x1024 | 380x300mm  | 19.1 |      | A470E |
| Iiyama       | IVM4832 | PL1902       | 1280x1024 | 380x300mm  | 19.1 | 2007 | 6DD55 |
| Iiyama       | IVM483D | PL1908W      | 1680x1050 | 410x260mm  | 19.1 | 2010 | 13B1C |
| Iiyama       | IVM5351 | PLH510       | 1600x1200 | 410x310mm  | 20.2 |      | 2FEC5 |
| Iiyama       | IVM5398 | PLE2003WS    | 1680x1050 | 430x270mm  | 20.0 | 2008 | ECBCB |
| Iiyama       | IVM53A1 | PL2083H      | 1600x900  | 430x240mm  | 19.4 | 2015 | ADDD9 |
| Iiyama       | IVM5601 | PLB2403WS    | 1920x1200 | 520x330mm  | 24.2 | 2008 | 8A053 |
| Iiyama       | IVM5601 | PLB2403WS    | 1920x1200 | 520x330mm  | 24.2 | 2007 | 1C9F6 |
| Iiyama       | IVM5602 | PL2201W      | 1680x1050 | 490x320mm  | 23.0 | 2007 | 93B74 |
| Iiyama       | IVM5604 | PLE2403WS    | 1920x1200 | 520x330mm  | 24.2 | 2008 | 8F458 |
| Iiyama       | IVM5608 | PLE2607WS    | 1920x1200 | 550x340mm  | 25.5 | 2008 | 19482 |
| Iiyama       | IVM560C | PL2409HD     | 1920x1080 | 520x290mm  | 23.4 | 2012 | 20E4C |
| Iiyama       | IVM560C | PL2409HD     | 1920x1080 | 520x290mm  | 23.4 | 2011 | 61060 |
| Iiyama       | IVM560C | PL2409HD     | 1920x1080 | 520x290mm  | 23.4 | 2010 | 30BEA |
| Iiyama       | IVM560C | PL2409HD     | 1920x1080 | 520x290mm  | 23.4 | 2009 | 50394 |
| Iiyama       | IVM560D | PLE2407HDS   | 1920x1080 | 520x300mm  | 23.6 | 2012 | 238F4 |
| Iiyama       | IVM560D | PLE2407HDS   | 1920x1080 | 520x300mm  | 23.6 | 2010 | 05EAF |
| Iiyama       | IVM560D | PLE2407HDS   | 1920x1080 | 520x300mm  | 23.6 | 2009 | 296D4 |
| Iiyama       | IVM560F | PL2206W      | 1680x1050 | 480x300mm  | 22.3 | 2012 | 18FB7 |
| Iiyama       | IVM5613 | PLT2250MTS   | 1920x1080 | 480x270mm  | 21.7 | 2011 | 40ED7 |
| Iiyama       | IVM5613 | PLT2250MTS   | 1920x1080 | 480x270mm  | 21.7 | 2009 | 66A80 |
| Iiyama       | IVM5616 | PLE2208HDD   | 1920x1080 | 480x270mm  | 21.7 | 2011 | 77767 |
| Iiyama       | IVM5617 | PL2271HD     | 1920x1080 | 480x270mm  | 21.7 | 2011 | 9823B |
| Iiyama       | IVM5617 | PL2271HD     | 1920x1080 | 480x270mm  | 21.7 | 2010 | 3E6C2 |
| Iiyama       | IVM561A | PL2273HD     | 1920x1080 | 480x270mm  | 21.7 | 2014 | 6388E |
| Iiyama       | IVM561D | PL2377       | 1920x1080 | 510x290mm  | 23.1 | 2012 | 43546 |
| Iiyama       | IVM561D | PL2377       | 1920x1080 | 510x290mm  | 23.1 | 2011 | 0F105 |
| Iiyama       | IVM561F | PL2280W      | 1680x1050 | 470x300mm  | 22.0 | 2014 | 3B86F |
| Iiyama       | IVM5620 | PL2280H      | 1920x1080 | 480x270mm  | 21.7 | 2012 | 9F70F |
| Iiyama       | IVM5621 | PLX2380H     | 1920x1080 | 510x290mm  | 23.1 | 2013 | 3F729 |
| Iiyama       | IVM5621 | PLX2380H     | 1920x1080 | 510x290mm  | 23.1 | 2012 | D61C1 |
| Iiyama       | IVM5624 | PL2278H      | 1920x1080 | 480x270mm  | 21.7 | 2014 | 62E48 |
| Iiyama       | IVM5624 | PL2278H      | 1920x1080 | 480x270mm  | 21.7 | 2012 | 2CD66 |
| Iiyama       | IVM562D | PL2390       | 1920x1080 | 510x290mm  | 23.1 | 2016 | 4CFDE |
| Iiyama       | IVM562D | PL2390       | 1920x1080 | 510x290mm  | 23.1 | 2015 | 18882 |
| Iiyama       | IVM562D | PL2390       | 1920x1080 | 510x290mm  | 23.1 | 2014 | 593AD |
| Iiyama       | IVM5631 | PLX2283H-DP  | 1920x1080 | 480x270mm  | 21.7 | 2016 | AA40E |
| Iiyama       | IVM6106 | PLX2472HD    | 1920x1080 | 530x300mm  | 24.0 | 2011 | AC14D |
| Iiyama       | IVM6107 | PL2473HD     | 1920x1080 | 520x290mm  | 23.4 | 2012 | E4149 |
| Iiyama       | IVM610B | PL2480H      | 1920x1080 | 520x290mm  | 23.4 | 2016 | 0F4F5 |
| Iiyama       | IVM610B | PL2480H      | 1920x1080 | 520x290mm  | 23.4 | 2012 | 182E0 |
| Iiyama       | IVM610E | PL2481H      | 1920x1080 | 520x290mm  | 23.4 | 2012 | 70D64 |
| Iiyama       | IVM610F | X2485        | 1920x1200 | 520x320mm  | 24.0 | 2015 | 56DEE |
| Iiyama       | IVM610F | X2485        | 1920x1200 | 520x320mm  | 24.0 | 2012 | F55BC |
| Iiyama       | IVM6113 | PLE2483H     | 1920x1080 | 530x300mm  | 24.0 | 2017 | 87B2B |
| Iiyama       | IVM6113 | PLE2483H     | 1920x1080 | 530x300mm  | 24.0 | 2015 | 52DA6 |
| Iiyama       | IVM6114 | PLX2483H     | 1920x1080 | 530x300mm  | 24.0 | 2014 | 08283 |
| Iiyama       | IVM611A | PL2488H      | 1920x1080 | 530x300mm  | 24.0 | 2016 | 192EE |
| Iiyama       | IVM611D | PLX2481H     | 1920x1080 | 520x290mm  | 23.4 | 2015 | 22054 |
| Iiyama       | IVM611F | PL2490       | 1920x1080 | 530x300mm  | 24.0 | 2016 | C1BC8 |
| Iiyama       | IVM6120 | PL2590       | 1920x1080 | 550x310mm  | 24.9 | 2016 | 002CF |
| Iiyama       | IVM6121 | PLE2482H     | 1920x1080 | 530x300mm  | 24.0 | 2016 | FC4A5 |
| Iiyama       | IVM6122 | X2485        | 1920x1200 | 520x320mm  | 24.0 | 2016 | 37F7E |
| Iiyama       | IVM6128 | X2483_2481   | 1920x1080 | 530x300mm  | 24.0 | 2016 | 9C1DE |
| Iiyama       | IVM612F | PL2492H      | 1920x1080 | 530x300mm  | 24.0 | 2018 | 76F08 |
| Iiyama       | IVM612F | PL2492H      | 1920x1080 | 530x300mm  | 24.0 | 2017 | 7F343 |
| Iiyama       | IVM612F | PL2492H      | 1920x1080 | 530x300mm  | 24.0 | 2016 | 6201D |
| Iiyama       | IVM6132 | PL2530H      | 1920x1080 | 540x300mm  | 24.3 | 2018 | 04B89 |
| Iiyama       | IVM6132 | PL2530H      | 1920x1080 | 540x300mm  | 24.3 | 2017 | 3683D |
| Iiyama       | IVM6145 | PL2493H      | 1920x1080 | 530x300mm  | 24.0 | 2018 | 6F868 |
| Iiyama       | IVM6602 | PLB2712HDS   | 1920x1080 | 600x340mm  | 27.2 | 2011 | 98ECE |
| Iiyama       | IVM6602 | PLB2712HDS   | 1920x1080 | 600x340mm  | 27.2 | 2010 | D7AE9 |
| Iiyama       | IVM6605 | PL2776HD     | 1920x1080 | 600x340mm  | 27.2 | 2012 | CD997 |
| Iiyama       | IVM6606 | PL2773HD     | 1920x1080 | 600x340mm  | 27.2 | 2012 | 23F77 |
| Iiyama       | IVM6608 | PLX2780H     | 1920x1080 | 600x340mm  | 27.2 | 2013 | 0ECE6 |
| Iiyama       | IVM6608 | PLX2780H     | 1920x1080 | 600x340mm  | 27.2 | 2012 | 0E3AB |
| Iiyama       | IVM660C | XB2776QS-B1  | 2560x1440 | 600x340mm  | 27.2 | 2013 | 5FE1F |
| Iiyama       | IVM660F | PL2735M      | 1920x1080 | 600x340mm  | 27.2 | 2014 | 354E6 |
| Iiyama       | IVM6611 | PLX2783H     | 1920x1080 | 600x340mm  | 27.2 | 2015 | 2F34A |
| Iiyama       | IVM6611 | PLX2783H     | 1920x1080 | 600x340mm  | 27.2 | 2014 | E2381 |
| Iiyama       | IVM6611 | PLX2783H     | 1920x1080 | 600x340mm  | 27.2 | 2013 | E87D1 |
| Iiyama       | IVM6615 | PL2779Q      | 2560x1440 | 600x340mm  | 27.2 |      | 288CD |
| Iiyama       | IVM6616 | PL2790       | 1920x1080 | 600x340mm  | 27.2 | 2016 | E6622 |
| Iiyama       | IVM661D | PL2783Q      | 2560x1440 | 600x340mm  | 27.2 | 2016 | D0BCC |
| Iiyama       | IVM6628 | PL2788H      | 1920x1080 | 600x340mm  | 27.2 | 2016 | 79F9B |
| Iiyama       | IVM662E | PLTF2738     | 1920x1080 | 600x340mm  | 27.2 | 2017 | DFC8E |
| Iiyama       | IVM662F | PL2792Q      | 2560x1440 | 600x340mm  | 27.2 | 2019 | 41099 |
| Iiyama       | IVM6635 | PL2788Q      | 2560x1440 | 600x340mm  | 27.2 | 2017 | DDDF6 |
| Iiyama       | IVM6637 | PL2792Q      | 2560x1440 | 600x340mm  | 27.2 | 2017 | 2C8C4 |
| Iiyama       | IVM6644 | PL2730Q      | 2560x1440 | 600x340mm  | 27.2 | 2018 | 4E9BB |
| Iiyama       | IVM6648 | PLX2783H     | 1920x1080 | 600x340mm  | 27.2 | 2017 | B754B |
| Iiyama       | IVM7105 | PL2888UH     | 3840x2160 | 620x340mm  | 27.8 |      | 7EA11 |
| Iiyama       | IVM7106 | PL2888H      | 1920x1080 | 620x340mm  | 27.8 | 2017 | 2C867 |
| InfoVision   | IVO03F4 | STARRY XR... | 1920x1200 | 260x160mm  | 12.0 | 2017 | 9F024 |
| InfoVision   | IVO03F4 | STARRY KR... | 1366x768  | 250x140mm  | 11.3 | 2015 | A0B8E |
| InfoVision   | IVO03F4 | M101NWT2 R3  | 1024x600  | 220x130mm  | 10.1 | 2011 | 77E36 |
| InfoVision   | IVO03F4 | M101NWT2 ... | 1024x600  | 220x130mm  | 10.1 | 2011 | EC8C7 |
| InfoVision   | IVO03F4 | M101NWT2 R0  | 1024x600  | 220x130mm  | 10.1 | 2010 | 0989A |
| InfoVision   | IVO03F4 | M101NWT2 R1  | 1024x600  | 220x130mm  | 10.1 | 2010 | 625AD |
| InfoVision   | IVO03F4 | M101NWT2 R2  | 1024x600  | 220x130mm  | 10.1 | 2010 | C5265 |
| InfoVision   | IVO03FA | M101NWN8 R0  | 1366x768  | 220x130mm  | 10.1 | 2013 | 3F89E |
| InfoVision   | IVO0489 | M116NWR1 R3  | 1366x768  | 260x140mm  | 11.6 | 2011 | E64A1 |
| InfoVision   | IVO0489 | M116NWR1 R4  | 1366x768  | 260x140mm  | 11.6 | 2011 | F85A6 |
| InfoVision   | IVO0489 | M116NWR1 R0  | 1366x768  | 260x140mm  | 11.6 | 2010 | 671E5 |
| InfoVision   | IVO048C | M116NWR4 R1  | 1366x768  | 260x140mm  | 11.6 | 2015 | 231EB |
| InfoVision   | IVO048E | M116NWR6 R1  | 1366x768  | 260x140mm  | 11.6 | 2015 | A1B42 |
| InfoVision   | IVO04E3 | M125NWN1 R0  | 1366x768  | 280x160mm  | 12.7 | 2012 | 91BE7 |
| InfoVision   | IVO04E5 | M125NWR3 R0  | 1366x768  | 280x160mm  | 12.7 | 2014 | FF2B3 |
| InfoVision   | IVO04E6 | M125NWF4 R0  | 1920x1080 | 280x160mm  | 12.7 | 2017 | D1B16 |
| InfoVision   | IVO0533 | M133NWN1 R4  | 1366x768  | 290x170mm  | 13.2 | 2014 | 853C2 |
| InfoVision   | IVO0533 | M133NWN1 R0  | 1366x768  | 290x160mm  | 13.0 | 2012 | 62173 |
| InfoVision   | IVO0533 | M133NWN1 R3  | 1366x768  | 290x170mm  | 13.2 | 2012 | 7A91F |
| InfoVision   | IVO0533 | M133NWN1 R1  | 1366x768  | 290x160mm  | 13.0 | 2012 | F857B |
| InfoVision   | IVO0535 | M133NVF3 R0  | 1920x1080 | 290x170mm  | 13.2 | 2017 | F5820 |
| InfoVision   | IVO0579 | M140NWR1 R0  | 1366x768  | 310x170mm  | 13.9 | 2009 | 33392 |
| InfoVision   | IVO057A | M140NWR2 R1  | 1366x768  | 310x170mm  | 13.9 | 2015 | A6A3E |
| InfoVision   | IVO057A | M140NWR2 R0  | 1366x768  | 310x170mm  | 13.9 | 2010 | 6D597 |
| InfoVision   | IVO057A | M140NWR2 R1  | 1366x768  | 310x170mm  | 13.9 | 2010 | BA3D5 |
| InfoVision   | IVO057C | M140NWR4 R2  | 1366x768  | 310x170mm  | 13.9 | 2013 | 3B667 |
| InfoVision   | IVO057C | M140NWR4 R1  | 1366x768  | 310x170mm  | 13.9 | 2013 | 455D7 |
| InfoVision   | IVO057D | R140NWF5 R6  | 1920x1080 | 310x170mm  | 13.9 | 2017 | 47248 |
| InfoVision   | IVO057D | M140NWF5 R2  | 1920x1080 | 310x170mm  | 13.9 | 2016 | 3E832 |
| InfoVision   | IVO057F | M140NVF7 R2  | 1920x1080 | 310x170mm  | 13.9 | 2017 | 608C2 |
| InfoVision   | IVO0619 | M156NWR1 R0  | 1366x768  | 340x190mm  | 15.3 | 2008 | 84BDC |
| InfoVision   | IVO061C | M156NVF4 R0  | 1920x1080 | 340x190mm  | 15.3 | 2017 | A7413 |
| InnoLux D... | CMI001B | BT140GW01V9  | 1366x768  | 310x180mm  | 14.1 | 2010 | 99D0B |
| InnoLux D... | CMI0023 | BT140GW01VA  | 1366x768  | 310x180mm  | 14.1 | 2010 | A357A |
| InnoLux D... | INL0001 | U884M        | 1366x768  | 340x190mm  | 15.3 | 2009 | B7455 |
| InnoLux D... | INL0003 |              | 1024x600  | 220x120mm  | 9.9  | 2009 | 80BFB |
| InnoLux D... | INL0005 |              | 1366x768  | 340x190mm  | 15.3 | 2009 | 95ACE |
| InnoLux D... | INL0006 | W466R        | 1366x768  | 340x190mm  | 15.3 | 2009 | 8A149 |
| InnoLux D... | INL0007 | INLDisplay   | 1366x768  | 340x190mm  | 15.3 | 2009 | F6A03 |
| InnoLux D... | INL000A | 1G5D3        | 1366x768  | 340x190mm  | 15.3 | 2009 | 4385C |
| InnoLux D... | INL000D | BT101IW03V1  | 1024x600  | 220x120mm  | 9.9  | 2009 | 28C70 |
| InnoLux D... | INL0011 | INLDisplay   | 1024x600  | 220x120mm  | 9.9  | 2009 | 536FA |
| InnoLux D... | INL0013 | 19JTP        | 1366x768  | 310x170mm  | 13.9 | 2010 | 5BAE7 |
| InnoLux D... | INL0014 | HR1VT        | 1366x768  | 310x180mm  | 14.1 | 2010 | ACC90 |
| InnoLux D... | INL0015 | BT140GW01V5  | 1366x768  | 310x180mm  | 14.1 | 2010 | 13AD3 |
| InnoLux D... | INL0017 | BT140GW03V2  | 1366x768  | 310x180mm  | 14.1 | 2010 | 5AE65 |
| InnoLux D... | INL0028 | BT140GW02V5  | 1366x768  | 310x180mm  | 14.1 | 2010 | 7FD5C |
| Insignia     | BBY0032 | NS-19E320A13 | 1680x1050 | 410x230mm  | 18.5 | 2012 | FBA55 |
| Insignia     | BBY0032 | NS-15E720A12 | 1360x768  | 360x210mm  | 16.4 | 2011 | 33489 |
| Insignia     | BBY0050 |              | 1360x768  | 700x390mm  | 31.5 | 2015 | 92BC9 |
| Insignia     | BBY1511 | DX-15L150A11 | 1360x768  | 340x190mm  | 15.3 | 2010 | 49AAB |
| Insignia     | BBY3533 |              | 1680x1050 | 850x480mm  | 38.4 | 2013 | A01DE |
| Insignia     | BBY3942 |              | 1920x1080 | 850x480mm  | 38.4 | 2013 | 4327C |
| Insignia     | BBY4000 | HDMI         | 1920x1080 | 1150x650mm | 52.0 | 2010 | B6A4A |
| JDI          | JDI422A | LPM139M422A  | 3000x2000 | 290x200mm  | 13.9 | 2017 | F78B9 |
| JVC          | AMR1007 | EM32FL       | 1920x1080 | 700x390mm  | 31.5 | 2014 | 4525D |
| JVC          | JVC21BE | FPDEUFT3     | 1920x540  | 220x90mm   | 9.4  | 2006 | 5CE9E |
| Jean         | JEN1003 | GM2200       | 1680x1050 | 470x300mm  | 22.0 | 2008 | 30E73 |
| KTC          | KTC2400 | 24'TV        | 1360x768  | 520x290mm  | 23.4 | 2016 | 8B43F |
| KTC          | KTC4200 | 42 TV        | 1920x1080 | 980x580mm  | 44.8 | 2012 | 9619F |
| KTC          | KTC4300 | 43'TV        | 1920x1080 | 950x540mm  | 43.0 | 2016 | 412B1 |
| Konka        | KOA0030 | TV MONIOR    | 1920x540  | 1150x650mm | 52.0 | 2008 | 6D9B2 |
| LG Display   | LGD0000 | F5KCX        | 1366x768  | 260x140mm  | 11.6 | 2013 | 47507 |
| LG Display   | LGD0000 | LP116WH6-... | 1366x768  | 260x140mm  | 11.6 | 2013 | 63829 |
| LG Display   | LGD0000 |              | 2560x1440 | 290x170mm  | 13.2 | 2013 | 97453 |
| LG Display   | LGD0000 | DH091        | 1920x1080 | 340x190mm  | 15.3 | 2009 | 80702 |
| LG Display   | LGD0000 |              | 1024x576  | 220x130mm  | 10.1 | 2008 | 8A00B |
| LG Display   | LGD0164 | LP141WX5-... | 1280x800  | 300x190mm  | 14.0 | 2008 | B3536 |
| LG Display   | LGD016D | F253H        | 1280x800  | 330x210mm  | 15.4 | 2008 | 89E57 |
| LG Display   | LGD017F | LP089WS1-... | 1024x600  | 190x110mm  | 8.6  | 2008 | 85290 |
| LG Display   | LGD018B | LP140WH1-... | 1366x768  | 310x170mm  | 13.9 | 2009 | 39FC2 |
| LG Display   | LGD018B | LP140WH1-... | 1366x768  | 310x170mm  | 13.9 | 2008 | 4181F |
| LG Display   | LGD018B | LP140WH1-... | 1366x768  | 310x170mm  | 13.9 | 2008 | B2FF8 |
| LG Display   | LGD018B | X976H        | 1366x768  | 310x170mm  | 13.9 | 2008 | F64CC |
| LG Display   | LGD0191 |              | 1280x800  | 300x190mm  | 14.0 | 2008 | DE001 |
| LG Display   | LGD01AC | LP154WX7-... | 1280x800  | 330x210mm  | 15.4 | 2008 | A22FB |
| LG Display   | LGD01B9 |              | 1024x600  | 220x130mm  | 10.1 | 2008 | CF84D |
| LG Display   | LGD01BC | LP133WH1-... | 1366x768  | 290x170mm  | 13.2 | 2009 | 48EC0 |
| LG Display   | LGD01C2 |              | 1366x768  | 340x190mm  | 15.3 | 2008 | 07273 |
| LG Display   | LGD01CA | LP173WD1-... | 1600x900  | 380x210mm  | 17.1 | 2008 | 6BCA7 |
| LG Display   | LGD01DA | LP133WH1-... | 1366x768  | 290x170mm  | 13.2 | 2009 | 5AAF7 |
| LG Display   | LGD01DD | LP173WD1-... | 1600x900  | 380x210mm  | 17.1 | 2008 | 1101B |
| LG Display   | LGD01DF |              | 1366x768  | 340x190mm  | 15.3 | 2008 | CDEE3 |
| LG Display   | LGD01E0 |              | 1366x768  | 340x190mm  | 15.3 | 2008 | 65B77 |
| LG Display   | LGD01E1 | LP140WH1-... | 1366x768  | 310x170mm  | 13.9 | 2008 | D85E7 |
| LG Display   | LGD01E6 | LP140WH1-... | 1366x768  | 310x170mm  | 13.9 | 2008 | B3069 |
| LG Display   | LGD01E8 | LP156WH2-... | 1366x768  | 340x190mm  | 15.3 | 2008 | AD1EF |
| LG Display   | LGD01E9 | LP156WF1-... | 1920x1080 | 350x190mm  | 15.7 | 2009 | 68B85 |
| LG Display   | LGD01F4 | F253H        | 1280x800  | 330x210mm  | 15.4 | 2009 | 33C7C |
| LG Display   | LGD01F5 | G022H        | 1280x800  | 300x190mm  | 14.0 | 2009 | F090D |
| LG Display   | LGD01F6 | LP154WX4-... | 1280x800  | 330x210mm  | 15.4 | 2009 | 69421 |
| LG Display   | LGD01F7 | LP133WH2-... | 1366x768  | 290x160mm  | 13.0 | 2009 | B13EC |
| LG Display   | LGD0201 |              | 1366x768  | 310x170mm  | 13.9 | 2009 | 4B044 |
| LG Display   | LGD0202 |              | 1366x768  | 350x190mm  | 15.7 | 2009 | B630F |
| LG Display   | LGD0203 |              | 1024x576  | 220x130mm  | 10.1 | 2009 | E7D40 |
| LG Display   | LGD0206 | F050T        | 1024x600  | 220x130mm  | 10.1 | 2009 | 893A5 |
| LG Display   | LGD020C | D998K        | 1600x900  | 350x190mm  | 15.7 | 2009 | B224F |
| LG Display   | LGD020C | MT6KG        | 1600x900  | 350x190mm  | 15.7 | 2009 | D7258 |
| LG Display   | LGD0210 |              | 1366x768  | 350x190mm  | 15.7 | 2009 | 1E353 |
| LG Display   | LGD0211 |              | 1280x800  | 300x190mm  | 14.0 | 2009 | F5E72 |
| LG Display   | LGD0212 |              | 1366x768  | 310x170mm  | 13.9 | 2009 | 10908 |
| LG Display   | LGD0214 | G028T        | 1600x900  | 350x190mm  | 15.7 | 2009 | 84403 |
| LG Display   | LGD0215 | C088T        | 1920x1080 | 350x190mm  | 15.7 | 2009 | BDD6C |
| LG Display   | LGD0217 | LP133WH2-... | 1366x768  | 290x160mm  | 13.0 | 2009 | 925C8 |
| LG Display   | LGD021B |              | 1366x768  | 340x190mm  | 15.3 | 2009 | 66715 |
| LG Display   | LGD021C |              | 1366x768  | 260x140mm  | 11.6 | 2009 | 9DDB3 |
| LG Display   | LGD021D | MC13K        | 1600x900  | 380x210mm  | 17.1 | 2009 | 5897B |
| LG Display   | LGD021F |              | 1366x768  | 340x190mm  | 15.3 | 2009 | F862B |
| LG Display   | LGD0220 |              | 1366x768  | 350x190mm  | 15.7 | 2009 | 4FA6A |
| LG Display   | LGD0220 |              | 1920x1080 | 350x190mm  | 15.7 | 2009 | 630A3 |
| LG Display   | LGD0221 |              | 1366x768  | 340x190mm  | 15.3 | 2009 | 2101F |
| LG Display   | LGD0222 |              | 1366x768  | 340x190mm  | 15.3 | 2009 | 6621A |
| LG Display   | LGD0226 | LP173WD1-... | 1600x900  | 380x210mm  | 17.1 | 2009 | D7415 |
| LG Display   | LGD022C | LP133WH1-... | 1366x768  | 290x170mm  | 13.2 | 2009 | 4CF8C |
| LG Display   | LGD0230 |              | 1366x768  | 340x190mm  | 15.3 | 2009 | 51C2C |
| LG Display   | LGD0232 |              | 1366x768  | 260x140mm  | 11.6 | 2009 | E1B1D |
| LG Display   | LGD023F | LP133WH2-... | 1366x768  | 290x160mm  | 13.0 | 2009 | 43736 |
| LG Display   | LGD0242 |              | 1280x800  | 330x210mm  | 15.4 | 2009 | EFB70 |
| LG Display   | LGD0248 |              | 1366x768  | 220x130mm  | 10.1 | 2009 | 4342F |
| LG Display   | LGD0249 | 6HWVV        | 1280x800  | 300x190mm  | 14.0 | 2010 | 175B5 |
| LG Display   | LGD024B | R869R        | 1366x768  | 340x190mm  | 15.3 | 2010 | 39B86 |
| LG Display   | LGD024D | DR347        | 1366x768  | 290x170mm  | 13.2 | 2010 | 7779B |
| LG Display   | LGD024F |              | 1280x800  | 260x160mm  | 12.0 | 2010 | 17EAE |
| LG Display   | LGD0250 | LP156WH2-... | 1366x768  | 350x190mm  | 15.7 | 2010 | C0F41 |
| LG Display   | LGD0251 | LP140WH1-... | 1366x768  | 310x170mm  | 13.9 | 2009 | 900A5 |
| LG Display   | LGD0254 | LP140WD1-... | 1600x900  | 310x170mm  | 13.9 | 2010 | 9A721 |
| LG Display   | LGD0254 | LP140WD1-... | 1600x900  | 310x170mm  | 13.9 | 2009 | 8CC0E |
| LG Display   | LGD0257 | 875VK        | 1440x900  | 300x190mm  | 14.0 | 2009 | 963C8 |
| LG Display   | LGD0258 |              | 1600x900  | 350x190mm  | 15.7 | 2011 | 23746 |
| LG Display   | LGD0258 | LP156WD1-... | 1600x900  | 350x190mm  | 15.7 | 2009 | 0CF9C |
| LG Display   | LGD0259 |              | 1920x1080 | 350x190mm  | 15.7 | 2011 | 2DFFE |
| LG Display   | LGD0259 | LP156WF1-... | 1920x1080 | 350x190mm  | 15.7 | 2009 | 69FF0 |
| LG Display   | LGD0266 | 1K0R2        | 1366x768  | 340x190mm  | 15.3 | 2009 | 2147A |
| LG Display   | LGD026A |              | 1366x768  | 340x190mm  | 15.3 | 2009 | 788CC |
| LG Display   | LGD026B |              | 1366x768  | 340x190mm  | 15.3 | 2009 | 4DA15 |
| LG Display   | LGD026C |              | 1366x768  | 340x190mm  | 15.3 | 2009 | C878A |
| LG Display   | LGD026E |              | 1024x600  | 220x130mm  | 10.1 | 2009 | D6F64 |
| LG Display   | LGD027A |              | 1600x900  | 380x210mm  | 17.1 | 2011 | 41A4F |
| LG Display   | LGD027A | LP173WD1-... | 1600x900  | 380x210mm  | 17.1 | 2009 | B7C50 |
| LG Display   | LGD027B |              | 1600x900  | 380x210mm  | 17.1 | 2011 | 42C7D |
| LG Display   | LGD027B | LP173WD1-... | 1600x900  | 380x210mm  | 17.1 | 2009 | BD06E |
| LG Display   | LGD027C |              | 1366x768  | 340x190mm  | 15.3 | 2009 | 002C8 |
| LG Display   | LGD027E | LP141WX3-... | 1280x800  | 300x190mm  | 14.0 | 2009 | EE866 |
| LG Display   | LGD027F | LP141WX3-... | 1280x800  | 300x190mm  | 14.0 | 2009 | 101A5 |
| LG Display   | LGD0283 | LP173WF1-... | 1920x1080 | 380x220mm  | 17.3 | 2010 | AE8F7 |
| LG Display   | LGD0289 | LP173WD1-... | 1600x900  | 380x210mm  | 17.1 | 2009 | E9585 |
| LG Display   | LGD028A | MW2VJ        | 1366x768  | 340x190mm  | 15.3 | 2010 | EDEEC |
| LG Display   | LGD028D | LP140WH2-... | 1366x768  | 310x170mm  | 13.9 | 2010 | 49726 |
| LG Display   | LGD0291 | GVK1X        | 1366x768  | 310x170mm  | 13.9 | 2009 | A44AE |
| LG Display   | LGD0292 | 99XNX        | 1366x768  | 310x170mm  | 13.9 | 2009 | B16B2 |
| LG Display   | LGD0293 | LP145WH1-... | 1366x768  | 320x180mm  | 14.5 | 2009 | 61DB1 |
| LG Display   | LGD0295 |              | 1024x600  | 220x130mm  | 10.1 | 2009 | 1D59F |
| LG Display   | LGD029E | P727R        | 1600x900  | 340x190mm  | 15.3 | 2009 | 8AE3F |
| LG Display   | LGD02A6 | LP156WH2-... | 1366x768  | 350x190mm  | 15.7 | 2010 | 072AA |
| LG Display   | LGD02A6 | LP156WH2-... | 1366x768  | 350x190mm  | 15.7 | 2010 | 49D07 |
| LG Display   | LGD02A6 | LP156WH2-... | 1366x768  | 350x190mm  | 15.7 | 2010 | 5AC22 |
| LG Display   | LGD02A7 | LP140WH1-... | 1366x768  | 310x170mm  | 13.9 | 2010 | 6A3E0 |
| LG Display   | LGD02AC | LP156WH2-... | 1366x768  | 340x190mm  | 15.3 | 2010 | 71F8A |
| LG Display   | LGD02AD | LP156WH2-... | 1366x768  | 340x190mm  | 15.3 | 2010 | ABAA2 |
| LG Display   | LGD02B2 | LP140WH1-... | 1366x768  | 310x170mm  | 13.9 | 2010 | BD172 |
| LG Display   | LGD02B3 | LP140WH1-... | 1366x768  | 310x170mm  | 13.9 | 2010 | 77D10 |
| LG Display   | LGD02BE | LP116WH1-... | 1366x768  | 260x140mm  | 11.6 | 2010 | 5C2D0 |
| LG Display   | LGD02C0 | LP133WH2-... | 1366x768  | 290x160mm  | 13.0 | 2010 | B262A |
| LG Display   | LGD02C5 |              | 1920x1080 | 380x210mm  | 17.1 | 2010 | B81D1 |
| LG Display   | LGD02CF | CF9FX        | 1366x768  | 340x190mm  | 15.3 | 2010 | 3A55D |
| LG Display   | LGD02D1 | LP173WD1-... | 1600x900  | 380x210mm  | 17.1 | 2010 | 3BEFE |
| LG Display   | LGD02D1 | LP173WD1-... | 1600x900  | 380x210mm  | 17.1 | 2010 | D066A |
| LG Display   | LGD02D3 | LP125WH2-... | 1366x768  | 280x160mm  | 12.7 | 2011 | 866F2 |
| LG Display   | LGD02D8 | LP125WH2-... | 1366x768  | 280x160mm  | 12.7 | 2012 | 6191D |
| LG Display   | LGD02D8 | LP125WH2-... | 1366x768  | 280x160mm  | 12.7 | 2010 | C6848 |
| LG Display   | LGD02D9 | LP156WF1-... | 1920x1080 | 350x190mm  | 15.7 | 2011 | FAF7C |
| LG Display   | LGD02D9 | MC6JN        | 1920x1080 | 340x190mm  | 15.3 | 2010 | 564F6 |
| LG Display   | LGD02DA | VCV1F        | 1920x1080 | 380x220mm  | 17.3 | 2010 | 0D67D |
| LG Display   | LGD02DA | D3WCF        | 1920x1080 | 380x210mm  | 17.1 | 2010 | D7519 |
| LG Display   | LGD02DA | K6PJ1        | 1920x1080 | 380x210mm  | 17.1 | 2010 | DB137 |
| LG Display   | LGD02DC | LP156WH4-... | 1366x768  | 340x190mm  | 15.3 | 2010 | 062D1 |
| LG Display   | LGD02DC | 9HXXJ        | 1366x768  | 340x190mm  | 15.3 | 2010 | EDAC6 |
| LG Display   | LGD02DF | P7FFH        | 1600x900  | 310x170mm  | 13.9 | 2010 | DACF2 |
| LG Display   | LGD02DF | GJ494        | 1600x900  | 310x170mm  | 13.9 | 2010 | FA6D8 |
| LG Display   | LGD02E1 | DXDWY        | 1600x900  | 380x210mm  | 17.1 | 2010 | 872F0 |
| LG Display   | LGD02E2 | LP140WD2-... | 1600x900  | 310x170mm  | 13.9 | 2010 | A7B90 |
| LG Display   | LGD02E3 | LP156WH4-... | 1366x768  | 340x190mm  | 15.3 | 2010 | 5906D |
| LG Display   | LGD02E3 | PPCTF        | 1366x768  | 340x190mm  | 15.3 | 2010 | B9F52 |
| LG Display   | LGD02E9 | LP140WH4-... | 1366x768  | 310x170mm  | 13.9 | 2010 | 39C84 |
| LG Display   | LGD02EB | KJ262        | 1366x768  | 310x170mm  | 13.9 | 2010 | 814A7 |
| LG Display   | LGD02EB | LP140WH4-... | 1366x768  | 310x170mm  | 13.9 | 2010 | 8F01A |
| LG Display   | LGD02EC | 8MJWP        | 1366x768  | 290x160mm  | 13.0 | 2011 | 13D16 |
| LG Display   | LGD02EC | WX8YV        | 1366x768  | 290x160mm  | 13.0 | 2011 | 91690 |
| LG Display   | LGD02EC | WX8YV        | 1366x768  | 290x160mm  | 13.0 | 2010 | 98EB8 |
| LG Display   | LGD02F1 |              | 1366x768  | 340x190mm  | 15.3 | 2011 | BAB81 |
| LG Display   | LGD02F1 | LP156WH4-... | 1366x768  | 340x190mm  | 15.3 | 2010 | 7812C |
| LG Display   | LGD02F2 |              | 1366x768  | 340x190mm  | 15.3 | 2011 | B3369 |
| LG Display   | LGD02F2 | LP156WH4-... | 1366x768  | 340x190mm  | 15.3 | 2010 | B8620 |
| LG Display   | LGD02F6 | LP173WD1-... | 1600x900  | 380x210mm  | 17.1 | 2010 | E23A1 |
| LG Display   | LGD02F7 | LP173WD1-... | 1600x900  | 380x210mm  | 17.1 | 2010 | 94E83 |
| LG Display   | LGD02F8 | LP140WH4-... | 1366x768  | 310x170mm  | 13.9 | 2010 | 5EA98 |
| LG Display   | LGD0301 | LP173WD1-... | 1600x900  | 380x210mm  | 17.1 | 2010 | FB56B |
| LG Display   | LGD0302 | 4HDDC        | 1366x768  | 290x160mm  | 13.0 | 2010 | D6AA2 |
| LG Display   | LGD0303 | CVW69        | 1600x900  | 380x210mm  | 17.1 | 2010 | 57E55 |
| LG Display   | LGD0306 |              | 1600x900  | 310x170mm  | 13.9 | 2010 | 5171E |
| LG Display   | LGD030A | LP156WH3-... | 1366x768  | 350x190mm  | 15.7 | 2010 | 5B747 |
| LG Display   | LGD0310 | 4259G        | 1920x1080 | 380x210mm  | 17.1 | 2011 | E2C72 |
| LG Display   | LGD0311 | LP133WH2-... | 1366x768  | 290x160mm  | 13.0 | 2011 | 7C6E4 |
| LG Display   | LGD0312 | LP133WH4-... | 1366x768  | 290x170mm  | 13.2 | 2011 | 0879E |
| LG Display   | LGD031B | LP140WH6-... | 1366x768  | 310x170mm  | 13.9 | 2011 | B013F |
| LG Display   | LGD031D | LP116WH1-... | 1366x768  | 260x140mm  | 11.6 | 2010 | A18BF |
| LG Display   | LGD0323 | LP156WF4-... | 1920x1080 | 350x190mm  | 15.7 | 2012 | 2BFB2 |
| LG Display   | LGD0323 | LP156WF4-... | 1920x1080 | 350x190mm  | 15.7 | 2011 | 1F7AF |
| LG Display   | LGD0327 | D70PV        | 1366x768  | 310x170mm  | 13.9 | 2011 | A25AD |
| LG Display   | LGD0329 | LP125WH2-... | 1366x768  | 280x160mm  | 12.7 | 2011 | E9A7C |
| LG Display   | LGD032C | DC9YJ        | 1920x1080 | 340x190mm  | 15.3 | 2011 | CFDEE |
| LG Display   | LGD032E | LP156WHB-... | 1366x768  | 350x190mm  | 15.7 | 2014 | 033C1 |
| LG Display   | LGD032E | LP156WH3-... | 1366x768  | 350x190mm  | 15.7 | 2011 | E696A |
| LG Display   | LGD0335 | LP140WH2-... | 1366x768  | 310x170mm  | 13.9 | 2011 | 3D4AD |
| LG Display   | LGD0335 |              | 1366x768  | 310x170mm  | 13.9 | 2011 | 73050 |
| LG Display   | LGD0338 | D0WRM        | 1600x900  | 340x190mm  | 15.3 | 2011 | 65342 |
| LG Display   | LGD033A | LP156WH4-... | 1366x768  | 340x190mm  | 15.3 | 2013 | C0AAC |
| LG Display   | LGD033A | LP156WH4-... | 1366x768  | 340x190mm  | 15.3 | 2012 | 33F80 |
| LG Display   | LGD033A | 8G1JY        | 1366x768  | 340x190mm  | 15.3 | 2012 | ADEBC |
| LG Display   | LGD033A | LP156WH4-... | 1366x768  | 340x190mm  | 15.3 | 2011 | 12BAC |
| LG Display   | LGD033A | LP156WH4-... | 1366x768  | 340x190mm  | 15.3 | 2011 | 1636C |
| LG Display   | LGD033A | LP156WH4-... | 1366x768  | 340x190mm  | 15.3 | 2011 | 2469E |
| LG Display   | LGD033B | 53H59        | 1366x768  | 340x190mm  | 15.3 | 2012 | DBE3F |
| LG Display   | LGD033B | LP156WH4-... | 1366x768  | 340x190mm  | 15.3 | 2012 | E49DD |
| LG Display   | LGD033B | LP156WH4-... | 1366x768  | 340x190mm  | 15.3 | 2011 | 089DF |
| LG Display   | LGD033C | LP140WH4-... | 1366x768  | 310x170mm  | 13.9 | 2011 | F4E8B |
| LG Display   | LGD033E | 3HT47        | 1366x768  | 310x170mm  | 13.9 | 2012 | 3C504 |
| LG Display   | LGD033E | LP140WH4-... | 1366x768  | 310x170mm  | 13.9 | 2011 | 1BC96 |
| LG Display   | LGD033F | LP140WH8-... | 1366x768  | 310x170mm  | 13.9 | 2014 | 5B6C8 |
| LG Display   | LGD033F | LP140WH2-... | 1366x768  | 310x170mm  | 13.9 | 2012 | 5CC99 |
| LG Display   | LGD033F |              | 1366x768  | 310x170mm  | 13.9 | 2011 | 07364 |
| LG Display   | LGD033F | LP140WH2-... | 1366x768  | 310x170mm  | 13.9 | 2011 | 3769A |
| LG Display   | LGD0340 | LP173WD1-... | 1600x900  | 380x220mm  | 17.3 | 2011 | 47907 |
| LG Display   | LGD034A |              | 1366x768  | 350x190mm  | 15.7 | 2011 | 029A9 |
| LG Display   | LGD034B | LP156WH3-... | 1366x768  | 350x190mm  | 15.7 | 2011 | B3863 |
| LG Display   | LGD034C | NPFN6        | 1366x768  | 290x160mm  | 13.0 | 2012 | 96D6A |
| LG Display   | LGD034C | Y38C6        | 1366x768  | 290x160mm  | 13.0 | 2012 | C1690 |
| LG Display   | LGD034D |              | 1366x768  | 340x190mm  | 15.3 | 2012 | 1AB4A |
| LG Display   | LGD0351 | LP156WH4-... | 1366x768  | 340x190mm  | 15.3 | 2011 | 295FB |
| LG Display   | LGD0353 | LP156WHB-... | 1366x768  | 350x190mm  | 15.7 | 2014 | FD753 |
| LG Display   | LGD0353 | LP156WH3-... | 1366x768  | 350x190mm  | 15.7 | 2012 | 1ADD3 |
| LG Display   | LGD0353 | LP156WH3-... | 1366x768  | 350x190mm  | 15.7 | 2011 | E5E94 |
| LG Display   | LGD0354 |              | 1366x768  | 290x170mm  | 13.2 | 2012 | 42EC0 |
| LG Display   | LGD0357 | LP173WD1-... | 1600x900  | 380x210mm  | 17.1 | 2012 | 35920 |
| LG Display   | LGD0360 | LP133WD2-... | 1600x900  | 290x170mm  | 13.2 | 2011 | 5A2B5 |
| LG Display   | LGD0362 | LP140WD2-... | 1600x900  | 310x170mm  | 13.9 | 2011 | 299AE |
| LG Display   | LGD0365 | JXWY4        | 1600x900  | 380x210mm  | 17.1 | 2011 | 805D1 |
| LG Display   | LGD0366 | 3NPR6        | 1600x900  | 310x170mm  | 13.9 | 2011 | A2CDD |
| LG Display   | LGD0368 |              | 1366x768  | 310x170mm  | 13.9 | 2012 | ABD76 |
| LG Display   | LGD036B | LP140WH6-... | 1366x768  | 310x170mm  | 13.9 | 2012 | 084C5 |
| LG Display   | LGD036C | 8Y92T        | 1366x768  | 280x160mm  | 12.7 | 2012 | 33FD3 |
| LG Display   | LGD0372 |              | 1600x900  | 380x210mm  | 17.1 | 2012 | 2EA3B |
| LG Display   | LGD0375 | LP140WH7-... | 1366x768  | 310x170mm  | 13.9 | 2012 | 2CB2E |
| LG Display   | LGD0375 | LP140WH7-... | 1366x768  | 310x170mm  | 13.9 | 2012 | B409F |
| LG Display   | LGD037A | LP125WH2-... | 1366x768  | 280x160mm  | 12.7 | 2012 | 02FEB |
| LG Display   | LGD037C | LP140WH2-... | 1366x768  | 310x170mm  | 13.9 | 2012 | B022B |
| LG Display   | LGD037F |              | 1920x1080 | 340x190mm  | 15.3 | 2011 | C4589 |
| LG Display   | LGD0380 | LP133WD2-... | 1600x900  | 290x170mm  | 13.2 | 2012 | 17087 |
| LG Display   | LGD0382 | LP140WD2-... | 1600x900  | 310x170mm  | 13.9 | 2012 | 268FB |
| LG Display   | LGD0383 |              | 1600x900  | 380x210mm  | 17.1 | 2012 | D4EF6 |
| LG Display   | LGD0384 |              | 1366x768  | 340x190mm  | 15.3 | 2012 | 3F9A8 |
| LG Display   | LGD0385 |              | 1366x768  | 310x170mm  | 13.9 | 2012 | D248F |
| LG Display   | LGD0386 |              | 1366x768  | 310x170mm  | 13.9 | 2012 | E85A3 |
| LG Display   | LGD038E | LP156WH4-... | 1366x768  | 340x190mm  | 15.3 | 2012 | C6ED6 |
| LG Display   | LGD0390 | LP173WD1-... | 1600x900  | 380x210mm  | 17.1 | 2012 | 89270 |
| LG Display   | LGD0391 | LP173WD1-... | 1600x900  | 380x210mm  | 17.1 | 2012 | B26AA |
| LG Display   | LGD0395 | LP156WH4-... | 1366x768  | 340x190mm  | 15.3 | 2012 | 406A7 |
| LG Display   | LGD0395 |              | 1366x768  | 340x190mm  | 15.3 | 2012 | F596F |
| LG Display   | LGD0396 | LP173WD1-... | 1600x900  | 380x210mm  | 17.1 | 2012 | CF25A |
| LG Display   | LGD0398 | LP101WH4-... | 1366x768  | 220x130mm  | 10.1 | 2012 | F4178 |
| LG Display   | LGD039A | LP156WF4-... | 1920x1080 | 340x190mm  | 15.3 | 2012 | 5CAF5 |
| LG Display   | LGD039B | LP125WH2-... | 1366x768  | 280x160mm  | 12.7 | 2012 | 26BB1 |
| LG Display   | LGD039F | LP156WH3-... | 1366x768  | 350x190mm  | 15.7 | 2012 | 15225 |
| LG Display   | LGD039F | LP156WH3-... | 1366x768  | 350x190mm  | 15.7 | 2012 | 61E27 |
| LG Display   | LGD039F |              | 1366x768  | 350x190mm  | 15.7 | 2012 | 7D558 |
| LG Display   | LGD03A3 | LP125WH2-... | 1366x768  | 280x160mm  | 12.7 | 2012 | CF719 |
| LG Display   | LGD03A5 | LP156WF4-... | 1920x1080 | 350x190mm  | 15.7 | 2012 | FD06C |
| LG Display   | LGD03AB | N3KMP        | 1366x768  | 340x190mm  | 15.3 | 2012 | 74E52 |
| LG Display   | LGD03AD |              | 1366x768  | 310x170mm  | 13.9 | 2012 | 88A42 |
| LG Display   | LGD03B3 | LP140WH2-... | 1366x768  | 310x170mm  | 13.9 | 2012 | 6E4BA |
| LG Display   | LGD03B3 |              | 1366x768  | 310x170mm  | 13.9 | 2012 | D4CD9 |
| LG Display   | LGD03B7 | MV65P        | 1366x768  | 310x170mm  | 13.9 | 2012 | 5E6BB |
| LG Display   | LGD03B8 | LP140WH2-... | 1366x768  | 310x170mm  | 13.9 | 2012 | 78CB8 |
| LG Display   | LGD03C4 | LP156WHU-... | 1366x768  | 350x190mm  | 15.7 | 2012 | CDAF4 |
| LG Display   | LGD03C8 | LP101WH4-... | 1366x768  | 220x130mm  | 10.1 | 2012 | BAED7 |
| LG Display   | LGD03CD | LP125WH2-... | 1366x768  | 280x160mm  | 12.7 | 2012 | B6E12 |
| LG Display   | LGD03CF |              | 1366x768  | 260x140mm  | 11.6 | 2012 | 9BBC2 |
| LG Display   | LGD03D2 | LP140WHU-... | 1366x768  | 310x170mm  | 13.9 | 2012 | 54848 |
| LG Display   | LGD03D2 |              | 1366x768  | 310x170mm  | 13.9 | 2012 | BCC81 |
| LG Display   | LGD03D3 | JY0DK        | 1600x900  | 310x170mm  | 13.9 | 2013 | 0819A |
| LG Display   | LGD03D5 | LP156WHA-... | 1366x768  | 350x190mm  | 15.7 | 2012 | 1351F |
| LG Display   | LGD03D7 | 70V03        | 1366x768  | 310x170mm  | 13.9 | 2012 | 7AFE0 |
| LG Display   | LGD03D9 |              | 1366x768  | 350x190mm  | 15.7 | 2012 | 82A96 |
| LG Display   | LGD03DB | LP156WH3-... | 1366x768  | 350x190mm  | 15.7 | 2012 | E0330 |
| LG Display   | LGD03DC | FM9FF        | 1366x768  | 280x160mm  | 12.7 | 2013 | 804EF |
| LG Display   | LGD03DE | LP173WD1-... | 1600x900  | 380x210mm  | 17.1 | 2012 | F4251 |
| LG Display   | LGD03DF | 35T9M        | 1366x768  | 340x190mm  | 15.3 | 2012 | D0610 |
| LG Display   | LGD03E0 |              | 1366x768  | 350x190mm  | 15.7 | 2012 | C9B1E |
| LG Display   | LGD03E3 | MDK7R        | 1366x768  | 310x170mm  | 13.9 | 2015 | C95E6 |
| LG Display   | LGD03E5 | RD70P        | 1366x768  | 310x170mm  | 13.9 | 2012 | 32C4F |
| LG Display   | LGD03E6 | LP156WHU-... | 1366x768  | 350x190mm  | 15.7 | 2012 | BCBBC |
| LG Display   | LGD03E8 |              | 1366x768  | 310x170mm  | 13.9 | 2012 | 0C5BB |
| LG Display   | LGD03E9 | LP156WHU-... | 1366x768  | 350x190mm  | 15.7 | 2012 | 0AD62 |
| LG Display   | LGD03EA | 0MJ2P        | 1920x1080 | 310x170mm  | 13.9 | 2013 | 754A5 |
| LG Display   | LGD03ED | LP125WH2-... | 1366x768  | 280x160mm  | 12.7 | 2012 | 62489 |
| LG Display   | LGD03EE |              | 1366x768  | 280x160mm  | 12.7 | 2012 | 17D9F |
| LG Display   | LGD03F0 | LP140WHU-... | 1366x768  | 310x170mm  | 13.9 | 2012 | 0A42E |
| LG Display   | LGD03F1 |              | 1600x900  | 310x170mm  | 13.9 | 2013 | 934F6 |
| LG Display   | LGD03F7 | LP156WH4-... | 1366x768  | 340x190mm  | 15.3 | 2013 | 7E675 |
| LG Display   | LGD03F8 | LP156WH3-... | 1366x768  | 350x190mm  | 15.7 | 2012 | 22393 |
| LG Display   | LGD03FA | LP140WH2-... | 1366x768  | 310x170mm  | 13.9 | 2012 | DA865 |
| LG Display   | LGD03FB |              | 1920x1080 | 380x210mm  | 17.1 | 2013 | 32E50 |
| LG Display   | LGD03FC | LP140WD2-... | 1600x900  | 310x170mm  | 13.9 | 2013 | E58C4 |
| LG Display   | LGD03FD | WV501        | 1920x1080 | 280x160mm  | 12.7 | 2012 | B5026 |
| LG Display   | LGD03FF | LP140WF1-... | 1920x1080 | 310x170mm  | 13.9 | 2013 | FE7B6 |
| LG Display   | LGD0404 |              | 1366x768  | 280x160mm  | 12.7 | 2013 | 320D1 |
| LG Display   | LGD0406 | LP140WF1-... | 1920x1080 | 310x170mm  | 13.9 | 2013 | F37E3 |
| LG Display   | LGD040A |              | 1920x1080 | 310x170mm  | 13.9 | 2013 | 5AB61 |
| LG Display   | LGD040A | LP140WF1-... | 1920x1080 | 310x170mm  | 13.9 | 2013 | 5F3B6 |
| LG Display   | LGD040A | WYPXG        | 1920x1080 | 310x170mm  | 13.9 | 2013 | CFB01 |
| LG Display   | LGD040B |              | 1366x768  | 290x160mm  | 13.0 | 2013 | 8793D |
| LG Display   | LGD040E | 890N5        | 1920x1080 | 350x190mm  | 15.7 | 2013 | 66EFB |
| LG Display   | LGD040F | LP156WF4-... | 1920x1080 | 350x190mm  | 15.7 | 2013 | 280D9 |
| LG Display   | LGD0414 | LP125WF2-... | 1920x1080 | 280x160mm  | 12.7 | 2013 | 09B35 |
| LG Display   | LGD0416 | LP156WF1-... | 1920x1080 | 350x190mm  | 15.7 | 2013 | B9CBD |
| LG Display   | LGD0418 | LP140QH1-... | 2560x1440 | 310x170mm  | 13.9 | 2013 | C8539 |
| LG Display   | LGD0419 | LP140QH1-... | 2560x1440 | 310x170mm  | 13.9 | 2013 | 9C556 |
| LG Display   | LGD042C | LP156WF4-... | 1920x1080 | 350x190mm  | 15.7 | 2013 | 62E3D |
| LG Display   | LGD042D | LP133WF2-... | 1920x1080 | 290x170mm  | 13.2 | 2013 | 20331 |
| LG Display   | LGD0430 | LP156WH3-... | 1366x768  | 350x190mm  | 15.7 | 2013 | 25E92 |
| LG Display   | LGD0435 | LP140WD2-... | 1600x900  | 310x170mm  | 13.9 | 2013 | 64448 |
| LG Display   | LGD0437 | LP125WF2-... | 1920x1080 | 280x160mm  | 12.7 | 2013 | 5D99D |
| LG Display   | LGD0438 | LP156WHU-... | 1366x768  | 340x190mm  | 15.3 | 2013 | 72DDF |
| LG Display   | LGD0439 | LP156WH3-... | 1366x768  | 340x190mm  | 15.3 | 2013 | EBA37 |
| LG Display   | LGD043D | MN3MC        | 1366x768  | 340x190mm  | 15.3 | 2013 | 1B24E |
| LG Display   | LGD0443 | LP156WF4-... | 1920x1080 | 350x190mm  | 15.7 | 2013 | A75EF |
| LG Display   | LGD0446 | LP140WF3-... | 1920x1080 | 310x170mm  | 13.9 | 2013 | 42E99 |
| LG Display   | LGD0448 | LP156WF4-... | 1920x1080 | 350x190mm  | 15.7 | 2013 | 555A3 |
| LG Display   | LGD0449 |              | 1920x1080 | 280x160mm  | 12.7 | 2013 | 250AD |
| LG Display   | LGD044B | LP156WH3-... | 1366x768  | 340x190mm  | 15.3 | 2013 | A42BF |
| LG Display   | LGD044F | LP156WF4-... | 1920x1080 | 350x190mm  | 15.7 | 2014 | 675DB |
| LG Display   | LGD0450 | T6T7K        | 1366x768  | 280x160mm  | 12.7 | 2014 | CED78 |
| LG Display   | LGD0454 | LP140WH8-... | 1366x768  | 310x170mm  | 13.9 | 2014 | EFE85 |
| LG Display   | LGD0455 | LP140WH8-... | 1366x768  | 310x170mm  | 13.9 | 2014 | 34E08 |
| LG Display   | LGD0455 |              | 1366x768  | 310x170mm  | 13.9 | 2014 | 3B0DD |
| LG Display   | LGD0456 | LP156WHB-... | 1366x768  | 340x190mm  | 15.3 | 2014 | 1BB64 |
| LG Display   | LGD0456 | DCR74        | 1366x768  | 340x190mm  | 15.3 | 2014 | FD8AD |
| LG Display   | LGD0458 | NGFY3        | 1366x768  | 310x170mm  | 13.9 | 2014 | 6A0BE |
| LG Display   | LGD045C | LP156WHB-... | 1366x768  | 340x190mm  | 15.3 | 2014 | 05170 |
| LG Display   | LGD045C | LP156WHB-... | 1366x768  | 350x190mm  | 15.7 | 2014 | 8F54E |
| LG Display   | LGD045D | LP156WHB-... | 1366x768  | 340x190mm  | 15.3 | 2014 | 0F332 |
| LG Display   | LGD045D | LP156WHB-... | 1366x768  | 350x190mm  | 15.7 | 2014 | 7C157 |
| LG Display   | LGD045E |              | 1366x768  | 310x170mm  | 13.9 | 2014 | 290BD |
| LG Display   | LGD045E | LP140WH8-... | 1366x768  | 310x170mm  | 13.9 | 2014 | 397C4 |
| LG Display   | LGD0460 | LP156WHB-... | 1366x768  | 340x190mm  | 15.3 | 2014 | AEBCB |
| LG Display   | LGD0461 | LD116WF1-... | 1920x1080 | 260x140mm  | 11.6 | 2014 | 0F6EE |
| LG Display   | LGD0464 | W6TN0        | 1366x768  | 290x160mm  | 13.0 | 2014 | 4AE8B |
| LG Display   | LGD0465 |              | 1366x768  | 340x190mm  | 15.3 | 2014 | 32D63 |
| LG Display   | LGD0465 | LP156WHB-... | 1366x768  | 340x190mm  | 15.3 | 2014 | 4BF12 |
| LG Display   | LGD0468 | LP156WHB-... | 1366x768  | 340x190mm  | 15.3 | 2014 | 5D9A5 |
| LG Display   | LGD0469 | LP173WF4-... | 1920x1080 | 380x210mm  | 17.1 | 2014 | 213AA |
| LG Display   | LGD046B |              | 1366x768  | 340x190mm  | 15.3 | 2014 | D4DA0 |
| LG Display   | LGD046C | LP173WF4-... | 1920x1080 | 380x210mm  | 17.1 | 2014 | 63682 |
| LG Display   | LGD046D | RN91N        | 1920x1080 | 310x170mm  | 13.9 | 2014 | 82661 |
| LG Display   | LGD046D | LP140WF3-... | 1920x1080 | 310x170mm  | 13.9 | 2014 | B4587 |
| LG Display   | LGD046E |              | 1920x1080 | 380x210mm  | 17.1 | 2014 | 40175 |
| LG Display   | LGD046F | LP156WF6-... | 1920x1080 | 340x190mm  | 15.3 | 2016 | 9D227 |
| LG Display   | LGD046F | LP156WF6-... | 1920x1080 | 340x190mm  | 15.3 | 2014 | 155B0 |
| LG Display   | LGD046F | LP156WF6-... | 1920x1080 | 350x190mm  | 15.7 | 2014 | 46AA0 |
| LG Display   | LGD046F | 3874Y        | 1920x1080 | 340x190mm  | 15.3 | 2014 | 53675 |
| LG Display   | LGD046F |              | 1920x1080 | 350x190mm  | 15.7 | 2014 | 5F06F |
| LG Display   | LGD046F | C3MWM        | 1920x1080 | 340x190mm  | 15.3 | 2014 | C46C8 |
| LG Display   | LGD0470 | LP156WF6-... | 1920x1080 | 350x190mm  | 15.7 | 2014 | 3A20B |
| LG Display   | LGD0484 | 015J5        | 1366x768  | 340x190mm  | 15.3 | 2014 | 5B1E3 |
| LG Display   | LGD048A |              | 1920x1080 | 280x160mm  | 12.7 | 2013 | 89C2E |
| LG Display   | LGD048C |              | 1920x1080 | 290x170mm  | 13.2 | 2014 | 08AF1 |
| LG Display   | LGD048C | LP133WF2-... | 1920x1080 | 290x170mm  | 13.2 | 2014 | 211D8 |
| LG Display   | LGD048D | P99F7        | 1366x768  | 260x140mm  | 11.6 | 2014 | 32B3E |
| LG Display   | LGD0490 | 53X2G        | 1920x1080 | 310x170mm  | 13.9 | 2014 | 70E26 |
| LG Display   | LGD0492 | XX5H6        | 1920x1080 | 340x190mm  | 15.3 | 2014 | 44B36 |
| LG Display   | LGD0493 |              | 1366x768  | 340x190mm  | 15.3 | 2014 | C6923 |
| LG Display   | LGD0494 |              | 1366x768  | 290x170mm  | 13.2 | 2014 | 5CC76 |
| LG Display   | LGD0497 | LP116WH7-... | 1366x768  | 260x140mm  | 11.6 | 2015 | 7EB9A |
| LG Display   | LGD0497 |              | 1366x768  | 256x144mm  | 11.6 | 2015 | C5BFD |
| LG Display   | LGD049A | LP140QH1-... | 2560x1440 | 310x170mm  | 13.9 | 2014 | A4E8C |
| LG Display   | LGD049B | F7HH2        | 1920x1080 | 340x190mm  | 15.3 | 2015 | 370C6 |
| LG Display   | LGD04A4 | LP140WF6-... | 1920x1080 | 310x170mm  | 13.9 | 2015 | 143A7 |
| LG Display   | LGD04A7 | LP156WFC-... | 1920x1080 | 340x190mm  | 15.3 | 2018 | 1F8B4 |
| LG Display   | LGD04A7 | LP156WF6-... | 1920x1080 | 340x190mm  | 15.3 | 2015 | 0B7DE |
| LG Display   | LGD04A7 | LP156WF6-... | 1920x1080 | 340x190mm  | 15.3 | 2015 | 43469 |
| LG Display   | LGD04AB |              | 1366x768  | 290x170mm  | 13.2 | 2015 | 2CE55 |
| LG Display   | LGD04AF | K96D2        | 1366x768  | 340x190mm  | 15.3 | 2015 | DFF08 |
| LG Display   | LGD04B3 |              | 1920x1080 | 350x190mm  | 15.7 | 2015 | D4CB8 |
| LG Display   | LGD04B7 | LP156WHB-... | 1366x768  | 340x190mm  | 15.3 | 2015 | 898DD |
| LG Display   | LGD04B9 | R52WF        | 1920x1080 | 340x190mm  | 15.3 | 2015 | 88B7A |
| LG Display   | LGD04BA | LP173WD1-... | 1600x900  | 380x210mm  | 17.1 | 2015 | F637F |
| LG Display   | LGD04BD | LP156WHB-... | 1366x768  | 340x190mm  | 15.3 | 2015 | AC9EC |
| LG Display   | LGD04C7 | LP140WH8-... | 1366x768  | 310x170mm  | 13.9 | 2015 | 5946F |
| LG Display   | LGD04CB | LP133WF2-... | 1920x1080 | 290x170mm  | 13.2 | 2015 | F3BC2 |
| LG Display   | LGD04D0 | LP156WHB-... | 1366x768  | 340x190mm  | 15.3 | 2015 | 4A508 |
| LG Display   | LGD04D4 | LP156UD1-... | 3840x2160 | 340x190mm  | 15.3 | 2015 | 0A123 |
| LG Display   | LGD04D9 | 53FC4        | 3840x2160 | 340x190mm  | 15.3 | 2015 | AAAB0 |
| LG Display   | LGD04E1 | LP156WHB-... | 1366x768  | 340x190mm  | 15.3 | 2015 | C48B7 |
| LG Display   | LGD04E2 | LP156WHB-... | 1366x768  | 340x190mm  | 15.3 | 2015 | 94EE2 |
| LG Display   | LGD04E8 | LP173WF4-... | 1920x1080 | 380x210mm  | 17.1 | 2017 | 58A56 |
| LG Display   | LGD04E8 | LP173WF4-... | 1920x1080 | 380x210mm  | 17.1 | 2016 | 4350D |
| LG Display   | LGD04E8 | LP173WF4-... | 1920x1080 | 380x210mm  | 17.1 | 2015 | AE06B |
| LG Display   | LGD04EF | LP133WF4-... | 1920x1080 | 290x170mm  | 13.2 | 2015 | 2DC9F |
| LG Display   | LGD04F5 | LP140WF7-... | 1920x1080 | 310x170mm  | 13.9 | 2016 | 441E8 |
| LG Display   | LGD04F6 |              | 1920x1080 | 340x190mm  | 15.3 | 2016 | CD878 |
| LG Display   | LGD04FB | LP140WHU-... | 1366x768  | 310x170mm  | 13.9 | 2016 | 14202 |
| LG Display   | LGD04FC | LP156WHU-... | 1366x768  | 340x190mm  | 15.3 | 2015 | DC3DE |
| LG Display   | LGD04FD | LP156WHU-... | 1366x768  | 340x190mm  | 15.3 | 2015 | 66CF1 |
| LG Display   | LGD04FF | LP140WF6-... | 1920x1080 | 310x170mm  | 13.9 | 2015 | 34F12 |
| LG Display   | LGD0503 |              | 1366x768  | 340x190mm  | 15.3 | 2015 | 1B7DC |
| LG Display   | LGD0504 |              | 1366x768  | 340x190mm  | 15.3 | 2015 | 7FD9B |
| LG Display   | LGD0505 | LP156WHU-... | 1366x768  | 340x190mm  | 15.3 | 2015 | 680DE |
| LG Display   | LGD0506 | LP156WHU-... | 1366x768  | 340x190mm  | 15.3 | 2015 | ABCA1 |
| LG Display   | LGD0508 |              | 1366x768  | 310x170mm  | 13.9 | 2016 | 0767C |
| LG Display   | LGD0512 | LP133QD1-... | 3200x1800 | 290x170mm  | 13.2 | 2016 | CE5DA |
| LG Display   | LGD0519 |              | 1920x1080 | 340x190mm  | 15.3 | 2016 | 42C31 |
| LG Display   | LGD051E | LP139UD1-... | 3840x2160 | 310x170mm  | 13.9 | 2016 | DB87A |
| LG Display   | LGD051F | DJ58M        | 1920x1080 | 340x190mm  | 15.3 | 2017 | 058CC |
| LG Display   | LGD0521 | LP140WF6-... | 1920x1080 | 310x170mm  | 13.9 | 2016 | 061C3 |
| LG Display   | LGD0521 | LP140WF6-... | 1920x1080 | 310x170mm  | 13.9 | 2016 | B3B0D |
| LG Display   | LGD0525 | H97H1        | 1366x768  | 340x190mm  | 15.3 | 2016 | 11243 |
| LG Display   | LGD0527 | CXHRR        | 1366x768  | 310x170mm  | 13.9 | 2016 | 4FDD8 |
| LG Display   | LGD052D |              | 1920x1080 | 290x170mm  | 13.2 | 2016 | ED79C |
| LG Display   | LGD052F |              | 1920x1080 | 340x190mm  | 15.3 | 2016 | 831B2 |
| LG Display   | LGD0532 |              | 1920x1080 | 340x190mm  | 15.3 | 2016 | 53C1D |
| LG Display   | LGD0533 | LP156WF6-... | 1920x1080 | 340x190mm  | 15.3 | 2016 | 8091D |
| LG Display   | LGD053B | LP133WF2-... | 1920x1080 | 290x170mm  | 13.2 | 2016 | 68465 |
| LG Display   | LGD053F | 4XK13        | 1920x1080 | 340x190mm  | 15.3 | 2016 | 5EE39 |
| LG Display   | LGD0540 | NT2RR        | 1920x1080 | 340x190mm  | 15.3 | 2017 | 84A44 |
| LG Display   | LGD0540 | G49P6        | 1920x1080 | 340x190mm  | 15.3 | 2016 | 5538D |
| LG Display   | LGD0541 | LP156WF9-... | 1920x1080 | 340x190mm  | 15.3 | 2016 | 80412 |
| LG Display   | LGD0542 | DND8T        | 1920x1080 | 280x160mm  | 12.7 | 2016 | 7A213 |
| LG Display   | LGD0554 | MP0.0_ LP... | 3240x2160 | 320x210mm  | 15.1 | 2016 | 4465D |
| LG Display   | LGD0555 | MP0.1_ LP... | 2736x1824 | 260x170mm  | 12.2 | 2016 | 01AFA |
| LG Display   | LGD0555 | MP0.2_ LP... | 2736x1824 | 260x170mm  | 12.2 | 2016 | C5EDC |
| LG Display   | LGD0555 | MP0.1_ LP... | 2736x1824 | 260x170mm  | 12.2 | 2016 | EA88B |
| LG Display   | LGD0557 | R6D8G        | 1920x1080 | 310x170mm  | 13.9 | 2016 | 552BA |
| LG Display   | LGD055D |              | 3000x2000 | 260x170mm  | 12.2 | 2016 | E544A |
| LG Display   | LGD0563 | LP156WFC-... | 1920x1080 | 340x190mm  | 15.3 | 2018 | DFBCA |
| LG Display   | LGD0563 | LP156WF9-... | 1920x1080 | 340x190mm  | 15.3 | 2017 | 9F612 |
| LG Display   | LGD0569 | LP140WF6-... | 1920x1080 | 310x170mm  | 13.9 | 2016 | B4B29 |
| LG Display   | LGD056D | LP173WF4-... | 1920x1080 | 380x210mm  | 17.1 | 2016 | 5D097 |
| LG Display   | LGD056E |              | 1920x1080 | 340x190mm  | 15.3 | 2016 | A3B50 |
| LG Display   | LGD0570 | LP156WF6-... | 1920x1080 | 340x190mm  | 15.3 | 2016 | 4493D |
| LG Display   | LGD0572 | RRMTR        | 1920x1080 | 290x170mm  | 13.2 | 2016 | 6B220 |
| LG Display   | LGD0573 | LP156WF9-... | 1920x1080 | 340x190mm  | 15.3 | 2016 | 4F5F4 |
| LG Display   | LGD0574 |              | 1920x1080 | 310x170mm  | 13.9 | 2016 | 343FC |
| LG Display   | LGD057A | LP140WF7-... | 1920x1080 | 310x170mm  | 13.9 | 2016 | 1A4F4 |
| LG Display   | LGD0588 |              | 3840x2160 | 290x160mm  | 13.0 | 2016 | 11610 |
| LG Display   | LGD058B | LP140QH2-... | 2560x1440 | 310x170mm  | 13.9 | 2016 | 0AE56 |
| LG Display   | LGD058C |              | 1920x1080 | 340x190mm  | 15.3 | 2016 | 5F4C0 |
| LG Display   | LGD0590 | LP156WF6-... | 1920x1080 | 340x190mm  | 15.3 | 2017 | 45122 |
| LG Display   | LGD0596 | DJ7G6        | 1920x1080 | 380x210mm  | 17.1 | 2017 | DA793 |
| LG Display   | LGD0597 |              | 1920x1080 | 290x170mm  | 13.2 | 2017 | 3BD16 |
| LG Display   | LGD0599 | LP140WF8-... | 1920x1080 | 310x170mm  | 13.9 | 2017 | 85107 |
| LG Display   | LGD0599 |              | 1920x1080 | 310x170mm  | 13.9 | 2017 | E9541 |
| LG Display   | LGD059D | V1M58        | 1920x1080 | 310x170mm  | 13.9 | 2017 | E17EF |
| LG Display   | LGD059E |              | 1920x1080 | 380x210mm  | 17.1 | 2017 | 9E20D |
| LG Display   | LGD05AB | LP140WF7-... | 1920x1080 | 310x170mm  | 13.9 | 2016 | 83150 |
| LG Display   | LGD05B3 | LP133WF4-... | 1920x1080 | 290x170mm  | 13.2 | 2017 | 5F26A |
| LG Display   | LGD05B4 | LP133WF4-... | 1920x1080 | 290x170mm  | 13.2 | 2017 | 263B8 |
| LG Display   | LGD05B9 | LP173WF4-... | 1920x1080 | 380x210mm  | 17.1 | 2017 | BD925 |
| LG Display   | LGD05BC | LP139UD1-... | 3840x2160 | 310x170mm  | 13.9 | 2017 | 52B8C |
| LG Display   | LGD05BD |              | 3840x2160 | 290x160mm  | 13.0 | 2017 | BDCC4 |
| LG Display   | LGD05C0 |              | 1920x1080 | 344x194mm  | 15.5 | 2017 | 231D6 |
| LG Display   | LGD05C8 | LP156WFC-... | 1920x1080 | 340x190mm  | 15.3 | 2017 | 1F87C |
| LG Display   | LGD05CB | 7RJC1        | 1920x1080 | 310x170mm  | 13.9 | 2018 | 03F54 |
| LG Display   | LGD05D0 | 10NPP        | 1920x1080 | 340x190mm  | 15.3 | 2018 | 7A360 |
| LG Display   | LGD05D5 |              | 1920x1080 | 340x190mm  | 15.3 | 2018 | E3222 |
| LG Display   | LGD05D8 | LP156WFC-... | 1920x1080 | 340x190mm  | 15.3 | 2018 | EFE28 |
| LG Display   | LGD05E5 | LP156WFC-... | 1920x1080 | 340x190mm  | 15.3 | 2018 | C07E3 |
| LG Display   | LGD05F0 |              | 1920x1080 | 310x170mm  | 13.9 | 2018 | 60112 |
| LG Display   | LGD05F1 |              | 1920x1080 | 310x170mm  | 13.9 | 2018 | E413D |
| LG Display   | LGD05F6 | LP140WFA-... | 1920x1080 | 310x170mm  | 13.9 | 2018 | ACE46 |
| LG Display   | LGD061B | LP140WF7-... | 1920x1080 | 310x170mm  | 13.9 | 2018 | 5E1E1 |
| LG Display   | LGD0ABC | G022H        | 1280x800  | 300x190mm  | 14.0 | 2008 | 5D582 |
| LG Display   | LGD1325 |              | 1600x900  | 380x210mm  | 17.1 | 2013 | 5839F |
| LG Display   | LGD2297 | LP156WH3-... | 1366x768  | 350x190mm  | 15.7 | 2010 | 7012C |
| LG Display   | LGD3901 | LP171WU3-... | 1920x1200 | 370x230mm  | 17.2 | 2008 | 0CB03 |
| LG Display   | LGD40A0 | LP140WH2-... | 1366x768  | 310x170mm  | 13.9 | 2010 | D69AE |
| LG Display   | LGD4C01 | FR879        | 1920x1200 | 370x230mm  | 17.2 | 2008 | 22538 |
| LG Display   | LGD6301 |              | 1366x768  | 340x190mm  | 15.3 | 2008 | 6015B |
| LG Display   | LGD6302 | LP156WH1-... | 1366x768  | 340x190mm  | 15.3 | 2009 | 9BFDF |
| LG Display   | LGD6E01 | LP156WH1-... | 1366x768  | 340x190mm  | 15.3 | 2008 | 6C3D6 |
| LG Display   | LGD6E01 | J553H        | 1366x768  | 340x190mm  | 15.3 | 2008 | F0209 |
| LG Display   | LGD7001 | 8PTNR        | 1366x768  | 340x190mm  | 15.3 | 2008 | D36D5 |
| LG Display   | LGD8A01 | P121H        | 1366x768  | 340x190mm  | 15.3 | 2008 | 50C53 |
| LG Display   | LGD8C01 | LP156WH2-... | 1366x768  | 340x190mm  | 15.3 | 2008 | 8ADC7 |
| LG Display   | LGDB901 |              | 1024x576  | 220x130mm  | 10.1 | 2008 | 0DACB |
| LG Display   | LGDCF01 |              | 1366x768  | 340x190mm  | 15.3 | 2008 | 1E354 |
| LG Display   | LGDD801 | LP156WH1-... | 1366x768  | 340x190mm  | 15.3 | 2008 | 037AB |
| LG Display   | LGDE036 |              | 3840x2160 | 520x320mm  | 24.0 | 2014 | 7E08B |
| LG Display   | LGDE400 | U812G &<HXx  | 1920x1200 | 370x230mm  | 17.2 | 2007 | 39A11 |
| LG Display   | LGDE400 | W660G        | 1920x1200 | 370x230mm  | 17.2 | 2007 | 7E7F3 |
| LG Display   | LGDF200 | LP154WU1-... | 1920x1200 | 330x210mm  | 15.4 | 2008 | 2DBE9 |
| LG Philips   | LGP0000 |              | 1280x800  | 330x210mm  | 15.4 |      | C9EB5 |
| LG Philips   | LGP0000 | LP154W01-... | 1280x800  | 330x210mm  | 15.4 |      | F1D44 |
| LG Philips   | LGP0657 |              | 1024x768  | 300x220mm  | 14.6 |      | 584F7 |
| LG Philips   | LPL0000 | WU682        | 1280x800  | 330x210mm  | 15.4 | 2008 | F5C96 |
| LG Philips   | LPL0000 | X176G *@MVy  | 1440x900  | 370x230mm  | 17.2 | 2007 | 1DDB2 |
| LG Philips   | LPL0000 | UN864        | 1280x800  | 290x180mm  | 13.4 | 2007 | 5402C |
| LG Philips   | LPL0000 | CY185        | 1280x800  | 300x190mm  | 14.0 | 2007 | 62E0E |
| LG Philips   | LPL0000 | RP776        | 1280x800  | 300x190mm  | 14.0 | 2007 | D084D |
| LG Philips   | LPL0000 | T635C        | 1280x800  | 330x210mm  | 15.4 | 2007 | E0ED2 |
| LG Philips   | LPL0000 | GR430 .>ISt  | 1440x900  | 370x230mm  | 17.2 | 2007 | F5B50 |
| LG Philips   | LPL0000 | HH258 &<HXx  | 1920x1200 | 370x230mm  | 17.2 | 2006 | A74E8 |
| LG Philips   | LPL0000 | YY265        | 1280x800  | 300x190mm  | 14.0 | 2006 | B109E |
| LG Philips   | LPL0000 | LP150X08-... | 1024x768  | 300x220mm  | 14.6 |      | 1F634 |
| LG Philips   | LPL0000 | LP171WX2-... | 1440x900  | 370x230mm  | 17.2 |      | 1FE98 |
| LG Philips   | LPL0000 | DD284 (?NX   | 1680x1050 | 330x210mm  | 15.4 |      | 3586E |
| LG Philips   | LPL0000 | LP154W01-... | 1280x800  | 330x210mm  | 15.4 |      | 570D1 |
| LG Philips   | LPL0000 | LP150X08-... | 1024x768  | 300x220mm  | 14.6 |      | 7B8B5 |
| LG Philips   | LPL0000 | YD477 &5@Im  | 1440x900  | 370x230mm  | 17.2 |      | 93F7E |
| LG Philips   | LPL0000 | RC9091154WU1 | 1920x1200 | 330x210mm  | 15.4 |      | 9D072 |
| LG Philips   | LPL0000 | TL575        | 1024x768  | 300x230mm  | 14.9 |      | 9FAFE |
| LG Philips   | LPL0000 | LP154W01-A5  | 1280x800  | 330x210mm  | 15.4 |      | AB6BA |
| LG Philips   | LPL0000 | LP154W01-... | 1280x800  | 330x210mm  | 15.4 |      | BDF8F |
| LG Philips   | LPL0000 | LP171WX2-... | 1440x900  | 370x230mm  | 17.2 |      | E4527 |
| LG Philips   | LPL0000 | LP154W01-... | 1280x800  | 330x210mm  | 15.4 |      | FB3A8 |
| LG Philips   | LPL0001 | LP140WX1-... | 1280x768  | 300x180mm  | 13.8 |      | B4D0D |
| LG Philips   | LPL0050 |              | 1680x1050 | 430x270mm  | 20.0 | 2006 | 0A5AA |
| LG Philips   | LPL00E0 | JW804        | 1440x900  | 300x190mm  | 14.0 | 2006 | CF361 |
| LG Philips   | LPL00E5 | R778G +=LSy  | 1440x900  | 300x190mm  | 14.0 | 2008 | CA974 |
| LG Philips   | LPL00E5 | TM246        | 1440x900  | 300x190mm  | 14.0 | 2006 | DA0AB |
| LG Philips   | LPL0120 |              | 1280x800  | 330x210mm  | 15.4 | 2008 | BEEBE |
| LG Philips   | LPL0123 |              | 1280x800  | 330x210mm  | 15.4 | 2008 | 743EE |
| LG Philips   | LPL0129 | LP141WX3-... | 1280x800  | 300x190mm  | 14.0 | 2007 | E28F4 |
| LG Philips   | LPL012A | LP141WX3-... | 1280x800  | 300x190mm  | 14.0 | 2007 | BE3BB |
| LG Philips   | LPL012B | GY219        | 1280x800  | 300x190mm  | 14.0 | 2008 | CA2FB |
| LG Philips   | LPL0133 | LP141WX3-... | 1280x800  | 300x190mm  | 14.0 | 2008 | C8565 |
| LG Philips   | LPL0138 | NY695 +=LSy  | 1440x900  | 300x190mm  | 14.0 | 2007 | E80EF |
| LG Philips   | LPL0140 | CT008        | 1440x900  | 300x190mm  | 14.0 | 2008 | FDAEE |
| LG Philips   | LPL0144 | LP154WP1-... | 1440x900  | 330x210mm  | 15.4 | 2008 | 7B07B |
| LG Philips   | LPL017B | LGE00        | 1440x900  | 330x210mm  | 15.4 | 2008 | F3005 |
| LG Philips   | LPL017D | LP154WX5-... | 1280x800  | 330x210mm  | 15.4 | 2008 | 01E42 |
| LG Philips   | LPL0201 | C362C &4>Dd  | 1280x800  | 330x210mm  | 15.4 | 2007 | 2F527 |
| LG Philips   | LPL0201 | R782G &4>Dd  | 1280x800  | 330x210mm  | 15.4 | 2007 | DF680 |
| LG Philips   | LPL0301 | D196J $2=Eh  | 1280x800  | 330x210mm  | 15.4 | 2008 | D728E |
| LG Philips   | LPL0301 | W654G $2=Eh  | 1280x800  | 330x210mm  | 15.4 | 2007 | 851F4 |
| LG Philips   | LPL0301 | NU763 $2=Eh  | 1280x800  | 330x210mm  | 15.4 | 2007 | BFEE8 |
| LG Philips   | LPL0301 | XU235 $2=Eh  | 1280x800  | 330x210mm  | 15.4 | 2007 | C8D6D |
| LG Philips   | LPL0701 | LP154WX4-... | 1280x800  | 330x210mm  | 15.4 | 2007 | EA9EC |
| LG Philips   | LPL0801 |              | 1920x1200 | 370x230mm  | 17.2 | 2007 | C4C0C |
| LG Philips   | LPL0A01 | LP171WP4-... | 1440x900  | 370x230mm  | 17.2 | 2007 | FB5BB |
| LG Philips   | LPL0AA8 | TN162        | 1440x900  | 330x210mm  | 15.4 | 2008 | A3C9F |
| LG Philips   | LPL0C01 | Y167G '?GRv  | 1280x800  | 300x190mm  | 14.0 | 2007 | 363EC |
| LG Philips   | LPL0C01 | CY185 '?GRv  | 1280x800  | 300x190mm  | 14.0 | 2007 | 71263 |
| LG Philips   | LPL1101 | LP141WX3-... | 1280x800  | 300x190mm  | 14.0 | 2007 | 1E124 |
| LG Philips   | LPL1146 | LP141WX1-... | 1280x800  | 300x190mm  | 14.0 |      | EF62C |
| LG Philips   | LPL1151 | LP150X08-... | 1024x768  | 300x220mm  | 14.6 |      | DB24A |
| LG Philips   | LPL1188 | LP171WX2-... | 1440x900  | 370x230mm  | 17.2 |      | 90DEF |
| LG Philips   | LPL1201 | LP141WX3-... | 1280x800  | 300x190mm  | 14.0 | 2007 | FEFC8 |
| LG Philips   | LPL1279 | LP154W02-... | 1680x1050 | 330x210mm  | 15.4 |      | 5B0C0 |
| LG Philips   | LPL1288 | LP171WX2-... | 1440x900  | 370x230mm  | 17.2 |      | 9F816 |
| LG Philips   | LPL1676 | LP154W01-... | 1280x800  | 330x210mm  | 15.4 |      | 692C8 |
| LG Philips   | LPL1901 | LP154WE2-... | 1680x1050 | 330x210mm  | 15.4 | 2007 | 84BAA |
| LG Philips   | LPL1A01 | LP171WP7-... | 1440x900  | 370x230mm  | 17.2 | 2007 | C2729 |
| LG Philips   | LPL1D01 | LP133WX1-... | 1280x800  | 290x180mm  | 13.4 | 2007 | A009A |
| LG Philips   | LPL1E01 | LP154WX5-... | 1280x800  | 330x210mm  | 15.4 | 2007 | 66E0A |
| LG Philips   | LPL2188 | LP171WP4-... | 1440x900  | 370x230mm  | 17.2 |      | AE306 |
| LG Philips   | LPL2201 | LP133WX2-... | 1280x800  | 290x180mm  | 13.4 | 2007 | 21B89 |
| LG Philips   | LPL2601 | LP133WX1-... | 1280x800  | 290x180mm  | 13.4 | 2007 | C1AAB |
| LG Philips   | LPL2900 | LP154W01-... | 1280x800  | 330x210mm  | 15.4 |      | 1731A |
| LG Philips   | LPL2A00 | LP154W01-... | 1280x800  | 330x210mm  | 15.4 |      | AFCE2 |
| LG Philips   | LPL2D01 | YP024        | 1920x1200 | 330x210mm  | 15.4 | 2008 | CFC96 |
| LG Philips   | LPL2F01 | LP154WX4-... | 1280x800  | 330x210mm  | 15.4 | 2008 | C3813 |
| LG Philips   | LPL3101 |              | 1280x800  | 330x210mm  | 15.4 | 2008 | 667F8 |
| LG Philips   | LPL3188 | LP171WP4-... | 1440x900  | 370x230mm  | 17.2 |      | DE7F9 |
| LG Philips   | LPL3701 | LP154WE2-... | 1680x1050 | 330x210mm  | 15.4 | 2008 | 5605E |
| LG Philips   | LPL3B01 | LP154WX4-... | 1280x800  | 330x210mm  | 15.4 | 2007 | B0F69 |
| LG Philips   | LPL3D01 |              | 1280x800  | 330x210mm  | 15.4 | 2008 | 040A6 |
| LG Philips   | LPL5000 | YG365        | 1280x800  | 330x210mm  | 15.4 |      | 1D068 |
| LG Philips   | LPL6500 | LP150X08-... | 1024x768  | 300x220mm  | 14.6 |      | 0841F |
| LG Philips   | LPL7900 | LP154W01-... | 1280x800  | 330x210mm  | 15.4 |      | AFE18 |
| LG Philips   | LPL8D00 | LP141WX1-... | 1280x800  | 300x190mm  | 14.0 | 2007 | C7A52 |
| LG Philips   | LPL8D00 | LP141WX1-... | 1280x800  | 300x190mm  | 14.0 |      | C45CE |
| LG Philips   | LPLA001 | LP171WP4-... | 1440x900  | 370x230mm  | 17.2 | 2007 | AA262 |
| LG Philips   | LPLA002 | LP171WP4-... | 1440x900  | 370x230mm  | 17.2 | 2007 | 64820 |
| LG Philips   | LPLA002 | LP171WP4-... | 1440x900  | 370x230mm  | 17.2 | 2007 | E022A |
| LG Philips   | LPLA101 | LP171WP4-... | 1440x900  | 370x230mm  | 17.2 | 2007 | 84BC2 |
| LG Philips   | LPLA103 | LP171WP4-... | 1440x900  | 370x230mm  | 17.2 | 2007 | 64D0D |
| LG Philips   | LPLA104 | LP171WP4-... | 1440x900  | 370x230mm  | 17.2 | 2007 | 20BAE |
| LG Philips   | LPLA105 | LP171WP4-... | 1440x900  | 370x230mm  | 17.2 | 2007 | 3A1A8 |
| LG Philips   | LPLA106 | LP171WP4-... | 1440x900  | 370x230mm  | 17.2 | 2008 | 381CA |
| LG Philips   | LPLA500 | LP141WX1-... | 1280x800  | 300x190mm  | 14.0 |      | C1354 |
| LG Philips   | LPLA900 | CD516 .>LWz  | 1280x800  | 330x210mm  | 15.4 | 2006 | 4714D |
| LG Philips   | LPLAB00 | LP154WX4-... | 1280x800  | 330x210mm  | 15.4 | 2008 | 38527 |
| LG Philips   | LPLB300 | LP141WX1-... | 1280x800  | 300x190mm  | 14.0 | 2006 | 6219D |
| LG Philips   | LPLB400 | LP141WX1-... | 1280x800  | 300x190mm  | 14.0 | 2006 | B2AB6 |
| LG Philips   | LPLB600 | LP121WX1-... | 1280x800  | 260x160mm  | 12.0 | 2006 | 3B704 |
| LG Philips   | LPLB900 | LP154W01-... | 1280x800  | 330x210mm  | 15.4 | 2006 | C061C |
| LG Philips   | LPLBB00 | LP150X08-... | 1024x768  | 300x220mm  | 14.6 | 2006 | BA98B |
| LG Philips   | LPLBC00 | LP154W01-... | 1280x800  | 330x210mm  | 15.4 | 2006 | 8357F |
| LG Philips   | LPLBD00 | LP154W01-... | 1280x800  | 330x210mm  | 15.4 | 2006 | 8A3EE |
| LG Philips   | LPLC100 | LP154W01-... | 1280x800  | 330x210mm  | 15.4 | 2006 | 88272 |
| LG Philips   | LPLC700 | LP154W01-... | 1280x800  | 330x210mm  | 15.4 | 2006 | D010C |
| LG Philips   | LPLC800 | LP154W01-... | 1280x800  | 330x210mm  | 15.4 | 2006 | DCECF |
| LG Philips   | LPLCA00 | LP154W01-... | 1280x800  | 330x210mm  | 15.4 | 2006 | 20E30 |
| LG Philips   | LPLCB00 | LP154W01-... | 1280x800  | 330x210mm  | 15.4 | 2006 | F80B4 |
| LG Philips   | LPLCD00 | LP154WE2-... | 1680x1050 | 330x210mm  | 15.4 | 2006 | 9886B |
| LG Philips   | LPLD600 | LP154WX4-... | 1280x800  | 330x210mm  | 15.4 | 2006 | FA064 |
| LG Philips   | LPLD800 | LP154WX4-... | 1280x800  | 330x210mm  | 15.4 | 2006 | 918C8 |
| LG Philips   | LPLDA00 | LP171WE3-... | 1680x1050 | 370x230mm  | 17.2 | 2007 | 63E8A |
| LG Philips   | LPLDB00 | LP154WX4-... | 1280x800  | 330x210mm  | 15.4 | 2006 | 6FB6C |
| LG Philips   | LPLDC00 | LP154WX4-... | 1280x800  | 330x210mm  | 15.4 | 2006 | 8012E |
| LG Philips   | LPLDD00 | LP154WX4-... | 1280x800  | 330x210mm  | 15.4 | 2006 | E6144 |
| LG Philips   | LPLDE00 | R781G        | 1680x1050 | 330x210mm  | 15.4 | 2007 | 8843C |
| LG Philips   | LPLDF00 | KR515 #3=He  | 1440x900  | 330x210mm  | 15.4 | 2007 | 4CFF6 |
| LG Philips   | LPLE100 | LP154WX4-... | 1280x800  | 330x210mm  | 15.4 | 2006 | 201AA |
| LG Philips   | LPLE300 | LP154WX4-... | 1280x800  | 330x210mm  | 15.4 | 2006 | 1965E |
| LG Philips   | LPLE400 | MF770 &<HXx  | 1920x1200 | 370x230mm  | 17.2 | 2007 | 74F0C |
| LG Philips   | LPLE400 | RP616        | 1920x1200 | 370x230mm  | 17.2 | 2007 | F3A5A |
| LG Philips   | LPLE600 | LP154WX4-... | 1280x800  | 330x210mm  | 15.4 | 2006 | 423FF |
| LG Philips   | LPLE800 | LP141WX3-... | 1280x800  | 300x190mm  | 14.0 | 2007 | F40D9 |
| LG Philips   | LPLEC00 | LP154WX4-... | 1280x800  | 330x210mm  | 15.4 | 2006 | 5CCB2 |
| LG Philips   | LPLF700 |              | 1920x1200 | 370x230mm  | 17.2 | 2007 | 75F32 |
| Lanix        | LNX5312 | LX900T       | 1440x900  | 410x260mm  | 19.1 | 2011 | BC34F |
| Lenovo       | LEN0017 | A3/V1-E A... | 1440x900  | 420x270mm  | 19.7 | 2017 | 3AF8B |
| Lenovo       | LEN0017 | AIO310       | 1440x900  | 520x320mm  | 24.0 | 2016 | B8904 |
| Lenovo       | LEN0093 | E92I-C       | 1920x1080 | 480x270mm  | 21.7 | 2012 | 3C806 |
| Lenovo       | LEN00A1 | E73Z-D       | 1600x900  | 440x240mm  | 19.7 | 2013 | ABBC9 |
| Lenovo       | LEN0200 |              | 1920x1080 | 510x280mm  | 22.9 | 2011 | 97C90 |
| Lenovo       | LEN0900 | M900-B       | 1920x1080 | 530x300mm  | 24.0 | 2015 | 70A65 |
| Lenovo       | LEN0990 | LT1952p Wide | 1440x900  | 410x260mm  | 19.1 | 2015 | 2E905 |
| Lenovo       | LEN0A0C | LT2252p Wide | 1680x1050 | 470x300mm  | 22.0 | 2012 | 1622B |
| Lenovo       | LEN0A0C |              | 1680x1050 | 470x300mm  | 22.0 | 2012 | 5D9B2 |
| Lenovo       | LEN0A0C |              | 1680x1050 | 470x300mm  | 22.0 | 2011 | FC06D |
| Lenovo       | LEN0A0C |              | 1680x1050 | 470x300mm  | 22.0 | 2010 | 22A80 |
| Lenovo       | LEN0EC2 | LS2023wC     | 1600x900  | 440x250mm  | 19.9 | 2013 | 4B2B1 |
| Lenovo       | LEN1144 |              | 1920x1200 | 520x320mm  | 24.0 | 2011 | 47F58 |
| Lenovo       | LEN114F | L1900pA      | 1280x1024 | 370x300mm  | 18.8 | 2008 | 2E61F |
| Lenovo       | LEN114F | L193pC       | 1280x1024 | 400x320mm  | 20.2 | 2007 | E13F5 |
| Lenovo       | LEN1152 | L197 Wide    | 1440x900  | 410x260mm  | 19.1 | 2012 | DCDCD |
| Lenovo       | LEN1152 | L197 Wide    | 1440x900  | 410x260mm  | 19.1 | 2010 | D0970 |
| Lenovo       | LEN1152 | L194 Wide    | 1440x900  | 410x260mm  | 19.1 | 2008 | C1FC9 |
| Lenovo       | LEN1201 | 24ARR-B L... | 1920x1080 | 530x300mm  | 24.0 | 2017 | EE1DA |
| Lenovo       | LEN1201 | V510Z-B M... | 1920x1080 | 500x300mm  | 23.0 | 2016 | 3D1D6 |
| Lenovo       | LEN1201 | 510I-C MV... | 1920x1080 | 480x260mm  | 21.5 | 2016 | 5D5A9 |
| Lenovo       | LEN1201 | 510A-B LM... | 1920x1080 | 500x300mm  | 23.0 | 2016 | AD00F |
| Lenovo       | LEN1201 | 510I-B LM... | 1920x1080 | 500x300mm  | 23.0 | 2016 | BC5F7 |
| Lenovo       | LEN1201 | 510A-C LM... | 1920x1080 | 480x260mm  | 21.5 | 2016 | DD0AE |
| Lenovo       | LEN1201 | V510Z-B L... | 1920x1080 | 500x300mm  | 23.0 | 2016 | FA338 |
| Lenovo       | LEN1301 | 27ICB-A      | 2560x1440 | 600x330mm  | 27.0 | 2017 | 0BA7E |
| Lenovo       | LEN19DE |              | 1680x1050 | 470x300mm  | 22.0 | 2008 | 4E6F2 |
| Lenovo       | LEN2000 |              | 1600x900  | 440x230mm  | 19.5 | 2013 | 2A326 |
| Lenovo       | LEN2000 |              | 1920x1080 | 480x270mm  | 21.7 | 2013 | 591C3 |
| Lenovo       | LEN2000 |              | 1920x1080 | 510x290mm  | 23.1 | 2013 | 84FFA |
| Lenovo       | LEN2000 |              | 1920x1080 | 480x270mm  | 21.7 | 2012 | 4F7B7 |
| Lenovo       | LEN2000 |              | 1600x900  | 440x250mm  | 19.9 | 2012 | 6B3FD |
| Lenovo       | LEN2000 |              | 1366x768  | 410x230mm  | 18.5 | 2012 | BA097 |
| Lenovo       | LEN2000 |              | 1920x1080 | 510x290mm  | 23.1 | 2012 | E4F5F |
| Lenovo       | LEN240B | L171         | 1280x1024 | 340x270mm  | 17.1 | 2007 | C5427 |
| Lenovo       | LEN4000 | LTD121ECHB   | 1024x768  | 250x180mm  | 12.1 | 2006 | EBB8E |
| Lenovo       | LEN4000 | HT121X01-101 | 1024x768  | 250x180mm  | 12.1 |      | 23702 |
| Lenovo       | LEN4000 | LTN121XJ-L07 | 1024x768  | 250x180mm  | 12.1 |      | A5EDE |
| Lenovo       | LEN4002 | LTN121XP0... | 1024x768  | 250x180mm  | 12.1 | 2007 | C2006 |
| Lenovo       | LEN4002 | HV121X03-100 | 1024x768  | 250x180mm  | 12.1 | 2006 | F5DA9 |
| Lenovo       | LEN4005 | HV121P01-101 | 1400x1050 | 250x180mm  | 12.1 | 2006 | 420EB |
| Lenovo       | LEN4005 | HV121P01-100 | 1400x1050 | 250x180mm  | 12.1 | 2006 | A0C84 |
| Lenovo       | LEN4010 | LTD121EWVB   | 1280x800  | 260x160mm  | 12.0 | 2009 | 3DFE6 |
| Lenovo       | LEN4010 | HV121WX4-120 | 1280x800  | 260x160mm  | 12.0 | 2008 | C763D |
| Lenovo       | LEN4010 | B121EW03 V6  | 1280x800  | 260x160mm  | 12.0 | 2007 | DF3FD |
| Lenovo       | LEN4011 | B121EW09 V3  | 1280x800  | 260x160mm  | 12.0 | 2009 | 0C77A |
| Lenovo       | LEN4011 | LP121WX3-... | 1280x800  | 260x160mm  | 12.0 | 2009 | 2E9F1 |
| Lenovo       | LEN4011 | LTN121AP0... | 1280x800  | 260x160mm  | 12.0 | 2009 | 95917 |
| Lenovo       | LEN4011 | LTN121AT0... | 1280x800  | 260x160mm  | 12.0 | 2009 | BE2C7 |
| Lenovo       | LEN4011 | HV121WX6-110 | 1280x800  | 260x160mm  | 12.0 | 2009 | CAE68 |
| Lenovo       | LEN4011 | LTN121AP0... | 1280x800  | 260x160mm  | 12.0 | 2008 | 3A651 |
| Lenovo       | LEN4014 | LTD121EQ3B   | 1440x900  | 260x160mm  | 12.0 | 2009 | 05C1B |
| Lenovo       | LEN4020 | HT14X1B-201  | 1024x768  | 280x210mm  | 13.8 |      | 8048E |
| Lenovo       | LEN4020 | LTN141XA-L01 | 1024x768  | 290x210mm  | 14.1 |      | 8A1DB |
| Lenovo       | LEN4022 | LTD141EN9B   | 1400x1050 | 290x210mm  | 14.1 | 2007 | 57325 |
| Lenovo       | LEN4022 | LTN141P4-L02 | 1400x1050 | 290x210mm  | 14.1 |      | 110E3 |
| Lenovo       | LEN4031 | LTN141W1-L05 | 1280x800  | 300x190mm  | 14.0 | 2007 | 18FBE |
| Lenovo       | LEN4031 | LP141WX3-... | 1280x800  | 300x190mm  | 14.0 | 2007 | 349C0 |
| Lenovo       | LEN4031 | LP141WX3-... | 1280x800  | 300x190mm  | 14.0 | 2007 | 7FFEF |
| Lenovo       | LEN4031 | B141EW02 V4  | 1280x800  | 300x190mm  | 14.0 | 2006 | 1CB7A |
| Lenovo       | LEN4031 | N141I3-L03   | 1280x800  | 300x190mm  | 14.0 | 2006 | F7172 |
| Lenovo       | LEN4033 | N141C3-L07   | 1440x900  | 300x190mm  | 14.0 | 2008 | E8039 |
| Lenovo       | LEN4033 | AUOB141PW... | 1440x900  | 300x190mm  | 14.0 | 2007 | 17CB3 |
| Lenovo       | LEN4033 | LP141WP1-... | 1440x900  | 300x190mm  | 14.0 | 2007 | C9105 |
| Lenovo       | LEN4033 | LTN141WD-L05 | 1440x900  | 300x190mm  | 14.0 |      | 0DD7E |
| Lenovo       | LEN4035 | LP141WX5-... | 1280x800  | 300x190mm  | 14.0 | 2009 | 2F4AA |
| Lenovo       | LEN4035 | LTN141AT1... | 1280x800  | 300x190mm  | 14.0 | 2009 | 6047E |
| Lenovo       | LEN4035 | LTN141AT1... | 1280x800  | 300x190mm  | 14.0 | 2009 | 7A761 |
| Lenovo       | LEN4035 | LTN141AT1... | 1280x800  | 300x190mm  | 14.0 | 2009 | D0A01 |
| Lenovo       | LEN4035 | LP141WX5-... | 1280x800  | 300x190mm  | 14.0 | 2009 | EBB60 |
| Lenovo       | LEN4036 | LT141DEQ8B00 | 1440x900  | 300x190mm  | 14.0 | 2010 | BC99D |
| Lenovo       | LEN4036 | B141PW04 V0  | 1440x900  | 300x190mm  | 14.0 | 2009 | 563B2 |
| Lenovo       | LEN4036 | LP141WP3-... | 1440x900  | 300x190mm  | 14.0 | 2009 | CC67B |
| Lenovo       | LEN4036 | LTN141BT0... | 1440x900  | 300x190mm  | 14.0 | 2009 | D8CC4 |
| Lenovo       | LEN4036 | LTN141BT0... | 1440x900  | 300x190mm  | 14.0 | 2008 | C99A6 |
| Lenovo       | LEN4036 | LTN141BT0... | 1440x900  | 300x190mm  | 14.0 | 2007 | 0E0E5 |
| Lenovo       | LEN4037 | LTN141AT0... | 1280x800  | 300x190mm  | 14.0 | 2008 | EAF57 |
| Lenovo       | LEN4040 | LTN150XF-L03 | 1024x768  | 300x230mm  | 14.9 |      | 793AF |
| Lenovo       | LEN4043 | LP150E05-A2  | 1400x1050 | 300x230mm  | 14.9 |      | 9E599 |
| Lenovo       | LEN4050 | B154EW08 V0  | 1280x800  | 330x210mm  | 15.4 | 2008 | 1E4B2 |
| Lenovo       | LEN4050 | LTN154AT0... | 1280x800  | 330x210mm  | 15.4 | 2008 | F77FD |
| Lenovo       | LEN4050 | 154WX5-TLB2  | 1280x800  | 330x210mm  | 15.4 | 2007 | 533A3 |
| Lenovo       | LEN4050 | N154I3-L02   | 1280x800  | 330x210mm  | 15.4 | 2007 | 5E75A |
| Lenovo       | LEN4050 | 154WX4-TLC6  | 1280x800  | 330x210mm  | 15.4 | 2007 | B5944 |
| Lenovo       | LEN4050 | 154WX4-TLA6  | 1280x800  | 330x210mm  | 15.4 | 2007 | BE173 |
| Lenovo       | LEN4050 | B154EW02 V6  | 1280x800  | 330x210mm  | 15.4 | 2006 | 3EDA1 |
| Lenovo       | LEN4050 | LTN154X3-L02 | 1280x800  | 330x210mm  | 15.4 | 2006 | 47D06 |
| Lenovo       | LEN4050 | 154W01-TLE5  | 1280x800  | 330x210mm  | 15.4 | 2006 | D218B |
| Lenovo       | LEN4050 | LTN154X3-L0A | 1280x800  | 330x210mm  | 15.4 |      | A111C |
| Lenovo       | LEN4053 | B154SW01 V9  | 1680x1050 | 330x210mm  | 15.4 | 2007 | 106DA |
| Lenovo       | LEN4053 | LTN154P3-L02 | 1680x1050 | 330x210mm  | 15.4 | 2007 | 79062 |
| Lenovo       | LEN4055 | LTN154U2-L05 | 1920x1200 | 330x210mm  | 15.4 | 2007 | 153E6 |
| Lenovo       | LEN4055 | LP154WU1-... | 1920x1200 | 330x210mm  | 15.4 | 2007 | 3D9EC |
| Lenovo       | LEN4055 | LTN154U2-L05 | 1920x1200 | 330x210mm  | 15.4 | 2006 | B2D4B |
| Lenovo       | LEN4057 | LP154WX7-... | 1280x800  | 330x210mm  | 15.4 | 2009 | 78C45 |
| Lenovo       | LEN4057 | LP154WX7-... | 1280x800  | 330x210mm  | 15.4 | 2009 | AC53E |
| Lenovo       | LEN4057 | 154WX7-TLA2  | 1280x800  | 330x210mm  | 15.4 | 2008 | DDD34 |
| Lenovo       | LEN4057 | LTN154AT1... | 1280x800  | 330x210mm  | 15.4 | 2008 | F6F08 |
| Lenovo       | LEN406A | LTN170CT0... | 1920x1200 | 370x230mm  | 17.2 | 2009 | 2790E |
| Lenovo       | LEN4074 | LTD133EQ1B   | 1440x900  | 290x180mm  | 13.4 | 2009 | 1C0AE |
| Lenovo       | LEN4074 | LTD133EQ1B   | 1440x900  | 290x180mm  | 13.4 | 2008 | B2656 |
| Lenovo       | LEN4090 | B133XW03 V0  | 1366x768  | 290x160mm  | 13.0 | 2010 | A23E5 |
| Lenovo       | LEN4090 | B133XW01 V0  | 1366x768  | 290x160mm  | 13.0 | 2009 | 64C64 |
| Lenovo       | LEN40A0 | LTN140AT1... | 1366x768  | 310x170mm  | 13.9 | 2010 | 13158 |
| Lenovo       | LEN40A0 | LTN140AT1... | 1366x768  | 310x170mm  | 13.9 | 2010 | 14ED4 |
| Lenovo       | LEN40A0 | LP140WH1-... | 1366x768  | 310x170mm  | 13.9 | 2010 | A9645 |
| Lenovo       | LEN40A0 | B140XW01 V7  | 1366x768  | 310x170mm  | 13.9 | 2009 | 0FA8D |
| Lenovo       | LEN40A0 | LTN140AT0... | 1366x768  | 310x170mm  | 13.9 | 2009 | 30813 |
| Lenovo       | LEN40A0 | LTN140AT0... | 1366x768  | 310x170mm  | 13.9 | 2009 | 30C5F |
| Lenovo       | LEN40A0 | B140XW01 V6  | 1366x768  | 310x170mm  | 13.9 | 2009 | 5F071 |
| Lenovo       | LEN40A0 | LTN140AT0... | 1366x768  | 310x170mm  | 13.9 | 2009 | 783E3 |
| Lenovo       | LEN40A1 | B140RW02 V1  | 1600x900  | 310x170mm  | 13.9 | 2010 | 12B1C |
| Lenovo       | LEN40B0 | SAMSUNG L... | 1366x768  | 340x190mm  | 15.3 | 2010 | 0E971 |
| Lenovo       | LEN40B0 | LP156WH4-... | 1366x768  | 350x190mm  | 15.7 | 2010 | 335BA |
| Lenovo       | LEN40B0 | LP156WH2-... | 1366x768  | 350x190mm  | 15.7 | 2010 | 4421A |
| Lenovo       | LEN40B0 | B156XW02 V3  | 1366x768  | 340x190mm  | 15.3 | 2009 | 21941 |
| Lenovo       | LEN40B0 | LTN156AT0... | 1366x768  | 340x190mm  | 15.3 | 2009 | 61C96 |
| Lenovo       | LEN40B0 | LTN156AT0... | 1366x768  | 340x190mm  | 15.3 | 2009 | 66F94 |
| Lenovo       | LEN40B0 | LP156WH2-... | 1366x768  | 350x190mm  | 15.7 | 2009 | 8024A |
| Lenovo       | LEN40B0 | LP156WH2-... | 1366x768  | 350x190mm  | 15.7 | 2009 | 98734 |
| Lenovo       | LEN40B0 | B156XW02 V2  | 1366x768  | 340x190mm  | 15.3 | 2009 | 99666 |
| Lenovo       | LEN40B0 | LTN156AT0... | 1366x768  | 340x190mm  | 15.3 | 2009 | C0A73 |
| Lenovo       | LEN40B0 | LTN156AT0... | 1366x768  | 340x190mm  | 15.3 | 2009 | D0057 |
| Lenovo       | LEN40B0 | N156B6-L0A   | 1366x768  | 350x190mm  | 15.7 | 2009 | D0C12 |
| Lenovo       | LEN40B0 | LP156WH2-... | 1366x768  | 350x190mm  | 15.7 | 2008 | 5A269 |
| Lenovo       | LEN40B1 | LTN156KT0... | 1600x900  | 340x190mm  | 15.3 | 2010 | 64881 |
| Lenovo       | LEN40B1 | LP156WD1-... | 1600x900  | 350x190mm  | 15.7 | 2009 | 074FA |
| Lenovo       | LEN40B1 | B156RW01 V1  | 1600x900  | 340x190mm  | 15.3 | 2008 | B3734 |
| Lenovo       | LEN40B2 | B156HW01 V4  | 1920x1080 | 340x190mm  | 15.3 | 2009 | 742CA |
| Lenovo       | LEN40BA | LP156WFC-... | 1920x1080 | 340x190mm  | 15.3 | 2018 | ED275 |
| Lenovo       | LEN40BA | LP156WF9-... | 1920x1080 | 340x190mm  | 15.3 | 2017 | 86D28 |
| Lenovo       | LEN40D1 | B116XW02 V1  | 1366x768  | 260x140mm  | 11.6 | 2009 | 03657 |
| Lenovo       | LEN40D1 | LP116WH1-... | 1366x768  | 260x140mm  | 11.6 | 2009 | A682C |
| Lenovo       | LEN40D1 | LTN116AT0... | 1366x768  | 260x140mm  | 11.6 | 2009 | B2FB0 |
| Lenovo       | LEN4140 | ATNA40JU01-0 | 2560x1440 | 310x180mm  | 14.1 | 2015 | AAD0B |
| Lenovo       | LEN4BD9 | L171p        | 1280x1024 | 360x300mm  | 18.4 | 2007 | C33DC |
| Lenovo       | LEN60A8 | LT2423wC     | 1920x1080 | 530x300mm  | 24.0 | 2014 | 11CD8 |
| Lenovo       | LEN60B0 | E2323swA     | 1920x1080 | 530x310mm  | 24.2 | 2014 | 2E09A |
| Lenovo       | LEN60C8 | T2424pA      | 1920x1080 | 530x300mm  | 24.0 | 2017 | AC0EC |
| Lenovo       | LEN60C9 | T2454pA      | 1920x1200 | 530x300mm  | 24.0 | 2015 | 71AD4 |
| Lenovo       | LEN60CA | T2224pD      | 1920x1080 | 480x270mm  | 21.7 | 2016 | 770AA |
| Lenovo       | LEN61A6 | T24i-10      | 1920x1080 | 530x300mm  | 24.0 | 2017 | 30441 |
| Lenovo       | LEN61AE | P24h-10      | 2560x1440 | 530x300mm  | 24.0 | 2018 | 5B55B |
| Lenovo       | LEN61AF | P27h-10      | 2560x1440 | 600x340mm  | 27.2 | 2018 | A2AF6 |
| Lenovo       | LEN61C7 | S27i-10      | 1920x1080 | 600x340mm  | 27.2 | 2018 | 1A3EC |
| Lenovo       | LEN65C5 | LI2264d      | 1920x1080 | 480x270mm  | 21.7 | 2017 | 1E270 |
| Lenovo       | LEN65C5 | LI2264d      | 1920x1080 | 480x270mm  | 21.7 | 2016 | B42CA |
| Lenovo       | LEN65C7 | LI2364       | 1920x1080 | 510x290mm  | 23.1 | 2018 | BE942 |
| Lenovo       | LEN65CF | L24q-10      | 2560x1440 | 530x300mm  | 24.0 | 2017 | 42832 |
| Lenovo       | LEN65D6 | L24i-10      | 1920x1080 | 530x300mm  | 24.0 | 2018 | A1120 |
| Lenovo       | LEN8000 | LBG AIO PC   | 1600x900  | 440x230mm  | 19.5 | 2015 | 6146D |
| Lenovo       | LEN8000 | LBG AIO PC   | 1920x1080 | 520x290mm  | 23.4 | 2014 | 2D671 |
| Lenovo       | LEN8000 | LBG AIO PC   | 1920x1080 | 480x270mm  | 21.7 | 2014 | 4E8FC |
| Lenovo       | LEN8000 | LBG AIO PC   | 1920x1080 | 440x230mm  | 19.5 | 2014 | BE7C7 |
| Lenovo       | LENA700 | A700         | 1920x1080 | 510x290mm  | 23.1 | 2010 | 818E0 |
| Lenovo       | LENFFFF | LEN-S310     | 1366x768  | 430x250mm  | 19.6 | 2012 | AD785 |
| Lenovo       | QWA3602 | B320         | 1920x1080 | 510x290mm  | 23.1 | 2010 | FAA6D |
| MEK          | MEK0030 | LCD          | 1600x1200 | 700x390mm  | 31.5 | 2006 | 36596 |
| MSI          | MSI0000 | AIO PC AI... | 1600x900  | 440x250mm  | 19.9 | 2013 | 607AB |
| MSI          | MSI0001 | AIO PC       | 1600x900  | 440x240mm  | 19.7 | 2017 | A9AE0 |
| MSI          | MSI0001 | AIO PC       | 1920x1080 | 520x290mm  | 23.4 | 2013 | 81595 |
| MSI          | MSI0001 | AIO PC       | 1920x1080 | 480x270mm  | 21.7 | 2013 | DA51A |
| MSI          | MSI1462 |              | 2560x1440 | 590x350mm  | 27.0 | 2017 | 4BE63 |
| MSI          | MSI2382 | 00 AIO PC    | 1600x900  | 440x250mm  | 19.9 | 2010 | 0348D |
| MSI          | MSI2482 |              | 1920x1080 | 470x260mm  | 21.1 | 2010 | 315C8 |
| MSI          | MSI3EA2 | MAG241C      | 1920x1080 | 520x290mm  | 23.4 | 2019 | D5B9D |
| MSI          | MSI7459 | 00 Wind Net  | 1600x900  | 440x250mm  | 19.9 | 2009 | 890A4 |
| MSI          | MSID063 | AIO PC AI... | 1920x1080 | 480x270mm  | 21.7 | 2013 | CF737 |
| MStar        | MST0000 | demo         | 1920x1080 | 430x270mm  | 20.0 | 2007 | 557A8 |
| MStar        | MST0030 | LED TV       | 1920x1080 | 1150x650mm | 52.0 | 2018 | 251B0 |
| MStar        | MST0030 | Demo         | 3840x2160 | 1150x650mm | 52.0 | 2014 | E5670 |
| MStar        | MST0030 | Demo         | 1920x1080 | 1150x650mm | 52.0 | 2012 | 15BFC |
| MStar        | MST0030 | TV_MONITOR   | 1920x1080 | 1150x650mm | 52.0 | 2012 | D3C64 |
| MStar        | MST0030 | TV           | 1920x1080 | 1150x650mm | 52.0 | 2012 | F155D |
| MStar        | MST0030 | Demo         | 1920x540  | 1150x650mm | 52.0 | 2007 | 0602B |
| MStar        | MST1111 | OptiPlex ... | 1920x1080 | 510x290mm  | 23.1 | 2011 | 6E9B6 |
| Marantz      | MJI0024 | AVR          | 1920x1080 | 950x540mm  | 43.0 | 2012 | 97DFC |
| Mecer        | MUS9890 | TJ899        | 1280x1024 | 370x300mm  | 18.8 | 2008 | 521B7 |
| Medion       | MEA5107 | MD20831      | 1920x1080 | 530x300mm  | 24.0 | 2016 | F1DFC |
| Medion       | MED3604 | MD 20110     | 1920x1080 | 520x300mm  | 23.6 | 2009 | 6EAA9 |
| Medion       | MED3608 | MD 20120     | 1920x1080 | 520x300mm  | 23.6 | 2010 | 448FB |
| Medion       | MED3608 | MD 20120     | 1920x1080 | 520x300mm  | 23.6 | 2009 | 9FE4A |
| Medion       | MED3610 | MD 20094     | 1920x1200 | 550x340mm  | 25.5 | 2009 | 537A3 |
| Medion       | MED3621 | MD 20086     | 1680x1050 | 480x300mm  | 22.3 | 2010 | BCDBC |
| Medion       | MED3624 | MD 20165     | 1920x1080 | 480x280mm  | 21.9 | 2010 | 75DAB |
| Medion       | MED3634 | MD 20144     | 1920x1080 | 510x290mm  | 23.1 | 2011 | 21C8D |
| Medion       | MED3640 | MD20147      | 1920x1080 | 600x340mm  | 27.2 | 2010 | BA650 |
| Medion       | MED3659 | MD 20333     | 1920x1080 | 510x290mm  | 23.1 | 2012 | 01B35 |
| Medion       | MED3672 | MD20666      | 1920x1080 | 530x310mm  | 24.2 | 2012 | 5EA3B |
| Medion       | MED3682 | MD 20888     | 1920x1080 | 510x290mm  | 23.1 | 2012 | 99431 |
| Medion       | MED3688 | MD 20889     | 1920x1080 | 510x290mm  | 23.1 | 2012 | 88168 |
| Medion       | MED36A6 | MD 20431     | 1920x1080 | 520x290mm  | 23.4 | 2013 | 452E5 |
| Medion       | MED36B2 | MD20433      | 1920x1080 | 520x290mm  | 23.4 | 2013 | 71A38 |
| Medion       | MED36B3 | MD20433      | 1920x1080 | 520x290mm  | 23.4 | 2014 | 93E01 |
| Medion       | MED36C2 | MD21290      | 1920x1080 | 600x340mm  | 27.2 | 2014 | 10363 |
| Medion       | MED36D6 | MD20435      | 1920x1080 | 520x290mm  | 23.4 | 2014 | EE501 |
| Medion       | MED3910 | MD 20119     | 1440x900  | 410x260mm  | 19.1 | 2009 | 5346C |
| Medion       | MED4601 | MD20461      | 1920x1080 | 520x290mm  | 23.4 | 2013 | F3EC5 |
| Medion       | MED4602 | MD20461      | 1920x1080 | 520x290mm  | 23.4 | 2013 | 56516 |
| Medion       | MED86F6 | MD30422PV    | 1680x1050 | 470x300mm  | 22.0 | 2009 | 2332D |
| Medion       | MED89C3 | MD30999PD    | 1440x900  | 410x260mm  | 19.1 | 2007 | CD345 |
| Medion       | MED89ED | MD30699PU    | 1280x1024 | 380x300mm  | 19.1 | 2006 | E8329 |
| Medion Akoya | MEC7203 | MD21851      | 1920x1080 | 600x330mm  | 27.0 | 2017 | 665B7 |
| Medion Akoya | MEC7204 | MD21850      | 1920x1080 | 600x330mm  | 27.0 | 2017 | 7CBB5 |
| Medion Akoya | MEC7204 | MD21851      | 1920x1080 | 600x330mm  | 27.0 | 2017 | E4D11 |
| Mitac        | MTC0001 | KIVI TV      | 3840x2160 | 1220x690mm | 55.2 | 2018 | CBE4F |
| Mitac        | MTC0030 | LED TV       | 1920x1080 | 1150x650mm | 52.0 | 2014 | 0444C |
| Mitac        | MTC0B01 | LCD-TV       | 1920x540  | 700x390mm  | 31.5 | 2010 | 7A77B |
| Mitac        | MTC0B01 | MTC26T42     | 1920x540  | 700x390mm  | 31.5 | 2008 | 244FE |
| Mitac        | MTC9527 | TV           | 1920x1080 | 1150x650mm | 52.0 | 2011 | 2DAF9 |
| Mitac        | MTC9527 | TV           | 1366x768  | 1150x650mm | 52.0 | 2011 | DAFA8 |
| Mitac        | SZM0001 | LEDTV        | 1920x1080 | 1150x650mm | 52.0 | 2016 | 3F083 |
| Mitac        | SZM0308 | DSGi TV      | 1920x540  | 700x400mm  | 31.7 | 2016 | 1A397 |
| Mitac        | SZM0B01 | MTC26T42     | 1920x540  | 700x390mm  | 31.5 | 2014 | 0F9C0 |
| Mitsubishi   | MEL4743 | RDT1713VM    | 1280x1024 | 340x270mm  | 17.1 | 2006 | 90A27 |
| Mitsubishi   | MEL4758 | RDT201L      | 1600x1200 | 410x310mm  | 20.2 | 2007 | EDFFA |
| NEC          | NEC2B1D | EA244UHD     | 3840x2160 | 530x300mm  | 24.0 | 2016 | 8D857 |
| NEC          | NEC2BE5 | E233WM       | 1920x1080 | 510x290mm  | 23.1 | 2016 | E2D5B |
| NEC          | NEC2C63 | E245WMi      | 1920x1200 | 520x320mm  | 24.0 | 2017 | 058FD |
| NEC          | NEC65D4 | LCD1880SX    | 1280x1024 | 360x290mm  | 18.2 |      | D8095 |
| NEC          | NEC6604 |              | 1280x1024 | 340x270mm  | 17.1 |      | 0023A |
| NEC          | NEC6605 | LCD2080UX    | 1600x1200 | 410x310mm  | 20.2 |      | 47E7D |
| NEC          | NEC660B |              | 1280x1024 | 380x300mm  | 19.1 |      | 5793F |
| NEC          | NEC6626 |              | 1280x1024 | 380x300mm  | 19.1 |      | 8D287 |
| NEC          | NEC662C | LCD2180UX    | 1600x1200 | 430x320mm  | 21.1 |      | 57695 |
| NEC          | NEC6633 | LCD1960NXi   | 1280x1024 | 380x300mm  | 19.1 |      | 54670 |
| NEC          | NEC6659 | LCD72VM      | 1280x1024 | 340x270mm  | 17.1 |      | AAB1E |
| NEC          | NEC6662 | LCD1970NX    | 1280x1024 | 380x300mm  | 19.1 | 2008 | 17B8C |
| NEC          | NEC6662 | LCD1970NX    | 1280x1024 | 380x300mm  | 19.1 | 2007 | E7A55 |
| NEC          | NEC6662 | LCD1970NX    | 1280x1024 | 380x300mm  | 19.1 | 2006 | 946FB |
| NEC          | NEC6662 | LCD1970NX    | 1280x1024 | 380x300mm  | 19.1 |      | A19FB |
| NEC          | NEC6664 | LCD1770NX    | 1280x1024 | 340x270mm  | 17.1 |      | 91CDC |
| NEC          | NEC6665 | LCD1770NX    | 1280x1024 | 340x270mm  | 17.1 | 2008 | E5D65 |
| NEC          | NEC6665 | LCD1770NX    | 1280x1024 | 340x270mm  | 17.1 | 2006 | FB4A8 |
| NEC          | NEC6665 | LCD1770NX    | 1280x1024 | 340x270mm  | 17.1 |      | 05E1E |
| NEC          | NEC6667 | LCD1770NXM   | 1280x1024 | 340x270mm  | 17.1 | 2007 | F0F32 |
| NEC          | NEC667B | LCD2070NX    | 1600x1200 | 410x310mm  | 20.2 | 2007 | 81901 |
| NEC          | NEC668E | LCD1970NXp   | 1280x1024 | 380x300mm  | 19.1 | 2008 | 23FE6 |
| NEC          | NEC668E | LCD1970NXp   | 1280x1024 | 380x300mm  | 19.1 | 2006 | 351BD |
| NEC          | NEC6691 | 70GX2        | 1280x1024 | 340x270mm  | 17.1 |      | 88712 |
| NEC          | NEC6692 | 90GX2        | 1280x1024 | 380x300mm  | 19.1 | 2007 | 0FA21 |
| NEC          | NEC6692 | 90GX2        | 1280x1024 | 380x300mm  | 19.1 | 2006 | 7C6D0 |
| NEC          | NEC6699 | 20WGX2       | 1680x1050 | 430x270mm  | 20.0 | 2008 | C22FB |
| NEC          | NEC6699 | 20WGX2       | 1680x1050 | 430x270mm  | 20.0 | 2007 | 6DAEE |
| NEC          | NEC6699 | 20WGX2       | 1680x1050 | 430x270mm  | 20.0 | 2006 | 19E5F |
| NEC          | NEC66AC | LCD1990SXi   | 1280x1024 | 380x300mm  | 19.1 | 2007 | 1837D |
| NEC          | NEC66AC | LCD1990SXi   | 1280x1024 | 380x300mm  | 19.1 | 2006 | E63DD |
| NEC          | NEC66B0 | LCD2090UXi   | 1600x1200 | 410x310mm  | 20.2 | 2013 | 58E36 |
| NEC          | NEC66B0 | LCD2090UXi   | 1600x1200 | 410x310mm  | 20.2 | 2009 | 72BA9 |
| NEC          | NEC66B0 | LCD2090UXi   | 1600x1200 | 410x310mm  | 20.2 | 2008 | 8F051 |
| NEC          | NEC66B0 | LCD2090UXi   | 1600x1200 | 410x310mm  | 20.2 | 2007 | A5DA7 |
| NEC          | NEC66B8 | LCD1990FX    | 1280x1024 | 380x300mm  | 19.1 | 2007 | 21FEB |
| NEC          | NEC66BA | LCD2070WNX   | 1680x1050 | 430x270mm  | 20.0 | 2007 | A252C |
| NEC          | NEC66BC | LCD2190UXp   | 1600x1200 | 430x320mm  | 21.1 | 2011 | A22CD |
| NEC          | NEC66C0 | LCD175VXM+   | 1280x1024 | 340x270mm  | 17.1 | 2006 | 161B6 |
| NEC          | NEC66C1 | LCD195VXM+   | 1280x1024 | 380x300mm  | 19.1 | 2008 | 91E4A |
| NEC          | NEC66C1 | LCD195VXM+   | 1280x1024 | 380x300mm  | 19.1 | 2007 | 04126 |
| NEC          | NEC66C1 | LCD195VXM+   | 1280x1024 | 380x300mm  | 19.1 | 2006 | 370D4 |
| NEC          | NEC66C2 | LCD73V       | 1280x1024 | 340x270mm  | 17.1 | 2007 | F8BFC |
| NEC          | NEC66C2 | LCD73V       | 1280x1024 | 340x270mm  | 17.1 | 2006 | B5DC1 |
| NEC          | NEC66C6 | LCD2070VX    | 1600x1200 | 410x310mm  | 20.2 | 2007 | 02865 |
| NEC          | NEC66C9 | LCD1990FXp   | 1280x1024 | 380x300mm  | 19.1 | 2008 | CBA93 |
| NEC          | NEC66C9 | LCD1990FXp   | 1280x1024 | 380x300mm  | 19.1 | 2007 | 6F68C |
| NEC          | NEC66C9 | LCD1990FXp   | 1280x1024 | 380x300mm  | 19.1 | 2006 | FE76A |
| NEC          | NEC66CB | LCD2690WUXi  | 1920x1200 | 550x340mm  | 25.5 | 2008 | 62882 |
| NEC          | NEC66DB | LCD1990SX    | 1280x1024 | 380x300mm  | 19.1 | 2007 | 5EB5A |
| NEC          | NEC6711 | LCD225WXM    | 1680x1050 | 470x300mm  | 22.0 | 2008 | AE92A |
| NEC          | NEC671F | LCD22WMGX    | 1680x1050 | 470x300mm  | 22.0 | 2008 | B45D7 |
| NEC          | NEC6720 | LCD24WMCX    | 1920x1200 | 520x320mm  | 24.0 | 2008 | DAF57 |
| NEC          | NEC6728 | LCD195NX     | 1280x1024 | 380x300mm  | 19.1 | 2008 | 07021 |
| NEC          | NEC672F | LCD225WNX    | 1680x1050 | 470x300mm  | 22.0 | 2008 | 6652E |
| NEC          | NEC672F | LCD225WNX    | 1680x1050 | 470x300mm  | 22.0 | 2007 | 386DF |
| NEC          | NEC6735 | LCD73VXM     | 1280x1024 | 340x270mm  | 17.1 | 2009 | 0C4D4 |
| NEC          | NEC673E | EA191M       | 1280x1024 | 380x300mm  | 19.1 | 2009 | 83C22 |
| NEC          | NEC674A | P221W        | 1680x1050 | 470x300mm  | 22.0 | 2011 | D77B9 |
| NEC          | NEC674F | EA241WM      | 1920x1200 | 520x320mm  | 24.0 | 2011 | ED3CF |
| NEC          | NEC674F | EA241WM      | 1920x1200 | 520x320mm  | 24.0 | 2010 | A83D5 |
| NEC          | NEC674F | EA241WM      | 1920x1200 | 520x320mm  | 24.0 | 2009 | E5E72 |
| NEC          | NEC6750 | EA261WM      | 1920x1200 | 560x350mm  | 26.0 | 2009 | 210C9 |
| NEC          | NEC6752 | 24WMGX3      | 1920x1200 | 520x320mm  | 24.0 | 2008 | 90186 |
| NEC          | NEC6774 | F23W1A       | 1920x1080 | 510x290mm  | 23.1 | 2009 | 3BA30 |
| NEC          | NEC6778 | EA221WMe     | 1680x1050 | 470x300mm  | 22.0 | 2010 | 1BB20 |
| NEC          | NEC6778 | EA221WMe     | 1680x1050 | 470x300mm  | 22.0 | 2009 | 1A564 |
| NEC          | NEC6779 | EA231WMi     | 1920x1080 | 510x290mm  | 23.1 | 2010 | 95A14 |
| NEC          | NEC6779 | EA231WMi     | 1920x1080 | 510x290mm  | 23.1 | 2009 | AB600 |
| NEC          | NEC6788 | P461         | 1920x1080 | 1020x570mm | 46.0 | 2010 | 397D9 |
| NEC          | NEC678C | LCD2490WUXi2 | 1920x1200 | 520x320mm  | 24.0 | 2011 | 0978F |
| NEC          | NEC67CF | PA241W       | 1920x1200 | 520x320mm  | 24.0 | 2012 | 57589 |
| NEC          | NEC67CF | PA241W       | 1920x1200 | 520x320mm  | 24.0 | 2011 | 26014 |
| NEC          | NEC67D1 | PA241W       | 1920x1200 | 520x320mm  | 24.0 | 2011 | 806C4 |
| NEC          | NEC67DA | PA271W       | 2560x1440 | 600x340mm  | 27.2 | 2011 | F572A |
| NEC          | NEC67E0 | AS231WM      | 1920x1080 | 510x290mm  | 23.1 | 2011 | 38891 |
| NEC          | NEC67E4 | PA231W       | 1920x1080 | 510x290mm  | 23.1 | 2011 | 44458 |
| NEC          | NEC67EA | E231W        | 1920x1080 | 510x290mm  | 23.1 | 2014 | 202A0 |
| NEC          | NEC67EB | E231W        | 1920x1080 | 510x290mm  | 23.1 | 2010 | A5757 |
| NEC          | NEC680A | EX231W       | 1920x1080 | 510x290mm  | 23.1 | 2012 | 7A9BE |
| NEC          | NEC680D | EA192M       | 1280x1024 | 380x300mm  | 19.1 | 2011 | 56936 |
| NEC          | NEC680E | EA192M       | 1280x1024 | 380x300mm  | 19.1 | 2013 | 11757 |
| NEC          | NEC6814 | EA232WMi     | 1920x1080 | 510x290mm  | 23.1 | 2011 | 88E92 |
| NEC          | NEC6815 | EA232WMi     | 1920x1080 | 510x290mm  | 23.1 | 2011 | 006DE |
| NEC          | NEC681B | VE2402X      | 1920x1080 | 530x300mm  | 24.0 | 2012 | 87443 |
| NEC          | NEC684E | EX231Wp      | 1920x1080 | 510x290mm  | 23.1 | 2011 | 250A0 |
| NEC          | NEC685B | P241W        | 1920x1200 | 520x320mm  | 24.0 | 2012 | 0B7A5 |
| NEC          | NEC6864 | EA243WM      | 1920x1200 | 520x320mm  | 24.0 | 2012 | 7FB65 |
| NEC          | NEC6865 | EA243WM      | 1920x1200 | 520x320mm  | 24.0 | 2013 | 7D8C8 |
| NEC          | NEC6865 | EA243WM      | 1920x1200 | 520x320mm  | 24.0 | 2012 | 1171B |
| NEC          | NEC6866 | EA243WM      | 1920x1200 | 520x320mm  | 24.0 | 2013 | E8E2F |
| NEC          | NEC6866 | EA243WM      | 1920x1200 | 520x320mm  | 24.0 | 2012 | 1AA49 |
| NEC          | NEC6885 | AS241W       | 1920x1080 | 520x290mm  | 23.4 | 2012 | A829D |
| NEC          | NEC68C1 | EA224WMi     | 1920x1080 | 480x270mm  | 21.7 | 2016 | 236E1 |
| NEC          | NEC68C2 | EA224WMi     | 1920x1080 | 480x270mm  | 21.7 | 2013 | 08163 |
| NEC          | NEC68C2 | EA224WMi     | 1920x1080 | 480x270mm  | 21.7 | 2012 | 51CC4 |
| NEC          | NEC68D5 | EA244WMi     | 1920x1200 | 520x320mm  | 24.0 | 2013 | F1B70 |
| NEC          | NEC68D7 | EA244WMi     | 1920x1200 | 520x320mm  | 24.0 | 2014 | BAAA3 |
| NEC          | NEC68D7 | EA244WMi     | 1920x1200 | 520x320mm  | 24.0 | 2013 | 8A836 |
| NEC          | NEC6907 | AS222WM      | 1920x1080 | 480x270mm  | 21.7 | 2016 | C1943 |
| NEC          | NEC691F | EA234WMi     | 1920x1080 | 510x290mm  | 23.1 | 2013 | 53BF3 |
| NEC          | NEC6920 | EA234WMi     | 1920x1080 | 510x290mm  | 23.1 | 2016 | 41C56 |
| NEC          | NEC6947 | PA272W       | 1920x1080 | 600x340mm  | 27.2 | 2015 | A39C4 |
| NEC          | NEC6957 | EA193Mi      | 1280x1024 | 380x300mm  | 19.1 | 2013 | 5B9EF |
| NEC          | NEC695B | EA273WMi     | 1920x1080 | 600x340mm  | 27.2 | 2014 | 32225 |
| NEC          | NEC7A1B | NP-M311X     | 1920x1080 |            |      | 2014 | 82E47 |
| NEC          | NECA4C3 | PX-42VR5     | 1920x540  | 920x520mm  | 41.6 |      | DC7AC |
| Nvidia       | NVD0000 |              | 1920x1080 | 320x180mm  | 14.5 |      | 59261 |
| Nvidia       | NVD0200 |              | 1280x768  | 320x190mm  | 14.7 |      | 3F343 |
| Nvidia       | NVD0200 |              | 1280x800  | 320x200mm  | 14.9 |      | D99F0 |
| Nvidia       | NVD0200 |              | 1440x900  | 320x200mm  | 14.9 |      | E816F |
| Nvidia       | NVD0300 |              | 1280x800  | 320x200mm  | 14.9 |      | B0529 |
| Nvidia       | NVD0300 |              | 1920x1080 | 320x180mm  | 14.5 |      | FA6DD |
| Nvidia       | NVD0600 |              | 1280x800  | 320x200mm  | 14.9 |      | 5076D |
| Nvidia       | NVD0800 |              | 1920x1080 | 320x180mm  | 14.5 |      | 205C2 |
| Nvidia       | NVD0800 |              | 1280x800  | 320x200mm  | 14.9 |      | 2C7A1 |
| Nvidia       | NVD0B00 |              | 1680x1050 | 320x200mm  | 14.9 |      | 4BFD1 |
| ONN          | ONN0101 | ONA18HO015   | 1920x1080 | 470x290mm  | 21.7 | 2017 | 565DE |
| Olevia       | SYN001E | 432-S12      | 1360x768  | 710x400mm  | 32.1 | 2007 | BC8A4 |
| Olevia       | SYN0036 | 242-T11      | 1360x768  | 930x520mm  | 41.9 | 2007 | 7B7B2 |
| Olevia       | SYN3000 | Non-PnP      | 1920x1080 | 530x290mm  | 23.8 | 2013 | DBAA7 |
| Onkyo        | ONK0B32 | TX-SR309     | 1920x1080 | 710x400mm  | 32.1 | 2011 | 9AE63 |
| Onkyo        | ONK0C31 | TX-SR313     | 1920x1080 | 1650x930mm | 74.6 | 2012 | F8F6A |
| Onkyo        | ONK0D61 | TX-NR626     | 1920x1080 | 890x500mm  | 40.2 | 2013 | A45E5 |
| Onkyo        | ONK0E33 | HT-R393      | 1360x768  | 1150x650mm | 52.0 | 2014 | 02BF8 |
| Onkyo        | ONK1020 | TX-SR252     | 1920x1080 |            |      | 2015 | D3DB9 |
| Onkyo        | ONK1161 | AV Receiver  | 3840x2160 |            |      | 2017 | E98C9 |
| Orion        | ORN0030 | CLB40B962S   | 1920x1080 | 890x510mm  | 40.4 | 2016 | 46947 |
| Orion        | ORN1205 |              | 1920x540  |            |      | 2008 | 9F456 |
| Orion        | ORN1208 |              | 1920x1080 |            |      | 2011 | F5572 |
| Orion        | ORN1208 |              | 1360x768  |            |      | 2011 | FF535 |
| PANDA        | NCP0000 | LC116LF3L01  | 1920x1080 | 260x150mm  | 11.8 | 2016 | 2B474 |
| PANDA        | NCP0000 | LC116LF3L02  | 1920x1080 | 260x150mm  | 11.8 | 2016 | ED13F |
| PANDA        | NCP0000 | LC133LF4L01  | 1920x1080 | 290x170mm  | 13.2 | 2015 | 3CCAC |
| PANDA        | NCP000D | LM156LF3L01  | 1920x1080 | 340x190mm  | 15.3 | 2016 | 94EF5 |
| PANDA        | NCP0015 | LC133LF2L03  | 1920x1080 | 290x170mm  | 13.2 | 2016 | AF22E |
| PANDA        | NCP0019 | LC133LF1L02  | 1920x1080 | 290x170mm  | 13.2 | 2016 | C9F6F |
| PANDA        | NCP0021 | 8VMX1        | 1920x1080 | 340x190mm  | 15.3 | 2017 | A4125 |
| PANDA        | NCP0028 | LM156LF9L01  | 1920x1080 | 340x190mm  | 15.3 | 2017 | F007E |
| PANDA        | NCP002A | LM156LF1L    | 1920x1080 | 340x190mm  | 15.3 | 2018 | 1F0A0 |
| PANDA        | NCP002D | LM156LF-CL03 | 1920x1080 | 340x190mm  | 15.3 | 2018 | 4DB6B |
| PANDA        | NCP0036 | LM156LF-GL   | 1920x1080 | 340x190mm  | 15.3 | 2018 | CFAAF |
| PANDA        | NCP13FB | LM133LF1L01  | 1920x1080 | 290x170mm  | 13.2 | 2015 | FAF4B |
| PEGA         | PEG00D0 | 20           | 1920x1080 | 480x270mm  | 21.7 | 2009 | 0717E |
| Packard Bell | PKB008B | Viseo 223Ws  | 1680x1050 | 460x290mm  | 21.4 | 2009 | AAAE2 |
| Packard Bell | PKB008E | Maestro222Ws | 1920x1080 | 480x270mm  | 21.7 | 2009 | 75076 |
| Packard Bell | PKB01D4 | Maestro243DL | 1920x1080 | 530x300mm  | 24.0 | 2011 | AB75C |
| Packard Bell | PKB02F2 |              | 1920x1080 | 480x270mm  | 21.7 | 2013 | 96F1A |
| Packard Bell | PKB02F3 |              | 1920x1080 | 510x290mm  | 23.1 | 2013 | 79CE7 |
| Packard Bell | PKB036D | Maestro226DX | 1920x1080 | 480x270mm  | 21.7 | 2014 | 20302 |
| Packard Bell | PKB036D | Maestro226DX | 1920x1080 | 480x270mm  | 21.7 | 2013 | 77FEC |
| Packard Bell | PKB036E | Maestro236D  | 1920x1080 | 510x290mm  | 23.1 | 2013 | 94206 |
| Packard Bell | PKB0375 | Viseo193DX   | 1366x768  | 410x230mm  | 18.5 | 2013 | F2D76 |
| Packard Bell | PKB0385 | Viseo223DX   | 1920x1080 | 480x270mm  | 21.7 | 2014 | 2811C |
| Packard Bell | PKB0386 | Viseo243D    | 1920x1080 | 530x300mm  | 24.0 | 2014 | A94A2 |
| Packard Bell | PKB0386 | Viseo243D    | 1920x1080 | 530x300mm  | 24.0 | 2013 | 7AC76 |
| Packard Bell | PKB0395 | Viseo273D    | 1920x1080 | 600x340mm  | 27.2 | 2013 | 82565 |
| Panasonic    | MEI96A2 | VVX14T092N00 | 2256x1504 | 290x190mm  | 13.6 | 2016 | 25784 |
| Panasonic    | MEI96A2 | VVY13F001G10 | 1920x1080 | 290x170mm  | 13.2 |      | 1AF68 |
| Panasonic    | MEI96A2 | VVY13F001G00 | 1920x1080 | 290x170mm  | 13.2 |      | 1DD53 |
| Panasonic    | MEI96A2 | VVX14T058J02 | 2560x1440 | 310x170mm  | 13.9 |      | 27609 |
| Panasonic    | MEI96A2 | TDM13O56     | 3000x2000 | 290x190mm  | 13.6 |      | 4C254 |
| Panasonic    | MEI96A2 | VVX16T028J00 | 2880x1620 | 340x190mm  | 15.3 |      | 51F08 |
| Panasonic    | MEI96A2 | VVX13F009G10 | 1920x1080 | 290x170mm  | 13.2 |      | 72B22 |
| Panasonic    | MEI96A2 | VVX16T029D00 | 2880x1620 | 340x190mm  | 15.3 |      | 7A328 |
| Panasonic    | MEI96A2 | VVX14T058J10 | 2560x1440 | 310x170mm  | 13.9 |      | 7CA47 |
| Panasonic    | MEI96A2 | VVX14P048M00 | 3000x2000 | 290x190mm  | 13.6 |      | BCE1D |
| Panasonic    | MEI96A2 | VVX13F009G00 | 1920x1080 | 290x170mm  | 13.2 |      | BF46F |
| Panasonic    | MEI96A2 | VVX14T058J00 | 2560x1440 | 310x170mm  | 13.9 |      | FFFE0 |
| Panasonic    | MEIA045 |              | 1920x540  | 920x518mm  | 41.6 | 2006 | A916D |
| Panasonic    | MEIA064 | TV           | 1280x720  | 1434x806mm | 64.8 | 2007 | 333CA |
| Panasonic    | MEIA07C | TV           | 1920x1080 | 698x392mm  | 31.5 | 2008 | 7108A |
| Panasonic    | MEIA07D | TV           | 1920x1080 | 698x392mm  | 31.5 | 2008 | 4B4B8 |
| Panasonic    | MEIA081 | TV           | 1280x720  | 698x392mm  | 31.5 | 2008 | 58D52 |
| Panasonic    | MEIA09B | TV           | 1280x720  | 698x392mm  | 31.5 | 2010 | DAEDB |
| Panasonic    | MEIA0A7 | TV           | 1920x1080 | 698x392mm  | 31.5 | 2010 | 017F3 |
| Panasonic    | MEIA0AE | TV           | 1920x1080 | 698x392mm  | 31.5 | 2010 | B8F13 |
| Panasonic    | MEIA0C9 | TV           | 1920x1080 | 698x392mm  | 31.5 | 2011 | 36679 |
| Panasonic    | MEIA0CC | TV           | 1920x1080 | 698x392mm  | 31.5 | 2011 | 03996 |
| Panasonic    | MEIA296 | TV           | 1920x1080 | 1280x720mm | 57.8 | 2017 | 24417 |
| Panasonic    | MEIA296 | TV           | 1920x1080 | 1280x720mm | 57.8 | 2016 | 3A712 |
| Panasonic    | MEIA296 | TV           | 1920x1080 | 1280x720mm | 57.8 | 2015 | 7E06C |
| Panasonic    | MEIA296 | TV           | 1280x1024 | 1280x720mm | 57.8 | 2014 | 363AA |
| Panasonic    | MEIA296 | TV           | 1920x1080 | 700x390mm  | 31.5 | 2014 | A2F79 |
| Panasonic    | MEIA296 | TV           | 1920x1080 | 1280x720mm | 57.8 | 2013 | 14B06 |
| Panasonic    | MEIA296 | TV           | 1920x1080 | 698x392mm  | 31.5 | 2012 | A16B3 |
| Panasonic    | MEIC111 |              | 1920x540  | 697x392mm  | 31.5 | 2007 | 582A8 |
| Panasonic    | MEIC12B | TV           | 1920x1080 | 698x392mm  | 31.5 | 2008 | 8F500 |
| Panasonic    | MEIC12C | TV           | 1280x720  | 698x392mm  | 31.5 | 2008 | CE308 |
| Panasonic    | MEIC135 | TV           | 1920x1080 | 698x392mm  | 31.5 | 2009 | 60F7E |
| Panasonic    | MEIC136 | TV           | 1280x720  | 698x392mm  | 31.5 | 2009 | 05623 |
| Panasonic    | MEIC303 | TV           | 1920x1080 | 698x392mm  | 31.5 | 2010 | 3DF41 |
| Panasonic    | MEIC312 | TV           | 1920x1080 | 698x392mm  | 31.5 | 2011 | 28D13 |
| Panasonic    | MEIC31C | TV           | 1920x1080 | 698x392mm  | 31.5 | 2011 | 3463D |
| Panasonic    | MEIC327 | TV           | 1920x1080 | 698x392mm  | 31.5 | 2012 | 7A7F3 |
| Panasonic    | MEIC328 | TV           | 1920x1080 | 698x392mm  | 31.5 | 2012 | EEA09 |
| Panasonic    | MEIC32C | TV           | 1280x720  | 698x392mm  | 31.5 | 2012 | 1CB1F |
| Panasonic    | MEIC33A | PanasonicTV0 | 1920x1080 | 698x392mm  | 31.5 | 2012 | 735BD |
| Panasonic    | MEIC33B | TV           | 1366x768  | 520x290mm  | 23.4 | 2012 | 99A9C |
| Pegatron     | PEA3013 | H81-P1       | 1920x1080 | 520x290mm  | 23.4 | 2013 | 3F24D |
| Pegatron     | PEA3013 | H81-P2       | 1920x1080 | 520x290mm  | 23.4 | 2013 | 8863E |
| Philips      | PHL0000 | FTV          | 1920x1080 | 640x360mm  | 28.9 | 2016 | 3FC41 |
| Philips      | PHL0000 | FTV          | 3840x2160 |            |      | 2016 | 8E696 |
| Philips      | PHL0000 | FTV          | 1920x1080 | 640x360mm  | 28.9 | 2015 | 3BE17 |
| Philips      | PHL0000 | FTV          | 3840x2160 | 1280x720mm | 57.8 | 2015 | 40973 |
| Philips      | PHL0000 | FTV          | 1920x1080 | 640x360mm  | 28.9 | 2014 | 38A9B |
| Philips      | PHL0000 | FTV          | 1920x1080 | 640x360mm  | 28.9 | 2013 | 2A373 |
| Philips      | PHL0000 | FTV          | 1920x1080 | 640x360mm  | 28.9 | 2012 | 0F60C |
| Philips      | PHL0000 | FTV          | 1920x1080 | 1040x580mm | 46.9 | 2012 | 5E98F |
| Philips      | PHL0000 |              | 1920x1080 | 1280x720mm | 57.8 | 2012 | A5767 |
| Philips      | PHL0000 | FTV          | 1920x1080 | 640x360mm  | 28.9 | 2011 | 0AEAB |
| Philips      | PHL0000 | FTV          | 1920x1080 | 1280x720mm | 57.8 | 2010 | 1C012 |
| Philips      | PHL0000 | FTV          | 1920x1080 | 880x490mm  | 39.7 | 2010 | 1C45B |
| Philips      | PHL0000 | FTV          | 1920x1080 | 700x390mm  | 31.5 | 2010 | 42BD7 |
| Philips      | PHL0000 | FTV          | 1920x1080 | 1010x570mm | 45.7 | 2010 | CE064 |
| Philips      | PHL0000 |              | 1920x1080 |            |      | 2009 | 0BD70 |
| Philips      | PHL0000 | FTV          | 1920x1080 | 1280x720mm | 57.8 | 2009 | 841DC |
| Philips      | PHL0000 | FTV          | 1920x1080 | 640x360mm  | 28.9 | 2008 | 3D86E |
| Philips      | PHL0000 | FTV          | 1360x768  | 580x320mm  | 26.1 | 2008 | B3571 |
| Philips      | PHL0000 | FTV          | 1920x1080 | 640x360mm  | 28.9 | 2007 | 02D53 |
| Philips      | PHL0001 |              | 1920x1080 | 710x400mm  | 32.1 | 2011 | 79980 |
| Philips      | PHL0001 |              | 1920x1080 | 710x400mm  | 32.1 | 2009 | 00414 |
| Philips      | PHL0081 | 190V         | 1440x900  | 400x250mm  | 18.6 | 2011 | E1B9E |
| Philips      | PHL01EA | FTV          | 1920x1080 | 1440x810mm | 65.0 | 2017 | 00940 |
| Philips      | PHL0304 | FTV          | 1920x1080 | 1280x720mm | 57.8 | 2014 | D7BD9 |
| Philips      | PHL04C3 | FTV          | 1920x1080 | 1440x810mm | 65.0 | 2018 | 58B66 |
| Philips      | PHL04C3 | FTV          | 1920x1080 | 1440x810mm | 65.0 | 2017 | 15B90 |
| Philips      | PHL080D | 150B         | 1024x768  | 300x230mm  | 14.9 |      | 4A7FA |
| Philips      | PHL0812 | 150S         | 1024x768  | 300x230mm  | 14.9 |      | 339D2 |
| Philips      | PHL082C | 170B         | 1280x1024 | 340x270mm  | 17.1 |      | E29AE |
| Philips      | PHL0830 | 190B         | 1280x1024 | 380x300mm  | 19.1 | 2006 | 4FF2A |
| Philips      | PHL0839 | 170S         | 1280x1024 | 340x270mm  | 17.1 | 2007 | DE85C |
| Philips      | PHL0839 | 170S         | 1280x1024 | 340x270mm  | 17.1 | 2006 | A61E5 |
| Philips      | PHL083B | 170P         | 1280x1024 | 340x270mm  | 17.1 | 2006 | DA526 |
| Philips      | PHL083F | 190S         | 1280x1024 | 380x300mm  | 19.1 | 2007 | 16B6F |
| Philips      | PHL083F | 190S         | 1280x1024 | 380x300mm  | 19.1 | 2006 | 6C98A |
| Philips      | PHL0840 | 190B         | 1280x1024 | 380x300mm  | 19.1 | 2006 | 54A3A |
| Philips      | PHL0847 | 190V         | 1280x1024 | 380x300mm  | 19.1 | 2007 | 5EE23 |
| Philips      | PHL0849 | 190C         | 1280x1024 | 380x300mm  | 19.1 | 2007 | E9B20 |
| Philips      | PHL084C | 190X         | 1280x1024 | 380x300mm  | 19.1 | 2007 | ACB63 |
| Philips      | PHL0850 |              | 1680x1050 | 470x300mm  | 22.0 | 2007 | 12310 |
| Philips      | PHL0851 |              | 1680x1050 | 470x300mm  | 22.0 | 2007 | 19E2F |
| Philips      | PHL0855 |              | 1680x1050 | 470x300mm  | 22.0 | 2007 | 83B14 |
| Philips      | PHL0859 | 190S         | 1280x1024 | 380x300mm  | 19.1 | 2007 | 42511 |
| Philips      | PHL085A | 190C         | 1280x1024 | 380x300mm  | 19.1 | 2007 | B3C26 |
| Philips      | PHL0869 |              | 1680x1050 | 470x290mm  | 21.7 | 2008 | D39DA |
| Philips      | PHL086B | 190S         | 1280x1024 | 380x300mm  | 19.1 | 2009 | 68C7E |
| Philips      | PHL086B | 190S         | 1280x1024 | 380x300mm  | 19.1 | 2008 | CA672 |
| Philips      | PHL0879 | 19B          | 1280x1024 | 380x300mm  | 19.1 | 2011 | 24AA4 |
| Philips      | PHL087A |              | 1440x900  | 400x250mm  | 18.6 | 2010 | E881D |
| Philips      | PHL087A |              | 1440x900  | 400x250mm  | 18.6 | 2009 | 17362 |
| Philips      | PHL0886 | 220S         | 1680x1050 | 470x300mm  | 22.0 | 2010 | 635C7 |
| Philips      | PHL0887 | 191V         | 1366x768  | 410x230mm  | 18.5 | 2011 | 2E1A5 |
| Philips      | PHL0888 | 221V         | 1920x1080 | 480x270mm  | 21.7 | 2011 | 2F4AB |
| Philips      | PHL089B |              | 1920x1080 | 510x290mm  | 23.1 | 2011 | 295DE |
| Philips      | PHL089E | 245P         | 1920x1200 | 520x320mm  | 24.0 | 2010 | 370F2 |
| Philips      | PHL08A8 | 273PLPH      | 1920x1080 | 600x340mm  | 27.2 | 2013 | 69F68 |
| Philips      | PHL08B4 | 241PQPY      | 1920x1080 | 530x300mm  | 24.0 | 2012 | 4CB1D |
| Philips      | PHL08B6 | 19SL         | 1280x1024 | 380x300mm  | 19.1 | 2012 | 233BD |
| Philips      | PHL08C2 |              | 1920x1080 | 600x340mm  | 27.2 | 2012 | 0C46C |
| Philips      | PHL08C3 |              | 1920x1080 | 600x340mm  | 27.2 | 2016 | 0515C |
| Philips      | PHL08C3 |              | 1920x1080 | 600x340mm  | 27.2 | 2014 | EA48F |
| Philips      | PHL08D1 | 19S4         | 1280x1024 | 380x300mm  | 19.1 | 2013 | F9608 |
| Philips      | PHL08D4 | 241B4        | 1920x1080 | 530x300mm  | 24.0 | 2015 | 608E6 |
| Philips      | PHL08D7 |              | 3840x2160 | 620x340mm  | 27.8 | 2014 | 5677A |
| Philips      | PHL08E1 | BDM4065      | 3840x2160 | 880x490mm  | 39.7 | 2015 | 176B9 |
| Philips      | PHL08E1 | BDM4065      | 3840x2160 | 880x490mm  | 39.7 | 2014 | D7D16 |
| Philips      | PHL08E4 | 272S4L       | 2560x1440 | 600x340mm  | 27.2 | 2015 | 674E9 |
| Philips      | PHL08E7 | BDM3270      | 2560x1440 | 710x400mm  | 32.1 | 2017 | 98737 |
| Philips      | PHL08E7 | BDM3270      | 2560x1440 | 710x400mm  | 32.1 | 2016 | 7AC5A |
| Philips      | PHL08E7 | BDM3270      | 2560x1440 | 710x400mm  | 32.1 | 2015 | BF3F4 |
| Philips      | PHL08F3 | BDM3490      | 3440x1440 | 800x340mm  | 34.2 | 2016 | 77D9B |
| Philips      | PHL08FA | BDM4350      | 3840x2160 | 950x540mm  | 43.0 | 2018 | C0D96 |
| Philips      | PHL08FA | BDM4350      | 3840x2160 | 950x540mm  | 43.0 | 2017 | 32436 |
| Philips      | PHL08FD | 220S4LY      | 1680x1050 | 470x300mm  | 22.0 | 2016 | 46F17 |
| Philips      | PHL0903 | 240B7QPJ     | 1920x1200 | 520x320mm  | 24.0 | 2016 | 84BAC |
| Philips      | PHL090F | 243S7        | 1920x1080 | 530x300mm  | 24.0 | 2017 | 44440 |
| Philips      | PHL091F | 243S5L       | 1920x1080 | 520x290mm  | 23.4 | 2018 | 28096 |
| Philips      | PHL14CA |              | 1360x768  | 710x400mm  | 32.1 | 2006 | 24283 |
| Philips      | PHL150A |              | 1024x768  | 300x230mm  | 14.9 |      | E8F58 |
| Philips      | PHL151B | 1080p TV (2) | 1360x765  | 710x400mm  | 32.1 | 2007 | 9843F |
| Philips      | PHL2051 |              | 1600x900  | 440x250mm  | 19.9 | 2010 | F0F4A |
| Philips      | PHL3200 | FTV          | 1360x768  | 700x390mm  | 31.5 | 2016 | E2D7D |
| Philips      | PHL4650 |              | 1280x768  | 710x400mm  | 32.1 |      | 751B3 |
| Philips      | PHL5035 | TV           | 1920x1080 | 640x360mm  | 28.9 | 2014 | BB3D3 |
| Philips      | PHL9206 | FTV          | 1920x1080 | 1280x720mm | 57.8 | 2014 | 7F11E |
| Philips      | PHL97A2 | FTV          | 1920x1080 | 1280x720mm | 57.8 | 2015 | 5848C |
| Philips      | PHLC00E |              | 1280x1024 | 380x300mm  | 19.1 |      | 33CD5 |
| Philips      | PHLC017 | 190C         | 1280x1024 | 380x300mm  | 19.1 | 2007 | 4F835 |
| Philips      | PHLC01A |              | 1680x1050 | 470x300mm  | 22.0 | 2008 | D5433 |
| Philips      | PHLC024 |              | 1680x1050 | 470x290mm  | 21.7 | 2009 | 912E8 |
| Philips      | PHLC024 |              | 1680x1050 | 470x290mm  | 21.7 | 2008 | 1E232 |
| Philips      | PHLC02C | 230C         | 1920x1080 | 510x290mm  | 23.1 | 2009 | 5FDC5 |
| Philips      | PHLC030 | 220X         | 1680x1050 | 470x290mm  | 21.7 | 2010 | A0BF5 |
| Philips      | PHLC030 | 220X         | 1680x1050 | 470x290mm  | 21.7 | 2009 | 02F59 |
| Philips      | PHLC035 | 241E         | 1920x1080 | 520x290mm  | 23.4 | 2011 | 5589A |
| Philips      | PHLC035 | 241E         | 1920x1080 | 520x290mm  | 23.4 | 2010 | E4F76 |
| Philips      | PHLC036 | 244E         | 1920x1080 | 520x290mm  | 23.4 | 2012 | 8D317 |
| Philips      | PHLC036 | 244E         | 1920x1080 | 520x290mm  | 23.4 | 2011 | C42E4 |
| Philips      | PHLC036 | 244E         | 1920x1080 | 520x290mm  | 23.4 | 2010 | 0BDAD |
| Philips      | PHLC036 | 244E         | 1920x1080 | 520x290mm  | 23.4 | 2009 | 66A1A |
| Philips      | PHLC037 | 190C         | 1440x900  | 400x250mm  | 18.6 | 2009 | CEEC5 |
| Philips      | PHLC038 | 220C         | 1680x1050 | 470x290mm  | 21.7 | 2010 | 62636 |
| Philips      | PHLC038 | 220C         | 1680x1050 | 470x290mm  | 21.7 | 2009 | 3F9B9 |
| Philips      | PHLC04D | 192E         | 1366x768  | 410x230mm  | 18.5 | 2010 | 04445 |
| Philips      | PHLC050 |              | 1366x768  | 410x230mm  | 18.5 | 2011 | 1FEB1 |
| Philips      | PHLC050 |              | 1366x768  | 410x230mm  | 18.5 | 2010 | 1DCFF |
| Philips      | PHLC052 |              | 1920x1080 | 480x270mm  | 21.7 | 2012 | 74E68 |
| Philips      | PHLC054 |              | 1920x1080 | 480x270mm  | 21.7 | 2011 | 9E719 |
| Philips      | PHLC054 |              | 1920x1080 | 480x270mm  | 21.7 | 2010 | 49952 |
| Philips      | PHLC055 | 221E         | 1920x1080 | 480x270mm  | 21.7 | 2010 | FAF90 |
| Philips      | PHLC056 |              | 1920x1080 | 480x270mm  | 21.7 | 2011 | 91A60 |
| Philips      | PHLC057 | 226C         | 1920x1080 | 480x270mm  | 21.7 | 2010 | F1F9D |
| Philips      | PHLC05C |              | 1600x900  | 440x250mm  | 19.9 | 2011 | 62C31 |
| Philips      | PHLC05D | 232E         | 1920x1080 | 510x290mm  | 23.1 | 2011 | 2A4E4 |
| Philips      | PHLC060 | 243E         | 1920x1080 | 530x300mm  | 24.0 | 2010 | 26D65 |
| Philips      | PHLC061 | 244E         | 1920x1080 | 530x300mm  | 24.0 | 2010 | 295AF |
| Philips      | PHLC066 |              | 1920x1080 | 510x290mm  | 23.1 | 2011 | 43003 |
| Philips      | PHLC068 |              | 1920x1080 | 510x290mm  | 23.1 | 2010 | 0678F |
| Philips      | PHLC069 |              | 1920x1080 | 510x290mm  | 23.1 | 2011 | 483D5 |
| Philips      | PHLC069 |              | 1920x1080 | 510x290mm  | 23.1 | 2010 | 92F3E |
| Philips      | PHLC074 | 246EL2SBH    | 1920x1080 | 520x290mm  | 23.4 | 2011 | 33F4C |
| Philips      | PHLC079 |              | 1920x1080 | 480x270mm  | 21.7 | 2012 | 0C7F8 |
| Philips      | PHLC07A | 227ELPH      | 1920x1080 | 480x270mm  | 21.7 | 2012 | 7664B |
| Philips      | PHLC07B | 227ELH       | 1920x1080 | 480x270mm  | 21.7 | 2012 | 0247A |
| Philips      | PHLC07B | 227ELH       | 1920x1080 | 480x270mm  | 21.7 | 2011 | 61EBF |
| Philips      | PHLC07C |              | 1920x1080 | 600x340mm  | 27.2 | 2013 | 6CADF |
| Philips      | PHLC07C |              | 1920x1080 | 600x340mm  | 27.2 | 2012 | BB762 |
| Philips      | PHLC07C |              | 1920x1080 | 600x340mm  | 27.2 | 2011 | A7776 |
| Philips      | PHLC07D | 273ELH       | 1920x1080 | 600x340mm  | 27.2 | 2016 | 55B02 |
| Philips      | PHLC07D | 273ELH       | 1920x1080 | 600x340mm  | 27.2 | 2012 | 45304 |
| Philips      | PHLC07D | 273ELH       | 1920x1080 | 600x340mm  | 27.2 | 2011 | 16265 |
| Philips      | PHLC07F |              | 1366x768  | 410x230mm  | 18.5 | 2012 | 23BF6 |
| Philips      | PHLC07F |              | 1366x768  | 410x230mm  | 18.5 | 2011 | 31685 |
| Philips      | PHLC081 |              | 1920x1080 | 480x270mm  | 21.7 | 2012 | 34558 |
| Philips      | PHLC081 |              | 1920x1080 | 480x270mm  | 21.7 | 2011 | 86488 |
| Philips      | PHLC082 |              | 1920x1080 | 510x290mm  | 23.1 | 2012 | 85F63 |
| Philips      | PHLC082 |              | 1920x1080 | 510x290mm  | 23.1 | 2011 | 79AD7 |
| Philips      | PHLC084 |              | 1920x1080 | 520x290mm  | 23.4 | 2012 | 0FCAC |
| Philips      | PHLC085 | 247ELH       | 1920x1080 | 520x290mm  | 23.4 | 2012 | 392F9 |
| Philips      | PHLC08B |              | 1366x768  | 410x230mm  | 18.5 | 2012 | 786FB |
| Philips      | PHLC08B |              | 1366x768  | 410x230mm  | 18.5 | 2011 | E25A6 |
| Philips      | PHLC08C |              | 1600x900  | 440x250mm  | 19.9 | 2012 | 0F956 |
| Philips      | PHLC08C |              | 1600x900  | 440x250mm  | 19.9 | 2011 | 12A1D |
| Philips      | PHLC08D |              | 1920x1080 | 480x270mm  | 21.7 | 2012 | B29EC |
| Philips      | PHLC08E | 227EQPH      | 1920x1080 | 480x270mm  | 21.7 | 2012 | 6183E |
| Philips      | PHLC08E | 227EQPH      | 1920x1080 | 480x270mm  | 21.7 | 2011 | EAC55 |
| Philips      | PHLC08F | 273GDH       | 1920x1080 | 600x340mm  | 27.2 | 2011 | 42B65 |
| Philips      | PHLC090 |              | 1920x1080 | 510x290mm  | 23.1 | 2012 | 122FB |
| Philips      | PHLC091 | 237EQPH      | 1920x1080 | 510x290mm  | 23.1 | 2012 | 4FD71 |
| Philips      | PHLC0A0 | 239CQH       | 1920x1080 | 510x290mm  | 23.1 | 2012 | D2AD8 |
| Philips      | PHLC0A6 | 249CQ        | 1920x1080 | 530x300mm  | 24.0 | 2013 | 836B7 |
| Philips      | PHLC0A9 | 227E4Q       | 1920x1080 | 480x270mm  | 21.7 | 2012 | CBB4D |
| Philips      | PHLC0AA | 227E4QH      | 1920x1080 | 480x270mm  | 21.7 | 2013 | 80A97 |
| Philips      | PHLC0AA | 227E4QH      | 1920x1080 | 480x270mm  | 21.7 | 2012 | E2C67 |
| Philips      | PHLC0AD | 237E4        | 1920x1080 | 510x290mm  | 23.1 | 2013 | 063D6 |
| Philips      | PHLC0AF |              | 1366x768  | 410x230mm  | 18.5 | 2013 | 0E218 |
| Philips      | PHLC0AF |              | 1366x768  | 410x230mm  | 18.5 | 2012 | A0925 |
| Philips      | PHLC0B1 |              | 1920x1080 | 480x270mm  | 21.7 | 2014 | C0E65 |
| Philips      | PHLC0B1 |              | 1920x1080 | 480x270mm  | 21.7 | 2013 | 06A58 |
| Philips      | PHLC0B1 |              | 1920x1080 | 480x270mm  | 21.7 | 2012 | 83CD7 |
| Philips      | PHLC0B2 |              | 1680x1050 | 470x300mm  | 22.0 | 2014 | 57F9A |
| Philips      | PHLC0B3 |              | 1920x1080 | 510x290mm  | 23.1 | 2013 | 19217 |
| Philips      | PHLC0B3 |              | 1920x1080 | 510x290mm  | 23.1 | 2012 | BDAFD |
| Philips      | PHLC0B8 |              | 1920x1080 | 600x340mm  | 27.2 | 2014 | 80856 |
| Philips      | PHLC0B8 |              | 1920x1080 | 600x340mm  | 27.2 | 2013 | 001F7 |
| Philips      | PHLC0B8 |              | 1920x1080 | 600x340mm  | 27.2 | 2012 | 8A89D |
| Philips      | PHLC0B9 |              | 1920x1080 | 600x340mm  | 27.2 | 2013 | 5938D |
| Philips      | PHLC0BD |              | 2560x1080 | 670x280mm  | 28.6 | 2013 | 87DE3 |
| Philips      | PHLC0BE |              | 2560x1080 | 670x280mm  | 28.6 | 2013 | 46B1E |
| Philips      | PHLC0BF |              | 1600x900  | 430x240mm  | 19.4 | 2013 | 552B9 |
| Philips      | PHLC0C0 |              | 1920x1080 | 520x290mm  | 23.4 | 2015 | BDA88 |
| Philips      | PHLC0C0 | PHL 244E5    | 1920x1080 | 530x300mm  | 24.0 | 2014 | 0884E |
| Philips      | PHLC0C5 | PHL 246V5    | 1920x1080 | 530x300mm  | 24.0 | 2016 | F44DE |
| Philips      | PHLC0C5 | PHL 246V5    | 1920x1080 | 530x300mm  | 24.0 | 2015 | CDD17 |
| Philips      | PHLC0C5 | PHL 246V5    | 1920x1080 | 530x300mm  | 24.0 | 2014 | 4647D |
| Philips      | PHLC0C5 | PHL 246V5    | 1920x1080 | 530x300mm  | 24.0 | 2013 | 731C0 |
| Philips      | PHLC0C6 | PHL 224E5    | 1920x1080 | 480x270mm  | 21.7 | 2016 | C2DC7 |
| Philips      | PHLC0C6 | PHL 224E5    | 1920x1080 | 480x270mm  | 21.7 | 2015 | 0681B |
| Philips      | PHLC0C6 | PHL 224E5    | 1920x1080 | 480x270mm  | 21.7 | 2014 | 01D67 |
| Philips      | PHLC0C6 | PHL 224E5    | 1920x1080 | 480x270mm  | 21.7 | 2013 | 61E99 |
| Philips      | PHLC0C7 | PHL 234E5    | 1920x1080 | 510x290mm  | 23.1 | 2015 | 169AF |
| Philips      | PHLC0C7 | PHL 234E5    | 1920x1080 | 510x290mm  | 23.1 | 2013 | AC7BF |
| Philips      | PHLC0C8 | PHL 274E5    | 1920x1080 | 600x340mm  | 27.2 | 2016 | B3431 |
| Philips      | PHLC0C8 | PHL 274E5    | 1920x1080 | 600x340mm  | 27.2 | 2015 | 37661 |
| Philips      | PHLC0C8 | PHL 274E5    | 1920x1080 | 600x340mm  | 27.2 | 2014 | CD4C3 |
| Philips      | PHLC0C8 | PHL 274E5    | 1920x1080 | 600x340mm  | 27.2 | 2013 | 2852E |
| Philips      | PHLC0CD | PHL 193V5    | 1366x768  | 410x230mm  | 18.5 | 2018 | C0586 |
| Philips      | PHLC0CD | PHL 193V5    | 1366x768  | 410x230mm  | 18.5 | 2016 | 07654 |
| Philips      | PHLC0CD | PHL 193V5    | 1366x768  | 410x230mm  | 18.5 | 2014 | 8D890 |
| Philips      | PHLC0CF | PHL 223V5    | 1920x1080 | 480x270mm  | 21.7 | 2018 | D9CB3 |
| Philips      | PHLC0CF | PHL 223V5    | 1920x1080 | 480x270mm  | 21.7 | 2016 | 42AAD |
| Philips      | PHLC0CF | PHL 223V5    | 1920x1080 | 480x270mm  | 21.7 | 2015 | 02035 |
| Philips      | PHLC0CF | PHL 223V5    | 1920x1080 | 480x270mm  | 21.7 | 2013 | 4FA22 |
| Philips      | PHLC0D0 | PHL 233V5    | 1920x1080 | 510x290mm  | 23.1 | 2013 | BA47E |
| Philips      | PHLC0D1 | PHL 243V5    | 1920x1080 | 520x290mm  | 23.4 | 2018 | 3B5BD |
| Philips      | PHLC0D1 | PHL 243V5    | 1920x1080 | 520x290mm  | 23.4 | 2016 | 1E64E |
| Philips      | PHLC0D1 | PHL 243V5    | 1920x1080 | 520x290mm  | 23.4 | 2015 | 0CB7D |
| Philips      | PHLC0D1 | PHL 243V5    | 1920x1080 | 520x290mm  | 23.4 | 2014 | 38A30 |
| Philips      | PHLC0D1 | PHL 243V5    | 1920x1080 | 520x290mm  | 23.4 | 2013 | F1081 |
| Philips      | PHLC0D2 | PHL 273V5    | 1920x1080 | 600x340mm  | 27.2 | 2016 | 42521 |
| Philips      | PHLC0D2 | PHL 273V5    | 1920x1080 | 600x340mm  | 27.2 | 2014 | 439E1 |
| Philips      | PHLC0D2 | PHL 273V5    | 1920x1080 | 600x340mm  | 27.2 | 2013 | E7A1C |
| Philips      | PHLC0D7 | PHL 231C5T   | 1920x1080 | 510x290mm  | 23.1 | 2013 | 7F4D9 |
| Philips      | PHLC0DE | PHL 284E5    | 1920x1080 | 620x340mm  | 27.8 | 2015 | A682B |
| Philips      | PHLC0DE | PHL 284E5    | 1920x1080 | 620x340mm  | 27.8 | 2014 | 3CB50 |
| Philips      | PHLC0DE | PHL 284E5    | 1920x1080 | 620x340mm  | 27.8 | 2013 | 50A94 |
| Philips      | PHLC0E7 | PHL 247E6    | 1920x1080 | 520x290mm  | 23.4 | 2016 | FF361 |
| Philips      | PHLC0E7 | PHL 247E6    | 1920x1080 | 520x290mm  | 23.4 | 2015 | 1353D |
| Philips      | PHLC0E7 | PHL 247E6    | 1920x1080 | 520x290mm  | 23.4 | 2014 | A4D3D |
| Philips      | PHLC101 | PHL 237E7    | 1920x1080 | 510x290mm  | 23.1 | 2016 | 901EF |
| Philips      | PHLC101 | PHL 237E7    | 1920x1080 | 510x290mm  | 23.1 | 2015 | DCFAA |
| Philips      | PHLC107 | PHL 246E7    | 1920x1080 | 520x290mm  | 23.4 | 2017 | 062C0 |
| Philips      | PHLC108 | PHL 276E7    | 1920x1080 | 600x340mm  | 27.2 | 2017 | 34009 |
| Philips      | PHLC10A | PHL 240V5    | 1920x1080 | 530x300mm  | 24.0 | 2015 | B70C1 |
| Philips      | PHLC10C | PHL 240V5A   | 1920x1080 | 530x300mm  | 24.0 | 2017 | 432E8 |
| Philips      | PHLC114 | PHL 223V5LH  | 1920x1080 | 480x270mm  | 21.7 | 2015 | 734AC |
| Philips      | PHLC121 | PHL 323E7    | 1920x1080 | 700x390mm  | 31.5 | 2017 | 20762 |
| Philips      | PHLC155 | PHL 243V7    | 1920x1080 | 530x300mm  | 24.0 | 2018 | 2E176 |
| Philips      | PHLC155 | PHL 243V7    | 1920x1080 | 530x300mm  | 24.0 | 2017 | F5CC9 |
| Philips      | PHLC156 | PHL 273V7    | 1920x1080 | 600x340mm  | 27.2 | 2018 | 66D9B |
| Philips      | PHLC161 | PHL 278E8Q   | 1920x1080 | 600x340mm  | 27.2 | 2018 | B50F6 |
| Philips      | PHLC164 | PHL 328E8Q   | 1920x1080 | 700x390mm  | 31.5 | 2017 | A2B3A |
| Philips      | PHLC17C | PHL 246E9Q   | 1920x1080 | 530x300mm  | 24.0 | 2018 | 0039A |
| Philips      | PHLC17D | PHL 226E9Q   | 1920x1080 | 480x270mm  | 21.7 | 2019 | 2C8AE |
| Philips      | PHLD05C | 22PFL3404 EU | 1360x768  | 480x270mm  | 21.7 | 2009 | 50AB0 |
| Philips      | PHLD05F | 26PFL3404 EU | 1360x768  | 570x320mm  | 25.7 | 2008 | 553E7 |
| Philips      | PHLD06B | PHI32PFL3605 | 1680x1050 | 700x400mm  | 31.7 | 2010 | 6316E |
| Philips      | PHLD06C | 32PFL3605H   | 1920x1080 | 640x360mm  | 28.9 | 2010 | 311E6 |
| Philips      | PHLD06D | PHI42PFL3605 | 1680x1050 | 930x520mm  | 41.9 | 2010 | 24953 |
| Philips      | PHLD073 | PHI22PFL3405 | 1360x768  | 480x270mm  | 21.7 | 2010 | F12C1 |
| Philips      | PHLD075 | PHL26PFL3405 | 1360x768  | 580x330mm  | 26.3 | 2010 | 507F6 |
| Philips      | PHLD076 | PHI32PFL5604 | 1680x1050 | 700x400mm  | 31.7 | 2009 | 2CAF6 |
| Pioneer      | PIO0000 | VSX-323      | 1920x1080 | 1150x650mm | 52.0 | 2013 | 10CB1 |
| Pioneer      | PIO0000 | VSX-827      | 1920x1080 |            |      | 2012 | 790E7 |
| Pioneer      | PIO0000 | VSX-921      | 1920x1080 | 1280x720mm | 57.8 | 2010 | 1C8D0 |
| Pioneer      | PIO0000 | VSX-1020     | 1366x768  | 700x390mm  | 31.5 | 2010 | 4C5B1 |
| Pioneer      | PIO0000 | VSX-519      | 1920x1080 |            |      | 2009 | C7742 |
| Pioneer      | PIO0000 | VSX-LX51     | 1920x1080 | 1100x620mm | 49.7 | 2008 | 271E5 |
| Pioneer      | PIO0000 | VSX-03TXH    | 1920x1080 | 1060x590mm | 47.8 | 2008 | D99DC |
| Pioneer      | PIO000E | PDP-503P     | 1280x768  | 1090x620mm | 49.4 |      | 4B504 |
| Plain Tre... | PTS06A5 |              | 1280x1024 | 340x270mm  | 17.1 |      | B1926 |
| Plain Tre... | PTS076F |              | 1440x900  | 410x260mm  | 19.1 | 2007 | 2E39B |
| Plain Tre... | PTS0899 |              | 1680x1050 | 470x300mm  | 22.0 | 2007 | 1CB75 |
| Planar       | PLN2785 | PCT2785      | 1920x1080 | 600x340mm  | 27.2 |      | 2AB68 |
| Planar       | PNR6163 | PX212M       | 1600x1200 | 430x320mm  | 21.1 | 2007 | 3FF03 |
| Positivo     | NON2150 | EM2132H      | 1920x1080 | 480x260mm  | 21.5 | 2016 | 5FB26 |
| QBell        | QBL3EC6 |              | 1440x900  | 410x260mm  | 19.1 | 2008 | 2A32C |
| Quanta Di... | QDS0005 | C4011        | 1024x768  | 290x210mm  | 14.1 |      | 074E9 |
| Quanta Di... | QDS0011 | QUANTADIS... | 1024x768  | 300x230mm  | 14.9 |      | 20968 |
| Quanta Di... | QDS0015 | K4156        | 1024x768  | 290x210mm  | 14.1 |      | 0812A |
| Quanta Di... | QDS001D | QUANTADIS... | 1280x800  | 330x210mm  | 15.4 |      | 3CB55 |
| Quanta Di... | QDS001F | QUANTADIS... | 1280x800  | 300x190mm  | 14.0 |      | 089F7 |
| Quanta Di... | QDS0020 | QUANTADIS... | 1280x768  | 310x180mm  | 14.1 |      | D5296 |
| Quanta Di... | QDS0024 | QUANTADIS... | 1024x768  | 290x210mm  | 14.1 |      | C5987 |
| Quanta Di... | QDS0025 | QUANTADIS... | 1024x768  | 300x230mm  | 14.9 | 2006 | 63E8F |
| Quanta Di... | QDS0025 | QUANTADIS... | 1024x768  | 300x230mm  | 14.9 |      | BBDA0 |
| Quanta Di... | QDS0026 | QUANTADIS... | 1280x800  | 330x210mm  | 15.4 |      | 9986E |
| Quanta Di... | QDS0027 | QUANTADIS... | 1280x800  | 330x210mm  | 15.4 |      | FF49C |
| Quanta Di... | QDS002B | QUANTADIS... | 1280x800  | 330x210mm  | 15.4 |      | 243C2 |
| Quanta Di... | QDS002C | QUANTADIS... | 1280x800  | 300x190mm  | 14.0 |      | 5369F |
| Quanta Di... | QDS0030 | KD145        | 1280x800  | 300x190mm  | 14.0 |      | C82AD |
| Quanta Di... | QDS0033 | QUANTADIS... | 1024x768  | 300x230mm  | 14.9 |      | 663F6 |
| Quanta Di... | QDS0035 | QUANTADIS... | 1280x800  | 330x210mm  | 15.4 |      | 623F6 |
| Quanta Di... | QDS0040 | QUANTADIS... | 1280x800  | 330x210mm  | 15.4 |      | C513D |
| Quanta Di... | QDS0041 | QUANTADIS... | 1280x800  | 330x210mm  | 15.4 |      | 3D72E |
| Quanta Di... | QDS0045 | QUANTADIS... | 1280x800  | 260x160mm  | 12.0 |      | AACC9 |
| Quanta Di... | QDS004B | QUANTADIS... | 1280x800  | 330x210mm  | 15.4 |      | EA254 |

Analog display
--------------

See list of analog monitors in the [AnalogDisplay.md](https://github.com/linuxhw/EDID/blob/master/AnalogDisplay.md) file.

